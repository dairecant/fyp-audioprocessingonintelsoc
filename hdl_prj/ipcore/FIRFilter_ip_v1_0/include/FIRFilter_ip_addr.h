/*
 * File Name:         hdl_prj\ipcore\FIRFilter_ip_v1_0\include\FIRFilter_ip_addr.h
 * Description:       C Header File
 * Created:           2018-03-12 08:53:04
*/

#ifndef FIRFILTER_IP_H_
#define FIRFILTER_IP_H_

#define  IPCore_Reset_FIRFilter_ip       0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_FIRFilter_ip      0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_FIRFilter_ip   0x8  //contains unique IP timestamp (yymmddHHMM): 1803120852

#endif /* FIRFILTER_IP_H_ */
