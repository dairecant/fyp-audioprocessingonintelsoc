-- FIR Filter 16x16
-- Created by Fearghal Morgan
-- 21st Mar 2018

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.FIRFilterPackage.all; 

entity FIRFilter is
port (clk      : in  std_logic;
	  reset    : in  std_logic;
	  enb      : in  std_logic;
	  dataIn    : in  std_logic_vector(15 downto 0); 
	  dataOut   : out std_logic_vector(15 downto 0);
	  c        : in  array16x16 -- coefficients
	 );
end FIRFilter;


architecture RTL of FIRFilter is
signal d      : array16x16;
signal mult   : array16x32; -- 8 x 16x16 multiply result (32-bits)
signal addLvl0: array8x36;  
signal addLvl1: array4x36;  
signal addLvl2: array2x36;  
signal addLvl3: std_logic_vector(35 downto 0); 

begin

d(0) <= dataIn;                     -- input to first register in delay path
delay_i: process (clk, reset)
begin
  if reset = '1' then
    for i in 1 to 15 loop
		d(i) <= (others => '0');      -- clear 
	end loop;
  elsif clk'event and clk = '1' then 
    if enb = '1' then
      for i in 1 to 15 loop
		d(i) <= d(i-1); 			  -- shift register for remaining delay path values
	  end loop;
    end if;	
  end if;	
end process;

mult_i: process (c, d)
begin
	for i in 0 to 15 loop
		mult(i) <= std_logic_vector( signed(c(i))  *  signed(d(i)) ); 
	end loop;
end process;

addLvl0(0) <= std_logic_vector( resize(signed (mult(0)), 36)   + resize(signed (mult(1)), 36) ); 
addLvl0(1) <= std_logic_vector( resize(signed (mult(2)), 36)   + resize(signed (mult(3)), 36) ); 
addLvl0(2) <= std_logic_vector( resize(signed (mult(4)), 36)   + resize(signed (mult(5)), 36) ); 
addLvl0(3) <= std_logic_vector( resize(signed (mult(6)), 36)   + resize(signed (mult(7)), 36) ); 
addLvl0(4) <= std_logic_vector( resize(signed (mult(8)), 36)   + resize(signed (mult(9)), 36) ); 
addLvl0(5) <= std_logic_vector( resize(signed(mult(10)), 36)   + resize(signed(mult(11)), 36) ); 
addLvl0(6) <= std_logic_vector( resize(signed(mult(12)), 36)   + resize(signed(mult(13)), 36) ); 
addLvl0(7) <= std_logic_vector( resize(signed(mult(14)), 36)   + resize(signed(mult(15)), 36) ); 

addLvl1(0) <= std_logic_vector( resize(signed(addLvl0(0)), 36) + resize(signed(addLvl0(1)), 36) ); 
addLvl1(1) <= std_logic_vector( resize(signed(addLvl0(2)), 36) + resize(signed(addLvl0(3)), 36) ); 
addLvl1(2) <= std_logic_vector( resize(signed(addLvl0(4)), 36) + resize(signed(addLvl0(5)), 36) ); 
addLvl1(3) <= std_logic_vector( resize(signed(addLvl0(6)), 36) + resize(signed(addLvl0(7)), 36) ); 
                                                                       
addLvl2(0) <= std_logic_vector( resize(signed(addLvl1(0)), 36) + resize(signed(addLvl1(1)), 36) );
addLvl2(1) <= std_logic_vector( resize(signed(addLvl1(2)), 36) + resize(signed(addLvl1(3)), 36) );
                                                                        
addLvl3    <= std_logic_vector( resize(signed(addLvl2(0)), 36) + resize(signed(addLvl2(1)), 36) );

dataOut_i: dataOut <= std_logic_vector(addLvl3(35 downto 20));

end RTL;