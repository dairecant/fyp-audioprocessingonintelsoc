-- Description : FIRFilterPackage 
-- Fearghal Morgan

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;

package FIRFilterPackage is

-- for 8-tap FIR filter
type array8x16Signed is array (7 downto 0) of signed(15 downto 0); 
type array8x32Signed is array (7 downto 0) of signed(31 downto 0); 
type array4x34Signed is array (3 downto 0) of signed(33 downto 0); 
type array2x34Signed is array (1 downto 0) of signed(33 downto 0); 

type array8x16 is array (7 downto 0) of std_logic_vector(15 downto 0); 
type array8x32 is array (7 downto 0) of std_logic_vector(31 downto 0); 
type array4x34 is array (3 downto 0) of std_logic_vector(33 downto 0); 
type array2x34 is array (1 downto 0) of std_logic_vector(33 downto 0); 

-- for 16-tap FIR filter
type array16x16Signed is array (15 downto 0) of signed(15 downto 0); 
type array16x32Signed is array (15 downto 0) of signed(31 downto 0); 
type array8x36Signed is array (7 downto 0) of signed(35 downto 0); 
type array4x36Signed is array (3 downto 0) of signed(35 downto 0); 
type array2x36Signed is array (1 downto 0) of signed(35 downto 0); 

type array16x16 is array (15 downto 0) of std_logic_vector(15 downto 0); 
type array16x32 is array (15 downto 0) of std_logic_vector(31 downto 0); 
type array8x36 is array (7 downto 0) of std_logic_vector(35 downto 0); 
type array4x36 is array (3 downto 0) of std_logic_vector(35 downto 0); 
type array2x36 is array (1 downto 0) of std_logic_vector(35 downto 0); 


end FIRFilterPackage;

package body FIRFilterPackage is
 
end FIRFilterPackage;