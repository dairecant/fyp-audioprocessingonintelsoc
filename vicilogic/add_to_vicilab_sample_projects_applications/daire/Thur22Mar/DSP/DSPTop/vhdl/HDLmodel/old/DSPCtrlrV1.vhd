library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

-- use FIR filter dataOut(15:0) (DSPDatToMem(15:0)), validated by validOut (ctrl3_1(0)) assertion from FIR filter, delay by one more clk period
-- Address for write is add - 22d
-- continue until CS = 
entity DSPCtrlr is
port (clk :           in std_logic; 
      rst :        	  in std_logic; 		
      goDSPPulse:     in std_logic;
      selFirstEffect: in std_logic_vector(2 downto 0);  
	  selFinalEffect: in std_logic_vector(1 downto 0);  
	  ctrl0_0:        in std_logic_vector(15 downto 0);  
	  ctrl0_1:        in std_logic_vector(15 downto 0);  
	  ctrl1_0:        in std_logic_vector(15 downto 0);  
	  ctrl1_1:        in std_logic_vector(15 downto 0);  
	  ctrl2_0:        in std_logic_vector(15 downto 0);  
	  ctrl2_1:        in std_logic_vector(15 downto 0);  
	  ctrl3_0:        in std_logic_vector(15 downto 0);  
	  ctrl3_1:        in std_logic_vector(15 downto 0);  
      DSPDatFromMem:  in std_logic_vector(15 downto 0);  
      DSPMemAdd:      out std_logic_vector(9 downto 0);
      DSPDatToMem:    out std_logic_vector(15 downto 0);
      DSPMemWr:       out std_logic;
      DSPDone:        out std_logic
	  );
end DSPCtrlr;

architecture RTL of DSPCtrlr is

component DSPFunct is
port  (clk :          in std_logic; 
       rst :          in std_logic; 
       selFirstEffect:in std_logic_vector(2 downto 0);  
	   selFinalEffect:in std_logic_vector(1 downto 0);  
	   ctrl0_0:       in std_logic_vector(15 downto 0);  
	   ctrl0_1:       in std_logic_vector(15 downto 0);  
	   ctrl1_0:       in std_logic_vector(15 downto 0);  
	   ctrl1_1:       in std_logic_vector(15 downto 0);  
	   ctrl2_0:       in std_logic_vector(15 downto 0);  
	   ctrl2_1:       in std_logic_vector(15 downto 0);  
	   ctrl3_0:       in std_logic_vector(15 downto 0);  
	   ctrl3_1:       in std_logic_vector(15 downto 0);  
       DSPDatFromMem: in std_logic_vector(15 downto 0);  
       DSPDatToMem:   out std_logic_vector(15 downto 0)
	   );
end component;

signal NS :         std_logic_vector(10 downto 0);
--signal CS :         std_logic_vector(9 downto 0);
signal CS :         std_logic_vector(10 downto 0);  -- adding extra bit since count must be >1023 since delay of 22 clk periods before first FIR filter value is ready. 
signal intDSPDone : std_logic;
signal intDSPMemAdd: std_logic_vector(10 downto 0);

begin

DSPFunct_i: DSPFunct 
port map (clk => clk, 
          rst => rst,         
		  selFirstEffect => selFirstEffect,
		  selFinalEffect => selFinalEffect,
		  ctrl0_0        => ctrl0_0,
		  ctrl0_1        => ctrl0_1,
		  ctrl1_0        => ctrl1_0,
		  ctrl1_1        => ctrl1_1,
		  ctrl2_0        => ctrl2_0,
		  ctrl2_1        => ctrl2_1,
		  ctrl3_0        => ctrl3_0,
		  ctrl3_1        => ctrl3_1,
          DSPdatFromMem => DSPDatFromMem,    
          DSPDatToMem => DSPDatToMem 
	      );

-- generate memory address
NSDecode: process (CS, goDSPPulse, ctrl1_1)
begin
	NS <= CS; -- default
	-- increment count if initialised (goDSPPulse asserted) or counting 0-1023 
	-- Counting stops after rolling over to 0, until next goDSPPulse assertion
	if ctrl1_1(1) = '0' then -- not FIR filter	
	   if (goDSPPulse = '1') or (unsigned(CS) > "00000000000") then	
			if (unsigned(CS) <= "01111111111") then
				NS <= std_logic_vector(unsigned(CS) + 1) ; -- increment 
			else -- (unsigned(CS) = "01111111111") ) then
				NS <= (others => '0');                     -- roll over to 0
			end if;
	   end if;
	else     	 	-- FIR filter
				    -- 01111111111     3FFh
			        -- 00000010110 +   26d (16h)
					-- 10000010101
	   if (goDSPPulse = '1') or ( (unsigned(CS) > "00000000000") and (unsigned(CS) <= "10000010101") ) then
				NS <= std_logic_vector(unsigned(CS) + 1) ; -- increment and roll over automatically since vector arith 
					-- towards end of sequence, re-reading memory inputs but don't have effect on the result since never propagate through the FIR filter.
	   end if;
	end if;
end process;

stateReg_i: process (clk, rst)
begin
    if rst = '1' then
        CS <= (others => '0');
    elsif clk'event and clk = '1' then
		CS <= NS;
    end if;
end process;
intDSPMemAdd <= std_logic_vector(unsigned(CS) - "00000010110"); -- current address - 22d
DSPMemAdd    <= intDSPMemAdd(9 downto 0);

-- generate memory write control signal and DSP done/ signal
OPDecode_i: process(CS, goDSPPulse, ctrl1_1)
begin
	intDSPDone <= '0'; -- default
    DSPMemWr   <= '0'; 
	if (unsigned(CS) = "00000000000") then
		intDSPDone <= '1';
    end if;
	if ctrl1_1(1) = '0' then -- not FIR filter
		if (goDSPPulse = '1') or (unsigned(CS) > "00000000000") then
			DSPMemWr <= '1'; -- assert in each clock period
		end if;
	else -- FIR filter
		if (unsigned(CS) > "00000000000") and (unsigned(CS) > "00000010101") then -- after validOut asserted
			DSPMemWr <= '1'; -- assert on each clk period after FIR filter dataOut becomes valid
		end if;
	end if;
end process;
DSPDone  <= intDSPDone;

end RTL;