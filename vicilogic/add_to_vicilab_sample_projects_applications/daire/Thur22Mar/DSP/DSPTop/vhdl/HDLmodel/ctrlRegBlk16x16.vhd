-- Synthesisable VHDL model for 8 x 16-bit registers
-- Created : Mar 21st 2018. F Morgan

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.FIRFilterPackage.all; 

entity ctrlRegBlk16x16 is
 Port (  clk :             in  std_logic;                     -- system clock strobe, low-to-high active edge
		 ld0 :             in  std_logic;    				  -- synchronously load 0s in array
		 ctrlRegAdd     :  in  std_logic_vector(3 downto 0);  -- register address
		 enCtrlRegWr    :  in  std_logic;                     -- register write enable, asserted high
		 ctrlRegIn      :  in  std_logic_vector(15 downto 0); -- data byte (to be written)
		 ctrlRegOut     :  out std_logic_vector(15 downto 0); -- addressed ctrlReg data 
		 ctrlReg        :  out array16x16			 		  -- ctrlReg data array
 		 );
end ctrlRegBlk16x16;

architecture RTL of ctrlRegBlk16x16 is
signal NS_ctrlRegBlk : array16x16;
signal CS_ctrlRegBlk : array16x16 := (others => (others => '0'));

begin

NSDecode_i: process(CS_ctrlRegBlk, enCtrlRegWr, ctrlRegAdd, ctrlRegIn) 
begin
  NS_ctrlRegBlk <= CS_ctrlRegBlk; -- default
  if enCtrlRegWr = '1' then
        NS_ctrlRegBlk(TO_INTEGER(unsigned(ctrlRegAdd))) <= ctrlRegIn; -- change addressed NS byte
  end if;
end process;

stateReg_i: process(clk) 
begin
 if clk'event and clk = '1' then
    if ld0 = '1' then
        CS_ctrlRegBlk <= (others => (others => '0')); -- synchronously load array with 0 
    else
        CS_ctrlRegBlk <= NS_ctrlRegBlk; 
    end if;
 end if;
end process;
asgnOP_i: 	  ctrlReg        <= CS_ctrlRegBlk;

ctrlRegOut_i: ctrlRegOut <= CS_ctrlRegBlk(TO_INTEGER(unsigned(ctrlRegAdd)));

end RTL;