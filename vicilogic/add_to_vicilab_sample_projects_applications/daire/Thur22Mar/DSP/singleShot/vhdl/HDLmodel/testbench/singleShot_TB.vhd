-- Engineer: Fearghal Morgan
-- viciLogic 
-- Creation Date: 18/5/2010
-- singleShot_TB:  testbench

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
ENTITY singleShot_TB IS END singleShot_TB;
 
ARCHITECTURE behavior OF singleShot_TB IS 
 
-- Component Declaration for the Unit Under Test (UUT)
COMPONENT singleShot
PORT(clk :   IN  std_logic;
	 rst :   IN  std_logic;
	 sw :    IN  std_logic;
	 aShot : OUT std_logic
  );
END COMPONENT;

-- Declare testbench-level signals. May be different from component signal names
SIGNAL clk : std_logic := '1'; -- initialise clk to '1' since std_logic default state is 'u' (undefined). 
signal rst : std_logic;
signal sw  : std_logic;

signal aShot : std_logic;

constant period   : time := 20 ns;	      -- 50MHz clk
signal   endOfSim : boolean := false;     -- assert at end of simulation to show end point.
signal   testNo   : integer range 0 to 5; -- facilitates test numbers 0-5. Aids locating each simulation waveform test 
 
BEGIN
 
-- Instantiate the Unit Under Test (UUT)
uut: singleShot PORT MAP (
		 clk   => clk,
		 rst   => rst,
		 sw    => sw,
		 aShot => aShot
	  );

-- clk stimulus continuing until simulation stimulus is no longer applied
clkStim : process (clk)
begin
  if endOfSim = false then
     clk <= not clk after period/2;
  end if;
end process;

StimulusProcess: PROCESS 
-- No sensitivity list required = auto-executes at start of testbench simulation
-- apply rst, sw stimulus
begin 
	report "%N : Simulation Start.";

    report "Test 0: Initialise i/ps. Toggle rst and move sim time to 0.2*period after active clk edge"; 
    testNo <= 0;
	sw     <= '0';  
    rst    <= '1';		 -- assert/deassert rst signal sequence
	wait for period*1.2; -- 0.2*period after active clk edge
    rst    <= '0';
    wait for period;	

	report "%N : Test1: Hold sw asserted for several periods. Should generate single pulse on aShot.";
	sw  <= '1';
	wait for period*3;
	sw  <= '0';
	wait for period*1;

	report "%N : Test2: Back-to-back sw toggles. Should generate single pulses on aShot.";
	sw  <= '1';
	wait for period;
	sw  <= '0';
	wait for period;
	sw  <= '1';
	wait for period;
	sw  <= '0';
	wait for period;

	sw  <= '1';
    wait for 2*period;
    sw  <= '0';
    wait for period;

	endOfSIm <= true;    -- assert flag
    report "simulation done";   
    wait;               -- wait forever
end process;

END;
