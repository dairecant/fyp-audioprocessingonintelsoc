-- Description : DSPPackage 
-- Copyright (c) 2011-2017 Fearghal Morgan

library IEEE;
use IEEE.STD_LOGIC_1164.all;

package DSPPackage is

type array1024x16 is array (0 to 1023) of std_logic_vector(15 downto 0); -- note 0 to 127

end DSPPackage;

package body DSPPackage is
 
end DSPPackage;