LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
USE ieee.std_logic_textio.all;             -- necessary when using fileIO
USE std.textio.all;	 
use work.array1024x16WithLoad_Package.all; -- package defining array
 
ENTITY CSRBlk1024x16WithLoad_TB IS END CSRBlk1024x16WithLoad_TB;
 
ARCHITECTURE behavior OF CSRBlk1024x16WithLoad_TB IS 
 
COMPONENT dualPortRegBlk1024x16WithLoad is
 Port (  clk		: in  std_logic;    					 
		 rst		: in  std_logic;    					 
         enPort0    : in  std_logic;

		 p0Load	    : in  std_logic;   
		 p0LoadDat  : in  std_logic_vector(15 downto 0);                      
		 p0Add 	 	: in  std_logic_vector( 9 downto 0);   
		 p0DatIn  	: in  std_logic_vector(15 downto 0);                      
		 p0Wr 	    : in  std_logic;   
	     p0DatOut   : out std_logic_vector(15 downto 0);                      

		 p1Load	    : in  std_logic;   
		 p1LoadDat  : in  std_logic_vector(15 downto 0);                      
		 p1Add 	 	: in  std_logic_vector( 9 downto 0);   
		 p1DatIn  	: in  std_logic_vector(15 downto 0);                      
		 p1Wr 	    : in  std_logic;   
	     p1DatOut   : out std_logic_vector(15 downto 0)                     
 		 );         
end COMPONENT;

-- Declare testbench-level signals.
SIGNAL clk :                       std_logic := '1'; -- initialise clk to '1' since std_logic default state is 'u' (undefined). 
signal rst :			 		   std_logic;
signal enPort0 :		           std_logic;
   
signal p0Load :	                   std_logic;
signal p0LoadDat :                 std_logic_vector(15 downto 0); 
signal p0Add : 	                   std_logic_vector(9 downto 0);  
signal p0DatIn :                   std_logic_vector(15 downto 0); 
signal p0Wr : 	                   std_logic;
signal p0DatOut:                   std_logic_vector(15 downto 0); 

signal p1Load :	                   std_logic;
signal p1LoadDat :                 std_logic_vector(15 downto 0); 
signal p1Add : 	                   std_logic_vector(9 downto 0);  
signal p1DatIn :                   std_logic_vector(15 downto 0); 
signal p1Wr : 	                   std_logic;
signal p1DatOut:                   std_logic_vector(15 downto 0); 

constant period    : time := 40 ns;	       -- 25MHz clk
signal   endOfSim  : boolean := false;     -- assert at end of simulation to show end point.
signal   testNo    : integer range 0 to 5; -- facilitates test numbers 0-5. Aids locating each simulation waveform test 

BEGIN

-- Instantiate the Unit Under Test (UUT)
uut: dualPortRegBlk1024x16WithLoad 
PORT MAP (clk                   => clk,
		 rst 			        => rst,
		 enPort0                => enPort0,

         p0Load 	            => p0Load, 	  
         p0LoadDat              => p0LoadDat, 
         p0Add  	            => p0Add,  	  
         p0DatIn                => p0DatIn,     
         p0Wr  	                => p0Wr,  	  
         p0DatOut               => p0DatOut,     

         p1Load 	            => p1Load, 	  
         p1LoadDat              => p1LoadDat, 
         p1Add  	            => p1Add,  	  
         p1DatIn                => p1DatIn,     
         p1Wr  	                => p1Wr,  	  
         p1DatOut               => p1DatOut
		 );

-- clk stimulus continuing until simulation stimulus is no longer applied
clkStim:	process (clk)
begin
 if (endOfSim = false) then 
   clk <= not clk after period/2;
 end if; 
end process;

STIMUsingFileIO : PROCESS 
-- No sensitivity list required = auto-executes at start of testbench simulation
 variable LL :        line;
 variable v_enPort0 : std_logic; 
 variable v_p0load :    std_logic; 
 variable v_p0loadDat : std_logic_vector(15 downto 0); 
 variable v_p0add :     std_logic_vector(9 downto 0); 
 variable v_p0datIn :   std_logic_vector(15 downto 0); 
 variable v_p0wr :      std_logic; 
 variable v_p1load :    std_logic; 
 variable v_p1loadDat : std_logic_vector(15 downto 0); 
 variable v_p1add :     std_logic_vector(9 downto 0); 
 variable v_p1datIn :   std_logic_vector(15 downto 0); 
 variable v_p1wr :      std_logic; 
 variable numClks :   integer range 0 to 255; 
 file     stimIn :    text open READ_MODE is "dualPortRegBlk1024x16WithLoadStim.txt";  -- input file declaration
  
 begin
  enPort0        <= '0';
  p0Load 	     <= '0';
  p0LoadDat      <= (others => '0');
  p0Add  	     <= (others => '0');
  p0DatIn        <= (others => '0');
  p0Wr  	     <= '0'; 
  p0DatIn        <= (others => '0');

  p1Load 	     <= '0';
  p1LoadDat      <= (others => '0');
  p1Add  	     <= (others => '0');
  p1DatIn        <= (others => '0');
  p1Wr  	     <= '0'; 
  p1DatIn        <= (others => '0');

  rst            <= '1';
  wait for period*1.2; -- 0.2*period after active clk edge
  rst            <= '0';
  wait for period;  
 
  -- o/p msg to simulation  transcript
  report "applying stimulus sequence"; 
 
  while not (endfile(stimIn)) loop  
   readline (stimIn, LL);
   read     (LL, v_enPort0);  
   enPort0   <= v_enPort0;
   
   read     (LL, v_p0Load);  
   p0Load    <= v_p0Load;
   read     (LL, v_p0LoadDat);  
   p0LoadDat <= v_p0LoadDat;  
   read     (LL, v_p0Add);  
   p0Add     <= v_p0Add;  
   read     (LL, v_p0DatIn);  
   p0DatIn   <= v_p0DatIn;  
   read     (LL, v_p0wr);  
   p0Wr      <= v_p0wr; 

   read     (LL, v_p1Load);  
   p1Load    <= v_p1Load;
   read     (LL, v_p1LoadDat);  
   p1LoadDat <= v_p1LoadDat;  
   read     (LL, v_p1Add);  
   p1Add     <= v_p1Add;  
   read     (LL, v_p1DatIn);  
   p1DatIn   <= v_p1DatIn;  
   read     (LL, v_p1wr);  
   p1Wr      <= v_p1wr; 

   read (LL, numClks);  -- read no, of clock periods to delay before getting next cmd read
   wait for numClks*period; 			
 end loop;  

 report "simulation done";   			   
 
 endOfSim <= true;
 wait; -- will wait forever
END PROCESS;

end;