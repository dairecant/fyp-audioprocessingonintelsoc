viciLab folder stucture 


specification 				documentation related to the application design
1. 	design
			designSpec
			testSpec
			relatedDocs
2.	drawings
		visio 				source drawings
		png 				png format drawings


vhdl 
1. 	HDLmodel
		HDL model source 
		HDL testbench 
2. 	xilinxprj				Xilinx Vivado project files	
		Simulation 			Vivado waveform configuration file / simulation files IO source files
3. 	intelprj				Intel Quartus project files	


verilog
Same structure as vhdl folder


vicilabprj 
1. 	cfg						FPGA/SoC .bit file and json file (extracted from the vicilab build). 	Does not require the other vicilab build files.

2. 	GUI						viciLab Graphical User Interface (GUI)-related files, e.g, 
		apps 				GUI application-specific python files in apps folder
		icons 				application-specific widget icons (icons folder)
							When icons become generic icons, move generic icons to vicilab viciClient/icons folder
		GUI 				vicilab GUI metatdata file, for saving/rebuilding GUIs
	
3. 	macro					viciLab macros, used to automate application of stimulus to the FPGA design 
								
4. 	prj						viciLab project files (.vlw, vla, vll)

5. 	log						Log of vicilab activities, uploads, builds, planahead/vivado versions etc, copy logs from the generated planahead/vivado projects (FC)
