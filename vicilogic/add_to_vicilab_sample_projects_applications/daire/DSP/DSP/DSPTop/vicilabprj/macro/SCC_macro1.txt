; viciLab SCCPYNQ1_macro1.txt macro 
; Created by Fearghal Morgan 
; October 2017
; =======================================================================================================
; Supported macro commands: 
;  ";" indicates comment, text in remainder of line is ignored. Line spaces supported. 
;  set/unset "signal name"
;  assert/deassert reset, e.g, assert reset, also supports signal names rst, Reset, Rst
;  start/stop clock (run clock forever until stop), also supports signal names clk, Clock, Clk
;  step clk by "number" (integer), number is no of clk cycles, e.g, step clk by 1. Also supports signal names clock, Clock, Clk
;
;  apply type nb to signal_name (where type = h (hex), b (binary, default if not included), d (decimal).
;  delay time unit (integer), unit is ms (msec) or s (second), default time = 1, default unit is s (sec)
;  delay (or sleep) => delay 1 second. delay 2 => delay 2 seconds
;  Example: delay 1s, delay 1, delay (all represent 1 second delay), also supports Delay, sleep, Sleep, wait, Wait
;
;  loop nb, create a loop for (i=0, i<nb_loop, i++)), nb is decimal
;  end loop (must be called to define the end of the loop). Loop forever is not implemented
; =======================================================================================================

set 		     singleCycCompTop:hostCtrlInstrMem     ; modify instruction memory array

set              singleCycCompTop:hostInstrMem_Load    ; load all elements in array with loadDat
apply h FE00 to  singleCycCompTop:hostInstrMem_LoadDat ; value = FE00 => NOP
step clk by 1
delay 1000 ms
unset            singleCycCompTop:hostInstrMem_Load    ; deassert

set 	         singleCycCompTop:hostInstrMem_Wr      ; write individual instruction memory locations
 apply h 0 to    singleCycCompTop:hostInstrMem_Add     
 apply h 2600 to singleCycCompTop:hostInstrMem_DatIn   ; INC R0, R0
 step clk by 1
 delay 1000 ms
 apply h 1 to    singleCycCompTop:hostInstrMem_Add     
 apply h 3600 to singleCycCompTop:hostInstrMem_DatIn   ; INV R0, R0
 step clk by 1
 delay 1000 ms
 apply h 2 to    singleCycCompTop:hostInstrMem_Add     
 apply h 0 to    singleCycCompTop:hostInstrMem_DatIn   ; END
 step clk by 1
 delay 1000 ms
unset 		     singleCycCompTop:hostInstrMem_Wr      ; write instruction memory element
unset 			 singleCycCompTop:hostCtrlInstrMem     ; modify instruction memory array
step clk by 1
delay 1000 ms

assert reset  										   ; Reset PC and registers. Instruction and data/stack memory are not resettable (they use load or wr function) 
delay 1000 ms
deassert reset
delay 1000 ms

loop 5												   ; PC is initially 0. Execute instructions
 set 			 singleCycCompTop:step
 step clk by 1
 delay 1000 ms
 unset 			 singleCycCompTop:step
 step clk by 1
 delay 1000 ms
end loop