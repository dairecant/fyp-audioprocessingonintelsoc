-- Description: Testbench for CB4CLED 4-bit cascadable up/down, loadable counter with chip enable, asynchronous rst
-- Engineer: Fearghal Morgan
-- viciLogic 
-- Date: 7/12/2012
-- Change History: Initial version

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all;
 
ENTITY DSPTop_TB IS  END DSPTop_TB; -- testbench entity has no inputs or outputs, i.e, it is a closed test environment.
 
ARCHITECTURE behavior OF DSPTop_TB IS 
 
COMPONENT DSPTop is
Port ( clk :        		 in std_logic;						-- system clock strobe
       rst :        		 in std_logic; 						-- asynchronous system reset, asserted

       goDSP :				 in  std_logic;
       DSPDone : 			 out std_logic;

       selFirstEffect: in std_logic_vector(2 downto 0);  
	   selFinalEffect: in std_logic_vector(1 downto 0);  
	   ctrl0_0:        in std_logic_vector(15 downto 0);  
	   ctrl0_1:        in std_logic_vector(15 downto 0);  
	   ctrl1_0:        in std_logic_vector(15 downto 0);  
	   ctrl1_1:        in std_logic_vector(15 downto 0);  
	   ctrl2_0:        in std_logic_vector(15 downto 0);  
	   ctrl2_1:        in std_logic_vector(15 downto 0);  
	   ctrl3_0:        in std_logic_vector(15 downto 0);  
	   ctrl3_1:        in std_logic_vector(15 downto 0);  

       host_EnDSPMem :     	 in  std_logic;
	   host_DSPMem_Load :    in  std_logic;                      
	   host_DSPMem_LoadDat : in  std_logic_vector(15 downto 0); 
	   host_DSPMem_Add :     in  std_logic_vector(9 downto 0);  
	   host_DSPMem_DatIn :   in  std_logic_vector(15 downto 0);  
	   host_DSPMem_Wr :      in  std_logic;                      
	   host_DSPMem_DatOut :  out std_logic_vector(15 downto 0)
--	   host_DSPMem_DatArrayOut : out std_logic_vector(511 downto 0)  
      );
END COMPONENT;    

-- Declare testbench-level signals. May be different from component signal names
SIGNAL     clk : std_logic := '1'; -- initialise clk to '1' since std_logic default state is 'u' (undefined). 
signal     rst : std_logic;

signal     goDSP :         std_logic;
signal     DSPDone :       std_logic;

signal     selFirstEffect: std_logic_vector(2 downto 0);  
signal	   selFinalEffect: std_logic_vector(1 downto 0);  
signal	   ctrl0_0:        std_logic_vector(15 downto 0);  
signal	   ctrl0_1:        std_logic_vector(15 downto 0);  
signal	   ctrl1_0:        std_logic_vector(15 downto 0);  
signal	   ctrl1_1:        std_logic_vector(15 downto 0);  
signal	   ctrl2_0:        std_logic_vector(15 downto 0);  
signal	   ctrl2_1:        std_logic_vector(15 downto 0);  
signal	   ctrl3_0:        std_logic_vector(15 downto 0);  
signal	   ctrl3_1:        std_logic_vector(15 downto 0);  

signal     host_EnDSPMem :       std_logic;
signal	   host_DSPMem_Load :    std_logic;                      
signal	   host_DSPMem_LoadDat : std_logic_vector(15 downto 0); 
signal	   host_DSPMem_Add :     std_logic_vector(9 downto 0);  
signal	   host_DSPMem_DatIn :   std_logic_vector(15 downto 0);  
signal	   host_DSPMem_Wr :      std_logic;                      
signal	   host_DSPMem_DatOut :  std_logic_vector(15 downto 0);  
--signal	   host_DSPMem_DatArrayOut :  std_logic_vector(511 downto 0);  

constant period   : time := 80 ns;	      -- 12.5MHz clk
signal   endOfSim : boolean := false;     -- assert at end of simulation to show end point.
signal   testNo   : integer range 0 to 5; -- facilitates test numbers 0-5. Aids locating each simulation waveform test 
signal   outputFileDescriptorWritten     : boolean := false;
signal   stimFileDescriptorLinesReadDone : boolean := false;
 
BEGIN
  
--	 Instantiate the Unit Under Test (UUT)
uut: DSPTop PORT MAP -- Instantiate the Unit Under Test (UUT)
         (clk       => clk,
          rst       => rst,

       goDSP     				=> goDSP,
       DSPDone    				=> DSPDone,
	   
	   selFirstEffect           => selFirstEffect,
	   selFinalEffect           => selFinalEffect,
	   ctrl0_0                  => ctrl0_0,      
	   ctrl0_1                  => ctrl0_1,      
	   ctrl1_0                  => ctrl1_0,      
	   ctrl1_1                  => ctrl1_1,      
	   ctrl2_0                  => ctrl2_0,     
	   ctrl2_1                  => ctrl2_1,      
	   ctrl3_0                  => ctrl3_0,     
	   ctrl3_1                  => ctrl3_1,      
	   
       host_EnDSPMem 			 => host_EnDSPMem, 			
	   host_DSPMem_Load 		 => host_DSPMem_Load, 		    
	   host_DSPMem_LoadDat 		 => host_DSPMem_LoadDat, 		
	   host_DSPMem_Add			 => host_DSPMem_Add,			 
	   host_DSPMem_DatIn		 => host_DSPMem_DatIn,		 
	   host_DSPMem_Wr 			 => host_DSPMem_Wr, 			
	   host_DSPMem_DatOut		 => host_DSPMem_DatOut		
--       host_DSPMem_DatArrayOut   => host_DSPMem_DatArrayOut
      );

-- clk stimulus continuing until simulation stimulus is no longer applied
clkStim : process (clk)
begin
  if endOfSim = false then
     clk <= not clk after period/2;
  end if;
end process;

stim_i: process 
begin 
	report "%N : Simulation Start.";

    report "Test 0: Initialise i/ps. Toggle rst and move sim time to 0.2*period after active clk edge"; 
    testNo <= 0;

       goDSP     				<= '0';
	   
	   selFirstEffect <= "001";
	   selFinalEffect <= "01";
	   ctrl0_0        <= (others => '0');
	   ctrl0_1        <= (others => '0');
	   ctrl1_0        <= (others => '0');
--	   ctrl1_0        <= X"0001";
	   ctrl1_0        <= X"678F";
	   ctrl1_1        <= (others => '0');
	   ctrl2_0        <= (others => '0');
	   ctrl2_1        <= (others => '0');
	   ctrl3_0        <= (others => '0');
	   ctrl3_1        <= (others => '0');
	   	   
       host_EnDSPMem 				<= '0';
	   host_DSPMem_Load 			<= '0';
	   host_DSPMem_LoadDat 		 	<= (others => '0');
	   host_DSPMem_Add				<= (others => '0');
	   host_DSPMem_DatIn			<= (others => '0');
	   host_DSPMem_Wr 				<= '0';
    rst     <= '1';		 -- assert/deassert rst signal sequence
	wait for period*1.2; -- 0.2*period after active clk edge
    rst     <= '0';
    wait for 2*period;	

    host_EnDSPMem  <= '1';
    host_DSPMem_Load <= '1';                       
    host_DSPMem_LoadDat <= X"F347";  
    wait for period;	
    host_EnDSPMem  <= '0';
    host_DSPMem_Load <= '0';                       
    wait for period;	
 	
	   ctrl1_1(0)     <= '1';
    ctrl1_1(1)     <= '1';

       goDSP     				<= '1';

    wait for 1200*period;	
	endOfSIm <= true;   -- assert flag
    report "simulation done";   
    wait;                                               -- wait forever
END PROCESS;


END;