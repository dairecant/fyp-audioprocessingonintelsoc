-- dspFunct(2:0) functions:
-- 0 echo foreground
-- 1,2,3 foreground red, green, blue only
-- 4 invert foreground
-- 5 colour detection (foreground image)
-- 6 not used
-- 7 delta (difference in foreground and background images)

-- Foreground image is in SDRAM quadrant1 (constantly updated from the webcam)
-- Background image is in SDRAM quadrant0 (must be specifically taken using the webcam)
-- dspBlk always reads bckgnd, then foreground, then writes result to quadrant2 (with data as the o/p of a combinational function)

-- Colour detection function
-- Reference: Kovac, J.; Peer, P.; Solina, F., "Human skin color clustering for face detection," EUROCON 2003. Computer as a Tool. 
-- The IEEE Region 8, vol.2, no., pp.144,148 vol.2, 22-24 Sept. 2003
-- The dspFunct has 
--   inputs  SDRAMAdd(21:0) and setDSPActive, and SDRAM data read by dspBlk
--   output signal vector result to SDRAM
--   internal signal regSDRAMAdd(21:0) (address of first coloured pixel)
-- Input setDSPActive enables clearing of foundColourPixelBlkGTDSPFunct7TO3 at the start of each new frame processing.
-- The colour detection algorithm stores the (SDRAMAdd) 22 bit address of the first coloured pixel in regSDRAMAdd. 
-- When the first coloured pixel is found, its address (SDRAMAdd) is registered in regSDRAMAdd and 
-- Flag setfoundColourPixelBlkGTDSPFunct7TO3 prevents any further update in regSDRAMAdd during the remaining processing of the current frame. 
-- regSDRAMAdd is 22 bit address 32-bit data (4M x 32-bit data)

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity dspFunctBlk is
    Port ( clk :          in  std_logic;
           rst :          in  std_logic;
		   dspFunct :     in  STD_LOGIC_VECTOR (7  downto 0);
           quad1Dat :     in  STD_LOGIC_VECTOR (31 downto 0);
           quad0Dat :     in  STD_LOGIC_VECTOR (31 downto 0);
           result :       out STD_LOGIC_VECTOR (31 downto 0);
           SDRAMAdd :     in  std_logic_vector (21 downto 0); -- 4M x 32-bit locations
           regSDRAMAddout :     out  std_logic_vector (21 downto 0); -- 4M x 32-bit locations
           setDSPActive : in  std_logic
           );
end dspFunctBlk;

architecture RTL of dspFunctBlk is
signal setfoundColourPixelBlkGTDSPFunct7TO3 : std_logic;
  
-- could write these values to the status register eventually.
-- vicilogic can view the regSDRAMAdd value and use to e.g, move robot arm
signal foundColourPixelBlkGTDSPFunct7TO3 : std_logic;      

signal colourpixelcnt : std_logic_vector(4 downto 0); 
signal reg_colourpixelcnt : std_logic_vector(4 downto 0);

signal setfoundRedColourPixelBlkGTDSPFunct7TO3 : std_logic;
signal foundRedColourPixelBlkGTDSPFunct7TO3 : std_logic;   
signal Redcolourpixelcnt : std_logic_vector(4 downto 0); 
signal reg_Redcolourpixelcnt : std_logic_vector(4 downto 0); 
signal regSDRAMAdd : std_logic_vector(21 downto 0);
signal foundContigRedColour : std_logic;

signal sigFlagA, sigFlagB, sigFlagC, sigFlagRed : boolean;

begin

process (clk, rst) -- counts contiguous colour pixels
begin
  if rst = '1' then 
     colourpixelcnt <= "00000";
  elsif clk'event and clk='1' then
    if setDSPActive = '1' then              -- clear at start of DSP 
        colourpixelcnt <= "00000";
    elsif (not sigFlagA) or (not sigFlagB) or (not sigFlagC) then -- clear cnt when colour not found and register the colour pixel blk size
        colourpixelcnt <= "00000";
    elsif (sigFlagA and sigFlagB and sigFlagC) then               -- increment when new or next contiguous colour pixel 
        colourpixelcnt <= colourpixelcnt + 1;
    end if;
  end if;
end process;

process (clk, rst)  
begin
  if rst = '1' then 
     reg_colourpixelcnt <= "00000";
  elsif clk'event and clk='1' then
    if setDSPActive = '1' then              					  -- clear at start of DSP 
        reg_colourpixelcnt <= "00000";
    elsif (not sigFlagA) or (not sigFlagB) or (not sigFlagC) then -- clear cnt when colour not found and register the colour pixel blk size
        reg_colourpixelcnt <= colourpixelcnt;
    end if;
  end if;
end process;

flagRegold: process (clk, rst)
begin
    if rst = '1' then 
        foundColourPixelBlkGTDSPFunct7TO3 <= '0';
        -- regSDRAMAdd <= (others => '0');
    elsif clk'event and clk = '1' then
        if setDSPActive = '1' then 
           foundColourPixelBlkGTDSPFunct7TO3 <= '0';            	 -- deassert at start of each fram processing
           -- regSDRAMAdd <= (others => '0');                          	 -- set as 0 at start of dsp task
        elsif foundColourPixelBlkGTDSPFunct7TO3 = '0' then
           if reg_colourpixelcnt >= dspFunct(7 downto 3) then
                foundColourPixelBlkGTDSPFunct7TO3 <= '1';            -- assert when first colour pixel found 
                if setfoundColourPixelBlkGTDSPFunct7TO3 = '1' then
                   -- regSDRAMAdd <= SDRAMAdd;                                 -- register SDRAM address
                end if;
           end if; 
        end if;
    end if;    
end process;

process (clk, rst) -- counts contiguous  colour pixels
begin
  if rst = '1' then 
     Redcolourpixelcnt <= "00000";
  elsif clk'event and clk='1' then
    if setDSPActive = '1' then              -- clear at start of DSP 
        Redcolourpixelcnt <= "00000";
    elsif not sigFlagRed then -- clear cnt when colour not found and register the colour pixel blk size
        Redcolourpixelcnt <= "00000";
    elsif sigFlagRed then               -- increment when new or next contiguous colour pixel 
       Redcolourpixelcnt <= Redcolourpixelcnt + 1;
    end if;
  end if;
end process;

process (clk, rst)  
begin
  if rst = '1' then 
     reg_Redcolourpixelcnt <= "00000";
  elsif clk'event and clk='1' then
    if setDSPActive = '1' then              					  -- clear at start of DSP 
        reg_Redcolourpixelcnt <= "00000";
    elsif not sigFlagRed then -- clear cnt when colour not found and register the colour pixel blk size
        reg_Redcolourpixelcnt <= redcolourpixelcnt;
    end if;
  end if;
end process;

flagReg: process (clk, rst)
begin
    if rst = '1' then 
        foundRedColourPixelBlkGTDSPFunct7TO3 <= '0';
         regSDRAMAdd <= (others => '0');
    elsif clk'event and clk = '1' then
        if setDSPActive = '1' then 
           foundRedColourPixelBlkGTDSPFunct7TO3 <= '0';            	 -- deassert at start of each fram processing
            regSDRAMAdd <= (others => '0');                          	 -- set as 0 at start of dsp task
        elsif foundRedColourPixelBlkGTDSPFunct7TO3 = '0' then
           if reg_Redcolourpixelcnt >= dspFunct(7 downto 3) then
                foundRedColourPixelBlkGTDSPFunct7TO3 <= '1';            -- assert when first colour pixel found 
                if setfoundRedColourPixelBlkGTDSPFunct7TO3 = '1' then
                    regSDRAMAdd <= SDRAMAdd;                                 -- register SDRAM address
                end if;
           end if; 
        end if;
    end if;    
end process;

dspFunct_i: process(dspFunct, quad1Dat, quad0Dat) 
variable varDSPFunct : std_logic_vector(2 downto 0);
variable foundFirstColourPixel: boolean; 
variable R, G, B : std_logic_vector (7 downto 0);
variable minIndex, maxIndex : integer range 1 to 3;
variable flagA, flagB, flagC, FlagRed : BOOLEAN;
begin
    varDSPFunct := dspFunct(2 downto 0); -- default assignments
	result <= (others => '0');     
    sigFlagA <= FALSE; 
    sigFlagB <= FALSE;
    sigFlagC <= FALSE;
    sigFlagRed <= FALSE;
    R := quad1Dat(31 downto 24);
    G := quad1Dat(23 downto 16);
    B := quad1Dat(15 downto 8);
    setfoundColourPixelBlkGTDSPFunct7TO3 <= '0';
    setfoundRedColourPixelBlkGTDSPFunct7TO3 <= '1';

	case varDSPFunct is
		when "000" => -- echo quadrant 1 image 
			result <= quad1Dat;
        when "001" => 
            if quad1Dat(31 downto 24) < 49 and  quad1Dat(23 downto 16) < 119 and quad1Dat(15 downto 8) > 119 then 
                result(15 downto 8) <= (others => '1');
                result(7 downto 0) <= (others => '0');
                result(31 downto 24) <= (others => '0');
            else
                result(31 downto 0) <= (others => '0');
            end if;
        when "010" =>  
            if quad1Dat(31 downto 24) < 30 and  quad1Dat(23 downto 16) > 48 and quad1Dat(15 downto 8) < 15 then 
                result(23 downto 16) <= (others => '1');
                result(31 downto 24) <= (others => '0');
                result(15 downto 0) <= (others => '0');
            else
                result(31 downto 0) <= (others => '0');
            end if;
        when "011" =>  
			-- result(15 downto 8)  <= quad1Dat(15 downto 8); -- filter all except blue
			-- alternative function, swap colour values to provide a different colour mix
            result(31 downto 24) <= quad1Dat(23 downto 16); -- red takes green
            result(23 downto 16) <= quad1Dat(15 downto 8);  -- green takes blue
            result(15 downto 8)  <= quad1Dat(31 downto 24); -- blue takes red 
        when "100" => -- invert
            result <= not quad1Dat;
            
        when "110" => 
        -- red detection
            if quad1Dat(31 downto 24) > 205 and  quad1Dat(23 downto 16) < 100 and quad1Dat(15 downto 8) < 100 then 
                result(31 downto 24) <= (others => '1');
                result(23 downto 0) <= (others => '0');
                FlagRed := TRUE;
            else
                result(31 downto 0) <= (others => '0');
                FlagRed := FALSE;
            end if;
            sigFlagRed <= FlagRed;

		when "101" =>						 -- colour detection, Kovac publication reference included in header.
		flagA := false;                      -- defaults
        flagB := false;
        flagC := false;
        maxIndex := 1;
        minIndex := 1;
        R := quad1Dat(31 downto 24);
        G := quad1Dat(23 downto 16);
        B := quad1Dat(15 downto 8);

        -- perform three tests (A,B,C) which combine to define presence of colour 
        -- include reference to paper describing this algorithm.
            -- test A
            if (R>X"5F" and  G>X"28"   and  B>X"14") then
              flagA := true;
            end if;
 
            -- test B
            if R>G and R>B then maxIndex := 3;           
            elsif G>R and G>B then maxIndex := 2;           
            elsif B>R and B>G then maxIndex := 1;           
            end if;
 
            if R<G and R<B then minIndex := 3;           
            elsif G<R and G<B then minIndex := 2;           
            elsif B<R and B<G then minIndex := 1;           
            end if;
 
            if (      quad1Dat( ((8*maxIndex)+7) downto (8*maxIndex) )  -  quad1Dat( ((8*minIndex)+7) downto (8*minIndex) )        ) > X"0F" then
               flagB := true;
            end if;
            
            -- test C
            if ( R>(G+X"0A") and R>B ) then
               flagC := true;
            end if;
            
            if flagA and flagB and flagC then
               -- replace colour area with red=0, green=0, strong blue colour=FFh.
               result <= x"0000FF00"; -- else default, black (all 0s)
            end if;
			
			sigFlagA <= flagA; -- take variable out to other process
			sigFlagB <= flagB;
			sigFlagC <= flagC;

			
		-- Delta frame. Subtract Quadrant 1 and 0, pixel-by-pixel,  larger value - smaller value
		-- Note: Delta is not a 32-bit subtraction
	    when "111" 	=>  
			for i in 0 to 3 loop
				if quad1Dat((i*8+7) downto (i*8)) > quad0Dat((i*8+7) downto (i*8)) then
					result((i*8+7) downto (i*8)) <= quad1Dat((i*8+7) downto (i*8)) - quad0Dat((i*8+7) downto (i*8));
				else
					result((i*8+7) downto (i*8)) <= quad0Dat((i*8+7) downto (i*8)) - quad1Dat((i*8+7) downto (i*8));
				end if;
			end loop;
    
		when others => 
			null;

		end case;

end process;

end RTL;