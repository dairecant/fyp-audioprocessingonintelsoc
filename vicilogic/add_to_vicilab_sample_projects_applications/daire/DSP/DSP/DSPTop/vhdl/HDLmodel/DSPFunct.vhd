-- DSPFunct
-- Selects various DSP configurations

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

entity DSPFunct is
port  (clk :          in std_logic; 
       rst :          in std_logic; 
       selFirstEffect:in std_logic_vector(2 downto 0);  
	   selFinalEffect:in std_logic_vector(1 downto 0);  
	   ctrl0_0:       in std_logic_vector(15 downto 0);  
	   ctrl0_1:       in std_logic_vector(15 downto 0);  
	   ctrl1_0:       in std_logic_vector(15 downto 0);  
	   ctrl1_1:       in std_logic_vector(15 downto 0);  
       validOut:      out std_logic;  
	   ctrl2_0:       in std_logic_vector(15 downto 0);  
	   ctrl2_1:       in std_logic_vector(15 downto 0);  
	   ctrl3_0:       in std_logic_vector(15 downto 0);  
	   ctrl3_1:       in std_logic_vector(15 downto 0);  
       DSPDatFromMem: in std_logic_vector(15 downto 0);  
       DSPDatToMem:   out std_logic_vector(15 downto 0)
	   );
end entity;

architecture struct of DSPFunct is

component FIRFilter_ip_src_Discrete_FIR_Filter_HDL_Optimized IS
  PORT( clk                               :   IN    std_logic;
        reset                             :   IN    std_logic;
        enb                               :   IN    std_logic;
        dataIn                            :   IN    std_logic_vector(15 DOWNTO 0); 
        dataOut                           :   OUT   std_logic_vector(15 DOWNTO 0);   
        validOut                          :   OUT   std_logic
        );
end component;

begin

effect0_i:  FIRFilter_ip_src_Discrete_FIR_Filter_HDL_Optimized
 PORT MAP( clk      => clk,
           reset    => rst,
           enb      => ctrl1_1(1),
		   dataIn   => DSPDatFromMem, 
           dataOut  => DSPDatToMem, 
		   validOut => validOut
     );	 
		 		 		 
end struct;