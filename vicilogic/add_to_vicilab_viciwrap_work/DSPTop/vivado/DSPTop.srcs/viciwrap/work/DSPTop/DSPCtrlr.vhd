library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

entity DSPCtrlr is
port (clk :           in std_logic; 
      rst :        	  in std_logic; 		
      goDSPPulse:     in std_logic;
      selFirstEffect: in std_logic_vector(2 downto 0);  
	  selFinalEffect: in std_logic_vector(1 downto 0);  
	  ctrl0_0:        in std_logic_vector(15 downto 0);  
	  ctrl0_1:        in std_logic_vector(15 downto 0);  
	  ctrl1_0:        in std_logic_vector(15 downto 0);  
	  ctrl1_1:        in std_logic_vector(15 downto 0);  
	  ctrl2_0:        in std_logic_vector(15 downto 0);  
	  ctrl2_1:        in std_logic_vector(15 downto 0);  
	  ctrl3_0:        in std_logic_vector(15 downto 0);  
	  ctrl3_1:        in std_logic_vector(15 downto 0);  
      DSPDatFromMem:  in std_logic_vector(15 downto 0);  
      DSPMemRdAdd:      out std_logic_vector(8 downto 0);
      DSPMemWrAdd:      out std_logic_vector(8 downto 0);
      DSPDatToMem:    out std_logic_vector(15 downto 0);
      DSPMemWr:       out std_logic;
      DSPDone:        out std_logic;
      
             	  -- host interface to/from ctrlRegBlk memory 
ld0           : in  std_logic; 
ctrlRegAdd  : in  std_logic_vector(3 downto 0);  -- register address
enCtrlRegWr : in  std_logic;                     -- register write enable, asserted high
ctrlRegIn   : in  std_logic_vector(15 downto 0); -- data byte (to be written)
ctrlRegOut  : out std_logic_vector(15 downto 0) -- addressed ctrlReg data 

	  );
end DSPCtrlr;

architecture RTL of DSPCtrlr is

component DSPFunct is
port  (clk :          in std_logic; 
       rst :          in std_logic; 
       selFirstEffect:in std_logic_vector(2 downto 0);  
	   selFinalEffect:in std_logic_vector(1 downto 0);  
	   ctrl0_0:       in std_logic_vector(15 downto 0);  
	   ctrl0_1:       in std_logic_vector(15 downto 0);  
	   ctrl1_0:       in std_logic_vector(15 downto 0);  
	   ctrl1_1:       in std_logic_vector(15 downto 0); 
       validOut:      out std_logic;  
	   ctrl2_0:       in std_logic_vector(15 downto 0);  
	   ctrl2_1:       in std_logic_vector(15 downto 0);  
	   ctrl3_0:       in std_logic_vector(15 downto 0);  
	   ctrl3_1:       in std_logic_vector(15 downto 0);  
       DSPDatFromMem: in std_logic_vector(15 downto 0);  
       DSPDatToMem:   out std_logic_vector(15 downto 0);
       
              	  -- host interface to/from ctrlRegBlk memory 
 ld0           : in  std_logic; 
 ctrlRegAdd  : in  std_logic_vector(3 downto 0);  -- register address
 enCtrlRegWr : in  std_logic;                     -- register write enable, asserted high
 ctrlRegIn   : in  std_logic_vector(15 downto 0); -- data byte (to be written)
 ctrlRegOut  : out std_logic_vector(15 downto 0) -- addressed ctrlReg data 

	   );
end component;

signal NSRdAdd :          std_logic_vector(8 downto 0);
signal CSRdAdd :          std_logic_vector(8 downto 0);   
signal NSWrAdd :          std_logic_vector(8 downto 0);
signal CSWrAdd :          std_logic_vector(8 downto 0);   
signal validOut:          std_logic;  

begin

DSPFunct_i: DSPFunct 
port map (clk => clk, 
          rst => rst,         
		  selFirstEffect => selFirstEffect,
		  selFinalEffect => selFinalEffect,
		  ctrl0_0        => ctrl0_0,
		  ctrl0_1        => ctrl0_1,
		  ctrl1_0        => ctrl1_0,
		  ctrl1_1        => ctrl1_1,
          validOut       => validOut,
		  ctrl2_0        => ctrl2_0,
		  ctrl2_1        => ctrl2_1,
		  ctrl3_0        => ctrl3_0,
		  ctrl3_1        => ctrl3_1,
          DSPdatFromMem  => DSPDatFromMem,
          DSPDatToMem    => DSPDatToMem,
          
          		   	  -- host interface to/from ctrlRegBlk memory 
    ld0        => ld0, 
    ctrlRegAdd => ctrlRegAdd,
    enCtrlRegWr => enCtrlRegWr,
    ctrlRegIn => ctrlRegIn, 
    ctrlRegOut => ctrlRegOut  
 
	      );

-- generate memory read address, incrementing on every active clk edge when (and after) goDSPPulse is asserted
NSRdAddDecode: process (CSRdAdd, goDSPPulse)
begin
	NSRdAdd <= CSRdAdd; -- default
	-- increment count if initialised (goDSPPulse asserted) or counting 0-1023 
	-- Counting stops after rolling over to 0, until next goDSPPulse assertion
	if (goDSPPulse = '1') or (unsigned(CSRdAdd) > "000000000") then	
		if (unsigned(CSRdAdd) <= "111111111") then
			NSRdAdd <= std_logic_vector(unsigned(CSRdAdd) + 1) ; -- increment 
		else  
			NSRdAdd <= (others => '0');                          -- roll over to 0
		end if;
	end if;
end process;

RdAddStateReg_i: process (clk, rst)
begin
    if rst = '1' then
        CSRdAdd <= (others => '0');
    elsif clk'event and clk = '1' then
		CSRdAdd <= NSRdAdd;
    end if;
end process;
DSPMemRdAdd     <= CSRdAdd;




-- generate memory write address, incrementing on every active clk edge when and after 
--  1. goDSPPulse is asserted for non clocked DSP components
--  2. validOut is asserted for clocked DSP components (ctrl1_1(0) asserted)
NSWrAddDecode: process (CSWrAdd, goDSPPulse, ctrl1_1(1), validOut)
begin
	NSWrAdd <= CSWrAdd; -- default
	-- increment count on every clock active edge from validOut assertion, count 0-1023 
	-- Counting stops after rolling over to 0, until next goDSPPulse assertion
	if ctrl1_1(1) = '0' then -- not FIR filter
	    if (goDSPPulse = '1') or (unsigned(CSWrAdd) > "000000000") then	
		   if (unsigned(CSWrAdd) <= "111111111") then
			   NSWrAdd <= std_logic_vector(unsigned(CSWrAdd) + 1) ; -- increment 
		   else  
			   NSWrAdd <= (others => '0');                          -- roll over to 0
		   end if;
        end if;
	else
	    if validOut = '1' then	
		   if (unsigned(CSWrAdd) <= "111111111") then
			   NSWrAdd <= std_logic_vector(unsigned(CSWrAdd) + 1) ; -- increment 
		   else  
			   NSWrAdd <= (others => '0');                          -- roll over to 0
		   end if;
        end if;
    end if;
end process;

WrAddStateReg_i: process (clk, rst)
begin
    if rst = '1' then
        CSWrAdd <= (others => '0');
    elsif clk'event and clk = '1' then
		CSWrAdd <= NSWrAdd;
    end if;
end process;
DSPMemWrAdd     <= CSWrAdd;


-- generate DSPDone signal
DSPDoneDecode_i: process(CSWrAdd)
begin
	DSPDone <= '0'; -- default
	if (unsigned(CSWrAdd) = "000000000") then
		DSPDone <= '1';
    end if;
end process;


-- generate memory write control signal 
DSPMemWrDecode_i: process(ctrl1_1, goDSPPulse, CSWrAdd, validOut)
begin
    DSPMemWr   <= '0'; 
	if ctrl1_1(1) = '0' then -- not FIR filter
		if (goDSPPulse = '1') or (unsigned(CSWrAdd) > "000000000") then
			DSPMemWr <= '1'; -- assert in each clock period
		end if;
	else                     -- FIR filter
		if validOut = '1' or (unsigned(CSWrAdd) > "000000000") then  
			DSPMemWr <= '1';                       
		end if;
	end if;
end process;


end RTL;