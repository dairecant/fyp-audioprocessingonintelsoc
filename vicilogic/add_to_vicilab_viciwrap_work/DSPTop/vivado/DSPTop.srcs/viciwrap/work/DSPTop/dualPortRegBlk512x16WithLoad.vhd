-- dualPortRegBlk512x16WithLoad
-- Synthesisable VHDL model for 512, writeable word (16-bit)-wide dual port register array, 
-- with load of all elements by either port 0 or port 1
-- Created :    Dec 2017. Fearghal Morgan
--
-- Supports two address/data/write/read ports (p0 and p1)
-- read
--   Port 0/1 can simultaneously read their addressed (p0Add/p1RdAdd) array data (on buses P0DatOut/P1DatOut respectively) 
--
-- write
--   If enPort0 is asserted,   
--      assertion of p0Load (has priority over p0Wr): all elements in the array are synchronously loaded with p0LoadDat
--      port 0 data write access to the memory array is active (writing data p0Dat to p0Add, on assertion of p0Wr)
--   If enPort0 is deasserted,   
--      assertion of p1Load (has priority over p1Wr): all elements in the array are synchronously loaded with p1LoadDat
--      port 1 data write access to the memory array is active (writing data p1Dat to p1WrAdd, on assertion of p1Wr)

-- Signal data dictionary
-- clk			      System clock strobe, rising edge active
-- rst			      System reset, assertion (high) clears all registers
-- enPort0			  Assertion (H) enables memory port 0 load (on assertion of p0Load), or writes (on assertion of p0Wr) 
--
-- p0Load 	 		  port 0 load enable. Assertion synchronously loads all memory array elements with p0LoadDat(15:0)
-- p0LoadDat(15:0)	  port 0 load data 
-- p0Add(7:0)  		  port 0 memory array address 
-- p0DatIn(15:0) 	  port 0 input data 
-- p0Wr 			  Assertion (H) enables synchronous write of addressed port 1 memory
-- P0DatOut(15:0)     port 0 data at p0Add
--
-- p1Load 	 		  port 1 load enable. Assertion synchronously loads all memory array elements with p1LoadDat(15:0)
-- p1LoadDat(15:0)	  port 1 load data 
-- p1RdAdd(7:0)       port 1 memory rd array address 
-- p1WrAdd(7:0)       port 1 memory wr array address 
-- p1DatIn(15:0) 	  port 1 input data 
-- p1Wr 			  Assertion (H) enables synchronous write of addressed port 1 memory
-- P1DatOut(15:0)     port 1 data at p1RdAdd

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.arrayPackage.all; 

entity dualPortRegBlk512x16WithLoad is
 Port (  clk		: in  std_logic;    					 
		 rst		: in  std_logic;    					 
         enPort0    : in  std_logic;

		 p0Load	    : in  std_logic;   
		 p0LoadDat  : in  std_logic_vector(15 downto 0);                      
		 p0Add 	 	: in  std_logic_vector( 8 downto 0);   
		 p0DatIn  	: in  std_logic_vector(15 downto 0);                      
		 p0Wr 	    : in  std_logic;   
	     p0DatOut   : out std_logic_vector(15 downto 0);                      

		 p1Load	    : in  std_logic;   
		 p1LoadDat  : in  std_logic_vector(15 downto 0);                      
		 p1RdAdd 	 	: in  std_logic_vector( 8 downto 0);   
		 p1WrAdd 	 	: in  std_logic_vector( 8 downto 0);   
		 p1DatIn  	: in  std_logic_vector(15 downto 0);                      
		 p1Wr 	    : in  std_logic;   
	     p1DatOut   : out std_logic_vector(15 downto 0)                     
 		 );         
end dualPortRegBlk512x16WithLoad;

architecture RTL of dualPortRegBlk512x16WithLoad is
signal XX_NS   : array512x16; -- next state
signal XX_CS   : array512x16; -- current state 
signal XX_load    : std_logic;   
signal XX_loadDat : std_logic_vector(15 downto 0);                      
signal XX_datIn   : std_logic_vector(15 downto 0);                      
signal XX_add     : std_logic_vector( 8 downto 0);                      
signal XX_wr      : std_logic;   

begin

genwrAddAndDat: process (enPort0,   p0Load, p0LoadDat, p0Add, p0DatIn, p0Wr,     p1Load, p1LoadDat, p1WrAdd, p1DatIn, p1Wr)
begin
  XX_load    <= p1Load; -- defaults to port 1
  XX_loadDat <= p1LoadDat; 
  XX_add     <= p1WrAdd; 
  XX_datIn   <= p1DatIn; 
  XX_wr      <= p1Wr; 
  if enPort0 = '1' then
    XX_load      <= p0Load;     
	XX_loadDat   <= p0LoadDat; 
    XX_add       <= p0Add; 
    XX_datIn     <= p0DatIn; 
    XX_wr        <= p0Wr; 
  end if;
end process;


XX_NSDecode_i: process(XX_CS, XX_wr, XX_add, XX_datIn) 
begin
  XX_NS    <= XX_CS;                            -- default
  if XX_wr = '1' then
     XX_NS(TO_INTEGER(unsigned(XX_add))) <= XX_datIn; -- write addressed element (convert vector to integers, as index)
  end if;
end process;

stateReg_i: process(clk, rst) 
begin
 if rst = '1' then
    XX_CS <= (others => (others => '0')); 
 elsif clk'event and clk = '1' then
   if XX_load = '1' then 
     XX_CS <= (others => XX_loadDat); -- load array
   else
    XX_CS <= XX_NS; 
   end if;
 end if;
end process;


p0DatOut_i: p0DatOut <= XX_CS(TO_INTEGER(unsigned(p0Add))); -- output the addressed port 0 element
p1DatOut_i: p1DatOut <= XX_CS(TO_INTEGER(unsigned(p1RdAdd))); -- output the addressed port 1 element

end RTL;