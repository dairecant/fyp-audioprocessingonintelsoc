-- FIR Filter 16x16
-- Created by Fearghal Morgan
-- 21st Mar 2018

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.FIRFilterPackage.all; 

entity FIRFilterTop is
port (clk         : in  std_logic;
	  reset       : in  std_logic;
	  enb         : in  std_logic;
	  dataIn       : in  std_logic_vector(15 downto 0); 
	  dataOut      : out std_logic_vector(15 downto 0);
	  validOut    : out  std_logic;
	  
	  -- host interface to/from ctrlRegBlk memory 
      ld0 	      : in  std_logic; 
	  ctrlRegAdd  : in  std_logic_vector(3 downto 0);  -- register address
      enCtrlRegWr : in  std_logic;                     -- register write enable, asserted high
	  ctrlRegIn   : in  std_logic_vector(15 downto 0); -- data byte (to be written)
	  ctrlRegOut  : out std_logic_vector(15 downto 0) -- addressed ctrlReg data 
	  );
end FIRFilterTop;

architecture struct of FIRFilterTop is 
signal coeff      : array16x16;             -- ctrlReg data array. 16x16-bit coefficients

component ctrlRegBlk16x16 is
 Port (  clk :             in  std_logic;                     -- system clock strobe, low-to-high active edge
		 ld0 :             in  std_logic;    				  -- synchronously load 0s in array
		 ctrlRegAdd :      in  std_logic_vector(3 downto 0);  -- register address
		 enCtrlRegWr:      in  std_logic;                     -- register write enable, asserted high
		 ctrlRegIn :       in  std_logic_vector(15 downto 0); -- data byte (to be written)
		 ctrlRegOut :      out std_logic_vector(15 downto 0); -- addressed ctrlReg data 
         ctrlReg        :  out array16x16	 				  -- ctrlReg data array
 		 );
end component;

component FIRFilter is
port (clk      : in  std_logic;
	  reset    : in  std_logic;
	  enb      : in  std_logic;
	  dataIn    : in  std_logic_vector(15 downto 0); 
	  dataOut   : out std_logic_vector(15 downto 0);
	  c        : in  array16x16 -- coefficients
	 );
end component;

begin

validOut <= '0';

FIRFilter_i: FIRFilter
port map (clk       => clk,
	      reset     => reset,
	      enb       => enb,
	      dataIn     => dataIn,
	      dataOut    => dataOut,
	      c         => coeff
	 );

ctrlRegBlk16x16_i: ctrlRegBlk16x16 
 Port map(clk			 => clk,
		 ld0 			 => ld0,
		 ctrlRegAdd      => ctrlRegAdd,
		 enCtrlRegWr     => enCtrlRegWr,
		 ctrlRegIn       => ctrlRegIn,
		 ctrlRegOut      => ctrlRegOut,
     	 ctrlReg         => coeff 
 		 );              
		 
end struct;