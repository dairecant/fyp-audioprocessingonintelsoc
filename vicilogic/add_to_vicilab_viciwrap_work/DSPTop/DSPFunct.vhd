-- DSPFunct
-- Selects various DSP configurations

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

entity DSPFunct is
port  (clk :          in std_logic; 
       rst :          in std_logic; 
       selFirstEffect:in std_logic_vector(2 downto 0);  
	   selFinalEffect:in std_logic_vector(1 downto 0);  
	   ctrl0_0:       in std_logic_vector(15 downto 0);  
	   ctrl0_1:       in std_logic_vector(15 downto 0);  
	   ctrl1_0:       in std_logic_vector(15 downto 0);  
	   ctrl1_1:       in std_logic_vector(15 downto 0);  
       validOut:      out std_logic;  
	   ctrl2_0:       in std_logic_vector(15 downto 0);  
	   ctrl2_1:       in std_logic_vector(15 downto 0);  
	   ctrl3_0:       in std_logic_vector(15 downto 0);  
	   ctrl3_1:       in std_logic_vector(15 downto 0);  
       DSPDatFromMem: in std_logic_vector(15 downto 0);  
       DSPDatToMem:   out std_logic_vector(15 downto 0);
       
       	  -- host interface to/from ctrlRegBlk memory 
       ld0           : in  std_logic; 
       ctrlRegAdd  : in  std_logic_vector(3 downto 0);  -- register address
       enCtrlRegWr : in  std_logic;                     -- register write enable, asserted high
       ctrlRegIn   : in  std_logic_vector(15 downto 0); -- data byte (to be written)
       ctrlRegOut  : out std_logic_vector(15 downto 0) -- addressed ctrlReg data 
	   );
end entity;

architecture struct of DSPFunct is

--component FIRFilter_ip_src_Discrete_FIR_Filter_HDL_Optimized IS
--  PORT( clk                               :   IN    std_logic;
--        reset                             :   IN    std_logic;
--        enb                               :   IN    std_logic;
--        dataIn                            :   IN    std_logic_vector(15 DOWNTO 0); 
--        dataOut                           :   OUT   std_logic_vector(15 DOWNTO 0);   
--        validOut                          :   OUT   std_logic
--        );
--end component;
component distortion_src_MATLAB_Function IS
  PORT( sample                            :   IN    std_logic_vector(15 DOWNTO 0);  -- int16
        clipVal                           :   IN    std_logic_vector(15 DOWNTO 0);  -- int16
        y                                 :   OUT   std_logic_vector(15 DOWNTO 0)  -- int16
       );
END component;
component Tremelo_src_Tremelo IS
  PORT( clk                               :   IN    std_logic;
        reset                             :   IN    std_logic;
        clk_enable                        :   IN    std_logic;
        sound                             :   IN    std_logic_vector(15 DOWNTO 0);  -- int16
        freq                              :   IN    std_logic_vector(15 DOWNTO 0);  -- int16
        ce_out                            :   OUT   std_logic;
        soundOut                          :   OUT   std_logic_vector(15 DOWNTO 0)  -- int16
        );
END component;

component FIRFilterTop is
port (clk         : in  std_logic;
	  reset       : in  std_logic;
	  enb         : in  std_logic;
	  dataIn       : in  std_logic_vector(15 downto 0); 
	  dataOut      : out std_logic_vector(15 downto 0);
	  validOut    : out  std_logic;
	  
	  -- host interface to/from ctrlRegBlk memory 
      ld0 	      : in  std_logic; 
	  ctrlRegAdd  : in  std_logic_vector(3 downto 0);  -- register address
      enCtrlRegWr : in  std_logic;                     -- register write enable, asserted high
	  ctrlRegIn   : in  std_logic_vector(15 downto 0); -- data byte (to be written)
	  ctrlRegOut  : out std_logic_vector(15 downto 0) -- addressed ctrlReg data 
	  );
end component;
signal ce_out_sig : std_logic;
signal datIn0 : std_logic_vector(15 downto 0);
signal datIn1 : std_logic_vector(15 downto 0) ;
signal datIn2 : std_logic_vector(15 downto 0) ;
signal datOut0 : std_logic_vector(15 downto 0) ; 
signal datOut1 : std_logic_vector(15 downto 0)  ;
signal datOut2 : std_logic_vector(15 downto 0)  ;

begin

datIn0 <= DSPDatFromMem;
     Tremelo0_i: Tremelo_src_Tremelo 
    PORT MAP( clk  => clk,
             reset  => rst,
             clk_enable => '1',
             sound => datIn0,-- int16
             freq  => ctrl0_0, -- int16
             ce_out => ce_out_sig,
             soundOut => datOut0              
             );		
datIn1 <= datOut0 when selFirstEffect(0) = '1' else DSPDatFromMem;

FIRFilterTop1_i:  FIRFilterTop
 PORT MAP( clk      => clk,
           reset    => rst,
           enb      => ctrl1_1(0),
		   dataIn   => datIn1, 
           dataOut  => datOut1, 
		   validOut => validOut,
		   	  -- host interface to/from ctrlRegBlk memory 
           ld0        => ld0, 
           ctrlRegAdd => ctrlRegAdd,
           enCtrlRegWr => enCtrlRegWr,
           ctrlRegIn => ctrlRegIn, 
           ctrlRegOut => ctrlRegOut  
     );	 
datIn2 <= DSPDatFromMem when selFirstEffect(2) = '1' 
            else datOut1 when selFirstEffect(1) = '1'
            else datOut0 when (selFirstEffect(0) = '1') and (selFirstEffect(1) = '0') ; 

     distortion2_i: distortion_src_MATLAB_Function 
       PORT MAP( sample => datIn2,
             clipVal => ctrl2_0,
             y   => datOut2
            );
 
 DSPDatToMem <= datOut0 when selFinalEffect = "01"
    else datOut1 when selFinalEffect = "10"
    else  datOut2 when selFinalEffect = "11"
    else datIn0;
 
 		 		 

--     effect0_i:  FIRFilter_ip_src_Discrete_FIR_Filter_HDL_Optimized
--      PORT MAP( clk      => clk,
--                reset    => rst,
--                enb      => ctrl1_1(1),
--                dataIn   => DSPDatFromMem, 
--                dataOut  => DSPDatToMem, 
--                validOut => validOut
--          );     

end struct;