# Copyright @ 2013-2017, National University of Ireland, Galway (NUI Galway).
# All Rights Reserved.
# No part of this software may be reproduced, stored or transmitted in any form, electronic or otherwise
# without the prior written permission of NUI Galway.
# Contact: Dr Fearghal Morgan, Room 3042, Engineering Building, National University of Ireland Galway, Galway, Ireland
# Email: fearghal.morgan@nuigalway.ie

import Queue
import binascii
import logging
import os
import threading
import time
import pyaudio
import wave
import struct
import cv2
import numpy
from PySide import QtGui, QtCore
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
import matplotlib.animation as animation
from matplotlib.figure import Figure
from matplotlib.animation import TimedAnimation
from matplotlib.lines import Line2D
import numpy as np
import pyqtgraph as pg
import random
import datetime
import sys
from PySide import QtGui
from PySide import QtCore
import functools
import numpy as np
import random as rd
import matplotlib
matplotlib.use("Qt4Agg")
import time
import threading
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas

import effectsResource
from ui.AudioDSP_LargeLayout import Ui_FilterAudio_Large
RATE = 8000
CHUNK = 512
CHANNELS = 1
FORMAT = pyaudio.paInt16

LOG = logging.getLogger(__name__)
addresses = range(CHUNK)
for i in range(len(addresses)):
    addresses[i] = hex(addresses[i])[2:]

class FPGACommands:
    # 1024 addresses, 2 bytes per address
    MEM_SIZE_BYTES = 1024 * 2

    def __init__(self, parent):
        self.parent = parent

    def send_commands(self, commands, block=False):
        return self.parent.send_commands(commands, block=block)

    def send_command(self, command, block=False):
        return self.parent.send_command(command, block=block)

    def write_audio_memory(self, frame):
        if len(frame) * 2 > self.MEM_SIZE_BYTES:
            LOG.error("Frame is too large to be saved in the memory")
            return
        #print(frame) ###array for matlab testing####
       # frame = np.array([-1,0,0,0,0,0,0,0,0,0,-1,0,0,0,0,1,-1,0,0,0,0,0,0,0,0,0,-1,0,0,0,0,0,0,1,0,0,0,1,0,1,0,0,1,0,0,0,0,0,0,0,0,
	# 				-1,0,0,0,0,0,-1,0,0,-1,0,0,0,0,1,1,0,0,0,-1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,-1,0,0,0,0,-1,0,0,1,0,0,-1,0,0,0,0,1,1,0,-1,0,0,-1,0,
	# 				-1,-1,0,-1,0,0,0,0,0,1,1,0,0,0,0,0,1,0,0,0,0,1,0,0,0,0,-1,0,-1,0,1,-1,0,0,0,-1,0,1,1,0,0,0,0,0,0,0,0,-1,0,0,0,0,1,0,0,0,0,-1,
	#				0,0,0,0,0,1,0,1,0,-1,0,1,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,1,0,-1,0,0,0,0,0,0,0,-1,0,0,0,-1,0,-1,-1,0,0,0,1,0,0,0,0,0,1,0,0,1,0,
	#				0,0,0,1,0,0,-1,-1,0,0,1,0,0,0,-1,1,0,-1,0,0,0,0,-1,0,-1,0,1,0,0,0,0,0,1,0,0,0,1,0,0,-1,0,-1,0,0,-1,-1,0,1,0,0,0,0,0,0,0,0,0,-1,
	#				1,0,1,-2,1,-2,1,0,-1,2,-4,7,-10,16,-23,0,-71,16,-145,-29,-378,-198,-985,1229,3310,2106,2064,1310,1170,617,473,479,309,103,-9,-81,
	#				-124,-441,-414,-604,-764,-517,-519,-692,-548,-760,-796,-694,-816,-622,-574,-585,-516,-533,-549,-410,-492,-348,-213,-239,-303,-249,
	#				-216,-45,-270,34,541,261,320,269,193,397,490,225,215,183,222,280,171,141,355,234,191,139,154,330,241,281,295,272,253,109,197,321,
	#				311,248,163,291,306,147,96,169,111,118,67,-147,-95,-69,-148,10,-12,-80,-89,-2,-135,-49,-9,-140,-82,-79,72,76,-94,-117,-139,-139,14,
	#				48,51,21,49,25,-46,-27,38,-17,-7,-18,-10,117,47,76,69,8,-155,-115,-113,-63,14,-136,-4,-11,40,-16,6,21,-8,20,57,-38,-79,-19,-83,-17,
	#				-47,-18,-119,-208,-54,39,-5,13,-40,-110,1,67,21,4,29,-29,-32,-5,-23,-10,19,-85,20,132,99,67,77,-58,-28,104,-11,-28,-34,-19,-91,-85,8,
	#				46,-29,-80,-60,13,-23,-36,3,-94,-65,64,40,105,-43,-49,45,27,48,-14,-66,-48,-11,13,84,0])
    #    frame = frame.astype(np.int16)
    #    print("raw frame")
    #    print(frame)
        data = binascii.hexlify(frame.tobytes())
       # print("Int16 Frame")
        #print((frame))
        #print ("hex frame")
       # print(data)
        write_data = []
        for i in range(0, len(data), 4):
            write_data.append(data[i:i+4])

        addresses = range(len(write_data))
        for i in range(len(addresses)):
            addresses[i] = hex(addresses[i])[2:]
        self.ctrl_write_many_loop(addresses, write_data)

    def read_audio_memory(self,filterBegun):
        read_data = self.ctrl_read_many_loop(addresses)

        for i in range(len(read_data)):
            read_data[i] = read_data[i].zfill(4)
       # print("raw data")
       # print(read_data)
        return binascii.unhexlify(''.join(read_data))
       # print("unhex data")



    def ctrl_write_many_loop(self, addresses,  data):
	
		commands = [{'cmd':'write', 'var_name': 'host_EnDSPMem', 'data':'1'},				
					{'cmd':'write', 'var_name': 'host_DSPMem_Wr', 'data':'1'},
					{'cmd':'write', 'var_name':'userclk', 'data':'1'},					
					{'cmd':'loop', 'loop_count':len(addresses), 'loop_num_ops':3}, #loop 3 operations 512 times - length of addresses
					{'cmd':'write', 'var_name':'host_DSPMem_Add', 'data': addresses}, #writes frame
					{'cmd':'write', 'var_name':'host_DSPMem_DatIn', 'data':data},
					{'cmd':'write', 'var_name':'userclk', 'data':'1'},
					{'cmd':'write', 'var_name': 'host_DSPMem_Wr', 'data':'0'}, #after loop
					{'cmd':'write', 'var_name': 'host_EnDSPMem', 'data':'0'},	#disable host access
					{'cmd':'write', 'var_name': 'goDSP', 'data':'1'}, #start processing
					{'cmd':'loop', 'loop_count':(len(addresses)), 'loop_num_ops':1}, 
					{'cmd':'write', 'var_name':'userclk', 'data':'1'}] #512 clock steps loop runs through each address, processes and 

		self.send_commands(commands, block=False)
		return True
        

    def ctrl_read_many_loop(self, addresses):

        commands = [{'cmd':'write', 'var_name': 'goDSP', 'data':'0'},#disables processing, read already enabled
		            {'cmd':'loop', 'loop_count':len(addresses), 'loop_num_ops':2}, #asynchronous, once address input change output changes
                    {'cmd':'write', 'var_name':'host_DSPMem_Add', 'data': addresses},
                    {'cmd':'read', 'var_name':'host_DSPMem_DatOut'}]

        resp = self.send_commands(commands, block=True)
        return resp[0]


class FilterAudio_Large(Ui_FilterAudio_Large, FPGACommands):

    def __init__(self, parent=None, config=None): ##Main
		Ui_FilterAudio_Large.__init__(self)
		FPGACommands.__init__(self, parent)
		self.microphone_in_processing = False
		self.PROCESSOR_ENABLED = False
		self.processing = False
		self.distort = False
		self.trem = False
		self.delay = False
		self.filter = False
		self.filterSwitchCount = 0
		self.filterBegun =False
		self.widget = QtGui.QWidget()
		self.widget.object = self
		self.setup(self.widget)

    def setup(self, w):
        Ui_FilterAudio_Large.setupUi(self, w)
        #pic = QtGui.QPixmap("logo.png")
        #self.logoLabel.setPixmap(pic)
        self.graphWidget = CustomFigCanvas()
        self.gridLayout_2.addWidget(self.graphWidget)
        self.startStop.clicked.connect(self.start_stop)
	#	setting up effects dials and switches
        self.tremFreqDial.setRange(0,16000) #min frequency 0.5 Hz
        self.tremFreqDial.setValue(4000)	
        self.tremFreqDial.setInvertedControls(True)
        self.tremFreqDial.valueChanged.connect(self.tremFreq)	#goes from slow to fast 		
        self.tremButton.clicked.connect(self.enableTremelo)	
		
        self.ThresholdDistortionDial.setRange(5000,32767)
        self.ThresholdDistortionDial.setInvertedControls(True) # lower dial = cleaner / higher = more distorted
        self.ThresholdDistortionDial.setValue(20000)	# NOTE : zero value pretty much inaudible may change range
        self.ThresholdDistortionDial.valueChanged.connect(self.distortionThresh)	 		
        self.distortButton.clicked.connect(self.enableDistortion)
        self.filterMixDial.setRange(0,32767)
        self.filterMixDial.setValue(32767)	
        self.filterMixDial.valueChanged.connect(self.filterMix)	
        self.filterButton.clicked.connect(self.enableFilter)   
        self.filterMixDial.setEnabled(False)
        self.filterButton.setEnabled(False)		
        self.tremButton.setEnabled(True)
        self.distortButton.setEnabled(True)
        self.filterButton.setEnabled(False)
	

    def filterMix(self):
        print("Fiter Mix Value: ")
        val = self.filterMixDial.value()
        val = np.int16(val)
        print(val)
        data =  hex(val)
        commands = [{'cmd':'write', 'var_name': 'ctrl1_0', 'data':data}]
        self.send_commands(commands, block = False )

    def distortionThresh(self):
        print("Distortion Threshold Value: ")
        val = self.ThresholdDistortionDial.value()
        val = np.int16(val)
        print(val)       
        data = hex(val)
        commands = [{'cmd':'write', 'var_name': 'ctrl2_0', 'data':data}]
        self.send_commands(commands, block = False )
		#INSERT CODE TO SEND SIGNAL TO FPGA
		
    def tremFreq(self):
        print("Tremolo Frequency Value: ")
        val = self.tremFreqDial.value()
        val = np.int16(val)
        print(val)
        data =  hex(val)
        commands = [{'cmd':'write', 'var_name': 'ctrl0_0', 'data':data}]
        self.send_commands(commands, block = False )
		

    def microphone(self):

        def get_frame(streamIn):
			frame = streamIn.read(CHUNK) #read frame - read as string
			frame = numpy.fromstring(frame, numpy.int16) #convert to 16 bit signed integers
			return frame
        self.frame = np.zeros((CHUNK,), dtype=np.int16)
        frame_times = 0
        moving_average_fps = 0
        count = 0
        audio = pyaudio.PyAudio() #initialise py audio class
		#setting up stream
        stream = audio.open(format=FORMAT, channels=CHANNELS,rate=RATE, input=True,output=True,frames_per_buffer=CHUNK)

        while 1:
                self.microphone_in_processing = True
                start_getting_audio_chunk = time.time()
                self.frame = get_frame(stream)
                if self.frame is not None:
                    if not self.processing:
                        self.stat_label.setText("")  # Remove statistics text
                    else:
                     if self.processing:
                        end_getting_audio_chunk = time.time()
                        start_send_time = time.time()
                        self.write_audio_memory(self.frame) ## writes to fpga
                        end_send_time = time.time() #end_send_time-start_send_time = time to send 
                        

                        start_read_time = time.time()
                        ret_audio = self.read_audio_memory(self.filterBegun)
                        end_read_time = time.time()
                        if ret_audio:
                            self.frameInt = numpy.fromstring(ret_audio, numpy.int16)
                            if (self.checkBox.checkState() != 0): #if enable plotting ticked, paint graph
                                self.graphWidget.plotData(self.frameInt)
                            start_displaying_frame_time = time.time()
                            stream.write(ret_audio)
                            end_displaying_frame_time = time.time()

                            # Calculate times
                            getting_audio_frame_time = (end_getting_audio_chunk - start_getting_audio_chunk)*1000
                            frame_send_time = (end_send_time - start_send_time)*1000
                            frame_read_time = (end_read_time - start_read_time)*1000
                            displaying_frame_time = (end_displaying_frame_time - start_displaying_frame_time)*1000
                            total_time = getting_audio_frame_time + frame_send_time + frame_read_time + displaying_frame_time

                            # Calculate the FPS
                            fps = 1000/total_time
                            text = "{:0.1f} FPS\n".format(fps)

                           #  Calculate the processing time
                            text += "Audio Source: Microphone\n"
                            text += "Frame Size: {:0.0f} 16 bit samples".format(CHUNK)
                            text += "\nTotal processing time: {:0.0f}ms\n".format(total_time)
                            text += " - Getting audio frame: {:0.0f}ms ({:0.0f}%)\n".format(getting_audio_frame_time, getting_audio_frame_time/total_time*100)
                            text += " - Frame send: {:0.0f}ms ({:0.0f}%)\n".format(frame_send_time, frame_send_time/total_time*100)
                            text += " - Frame read: {:0.0f}ms ({:0.0f}%)\n".format(frame_read_time, frame_read_time/total_time*100)
                            text += " - Playing received audio frame: {:0.0f}ms ({:0.0f}%)".format(displaying_frame_time, displaying_frame_time/total_time*100)
                        
                            # Display the information
                            self.stat_label.setText(text)
                        #break ##used for MATLAB single frame testing	
                self.microphone_in_processing = False

				


    def start_stop(self):
        self.processing = not self.processing
#starts audio stream
        if self.processing:
            self.startStop.setText("Stop Processing")

        else:
            self.startStop.setText("Start Processing")

    def startProcessing(self): #used for enable processing button -this has since been removed
        self.PROCESSOR_ENABLED = not self.PROCESSOR_ENABLED 

#OLD Method, was useed to display effects combination diagrams on application window
    def setDiagram(self):
#creates a binary string depending on what effects are active, this string appended to '.png'
        if self.trem:
            picture = '1'
        else:
            picture = '0'
        if self.filter:
            picture += '1'
        else:
            picture += '0'
        if self.distort:
            picture += '1'
        else:
            picture += '0'
        print(picture)
#in pictures within the folder there is diagram corresponding to every binary combination - i.e. every combination in the chain
        translateText = 	"<html><head/><body><p><img src=\":/newPrefix/"
        translateText += picture 
        translateText +=  ".png\"/></p></body></html>"
        print(translateText)

##EFFECTS BUTTONS ######
    def enableDistortion(self):
        self.distort = not self.distort #toggles switch
        if self.distort: #if on
            commands =[{'cmd':'write', 'var_name': 'selFinalEffect', 'data':'3'}] #last effect in chain so final effect will always be distortion
            self.distortButton.setText("Turn off Distortion")
      #      self.setDiagram()
            if self.filter: #setting first effect
                commands += [{'cmd':'write', 'var_name': 'selFirstEffect', 'data':'2'}]
            elif (self.trem):
			    commands += [{'cmd':'write', 'var_name': 'selFirstEffect', 'data':'001'}]
            elif (self.filter) and (self.trem):
			    commands += [{'cmd':'write', 'var_name': 'selFirstEffect', 'data':'3'}] #all effects active
            else:	
			    commands += [{'cmd':'write', 'var_name': 'selFirstEffect', 'data':'4'}]			
            
            self.send_commands(commands, block = False )
        else: #options for when turning distortion off
            self.distortButton.setText("Distortion")
		#check if other effects are active , change switch signals accordingly
            if (self.filter) and (not self.trem):
                commands = [{'cmd':'write', 'var_name': 'selFirstEffect', 'data':'2'}]
                commands +={'cmd':'write', 'var_name': 'selFinalEffect', 'data':'2'} 
				
            elif (self.filter) and (self.trem):
                commands = [{'cmd':'write', 'var_name': 'selFirstEffect', 'data':'001'}]
                commands +={'cmd':'write', 'var_name': 'selFinalEffect', 'data':'2'} 			
            elif (self.trem) and (not self.filter):
                commands = [{'cmd':'write', 'var_name': 'selFirstEffect', 'data':'001'}]
                commands +={'cmd':'write', 'var_name': 'selFinalEffect', 'data':'01'} 

            elif (not self.filter) and (not self.trem) and (not self.distort):
                self.startProcessing()	
                commands = [{'cmd':'write', 'var_name': 'selFirstEffect', 'data':'000'},
                           {'cmd':'write', 'var_name': 'selFinalEffect', 'data':'00'}] 
		
            self.send_commands(commands, block = False )

    def enableFilter(self):
        self.filter = not self.filter
        if self.filter:
           self.filterButton.setText("Turn off Filter")
		   #OLD LOGIC FOR PREVIOUS FILTER
           #self.filterSwitchCount += 1
		   
           #if self.filterSwitchCount == 1:
            #   self.filterBegun = True
            #   print("Filter count = 1")
          # else:
           #    self.filterBegun = False
     #      self.setDiagram()
           commands =[{'cmd':'write', 'var_name':'ctrl1_1', 'data':'0001'}]
           if self.distort:
                commands += [{'cmd':'write', 'var_name': 'selFinalEffect', 'data':'3'}]
           elif (self.trem):
                commands += [{'cmd':'write', 'var_name': 'selFirstEffect', 'data':'001'}]
           elif (self.filter) and (self.trem):
                commands += [{'cmd':'write', 'var_name': 'selFirstEffect', 'data':'3'}]
                commands += [{'cmd':'write', 'var_name': 'selFinalEffect', 'data':'2'}]
           else:	
                commands += [{'cmd':'write', 'var_name': 'selFirstEffect', 'data':'2'},
                             {'cmd':'write', 'var_name': 'selFinalEffect', 'data':'2'}]

           self.send_commands(commands, block = False )
        else:
           commands = [{'cmd':'write', 'var_name': 'ctrl1_1', 'data':'0000'}]
           self.send_commands(commands, block = False )  
           self.filterButton.setText("Filter")
           if self.trem:
                commands = [{'cmd':'write', 'var_name': 'selFirstEffect', 'data':'001'}]
                if self.distort:
                    commands = [{'cmd':'write', 'var_name': 'selFinalEffect', 'data':'3'}]
                else:
                    commands = [{'cmd':'write', 'var_name': 'selFinalEffect', 'data':'01'}]
           else:
                commands = [{'cmd':'write', 'var_name': 'selFirstEffect', 'data':'4'},
                            {'cmd':'write', 'var_name': 'selFinalEffect', 'data':'3'}]                
           if (not self.filter) and (not self.trem) and (not self.distort):
                self.startProcessing()	
                commands = [{'cmd':'write', 'var_name': 'selFirstEffect', 'data':'000'},
                           {'cmd':'write', 'var_name': 'selFinalEffect', 'data':'00'}] 		
		   
           self.send_commands(commands, block = False )



    def enableTremelo(self):
        self.trem = not self.trem
        commands = []
        if self.trem:
            self.tremButton.setText("Turn off Tremolo")
      #      self.setDiagram()
            commands = [{'cmd':'write', 'var_name': 'selFirstEffect', 'data':'001'}]
            if self.distort:
                commands += [{'cmd':'write', 'var_name': 'selFinalEffect', 'data':'3'}]
            elif self.filter:
                commands += [{'cmd':'write', 'var_name': 'selFirstEffect', 'data':'3'}]
            else:
			    commands += [{'cmd':'write', 'var_name': 'selFinalEffect', 'data':'01'}]
            self.send_commands(commands, block = False )
        else:
            self.tremButton.setText("Tremolo")
            if self.filter:
                commands = [{'cmd':'write', 'var_name': 'selFirstEffect', 'data':'2'}]
                if self.distort:
                    commands += [{'cmd':'write', 'var_name': 'selFinalEffect', 'data':'3'}]
                else:
                    commands += [{'cmd':'write', 'var_name': 'selFinalEffect', 'data':'2'}]                   
            elif self.distort:
                commands = [{'cmd':'write', 'var_name': 'selFirstEffect', 'data':'4'},
                           {'cmd':'write', 'var_name': 'selFinalEffect', 'data':'3'}]           
            elif (not self.filter) and (not self.trem) and (not self.distort):
                commands = [{'cmd':'write', 'var_name': 'selFirstEffect', 'data':'000'},
                           {'cmd':'write', 'var_name': 'selFinalEffect', 'data':'00'}] 
                self.startProcessing()

            self.send_commands(commands, block = False )
		
    @QtCore.Slot()
    def reset_text_results(self):
        self.selftest_results.setPlainText('')

    @QtCore.Slot(str)
    def append_text_results(self, result):
        self.selftest_results.append(result)

    def deserialize(self, data):
        pass

    def serialize(self):
        pass

    def bundle_data(self):
        return[]

    def unbundle_data(self, data):
        pass

										  


class RunnableFilterAudio_Large(FilterAudio_Large):

    """
    When the application is in runnable and is slow, this is not due to the code here.
    The command line widget (F3 on viciLab) is processing a lot of data and this is that processing which
    is responsible of the slow execution. Currently we cannot deactivate that function unless you are in
    possession of the source code of viciLab.
    """

    def __init__(self, *args, **kwargs):
        super(RunnableFilterAudio_Large, self).__init__(*args, **kwargs)

        self.processing = False

        self.init_design()

        # Sometimes the camera does not start, must restart the application to resolve that issue
        self.microphone_thread = threading.Thread(target=self.microphone)
        self.microphone_thread.daemon = True
        self.microphone_thread.start()

    def init_design(self):
        self.send_commands([{'cmd': 'write', 'var_name': 'userrst', 'data': '1'},
                            {'cmd': 'write', 'var_name': 'userrst', 'data': '0'}])

    def update_items(self, values):
        """
        values is a dictionary with the keys in the form:
        component.signal_name
        """
        pass


class EditableFilterAudio_Large(FilterAudio_Large):

    def __init__(self, *args, **kwargs):
        super(EditableFilterAudio_Large, self).__init__(*args, **kwargs)
        self.widget.setEnabled(False)



class CustomFigCanvas(FigureCanvas):
#based on stack overflow solution, link in thesis
    def __init__(self):

        self.addedData = []
        print(matplotlib.__version__)
        # The data
        self.xlim = CHUNK
        self.n = np.linspace(0, (self.xlim*1000)/RATE, self.xlim)

        # The window
        self.fig = Figure(facecolor='w', dpi=100)
        self.fig.set_figwidth(0.1)
        self.fig.set_figheight(0.1)
		
		
        self.ax1 = self.fig.add_subplot(111)
        self.ax1.set_title('Real Time Audio Plot')

        # self.ax1 settings
        self.ax1.set_xlabel('Time (ms)',fontsize=12)
        self.ax1.set_ylabel('Amplitude',fontsize=12)
        self.line1 = Line2D([], [], color='blue')
        self.line1_tail = Line2D([], [], color='red', linewidth=2)
        self.line1_head = Line2D([], [], color='red', marker='o', markeredgecolor='r')
        self.ax1.add_line(self.line1)
        self.ax1.add_line(self.line1_tail)
        self.ax1.add_line(self.line1_head)
        self.ax1.set_xlim(0, (self.xlim*1000)/RATE) #0-64ms
        self.ax1.set_ylim(-1,1)
        

        FigureCanvas.__init__(self, self.fig)
  #method for plotting single frames
    def plotData(self,data):
        data = data.astype(float)
        data = data/32768 # normalise amplitude 
        self.line1.set_data(self.n,data)
        self.fig.canvas.draw()
		
    	



