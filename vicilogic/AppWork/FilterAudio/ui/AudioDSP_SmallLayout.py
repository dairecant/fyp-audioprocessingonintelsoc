# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '../../../Users/viciLabDaireLiam/vicilab/viciclient/apps/FilterAudio/ui/AudioDSP_SmallLayout.ui'
#
# Created: Tue Apr 03 11:32:35 2018
#      by: pyside-uic 0.2.15 running on PySide 1.2.4
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_FilterAudio_Small(object):
    def setupUi(self, FilterAudio_Small):
        FilterAudio_Small.setObjectName("FilterAudio_Small")
        FilterAudio_Small.resize(419, 647)
        font = QtGui.QFont()
        font.setPointSize(8)
        FilterAudio_Small.setFont(font)
        self.horizontalLayout_3 = QtGui.QHBoxLayout(FilterAudio_Small)
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.tabWidget = QtGui.QTabWidget(FilterAudio_Small)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.tabWidget.sizePolicy().hasHeightForWidth())
        self.tabWidget.setSizePolicy(sizePolicy)
        self.tabWidget.setMinimumSize(QtCore.QSize(300, 300))
        self.tabWidget.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.tabWidget.setObjectName("tabWidget")
        self.processing_tab = QtGui.QWidget()
        self.processing_tab.setObjectName("processing_tab")
        self.verticalLayout = QtGui.QVBoxLayout(self.processing_tab)
        self.verticalLayout.setObjectName("verticalLayout")
        self.frame_1 = QtGui.QFrame(self.processing_tab)
        self.frame_1.setEnabled(True)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.frame_1.sizePolicy().hasHeightForWidth())
        self.frame_1.setSizePolicy(sizePolicy)
        self.frame_1.setMinimumSize(QtCore.QSize(377, 585))
        self.frame_1.setMaximumSize(QtCore.QSize(500, 16777215))
        self.frame_1.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame_1.setFrameShadow(QtGui.QFrame.Raised)
        self.frame_1.setObjectName("frame_1")
        self.verticalLayout_2 = QtGui.QVBoxLayout(self.frame_1)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.gridLayout = QtGui.QGridLayout()
        self.gridLayout.setObjectName("gridLayout")
        self.verticalLayout_5 = QtGui.QVBoxLayout()
        self.verticalLayout_5.setObjectName("verticalLayout_5")
        self.backgroundQWidget = QtGui.QWidget(self.frame_1)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.backgroundQWidget.sizePolicy().hasHeightForWidth())
        self.backgroundQWidget.setSizePolicy(sizePolicy)
        self.backgroundQWidget.setMinimumSize(QtCore.QSize(0, 100))
        self.backgroundQWidget.setObjectName("backgroundQWidget")
        self.verticalLayout_4 = QtGui.QVBoxLayout(self.backgroundQWidget)
        self.verticalLayout_4.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_4.setObjectName("verticalLayout_4")
        self.label_7 = QtGui.QLabel(self.backgroundQWidget)
        self.label_7.setObjectName("label_7")
        self.verticalLayout_4.addWidget(self.label_7)
        self.startStop = QtGui.QPushButton(self.backgroundQWidget)
        self.startStop.setMinimumSize(QtCore.QSize(0, 0))
        self.startStop.setObjectName("startStop")
        self.verticalLayout_4.addWidget(self.startStop)
        self.checkBox = QtGui.QCheckBox(self.backgroundQWidget)
        self.checkBox.setObjectName("checkBox")
        self.verticalLayout_4.addWidget(self.checkBox)
        self.horizontalLayout_4 = QtGui.QHBoxLayout()
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.verticalLayout_12 = QtGui.QVBoxLayout()
        self.verticalLayout_12.setObjectName("verticalLayout_12")
        self.tremButton = QtGui.QPushButton(self.backgroundQWidget)
        self.tremButton.setMinimumSize(QtCore.QSize(104, 0))
        self.tremButton.setObjectName("tremButton")
        self.verticalLayout_12.addWidget(self.tremButton)
        self.label_5 = QtGui.QLabel(self.backgroundQWidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_5.sizePolicy().hasHeightForWidth())
        self.label_5.setSizePolicy(sizePolicy)
        self.label_5.setMinimumSize(QtCore.QSize(0, 13))
        self.label_5.setObjectName("label_5")
        self.verticalLayout_12.addWidget(self.label_5)
        self.tremFreqDial = QtGui.QDial(self.backgroundQWidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.tremFreqDial.sizePolicy().hasHeightForWidth())
        self.tremFreqDial.setSizePolicy(sizePolicy)
        self.tremFreqDial.setMinimumSize(QtCore.QSize(20, 20))
        self.tremFreqDial.setObjectName("tremFreqDial")
        self.verticalLayout_12.addWidget(self.tremFreqDial)
        self.horizontalLayout_4.addLayout(self.verticalLayout_12)
        self.verticalLayout_7 = QtGui.QVBoxLayout()
        self.verticalLayout_7.setObjectName("verticalLayout_7")
        self.filterButton = QtGui.QPushButton(self.backgroundQWidget)
        self.filterButton.setMinimumSize(QtCore.QSize(104, 23))
        self.filterButton.setObjectName("filterButton")
        self.verticalLayout_7.addWidget(self.filterButton)
        self.label_3 = QtGui.QLabel(self.backgroundQWidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_3.sizePolicy().hasHeightForWidth())
        self.label_3.setSizePolicy(sizePolicy)
        self.label_3.setMinimumSize(QtCore.QSize(0, 13))
        self.label_3.setObjectName("label_3")
        self.verticalLayout_7.addWidget(self.label_3)
        self.filterMixDial = QtGui.QDial(self.backgroundQWidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.filterMixDial.sizePolicy().hasHeightForWidth())
        self.filterMixDial.setSizePolicy(sizePolicy)
        self.filterMixDial.setMinimumSize(QtCore.QSize(20, 20))
        self.filterMixDial.setObjectName("filterMixDial")
        self.verticalLayout_7.addWidget(self.filterMixDial)
        self.horizontalLayout_4.addLayout(self.verticalLayout_7)
        self.verticalLayout_3 = QtGui.QVBoxLayout()
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.distortButton = QtGui.QPushButton(self.backgroundQWidget)
        self.distortButton.setMinimumSize(QtCore.QSize(104, 23))
        self.distortButton.setObjectName("distortButton")
        self.verticalLayout_3.addWidget(self.distortButton)
        self.label_2 = QtGui.QLabel(self.backgroundQWidget)
        self.label_2.setObjectName("label_2")
        self.verticalLayout_3.addWidget(self.label_2)
        self.ThresholdDistortionDial = QtGui.QDial(self.backgroundQWidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.ThresholdDistortionDial.sizePolicy().hasHeightForWidth())
        self.ThresholdDistortionDial.setSizePolicy(sizePolicy)
        self.ThresholdDistortionDial.setMinimumSize(QtCore.QSize(20, 20))
        self.ThresholdDistortionDial.setObjectName("ThresholdDistortionDial")
        self.verticalLayout_3.addWidget(self.ThresholdDistortionDial)
        self.horizontalLayout_4.addLayout(self.verticalLayout_3)
        self.verticalLayout_4.addLayout(self.horizontalLayout_4)
        self.verticalLayout_5.addWidget(self.backgroundQWidget)
        self.gridLayout.addLayout(self.verticalLayout_5, 1, 0, 1, 1)
        self.verticalLayout_2.addLayout(self.gridLayout)
        self.verticalLayout.addWidget(self.frame_1)
        self.tabWidget.addTab(self.processing_tab, "")
        self.horizontalLayout_3.addWidget(self.tabWidget)

        self.retranslateUi(FilterAudio_Small)
        self.tabWidget.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(FilterAudio_Small)

    def retranslateUi(self, FilterAudio_Small):
        FilterAudio_Small.setWindowTitle(QtGui.QApplication.translate("FilterAudio_Small", "Form", None, QtGui.QApplication.UnicodeUTF8))
        self.label_7.setText(QtGui.QApplication.translate("FilterAudio_Small", "<html><head/><body><p align=\"center\"><img src=\":/newPrefix/logoSmall.PNG\"/></p><p align=\"center\"><span style=\" font-size:18pt; font-weight:600; vertical-align:sub;\">Audio DSP Processing</span></p></body></html>", None, QtGui.QApplication.UnicodeUTF8))
        self.startStop.setToolTip(QtGui.QApplication.translate("FilterAudio_Small", "<html><head/><body><p>Begins streaming process of audio to and from the FPGA in frames of 1024 samples.</p></body></html>", None, QtGui.QApplication.UnicodeUTF8))
        self.startStop.setText(QtGui.QApplication.translate("FilterAudio_Small", "Start Processing", None, QtGui.QApplication.UnicodeUTF8))
        self.checkBox.setText(QtGui.QApplication.translate("FilterAudio_Small", "Enable Plotting", None, QtGui.QApplication.UnicodeUTF8))
        self.tremButton.setToolTip(QtGui.QApplication.translate("FilterAudio_Small", "<html><head/><body><p>Acts as an amplitude modulator. Changes the amplitude between 0 and maximum proportional to a triangular sine wave of a specified frequency. More information : <a href=\"https://en.wikipedia.org/wiki/Tremolo_(electronic_effect)\"><span style=\" text-decoration: underline; color:#0000ff;\">https://en.wikipedia.org/wiki/Tremolo_(electronic_effect)</span></a></p></body></html>", None, QtGui.QApplication.UnicodeUTF8))
        self.tremButton.setText(QtGui.QApplication.translate("FilterAudio_Small", "Tremolo", None, QtGui.QApplication.UnicodeUTF8))
        self.label_5.setText(QtGui.QApplication.translate("FilterAudio_Small", "Frequency", None, QtGui.QApplication.UnicodeUTF8))
        self.tremFreqDial.setToolTip(QtGui.QApplication.translate("FilterAudio_Small", "<html><head/><body><p>Change frequency of tremelo.</p><p>Minimum frequency: 0.5Hz</p><p>Maximum frequency: 5Hz</p><p><br/></p></body></html>", None, QtGui.QApplication.UnicodeUTF8))
        self.filterButton.setToolTip(QtGui.QApplication.translate("FilterAudio_Small", "<html><head/><body><p>Acts as a low pass filter of cut-off frequency 500Hz with an order of 16. This blocks higher frequencies in the output audio, creating a muffled effect. </p><p>More information: <a href=\"https://en.wikipedia.org/wiki/Low-pass_filter\"><span style=\" text-decoration: underline; color:#0000ff;\">https://en.wikipedia.org/wiki/Low-pass_filter</span></a></p></body></html>", None, QtGui.QApplication.UnicodeUTF8))
        self.filterButton.setText(QtGui.QApplication.translate("FilterAudio_Small", "Filter", None, QtGui.QApplication.UnicodeUTF8))
        self.label_3.setText(QtGui.QApplication.translate("FilterAudio_Small", "Wet / Dry Mix", None, QtGui.QApplication.UnicodeUTF8))
        self.distortButton.setToolTip(QtGui.QApplication.translate("FilterAudio_Small", "<html><head/><body><p>Enables Distortion Effect. This effect acts as a clipper or limiter, limiting amplitude of the audio and creating a \'fuzzy\' effect.</p><p>More information: <a href=\"https://en.wikipedia.org/wiki/Distortion_(music)\"><span style=\" text-decoration: underline; color:#0000ff;\">https://en.wikipedia.org/wiki/Distortion_(music)</span></a></p></body></html>", None, QtGui.QApplication.UnicodeUTF8))
        self.distortButton.setWhatsThis(QtGui.QApplication.translate("FilterAudio_Small", "<html><head/><body><p><br/></p></body></html>", None, QtGui.QApplication.UnicodeUTF8))
        self.distortButton.setText(QtGui.QApplication.translate("FilterAudio_Small", "Distortion ", None, QtGui.QApplication.UnicodeUTF8))
        self.label_2.setText(QtGui.QApplication.translate("FilterAudio_Small", "Sensitivity", None, QtGui.QApplication.UnicodeUTF8))
        self.ThresholdDistortionDial.setToolTip(QtGui.QApplication.translate("FilterAudio_Small", "<html><head/><body><p>Modifies the threshold at which the distortion effect begins to clip.</p><p>Minimum: 0.2</p><p>Maximum 0.9</p><p><img src=\":/newPrefix/logoSmall.PNG\"/></p></body></html>", None, QtGui.QApplication.UnicodeUTF8))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.processing_tab), QtGui.QApplication.translate("FilterAudio_Small", "Processing", None, QtGui.QApplication.UnicodeUTF8))

import image_rc
import effectsResource
