/// @copyright  Copyright @ 2013-2017, National University of Ireland,
///             Galway (NUI Galway). All Rights Reserved.
///
/// @License  No part of this software may be reproduced, stored or transmitted
///           in any form, electronic or otherwise without the prior written
///           permission of NUI Galway.
///
/// Contact:  Dr Fearghal Morgan, Room 3042, Engineering Building,
///           National University of Ireland Galway,
///           Galway, Ireland
/// Email:    fearghal.morgan@nuigalway.ie
///
/// @brief    Types FPGA controller
/// @author   Arthur Vianes (arthur.vianes@gmail.com)
/// @date     July 2017

#ifdef __cplusplus // In case of C++ compilater
	extern "C" {   // C++ export
#endif

#ifndef H__FPGA_TYPES
#define H__FPGA_TYPES

	#include <assert.h>
	#include <stdint.h>
	#include <stdbool.h>
	#include <sys/types.h>

	typedef int_fast16_t           bit_addr_t;
	typedef union value32bit       value32bit_t;

	typedef uint32_t               word_t;
	typedef struct _32_as_halfword halfword_t;
	typedef struct _32_as_byte     byte_t;
	typedef struct _32_as_halfbyte halfbyte_t;
	typedef struct _32_as_bit      bit_t;


	struct _32_as_halfword {
		uint16_t at_0:16, at_1:16;
	};

	struct _32_as_byte {
		uint8_t at_0:8, at_1:8, at_2:8, at_3:8;
	};

	struct _32_as_halfbyte {
		uint8_t at_0:4, at_1:4, // Byte 0
		        at_2:4, at_3:4, // Byte 1
		        at_4:4, at_5:4, // Byte 2
		        at_6:4, at_7:4; // Byte 3
	};

	struct _32_as_bit {
		uint8_t at_00:1, at_01:1, at_02:1, at_03:1,
		        at_04:1, at_05:1, at_06:1, at_07:1, // Byte 0
		        at_08:1, at_09:1, at_10:1, at_11:1,
		        at_12:1, at_13:1, at_14:1, at_15:1, // Byte 1
		        at_16:1, at_17:1, at_18:1, at_19:1,
		        at_20:1, at_21:1, at_22:1, at_23:1, // Byte 2
		        at_24:1, at_25:1, at_26:1, at_27:1,
		        at_28:1, at_29:1, at_30:1, at_31:1; // Byte 3
	};

	union value32bit {
		word_t     as_word;
		halfword_t as_halfword;
		byte_t     as_byte;
		halfbyte_t as_halfbyte;
		bit_t      as_bit;
	};
	typedef struct fpga_mapping fpga_map_t;
	typedef fpga_map_t * fpga_mapping_t;


	struct fpga_mapping {
		char * file_name;
		int    offset;
		size_t length;

		int    fd;
		void * address;

		value32bit_t *i,
		             *o;

	};

	uint32_t uint32_reverse     ( register uint32_t v );
	uint32_t uint32_mask_up_0   ( register const bit_addr_t n );
	uint32_t uint32_mask_up_1   ( register const bit_addr_t n );
	uint32_t uint32_mask_down_0 ( register const bit_addr_t n );
	uint32_t uint32_mask_down_1 ( register const bit_addr_t n );
	uint32_t uint32_mask_0( register const bit_addr_t start,
	                        register const bit_addr_t end    );
	uint32_t uint32_mask_1( register const bit_addr_t start,
	                        register const bit_addr_t end    );

#endif

#ifdef __cplusplus
	}
#endif
