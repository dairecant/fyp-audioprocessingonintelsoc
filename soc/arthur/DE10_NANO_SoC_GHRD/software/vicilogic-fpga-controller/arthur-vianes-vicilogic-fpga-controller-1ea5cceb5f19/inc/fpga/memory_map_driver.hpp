/// @copyright  Copyright @ 2013-2017, National University of Ireland,
///             Galway (NUI Galway). All Rights Reserved.
///
/// @License  No part of this software may be reproduced, stored or transmitted
///           in any form, electronic or otherwise without the prior written
///           permission of NUI Galway.
///
/// Contact:  Dr Fearghal Morgan, Room 3042, Engineering Building,
///           National University of Ireland Galway,
///           Galway, Ireland
/// Email:    fearghal.morgan@nuigalway.ie
///
/// @brief    FPGA controller interface for C++
/// @author   Arthur Vianes (arthur.vianes@gmail.com)
/// @date     July 2017

#ifndef HPP__FPGA_MEMORYMAPDRIVER
#define HPP__FPGA_MEMORYMAPDRIVER

#include <bitset>
#include <vector>
#include <stdint.h>
#include <iostream>
#include <initializer_list>
#include <iomanip>
#include <stdint.h>

#include "fpga/types.h"
#include "fpga/fpga.h"
#include "fpga/segment.h"
#include "utils/metadata.hpp"
#include "fpga/controller.hpp"

namespace FPGA {

	class FPGA_Interface : public Metadata::Interface {
	public:
		class Segment : public Metadata::Interface::Segment {
		protected:
			fpga_segment_t &_segment;
		public:
			Segment(fpga_segment_t& segment) : _segment(segment) {}
			virtual void write (int addr, std::vector<uint32_t>const& data, int size) {
				fpga_segment_write_bits(&_segment, addr, addr+size-1, &data[0]);
			}
			virtual void read  (int addr, std::vector<uint32_t>     & data, int size) {
				data.resize((size+31)/32);
				fpga_segment_read_bits(&_segment, addr, addr+size-1, &data[0]);
			}
			virtual void write (int addr, std::vector<uint32_t>const& data) {
				fpga_segment_write_bits(&_segment, addr, addr+data.size()-1, &data[0]);
			}
			virtual void read  (int addr, std::vector<uint32_t>     & data) {
				data.resize((data.size()+31)/32);
				fpga_segment_read_bits(&_segment, addr, addr+data.size()-1, &data[0]);
			}
			virtual void write (std::vector<uint32_t>const& data) {
				fpga_segment_write_bits(&_segment, 0, data.size()-1, &data[0]);
			}
			virtual void read  (std::vector<uint32_t>     & data) {
				data.resize((data.size()+31)/32);
				fpga_segment_read_bits(&_segment, 0, data.size()-1, &data[0]);
			}
			fpga_segment_t& get_segment() const { return _segment; }
		};
	protected:
		Segment         _in, _out;
		fpga_segment_t& _control;

	public:
		FPGA_Interface (fpga_t& fpga) : _in(fpga.in), _out(fpga.out), _control(fpga.clock) {
			Interface::_segments.emplace("userctrl", &_in);
			Interface::_segments.emplace("sysprobe", &_out);
		//	Interface::_segments.emplace("control",  &_control);
		}
		void step (int n) {
			fpga_segment_clock_step(&_control, n);
		}
		void read () {
			fpga_segment_clock_reset(&_control);
		}
		void run (int en) {
			fpga_segment_clock_freeRun(&_control, en);
		}
	};

	class Memory_map_driver : public Control, protected fpga_t {
	protected:
		std::string _name;
	public:
		 Memory_map_driver (std::string const &name, int _offset, size_t _length);
		~Memory_map_driver ();
		void open ();
		void close ();

		virtual void write_bits  (int, const std::vector<uint32_t>&, int);
		virtual void write_words (int, const std::vector<uint32_t>&, int);

		virtual void read_bits   (int,       std::vector<uint32_t>&, int);
		virtual void read_words  (int,       std::vector<uint32_t>&, int);

		virtual void reset (bool);
		virtual void run   (bool);
		virtual void step  (unsigned int);

	};

//	class DE10_NANO ;
//	class PYNQ_Z1 ;
}

#endif
