/// @copyright  Copyright @ 2013-2017, National University of Ireland,
///             Galway (NUI Galway). All Rights Reserved.
///
/// @License  No part of this software may be reproduced, stored or transmitted
///           in any form, electronic or otherwise without the prior written
///           permission of NUI Galway.
///
/// Contact:  Dr Fearghal Morgan, Room 3042, Engineering Building,
///           National University of Ireland Galway,
///           Galway, Ireland
/// Email:    fearghal.morgan@nuigalway.ie
///
/// @brief    Segment memory
/// @author   Arthur Vianes (arthur.vianes@gmail.com)
/// @date     July 2017

#ifdef __cplusplus // In case of C++ compilater
	extern "C" {   // C++ export
#endif

#ifndef H__FPGA_SEGMENT
#define H__FPGA_SEGMENT

	#include "fpga/types.h"
	#include <assert.h>

//	enum fpga_segment_right {
//		FPGA_R = 1 << 0, // Readable
//		FPGA_W = 1 << 1, // Writable
//	};

//	typedef  enum fpga_segment_right  fpga_segment_right_e;
	typedef  struct fpga_segment      fpga_segment_t;

	struct fpga_segment {
		int offset; //< Memory offset from origin (Design specific)
		int length; //< Memory length (not used)

		value32bit_t *mem; //< Memory pointer
	};

	fpga_segment_t fpga_segment_create( int offset, int length);
	void fpga_segment_initalize( fpga_segment_t*, int offset, int length);
	void fpga_segment_openAt   ( fpga_segment_t*, uint32_t *origin );

	uint32_t fpga_segment_read_word ( fpga_segment_t*, int );
	void     fpga_segment_write_word( fpga_segment_t*, int, uint32_t );

	int  fpga_segment_read_bit ( fpga_segment_t*, int );
	void fpga_segment_write_bit( fpga_segment_t*, int, int );

	void fpga_segment_read_bits ( fpga_segment_t*, int,int, uint32_t      * );
	void fpga_segment_write_bits( fpga_segment_t*, int,int, uint32_t const* );

	void fpga_segment_read_words ( fpga_segment_t*, int,int, uint32_t      * );
	void fpga_segment_write_words( fpga_segment_t*, int,int, uint32_t const* );


	void fpga_segment_clock_step     (fpga_segment_t*, int);
	void fpga_segment_clock_1step    (fpga_segment_t*);
	void fpga_segment_clock_freeRun  (fpga_segment_t*, int);
	void fpga_segment_clock_reset    (fpga_segment_t*);

#endif

#ifdef __cplusplus
	}
#endif
