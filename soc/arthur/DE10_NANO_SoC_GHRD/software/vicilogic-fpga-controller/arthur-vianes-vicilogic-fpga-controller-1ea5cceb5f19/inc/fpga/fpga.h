/// @copyright  Copyright @ 2013-2017, National University of Ireland,
///             Galway (NUI Galway). All Rights Reserved.
///
/// @License  No part of this software may be reproduced, stored or transmitted
///           in any form, electronic or otherwise without the prior written
///           permission of NUI Galway.
///
/// Contact:  Dr Fearghal Morgan, Room 3042, Engineering Building,
///           National University of Ireland Galway,
///           Galway, Ireland
/// Email:    fearghal.morgan@nuigalway.ie
///
/// @brief    FPGA controller
/// @author   Arthur Vianes (arthur.vianes@gmail.com)
/// @date     July 2017

#ifdef __cplusplus // In case of C++ compilater
	extern "C" {   // C++ export
#endif

#ifndef H__FPGA_MAP
#define H__FPGA_MAP

	#include "fpga/types.h"
	#include "fpga/segment.h"

	#include <string.h>
	#include <assert.h>
	#include <stdio.h>
	#include <stdlib.h>
	#include <unistd.h>
	#include <fcntl.h>
	#include <sys/mman.h>
	#include <sys/stat.h>

	typedef   struct fpga_map   fpga_t;
	#define   FPGA_FILENAME_SIZE   50

	struct fpga_map {
		char   filename[FPGA_FILENAME_SIZE];
		int    offset; //< mapping offset (fpga specific)
		size_t length; //< mapping length

	//	fpga_segment_t tag;   //< Tag for FPGA & design checking
		fpga_segment_t clock; //< Segment for clock control
		fpga_segment_t in;    //< Input  signals
		fpga_segment_t out;   //< Output signals

		int    fd;     //< file descritor
		void * origin; //< mapping origin
	};

	fpga_t fpga_create                  (char const *, int, int);
	void   fpga_initialize     (fpga_t*, char const *, int, int);
	int    fpga_check          (fpga_t*);
	void   fpga_open           (fpga_t*);
	void   fpga_close          (fpga_t*);
	void   fpga_open_autoClose (fpga_t*);

	void   fpga_handler_onExit (int, void*);

#endif

#ifdef __cplusplus
	}
#endif
