/// @copyright  Copyright @ 2013-2017, National University of Ireland,
///             Galway (NUI Galway). All Rights Reserved.
///
/// @License  No part of this software may be reproduced, stored or transmitted
///           in any form, electronic or otherwise without the prior written
///           permission of NUI Galway.
///
/// Contact:  Dr Fearghal Morgan, Room 3042, Engineering Building,
///           National University of Ireland Galway,
///           Galway, Ireland
/// Email:    fearghal.morgan@nuigalway.ie
///
/// @brief    List of pre-built configuration
/// @author   Arthur Vianes (arthur.vianes@gmail.com)
/// @date     July 2017

#ifdef __cplusplus
	extern "C" { // C++ exportation
#endif

#ifndef H__FPGA_BOARDS
#define H__FPGA_BOARDS

	#include "fpga/fpga.h"

	#define PYNQ_Z1  (&PYNQ_Z1_FPGA)
	extern fpga_t      PYNQ_Z1_FPGA;

	#define DE10_NANO (&DE10_NANO_FPGA)
	extern fpga_t       DE10_NANO_FPGA;

	#define FPGA_DEFAULT DE10_NANO
#endif

#ifdef __cplusplus
	}
#endif
