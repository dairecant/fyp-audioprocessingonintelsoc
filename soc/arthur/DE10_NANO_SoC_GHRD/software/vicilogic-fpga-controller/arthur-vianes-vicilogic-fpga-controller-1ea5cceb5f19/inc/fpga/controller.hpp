/// @copyright  Copyright @ 2013-2017, National University of Ireland,
///             Galway (NUI Galway). All Rights Reserved.
///
/// @License  No part of this software may be reproduced, stored or transmitted
///           in any form, electronic or otherwise without the prior written
///           permission of NUI Galway.
///
/// Contact:  Dr Fearghal Morgan, Room 3042, Engineering Building,
///           National University of Ireland Galway,
///           Galway, Ireland
/// Email:    fearghal.morgan@nuigalway.ie
///
/// @brief    Abstract class for FPGA controllers
/// @author   Arthur Vianes (arthur.vianes@gmail.com)
/// @date     July 2017

#ifndef HPP__FPGA_CONTROLLERS
#define HPP__FPGA_CONTROLLERS

#include <vector>
#include <stdint.h>

namespace FPGA {

	/// @brief  Signal controller to read and write data over FPGA's signals
	/// Use for FPGA with memory mapped signals
	class Control_signal {
	public:
		virtual void write_bits  (int, const std::vector<uint32_t>&, int) = 0;
		virtual void write_words (int, const std::vector<uint32_t>&, int) = 0;

		virtual void read_words  (int,       std::vector<uint32_t>&, int) = 0;
		virtual void read_bits   (int,       std::vector<uint32_t>&, int) = 0;
	};

	/// @brief  Clock controller to clock, step, run
	/// Use for FPGA with a clock controller
	class Control_clock {
	public:
		virtual void reset ( bool         ) = 0;
		virtual void run   ( bool         ) = 0;
		virtual void step  ( unsigned int ) = 0;
	};

	class Control : public Control_signal, public Control_clock {};

}

#endif
