/// @copyright  Copyright @ 2013-2017, National University of Ireland,
///             Galway (NUI Galway). All Rights Reserved.
///
/// @License  No part of this software may be reproduced, stored or transmitted
///           in any form, electronic or otherwise without the prior written
///           permission of NUI Galway.
///
/// Contact:  Dr Fearghal Morgan, Room 3042, Engineering Building,
///           National University of Ireland Galway,
///           Galway, Ireland
/// Email:    fearghal.morgan@nuigalway.ie
///
/// @brief    Library to manage jsonrpc data on C++
/// @author   Arthur Vianes (arthur.vianes@gmail.com)
/// @date     July 2017

#ifndef __HPP__UTILS_jsonrpc
#define __HPP__UTILS_jsonrpc

#include "rapidjson/document.h"
#include <assert.h>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <boost/optional.hpp>
#include "utils/metadata.hpp"

/// @namespace jsonrpc
namespace jsonrpc {

	/// Error handling for jsonrpc specific exceptions.
	/// With error code and error message (but no error data).
	class Error : public std::exception {
	protected:
		int         _code;    //< Error code
		std::string _message; //< Error message

	public:
		explicit Error();
		explicit Error(rapidjson::Value const& tree);
		explicit Error(int code, const std::string& message);

		virtual const char*       what()    const throw();
		virtual std::string const message() const throw();
		virtual int               code()    const throw();

		/// build to json error object
		/// @param  json[out]  json object to build
		void build (::rapidjson::Document & json) const;

		/// parse error object from json
		/// @param  json[in]  json object to parse
		void parse (::rapidjson::Value const& json);
	};

	extern const Error PARSE_ERROR,     //< Pre-built error for parsing error
	                   INVALID_REQUEST, //< Pre-built error (not used ?)
	                   INVALID_METHOD,  //< Pre-built error for invalid method
	                   INVALID_PARAMS,  //< Pre-built error for invalid params
	                   INTERNAL_ERROR,  //< Pre-built error (not used ?)
	                   SERVER_ERROR;    //< Pre-built error (not used ?)

	/// Base of jsonrpc objects (request and response)
	/// Manage id and version
	template <int VERS_MAJOR=2, int VERS_MINOR=0>
	class Base {
	protected:
		boost::optional<int> _id; //< packet id, nullable

		/// Build version number
		static ::std::string VERSION() {
			return ::std::to_string(VERS_MAJOR) +"."+
			       ::std::to_string(VERS_MINOR);
		}

	public:
		void set_id (int id) { _id = id; }
		bool has_id () const { return static_cast<bool>(_id); }
		int  get_id () const { return *_id; }

		/// build to json error object
		/// @param  json[out]  json object to build
		void build (::rapidjson::Document& json) const {
			using namespace rapidjson;
			if( ! json.IsObject() ) json.SetObject();

			Value version, id;

			version.SetString( VERSION().c_str(),
			                   VERSION().size(),
			                   json.GetAllocator() ); // set version
			if(_id)  id.SetInt(*_id); // set id to int if is not null value

			json.AddMember("jsonrpc", version, json.GetAllocator());
			json.AddMember("id",      id,      json.GetAllocator());
		}

		/// parse base object from json
		/// @param  json[in]  json object to parse
		void parse (rapidjson::Value const& json) {
			if( ! json.IsObject()           ||
			    ! json.HasMember("id")      ||
			    ! json.HasMember("jsonrpc") ||
			      VERSION() != json["jsonrpc"].GetString() )
				throw PARSE_ERROR;

			if(json["id"].IsInt())
				_id = json["id"].GetInt();
		}
	};

	/// Jsonrpc request
	/// Manage method and params
	class Request : public Base<2,0> {
	protected:
		        std::string         _method;
		mutable rapidjson::Document _params;

	public:
		Request ();
		Request (std::string const &);
		Request (Base<> const & base, ::std::string const & method);

		void          set_method (::std::string const&);
		::std::string get_method () const;

		::rapidjson::Document& get_params ();

		void build (::rapidjson::Document &json) const;
		void parse (::rapidjson::Value const &json);
	};

	class Response : public Base<2,0> {
	protected:
		mutable ::rapidjson::Document   _result;
		boost::optional<jsonrpc::Error> _error;

	public:
		Response ();
		Response (::jsonrpc::Request const&);
		Response (::jsonrpc::Error   const&);
		Response (::rapidjson::Value const&);
		Response (::jsonrpc::Request const&, ::rapidjson::Value const&);
		Response (::jsonrpc::Request const&, ::jsonrpc  ::Error const&);

		void build (::rapidjson::Document & json) const;
		void parse (::rapidjson::Value const & tree);

		void from (Request const & req);
		void set_error  (::jsonrpc::Error const& err);
		void unset_error();
		::boost::optional<::jsonrpc::Error> get_error();

		void set_result (::rapidjson::Document const& err);
		::rapidjson::Document& get_result ();
		void get_result (::rapidjson::Document& );
	};
}

::std::ostream& operator << ( ::std::ostream &, ::jsonrpc::Request & );
::std::istream& operator >> ( ::std::istream &, ::jsonrpc::Request & );
::std::ostream& operator << ( ::std::ostream &, ::jsonrpc::Response & );
::std::istream& operator >> ( ::std::istream &, ::jsonrpc::Response & );

// ::std::istream& operator >> ( ::std::istream &, std::vector<::jsonrpc::Request> & );

#endif
