/// @copyright  Copyright @ 2013-2017, National University of Ireland,
///             Galway (NUI Galway). All Rights Reserved.
///
/// @License  No part of this software may be reproduced, stored or transmitted
///           in any form, electronic or otherwise without the prior written
///           permission of NUI Galway.
///
/// Contact:  Dr Fearghal Morgan, Room 3042, Engineering Building,
///           National University of Ireland Galway,
///           Galway, Ireland
/// Email:    fearghal.morgan@nuigalway.ie
///
/// @brief
/// @author   Arthur Vianes (arthur.vianes@gmail.com)
/// @date     July 2017

#ifndef HPP__UTILS_TASKS
#define HPP__UTILS_TASKS

void task_version (rapidjson::Document& in, rapidjson::Document& out) {
	std::cout << "TASK: \"version\" :" << in << std::endl;
	out.SetString("0.0");
}

void task_ping (rapidjson::Document& in, rapidjson::Document& out) {
	std::cout << "TASK: \"ping\" :" << in << std::endl;
	out.SetString("pong");
}

void task_write (rapidjson::Document& in, rapidjson::Document& out) {
	std::cout << "TASK: \"write\" :" << in << std::endl;
	//out.SetString("pong");
	throw jsonrpc::INVALID_PARAMS;
}

#endif
