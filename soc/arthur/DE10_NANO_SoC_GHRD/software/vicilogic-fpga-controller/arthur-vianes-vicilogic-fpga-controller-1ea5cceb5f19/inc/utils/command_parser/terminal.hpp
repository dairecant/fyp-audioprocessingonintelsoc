#ifndef _HPP__COMMANDPARSER_TERMINAL
#define _HPP__COMMANDPARSER_TERMINAL

#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <unordered_map>
#include <boost/regex.hpp>

class Command_parser {
public:
	class Arguments {
		public:
		Command_parser& _shell;
		const std::vector<std::string>& _args;
		Arguments(Command_parser& shell, const std::vector<std::string>& args);
	};
	typedef std::function<void(Command_parser::Arguments&)> Command;
protected:
	bool _exit;
	std::string _start_message;
	std::unordered_map< std::string,
	                    Command_parser::Command> _commands;
	std::unordered_map< std::string, std::string> _help;

	std::vector<std::string> lexical_analysis (const std::string &line) const;
	void run (const std::vector<std::string> &tokens);

public:
	Command_parser(std::string start_message);

	void append(const std::vector<std::string> names, Command_parser::Command func, std::string text);

	void help();
	void start();
	void stop();
};

#endif
