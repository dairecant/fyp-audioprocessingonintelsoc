#ifndef HPP__UTILS_COMMANDPARSER_JSONRPC
#define HPP__UTILS_COMMANDPARSER_JSONRPC

	#include <string>
	#include <unordered_map>
	#include "utils/jsonrpc.hpp"

	class Command_parser_json {
	public:
		typedef std::function< void( Command_parser_json&,
		                             rapidjson::Document&,
		                             rapidjson::Document& ) > Task;

	protected:
		std::unordered_map< std::string,
		                    Command_parser_json::Task> _tasks;
		bool _stop;
	public:
		Metadata::Map & _map;
		Command_parser_json(Metadata::Map &);

		Command_parser_json& append(std::string const& name, Command_parser_json::Task task);
		Command_parser_json& append(const std::vector<std::string> names, Command_parser_json::Task task);
		Command_parser_json::Task get ( std::string const & name);
		void run ( std::string const &, ::rapidjson::Document&, ::rapidjson::Document&);
		void run ( jsonrpc::Request&, jsonrpc::Response& );
		void run ( std::istream & in, std::ostream & out );
		void loop( std::istream & in, std::ostream & out );
		void stop ();
	};

#endif
