/// @copyright  Copyright @ 2013-2017, National University of Ireland,
///             Galway (NUI Galway). All Rights Reserved.
///
/// @License  No part of this software may be reproduced, stored or transmitted
///           in any form, electronic or otherwise without the prior written
///           permission of NUI Galway.
///
/// Contact:  Dr Fearghal Morgan, Room 3042, Engineering Building,
///           National University of Ireland Galway,
///           Galway, Ireland
/// Email:    fearghal.morgan@nuigalway.ie
///
/// @brief    Library to manage Vicilogic's metadata format on C++
/// @author   Arthur Vianes (arthur.vianes@gmail.com)
/// @date     July 2017

#ifndef __HPP__UTILS_METADATA
#define __HPP__UTILS_METADATA

#include "rapidjson/document.h"
#include <iostream>
#include <fstream>
#include <exception>
#include "rapidjson/istreamwrapper.h"
#include <unordered_map>
#include <string>
#include <utility>
#include <boost/optional.hpp>
#include <rapidjson/writer.h>
#include <rapidjson/ostreamwrapper.h>

std::ostream& operator<<(std::ostream& stream, ::rapidjson::Document const& d);
std::ostream& operator<<(std::ostream& stream, ::rapidjson::Value    const& d);

namespace Metadata {

	class Interface {
	public:
		typedef  std::string  key_t;
		class Segment {
			public:
			virtual void write (int, std::vector<uint32_t> const&, int) = 0;
			virtual void read  (int, std::vector<uint32_t>      &, int) = 0;
			virtual void write (int, std::vector<uint32_t> const&) = 0;
			virtual void read  (int, std::vector<uint32_t>      &) = 0;
			virtual void write (std::vector<uint32_t> const&) = 0;
			virtual void read  (std::vector<uint32_t>      &) = 0;
		};
		std::unordered_map<key_t, Segment*> _segments;

	public:
		virtual void run (int en) {}
		virtual void step (int n) {}
		virtual void reset () {}

		bool     has(key_t const& key) {
			return _segments.count(key)>0;
		}

		Segment& get(key_t const& key) {
			return *_segments[key];
		}

		boost::optional<Segment&> get_optional(key_t const& key) {
			if( has(key) )  return get(key);
			else            return boost::none;
		}

		Segment& operator[](key_t const& key) {
			return get(key);
		}
	};

	class Interface_test : public Interface {
	public:
		class Segment : public Interface::Segment {
			public:
			virtual void write (int, std::vector<uint32_t> const&, int);
			virtual void read  (int, std::vector<uint32_t>      &, int);
			virtual void write (int, std::vector<uint32_t> const&);
			virtual void read  (int, std::vector<uint32_t>      &);
			virtual void write (std::vector<uint32_t> const&);
			virtual void read  (std::vector<uint32_t>      &);
		};

	protected:
		Segment _in, _out;

	public:
		Interface_test () {
			Interface::_segments.emplace("userctrl", &_in);
			Interface::_segments.emplace("sysprobe", &_out);
		}
		virtual void run (int en) {}
		virtual void step (int n) {}
		virtual void reset () {}
	};

	class Parser;
	class Map;
	typedef ::std::string key_t;

	class Error : public std::exception {
	protected:
		::std::string _message;

	public:
		/** Constructor (C++ STL strings).
		 *  @param code    The error code.
		 *  @param message The error message.
		 */
		explicit Error();
		explicit Error(const std::string& message);

		virtual const char* what()    const throw ();
	};

	class Signal {
	public:
		virtual Signal& parse(
			::Metadata::Parser      & map,
			::rapidjson::Value const& sign
		) = 0;
		virtual int  size() const = 0;
		virtual void write (int, int, std::vector<uint32_t> const & ) = 0;
		virtual void read  (int, int, std::vector<uint32_t> & ) = 0;
		virtual void write (std::vector<uint32_t> const & ) = 0;
		virtual void read  (std::vector<uint32_t> & ) = 0;
		virtual Signal& verify_autoreference() = 0;
	};

	class Direct_memory : public Signal {
		protected:
			int _length,
				_length_min,
				_length_max;

			Metadata::Interface::Segment & _segment;

		public:
		Direct_memory(Metadata::Interface::Segment & );

		virtual int  size() const;
		virtual void write (int, int, std::vector<uint32_t> const & );
		virtual void read  (int, int, std::vector<uint32_t> & );
		virtual void write (std::vector<uint32_t> const & );
		virtual void read  (std::vector<uint32_t> & );
		virtual Signal& parse (
			::Metadata::Parser      & map,
			::rapidjson::Value const& sign
		);
		virtual Signal& verify_autoreference();
	};

	class Reference_memory : public Signal {
	protected:
		Signal * _ref;
		int  _low_bit, _high_bit;
		bool _loop;
	public:
		Reference_memory();
		Reference_memory(Signal & ref);

		virtual int  size() const;
		virtual void write (int, int, std::vector<uint32_t> const & );
		virtual void read  (int, int, std::vector<uint32_t> & );
		virtual void write (std::vector<uint32_t> const & );
		virtual void read  (std::vector<uint32_t> & );
		virtual Signal& parse (
			::Metadata::Parser       & map,
			::rapidjson::Value const & sign
		);
		virtual Signal& verify_autoreference();
	};

	class Map {
	protected:
		::std::unordered_map<key_t, Signal&> _map;
		Interface & _interface;

	public:
		Map(Interface&);
		void append (key_t const&, ::Metadata::Signal &);
		Signal & get (key_t const&);
		boost::optional<Signal&> get_optional (key_t const& key);
		Signal& operator[] (key_t const&);
		bool has (key_t const&) const;
		Interface & get_interface () const;
	};

	class Parser {
	protected:
		::Metadata::Map      &_map;
		::rapidjson::Document _tree;
	public:
		Parser(rapidjson::Document const & tree, ::Metadata::Map& map);
		Parser(std::ifstream             & file, ::Metadata::Map& map);
		::Metadata::Signal & parse(key_t key);
		void parse();
	};

}

#endif
