#ifndef H__UTILS_DEBUG
#include <stdio.h>

#ifndef DEBUG
#	define DEBUG 0
#endif


#	define COLOR_RED    "\e[31m"
#	define COLOR_GREEN  "\e[32m"
#	define COLOR_ORANGE "\e[33m"
#	define COLOR_NONE   "\e[0m"

#	define log(format, args ...) \
		if (DEBUG>0) fprintf(stderr, COLOR_ORANGE "[~]" COLOR_NONE " " format "\n\r", args)
/*
#	define log(format, args ...) \
		if (DEBUG>0) fprintf(stderr, "[+] "format"\n\r", args)

#	define log(format, args ...) \
		if (DEBUG>0) fprintf(stderr, "[-] "format"\n\r", args)
*/
#	define long_log(format, args ...) \
		if (DEBUG) fprintf(stderr, "    %s:%d on %s\n\n[~] " format "\n\r", \
		                   __FILE__, __LINE__, __PRETTY_FUNCTION__, args)

#endif
