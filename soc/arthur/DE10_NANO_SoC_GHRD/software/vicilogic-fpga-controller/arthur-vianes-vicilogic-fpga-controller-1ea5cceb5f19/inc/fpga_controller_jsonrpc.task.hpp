#ifndef HPP__TASK
#define HPP__TASK

#include <rapidjson/document.h>

#include <iostream>
#include <sstream>
#include <string>
#include <iomanip>
#include <sstream>

#include "fpga/memory_map_driver.hpp"
#include "utils/command_parser/jsonrpc.hpp"

void task_version  (Command_parser_json& parser, rapidjson::Document& in, rapidjson::Document& out);
void task_ping     (Command_parser_json& parser, rapidjson::Document& in, rapidjson::Document& out);
void task_write    (Command_parser_json& parser, rapidjson::Document& in, rapidjson::Document& out);
void task_metadata (Command_parser_json& parser, rapidjson::Document& in, rapidjson::Document& out);
void task_read     (Command_parser_json& parser, rapidjson::Document& in, rapidjson::Document& out);
void task_stop     (Command_parser_json& parser, rapidjson::Document& in, rapidjson::Document& out);
void task_step     (Command_parser_json& parser, rapidjson::Document& in, rapidjson::Document& out);
void task_run      (Command_parser_json& parser, rapidjson::Document& in, rapidjson::Document& out);
void task_reset    (Command_parser_json& parser, rapidjson::Document& in, rapidjson::Document& out);

#endif
