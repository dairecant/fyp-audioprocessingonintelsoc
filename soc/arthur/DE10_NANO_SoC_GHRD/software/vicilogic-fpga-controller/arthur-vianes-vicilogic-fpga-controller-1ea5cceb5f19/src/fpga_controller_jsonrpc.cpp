#include <fstream>
#include <iostream>
#include "utils/command_parser/jsonrpc.hpp"
#include "fpga/memory_map_driver.hpp"
#include "fpga/boards.h"
#include "fpga/segment.h"

#include "fpga_controller_jsonrpc.task.hpp"

int main () {
	fpga_open(FPGA_TARGET);

	FPGA::FPGA_Interface interface(*FPGA_TARGET);

	// Build a map for the fpga
	Metadata::Map map(interface);
	// Use a jsonrpc command parser
	Command_parser_json c(map);

	/*std::cerr << "Parsing ...";
	std::ifstream ifs("TLC.json");
	Metadata::Parser parser(ifs, map);
	parser.parse();
	std::cerr << " OK" << std::endl;*/

	c.append("stop",     task_stop)
	 .append("ping",     task_ping)
	 .append("version",  task_version)
	 .append("write",    task_write)
	 .append("read",     task_read)
	 .append("step",     task_step)
	 .append("run",      task_run)
	 .append("reset",    task_reset)
	 .append("metadata", task_metadata);

	c.loop(std::cin, std::cout);

	fpga_close(FPGA_TARGET);

	return EXIT_SUCCESS;
}
