#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <signal.h>

#include <iostream>
#include <iomanip>
#include <string>
#include <regex>
#include <vector>
#include <map>

#include "fpga/memory_map_driver.hpp"
#include "utils/command_parser/terminal.hpp"

FPGA::Memory_map_driver fpga("/dev/mem", 0x7AA00000, 0x10000);

void read_func (Command_parser::Arguments& arg) {
		if(arg._args.size() != 2 ) {
			printf("Read a 32 bits register\n" "Usage:\n"
				   "   r address\n");
			return;
		}
		int addr = strtol (arg._args[1].c_str(), NULL, 0);
		printf("   read  @ 0x%08X (%d)\n", addr, addr);

		std::vector<uint32_t> values;
		fpga.read_words(addr, values, 1);
		int value = values[0];
		printf("   value : 0x%08X (%d)\n", value, value);
}

void write_func (Command_parser::Arguments& arg) {
	std::vector<uint32_t> values;
	values.resize (1);
	if(arg._args.size() != 3 ) {
		printf("Write a 32 bits register\n" "Usage:\n"
			   "   w address value\n");
		return;
	}
	int addr  = strtol (arg._args[1].c_str(), NULL, 0);
	int value = strtol (arg._args[2].c_str(), NULL, 0);
	values[0] = value;

	printf("   write @ 0x%08X (%u)\n", addr, addr);
	fpga.write_words(addr, values, 1);
	printf("   value : 0x%08X (%u)\n", value, value);
}

void reset_func (Command_parser::Arguments& arg) {
	std::cout << "Generate clock reset\n";
	fpga.reset(1);
	fpga.reset(0);
}

void run_func (Command_parser::Arguments& arg) {
	std::cout << "run function" << "\n";
	static int fpga_freeRunStatus;
	fpga_freeRunStatus = arg._args.size() == 2 ? strtol(arg._args[1].c_str(), NULL, 0) : !fpga_freeRunStatus;
	printf("Clock free run %sable\n", fpga_freeRunStatus ? "en" : "dis");
	fpga.run(fpga_freeRunStatus);
}

void help_func (Command_parser::Arguments& arg) {
	arg._shell.help();
}

void step_func (Command_parser::Arguments& arg) {
	std::cout << "steps function" << "\n";
	int value;
	if(arg._args.size() == 1) value = 1;
	else                      value = strtol(arg._args[1].c_str(), NULL, 0);
	printf("Clock %d step%s\n", value, value>1 ? "s":"");
	fpga.step(value);
}

void exit_func (Command_parser::Arguments& arg) {
	arg._shell.stop();
}

int main() {
	fpga.open();
	Command_parser shell("COMMAND PARSER");
	shell.append({"run", "freerun", "f"}, run_func,   "clock free run");
	shell.append({"step", "s"},           step_func,  "clock step");
	shell.append({"reset", "0"},          reset_func, "reset FPGA");
	shell.append({"read", "r"},           read_func,  "Read data from FPGA");
	shell.append({"write", "w"},          write_func, "write data to FPGA");
	shell.append({"help", "h"},           help_func,  "Show this help message");
	shell.append({"exit", "quit", "q"},   exit_func,  "exit");

	shell.start();
}
