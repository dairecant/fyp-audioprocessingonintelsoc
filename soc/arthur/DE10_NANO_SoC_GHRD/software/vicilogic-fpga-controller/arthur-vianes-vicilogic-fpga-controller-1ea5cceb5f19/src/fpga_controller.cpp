#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <signal.h>
#include <iostream>

#include "fpga/fpga.h"
#include "fpga/boards.h"

#ifdef __cplusplus
	extern "C" { // c++ importation
#endif
#ifdef __cplusplus
	}
#endif

void exit_normal (int) {
	exit(0);
}

int main ( int argc, char * argv[] ) {
	//printf("FPGA Controller  for ViciLogic\n\n");
	std::cout << "FPGA Controller  for ViciLogic\n\n";
/*
	if (argc < 3) {
		printf("Error: 2 arguments exepted\n\n");
		printf("usage:\n"
		       "\t%s address length\n\n", argv[0]);

		printf("For PYNQ-Z1 board:\n"
		       "\t%s 0x7AA00000 0x10000\n\n", argv[0]);
	}
*/
	int fpga_freeRunStatus = 0;
/*
	int offset = (int) strtol (argv[1], NULL, 0),
	    length = (int) strtol (argv[2], NULL, 0);

	printf("Information\n");
	printf("Offset: 0x%08X (%d)\n", offset, offset);
	printf("Length: 0x%08X (%d)\n", length, length);
*/
	//fpga_mapping_t fpga = fpga_initialize("/dev/mem", offset , length);
	//fpga_open_autoClose(fpga);
	//fpga_mapping_t fpga = PYNQ_Z1;
	fpga_t* fpga = FPGA_TARGET;
	fpga_open_autoClose(fpga);
	signal(SIGINT, exit_normal);
	signal(SIGKILL, exit_normal);

	printf("origin: %p\n",fpga->origin);
	printf("clock: %p\n",fpga->clock.mem);
	printf("in : %p\n",fpga->in.mem);
	printf("out: %p\n",fpga->out.mem);
	printf("\nTape `h` for help.\n\n");
	char comm, c, arg[2][21];
	int run_continue = 1;

	while( run_continue ) {
		uint32_t arg_value[2], value;
		arg[0][0] = arg[1][0] = '\0';
		printf("\e[1;97m>>\e[0m ");
		int arg_count = scanf("%c%*[ ]%20[0-9A-Za-z]%*[ ]%20[0-9A-Za-z]",&comm, arg[0], arg[1]) -1;

		//printf("count %d\n", arg_count);
		switch (comm) {
		case 'r': // read
		case 'R':
			if(arg_count != 1 ) {
				printf("Read a 32 bits register\n" "Usage:\n"
				       "   r address\n");
				break;
			}
			arg_value[0] = strtol (arg[0], NULL, 0);
			printf("   read  @ 0x%08X (%d)\n", arg_value[0], arg_value[0]);

			value = fpga_segment_read_word(&fpga->out, arg_value[0]);
			printf("   value : 0x%08X (%d)\n", value, value);
			break;

		case 'q': // quit
		case 'Q':
			run_continue = 0;
			break;
		case 'm': // map
		{
			int i = 0;
			uint32_t values[50] = {0};

			fpga_segment_read_bits(&fpga->out, 50*32-1, 0, values);
			for(;i < 50; i++) {
				printf("   [%3d] 0x%08X (%d)\n", i,values[i], values[i]);
			}
			break;
		}
		case 'f': // clock free run
		case 'F':
			fpga_freeRunStatus = arg_count == 1 ? strtol(arg[0], NULL, 0) : !fpga_freeRunStatus;
			printf("Clock free run %sable\n", fpga_freeRunStatus ? "en" : "dis");
			fpga_segment_clock_freeRun(&fpga->clock, fpga_freeRunStatus);
			break;

		case 's': // clock step
		case 'S':
			if(arg_count == 0) value = 1;
			else               value = strtol(arg[0], NULL, 0);
			printf("Clock %d step%s\n", value, value>1 ? "s":"");
			fpga_segment_clock_step(&fpga->clock, value);
			break;


		case '0':
			printf("Generate clock reset\n");
			fpga_segment_clock_reset(&fpga->clock);
			break;

		case 'C':
		case 'c':
			printf("Reset all memory\n");
		//	fpga_cleanMemory(fpga);
			break;

		case 'w':
		case 'W':
			if(arg_count != 2 ) {
				printf("Write a 32 bits register\n" "Usage:\n"
				       "   w address value\n");
				break;
			}
			arg_value[0] = strtol (arg[0], NULL, 0);
			arg_value[1] = strtol (arg[1], NULL, 0);
			printf("   write @ 0x%08X (%u)\n", arg_value[0], arg_value[0]);
			fpga_segment_write_word(&fpga->in, arg_value[0], arg_value[1]);
		//	fpga_segment_write_bits(fpga->out, arg_value[0], arg_value[0]+32, &arg_value[1]);
			printf("   value : 0x%08X (%u)\n", arg_value[1], arg_value[1]);
			break;

		default:
			printf("Unknown command\n\n");
		case 'h':
			printf("Commands:\n"
			       "\tw address value  - Write a 32 bits register\n"
			       "\tr address        - Read a 32 bits register\n"
			       "\tf [en]           - Clock free run enable (en=1)/ disable (en=0)\n"
			       "\ts                - Clock 1 step\n"
			       "\ts [N]            - Clock N step(s) (if N is occure )\n"
			       "\tH                - Clock half steps\n"
			       "\t0                - Clock reset\n"
			       "\tc                - Clear memory\n"
			       "\tq                - Close FPGA file and quit\n"
			       "\th                - Print this help message\n"
			       "\n");

			while((getc(stdin))!='\n');
			continue;
		}

		if((c=getc(stdin))!='\n') ungetc('\n', stdin);
	}

	printf("\n");

	return 0;
}
