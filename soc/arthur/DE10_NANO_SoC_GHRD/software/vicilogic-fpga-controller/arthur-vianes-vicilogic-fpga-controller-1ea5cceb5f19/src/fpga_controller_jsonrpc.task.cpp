#include "fpga_controller_jsonrpc.task.hpp"

void task_version (
	Command_parser_json& parser,
	rapidjson::Document& in,
	rapidjson::Document& out
) {
	out.SetString("0.0");
}

void task_ping (
	Command_parser_json& parser,
	rapidjson::Document& in,
	rapidjson::Document& out
) {
	out.SetString("pong");
}

void task_step (
	Command_parser_json& parser,
	rapidjson::Document& in,
	rapidjson::Document& out
) {
	int id;
	if(in[0].IsInt())
		id = in[0].GetInt();
	else throw jsonrpc::INVALID_PARAMS;

	int n;
	if(in[1].IsInt())
		n = in[1].GetInt();
	else throw jsonrpc::INVALID_PARAMS;

	parser._map.get_interface().step(n);
}

void task_run (
	Command_parser_json& parser,
	rapidjson::Document& in,
	rapidjson::Document& out
) {
	int id;
	if(in[0].IsInt())
		id = in[0].GetInt();
	else throw jsonrpc::INVALID_PARAMS;

	int en;
	if(in[1].IsInt())
		en = in[1].GetInt();
	else throw jsonrpc::INVALID_PARAMS;

	parser._map.get_interface().run(en);
}

void task_reset (
	Command_parser_json& parser,
	rapidjson::Document& in,
	rapidjson::Document& out
) {
	int id;
	if(in[0].IsInt())
		id = in[0].GetInt();
	else throw jsonrpc::INVALID_PARAMS;

	parser._map.get_interface().reset();
}

void task_write (
	Command_parser_json& parser,
	rapidjson::Document& in,
	rapidjson::Document& out
) {
	if(!in.IsArray() || in.Size() != 3)
		throw jsonrpc::INVALID_PARAMS;

	int id;
	if(in[0].IsInt())
		id = in[0].GetInt();
	else throw jsonrpc::INVALID_PARAMS;

	std::string name;
	if(in[1].IsString())
		name = in[1].GetString();
	else throw jsonrpc::INVALID_PARAMS;

	std::string data;
	if(in[2].IsString())
		data = in[2].GetString();
	else throw jsonrpc::INVALID_PARAMS;

	std::vector<uint32_t> row_data;

	// Read hex 32 bits data from a string
	size_t len = data.length();
	for(size_t i = 0; i < len; i += 8) {
		std::istringstream strm(data.substr(i, 8));
		uint32_t x;
		strm >> std::hex >> std::setw( 8 ) >> x;
		row_data.push_back(x);
	}

	auto memory = parser._map.get_optional(name);

	if(memory) memory->write(row_data);
}

void task_metadata (
	Command_parser_json& parser,
	rapidjson::Document& in,
	rapidjson::Document& out
) {
	std::cerr << "Parsing ...";
	Metadata::Parser metadata_parser(in, parser._map);
	metadata_parser.parse();
	std::cerr << " OK" << std::endl;
}

void task_read (
	Command_parser_json& parser,
	rapidjson::Document& in,
	rapidjson::Document& out
) {
	if(!in.IsArray() || in.Size() != 2)
		throw jsonrpc::INVALID_PARAMS;
	int id;
	if(in[0].IsInt())
		id = in[0].GetInt();
	else throw jsonrpc::INVALID_PARAMS;

	std::string name;
	if(in[1].IsString())
		name = in[1].GetString();
	else throw jsonrpc::INVALID_PARAMS;

	auto memory = parser._map.get_optional(name);

	std::vector<uint32_t> value;
	if(memory) memory->read(value);
	else throw jsonrpc::INVALID_PARAMS;

	// Write hex 32 bits data to a string
	std::ostringstream ss;
	ss << std::hex << std::uppercase << std::setfill( '0' );
	std::for_each( value.cbegin(), value.cend(), [&]( uint32_t c ) { ss << std::setw( 8 ) << c; } );
	std::string result = ss.str();

	out.SetString(result.c_str(), result.size(), out.GetAllocator());
}

void task_stop (
	Command_parser_json& parser,
	rapidjson::Document& in,
	rapidjson::Document& out
) {
	parser.stop();
}
