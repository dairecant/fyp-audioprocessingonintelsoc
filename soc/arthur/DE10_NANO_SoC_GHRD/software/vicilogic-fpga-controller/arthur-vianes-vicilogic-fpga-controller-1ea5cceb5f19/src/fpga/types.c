/// @copyright  Copyright @ 2013-2017, National University of Ireland,
///             Galway (NUI Galway). All Rights Reserved.
///
/// @License  No part of this software may be reproduced, stored or transmitted
///           in any form, electronic or otherwise without the prior written
///           permission of NUI Galway.
///
/// Contact:  Dr Fearghal Morgan, Room 3042, Engineering Building,
///           National University of Ireland Galway,
///           Galway, Ireland
/// Email:    fearghal.morgan@nuigalway.ie
///
/// @brief    Types FPGA controller
/// @author   Arthur Vianes (arthur.vianes@gmail.com)
/// @date     July 2017

#include "fpga/types.h"

/// @brief Reverse bits-order for 32bits word
/// @param  v[in]  word to reverse
/// @return        reversed value
uint32_t
uint32_reverse ( register uint32_t v ) {
	v    = ( ((v & 0xaaaaaaaa) >>  1) | ((v & 0x55555555) <<  1) );
	v    = ( ((v & 0xcccccccc) >>  2) | ((v & 0x33333333) <<  2) );
	v    = ( ((v & 0xf0f0f0f0) >>  4) | ((v & 0x0f0f0f0f) <<  4) );
	v    = ( ((v & 0xff00ff00) >>  8) | ((v & 0x00ff00ff) <<  8) );
	return ( ( v               >> 16) | ( v               << 16) );
}

/// @brief Mask by 0 from top for n bit, on a 32bits word.
/// @b Exemple
/// call:   uint32_mask_up_0 (5);
/// result: [0000 0111  1111 1111  1111 1111  1111 1111]  <- lsb
/// @param[in]  n  Number of bit to mask.
/// @return        Mask 32 bits.
inline
uint32_t
uint32_mask_up_0 ( register const bit_addr_t n ) {
	     if (n < 0 ) return 0xffffffff;
	else if (n > 31) return 0x00000000;
	else             return 0xffffffff >> n;
}

/// @brief Mask by 0 from bottom for n bit, on a 32bits word.
/// @b Exemple
/// call:   uint32_mask_down_0 (5);
/// result: [1111 1111  1111 1111  1111 1111  1110 0000]  <- lsb
/// @param[in]  n  Number of bit to mask.
/// @return        Mask 32 bits.
inline
uint32_t
uint32_mask_down_0 ( register const bit_addr_t n ) {
	     if (n < 0 ) return 0xffffffff;
	else if (n > 31) return 0x00000000;
	else             return 0xffffffff << n;
}


/// @brief Mask by 1 from top for n bit, on a 32bits word.
/// @b Exemple
/// call:   uint32_mask_up_1 (5);
/// result: [1111 1000  0000 0000  0000 0000  0000 0000]  <- lsb
/// @param[in]  n  Number of bit to mask.
/// @return        Mask 32 bits.
inline
uint32_t
uint32_mask_up_1 ( register const bit_addr_t start ) {
	return ~uint32_mask_up_0(start);
}


/// @brief Mask by 1 from bottom for n bit, on a 32bits word.
/// @b Exemple
/// call:   uint32_mask_down_1 (5);
/// result: [0000 0000  0000 0000  0000 0000  0001 1111]  <- lsb
/// @param[in]  n  Number of bit to mask.
/// @return        Mask 32 bits.
inline
uint32_t
uint32_mask_down_1 ( register const bit_addr_t end ) {
	return ~uint32_mask_down_0(end);
}

/// Mask by 0 from `start` to `end` on a 32bits word.
/// (`start` and `end` included)
/// @b Exemple
/// call:   uint32_mask_0 (9, 11);
/// result: [1111 1111 1111 1111  1111 0001  1111 1111]  <- lsb
/// @param[in]  start  First bit to unset.
/// @param[in]  end    Last  bit to unset.
/// @return            Mask 32 bits.
inline
uint32_t
uint32_mask_0 ( register const bit_addr_t start,
                register const bit_addr_t end    ) {
	assert(start <= end);
	return uint32_mask_down_1(start) ^ uint32_mask_down_0(end+1);
}


/// Mask by 1 from `start` to `end` on a 32bits word.
/// (`start` and `end` included)
/// @b Exemple
/// call:   uint32_mask_1 (9, 11);
/// result: [0000 0000  0000 0000  0000 1110  0000 0000]  <- lsb
/// @param[in]  start  First bit to set.
/// @param[in]  end    Last  bit to set.
/// @return            Mask 32 bits.
inline
uint32_t
uint32_mask_1 ( register const bit_addr_t start,
                register const bit_addr_t end    ) {
	assert(start <= end);
	return uint32_mask_down_0(start) ^ uint32_mask_down_0(end+1);
}
