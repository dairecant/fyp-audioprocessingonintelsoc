/// @copyright  Copyright @ 2013-2017, National University of Ireland,
///             Galway (NUI Galway). All Rights Reserved.
///
/// @License  No part of this software may be reproduced, stored or transmitted
///           in any form, electronic or otherwise without the prior written
///           permission of NUI Galway.
///
/// Contact:  Dr Fearghal Morgan, Room 3042, Engineering Building,
///           National University of Ireland Galway,
///           Galway, Ireland
/// Email:    fearghal.morgan@nuigalway.ie
///
/// @brief    Segment memory
/// @author   Arthur Vianes (arthur.vianes@gmail.com)
/// @date     July 2017

#include "fpga/segment.h"
#include "utils/debug.h"
/// Read a word at index
/// @param[in,out]  that   Segment object to read
/// @param[in]      index  Word address
/// @return                Value
uint32_t
fpga_segment_read_word (
	fpga_segment_t *that,
	int             index
) {
	log("fpga_segment_read_word([that = %p], %d)", that, index);
	return that->mem[index].as_word;
}

/// Read a bit at index
/// @param[in,out]  that   Segment object to read
/// @param[in]      index  Bit address
/// @return                Value
int
fpga_segment_read_bit (
	fpga_segment_t *that,
	int             index
) {
	log("fpga_segment_read_bit([that = %p], %d)", that, index);
	int value = fpga_segment_read_word(that, index/32) & (1<<index%32);
	return value ? 1 : 0;
}

/// Read a range of bits from low to high
/// @param[in,out]  that    Segment object to read
/// @param[in]      low     Low  bit address (index included)
/// @param[in]      high    High bit address (index included)
/// @param[in]      values  Values to read
static void
fpga_segment_read_bits_ordered (
	fpga_segment_t *that,
	int             low,
	int             high,
	uint32_t       *values
) {
	log("fpga_segment_read_bits_ordered([that = %p], %d, %d, [data])", that, low, high);
	assert(low<=high);
	const int low_bit_offset  = low  % 32;
	const int high_bit_offset = high % 32;
	const int low_addr_32bit  = low  / 32;
	const int high_addr_32bit = high / 32;

	uint32_t value      = fpga_segment_read_word(that, low_addr_32bit),
	         next_value;

	int i;
	for( i=0 ; i<high_addr_32bit-low_addr_32bit ; i++) {
		next_value = fpga_segment_read_word(that, low_addr_32bit+i+1);

		values[i] = ( value      >>     low_bit_offset );
		if(low_bit_offset > 0 ) // prevent undefined behavior `(uint32_t) << 32`
		values[i]|= ( next_value << (32-low_bit_offset));

		value = next_value;
	}

	values[i] = (value & uint32_mask_1(low_bit_offset, high_bit_offset))
	             >> low_bit_offset;
}

/// Read a range of bits from high to low as renverse order
/// @param[in,out]  this    Segment object to read
/// @param[in]      low     Low  bit address (index included)
/// @param[in]      high    High bit address (index included)
/// @param[in]      values  Values to read
static void
fpga_segment_read_bits_reverse (
	fpga_segment_t *that,
	int             low,
	int             high,
	uint32_t       *values
) {
	log("fpga_segment_read_bits_reverse([that = %p], %d, %d, [data])", that, low, high);
	assert(low>=high);
	const int low_bit_offset  = 31 - (low  % 32); // offset for renverse order
	const int high_bit_offset = 31 - (high % 32);
	const int low_addr_32bit  =       low  / 32 ;
	const int high_addr_32bit =       high / 32 ;

	uint32_t next_value;
	uint32_t value = uint32_reverse( // reverse every word
		fpga_segment_read_word(that, low_addr_32bit)
	);

	int i;
	for( i=0 ; i < low_addr_32bit-high_addr_32bit ; i++ ) {
		next_value = uint32_reverse ( // reverse words and read order
			fpga_segment_read_word(that, low_addr_32bit-i-1)
		);

		values[i] = ( value      >>     low_bit_offset );
		if( low_bit_offset > 0 ) // prevent undefined behavior `(uint32) << 32`
		values[i]|= ( next_value << (32-low_bit_offset));

		value = next_value;
	}

	values[i] = (value & uint32_mask_1(low_bit_offset, high_bit_offset))
	             >> low_bit_offset;
}


/// Read a range of bits from low to high
/// @param[in,out]  this    Segment object to read
/// @param[in]      low     Low  bit address (index included)
/// @param[in]      high    High bit address (index included)
/// @param[in]      values  Values to read
void
fpga_segment_read_bits (
	fpga_segment_t *that,
	int             l,
	int             h,
	uint32_t       *values
) {
	if(l <= h)
		return fpga_segment_read_bits_ordered(that, l, h, values);
	else
		return fpga_segment_read_bits_reverse(that, l, h, values);
}

/// Read a range of words from low to high
/// @param[in,out]  this    Segment object to read
/// @param[in]      low     Low  word address (index included)
/// @param[in]      high    High word address (index included)
/// @param[in]      values  Values to read
void
fpga_segment_read_words (
	fpga_segment_t *that,
	int             low,
	int             high,
	uint32_t       *values
) {
	log("fpga_segment_read_words([that = %p], %d, %d, [data])", that, low, high);
	for( int addr=low, i=0 ; addr<=high ; i++, addr++ )
		values[i] = fpga_segment_read_word(that, addr);
}
