/// @copyright  Copyright @ 2013-2017, National University of Ireland,
///             Galway (NUI Galway). All Rights Reserved.
///
/// @License  No part of this software may be reproduced, stored or transmitted
///           in any form, electronic or otherwise without the prior written
///           permission of NUI Galway.
///
/// Contact:  Dr Fearghal Morgan, Room 3042, Engineering Building,
///           National University of Ireland Galway,
///           Galway, Ireland
/// Email:    fearghal.morgan@nuigalway.ie
///
/// @brief    Segment memory
/// @author   Arthur Vianes (arthur.vianes@gmail.com)
/// @date     July 2017

#include "fpga/segment.h"

/// Create an fpga_segment_t object
/// @param[in]  offset  Segment offset from origin
/// @param[in]  length  Segment length
/// @return             Segment object
fpga_segment_t
fpga_segment_create (
	int       offset,
	int       length
) {
	fpga_segment_t that;
	fpga_segment_initalize(&that, offset, length);
	return that;
}

/// Initialize an fpga_segment_t object
/// @param[in,out]  that    Segment object to initialize
/// @param[in]      offset  Segment offset from origin
/// @param[in]      length  Segment length
void
fpga_segment_initalize (
	fpga_segment_t *that,
	int             offset,
	int             length
) {
	that->offset = offset;
	that->length = length;
}

/// Open an fpga_segment_t object from origin
/// @param[in,out]  that    Segment object to open
/// @param[in]      origin  Origin for segment offset
/// @return                 Segment object
void
fpga_segment_openAt (
	fpga_segment_t *that,
	uint32_t       *origin
) {
	that->mem = (value32bit_t*)(origin + that->offset);
}
