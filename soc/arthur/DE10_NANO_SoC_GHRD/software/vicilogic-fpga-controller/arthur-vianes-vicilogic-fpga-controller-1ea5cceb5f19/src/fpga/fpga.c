/// @copyright  Copyright @ 2013-2017, National University of Ireland,
///             Galway (NUI Galway). All Rights Reserved.
///
/// @License  No part of this software may be reproduced, stored or transmitted
///           in any form, electronic or otherwise without the prior written
///           permission of NUI Galway.
///
/// Contact:  Dr Fearghal Morgan, Room 3042, Engineering Building,
///           National University of Ireland Galway,
///           Galway, Ireland
/// Email:    fearghal.morgan@nuigalway.ie
///
/// @brief    FPGA controller
/// @author   Arthur Vianes (arthur.vianes@gmail.com)
/// @date     July 2017

#include "fpga/fpga.h"

/// Create an fpga object
/// @param[in]  filename  I/O map file (49 char max)
/// @param[in]  offset    offset of map
/// @param[in]  length    length of map
fpga_t
fpga_create (
	char const * filename,
	int          offset,
	int          length
) {
	fpga_t that;
	fpga_initialize(&that, filename, offset, length);

	return that;
}

/// Initialize on FPGA object
/// @param      that      FPGA object to initialize
/// @param[in]  filename  I/O map file (49 char max)
/// @param[in]  offset    offset of map
/// @param[in]  length    length of map
void
fpga_initialize (
	fpga_t     * that,
	char const * filename,
	int          offset,
	int          length
) {
	size_t filename_length = strlen(filename);
	assert(filename_length < FPGA_FILENAME_SIZE);

	strcpy(that->filename, filename);
	that->offset = offset;
	that->length = length;
}

/// Check if FPGA object is valid
/// @param      that      FPGA object to check
int
fpga_check (
	fpga_t* that
) {
	return that != NULL;
}

/// Open an FPGA memory map
/// @param[in]  that  FPGA object to open
void
fpga_open (
	fpga_t* that
) {
	fprintf(stderr,"\e[33m(open fpga)\e[0m\n");

	// unlink(fpga->file_name); // To prevent symbolic link attack
	if ((that->fd = open(that->filename, O_RDWR | O_SYNC ) ) == -1) {
		perror("fopen");
		exit(1);
	}

	that->origin = mmap( NULL,
		that->length,           // fpga memory map span
		PROT_READ | PROT_WRITE, // read/write mode
		MAP_SHARED,             // Shared with other process
		that->fd,               // mem file
		that->offset            // offset on file
	);

	if( that->origin == MAP_FAILED ) {
		perror("mmap");
		close(that->fd);
		exit(1);
	}

	fpga_segment_openAt(& that->clock,  that->origin);
	fpga_segment_openAt(& that->in,     that->origin);
	fpga_segment_openAt(& that->out,    that->origin);
}

/// Close an FPGA memory map
/// @param[in]  that  FPGA object to close
void
fpga_close (
	fpga_t* that
) {
	fprintf(stderr,"\e[33m(close fpga)\e[0m\n");
	munmap(that->origin, that->length);
	close(that->fd);
}

/// Open an FPGA memory map
///
/// @code{.c}
/// #include "fpga/fpga.h"
/// #include "fpga/boards.h"
///
/// int main () {
/// 	fpga_open(PYNQ_Z1);
///
/// 	fpga_segment_write_word(&PYNQ_Z1->out, 0, 5);
///
/// 	fpga_close(PYNQ_Z1);
/// 	return 0;
/// }
/// @endcode
///
/// @param[in]  that  FPGA description object
void
fpga_open_autoClose (
	fpga_t* that
) {
	fpga_open(that);
	on_exit(fpga_handler_onExit, (void*)that); // close this fpga on exit
}

void
fpga_handler_onExit (
	int   ret_value,
	void* data
) {
	fpga_t *that = (fpga_t*) data;
	fpga_close(that);
}
