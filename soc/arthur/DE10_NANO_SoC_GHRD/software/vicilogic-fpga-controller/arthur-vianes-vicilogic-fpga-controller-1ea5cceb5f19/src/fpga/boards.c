/// @copyright  Copyright @ 2013-2017, National University of Ireland,
///             Galway (NUI Galway). All Rights Reserved.
///
/// @License  No part of this software may be reproduced, stored or transmitted
///           in any form, electronic or otherwise without the prior written
///           permission of NUI Galway.
///
/// Contact:  Dr Fearghal Morgan, Room 3042, Engineering Building,
///           National University of Ireland Galway,
///           Galway, Ireland
/// Email:    fearghal.morgan@nuigalway.ie
///
/// @brief    List of pre-built configuration
/// @author   Arthur Vianes (arthur.vianes@gmail.com)
/// @date     July 2017

#include "fpga/boards.h"

/// PYNQ-Z1
fpga_t PYNQ_Z1_FPGA   = { "/dev/mem", 0x7AA00000, 0x10000,
                            // New design
                            {  0,   2 }, //< clock segment
                            {  6,  32 }, //< in segment
                            { 36, 164 }  //< out segment
                        };

/// DE10-Nano
fpga_t DE10_NANO_FPGA = { "/dev/mem", 0xff200000, 0x10000,
                            {0,   1  }, //< clock segment
                            {0,   128}, //< in  segment
                            {128, 128}  //< out segment
                        };
