/// @copyright  Copyright @ 2013-2017, National University of Ireland,
///             Galway (NUI Galway). All Rights Reserved.
///
/// @License  No part of this software may be reproduced, stored or transmitted
///           in any form, electronic or otherwise without the prior written
///           permission of NUI Galway.
///
/// Contact:  Dr Fearghal Morgan, Room 3042, Engineering Building,
///           National University of Ireland Galway,
///           Galway, Ireland
/// Email:    fearghal.morgan@nuigalway.ie
///
/// @brief    FPGA controller interface for C++
/// @author   Arthur Vianes (arthur.vianes@gmail.com)
/// @date     July 2017

#include "fpga/memory_map_driver.hpp"

using namespace FPGA;

Memory_map_driver::Memory_map_driver (
	std::string const&  name,
	int                _offset,
	size_t             _length
) : _name(name) {
	fpga_initialize(this, _name.c_str(), _offset, _length);
	this->clock = {   0,   1 };
	this->in    = {   0, 128 };
	this->out   = { 128, 128 };
}

Memory_map_driver::~Memory_map_driver () {
	close();
}

/// Open FPGA connection
void
Memory_map_driver::open () {
	fpga_open(this);
	// return fpga_check();
}

void
Memory_map_driver::close() {
	fpga_close(this);
}

void
Memory_map_driver::write_bits (
	int                           bit_addr,
	const std::vector<uint32_t> & values,
	int                           size
) {
	fpga_segment_write_bits(&this->in, bit_addr, bit_addr+size-1, &values[0]);
}

void
Memory_map_driver::write_words (
	int                           word_addr,
	const std::vector<uint32_t> & values,
	int                           size
) {
	fpga_segment_write_words(&this->in, word_addr, word_addr+size-1, &values[0]);
}

void
Memory_map_driver::read_bits (
	int                     bit_addr,
	std::vector<uint32_t> & values,
	int                     size
) {
	values.resize ((size+31)/32);
	fpga_segment_read_bits(&this->out, bit_addr, bit_addr+size-1, &values[0]);
}

void
Memory_map_driver::read_words (
	int                     word_addr,
	std::vector<uint32_t> & values,
	int                     size
) {
	values.resize (size);
	fpga_segment_read_words(&this->out, word_addr, word_addr+size-1, &values[0]);
}

void
Memory_map_driver::reset (bool enable) {}

void
Memory_map_driver::run (bool enable) {}

void
Memory_map_driver::step (unsigned int number) {
	fpga_segment_clock_step( &this->clock, number );
}
