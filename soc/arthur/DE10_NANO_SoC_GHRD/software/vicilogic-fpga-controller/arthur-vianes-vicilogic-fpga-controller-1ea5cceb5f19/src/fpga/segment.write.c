/// @copyright  Copyright @ 2013-2017, National University of Ireland,
///             Galway (NUI Galway). All Rights Reserved.
///
/// @License  No part of this software may be reproduced, stored or transmitted
///           in any form, electronic or otherwise without the prior written
///           permission of NUI Galway.
///
/// Contact:  Dr Fearghal Morgan, Room 3042, Engineering Building,
///           National University of Ireland Galway,
///           Galway, Ireland
/// Email:    fearghal.morgan@nuigalway.ie
///
/// @brief    Segment memory
/// @author   Arthur Vianes (arthur.vianes@gmail.com)
/// @date     July 2017

#include "fpga/segment.h"
#include "utils/debug.h"

/// Write a word at index
/// @param[in,out]  that   Segment object to write
/// @param[in]      index  Word address
/// @param[in]      value  Value to write
void
fpga_segment_write_word (
	fpga_segment_t *that,
	int             index,
	uint32_t        value
) {
	log("fpga_segment_write_word([that = %p], %d, %d = 0x%08X)", that, index, value, value);
	that->mem[index].as_word = value;
}

/// Write a bit at index
/// @param[in,out]  that   Segment object to write
/// @param[in]      index  Bit address
/// @param[in]      value  Values to write
void fpga_segment_write_bit (
	fpga_segment_t *that,
	int             index,
	int             value
) {
	log("fpga_segment_write_bit([that = %p], %d, %d)", that, index, value);
	fpga_segment_write_word( that, index/32,
		(  fpga_segment_read_word(that, index/32) & ~(1<<(index%32))) |
		(value ? 1<<(index%32) : 0)
	);
}

/// Write a range of bits from low to high
/// @param[in,out]  that    Segment object to write
/// @param[in]      low     Low  bit address (index included)
/// @param[in]      high    High bit address (index included)
/// @param[in]      values  Values to write
static void
fpga_segment_write_bits_ordered  (
	fpga_segment_t *that,
	int             l,
	int             h,
	uint32_t const *values
) {
	log("fpga_segment_write_bits_ordered([that = %p], %d, %d, [data])", that, l, h);
	assert(l<=h);
	const int low_bit_offset  = l % 32; const int low_bit_offset_revers  = 32-low_bit_offset;
	const int low_addr_32bit  = l / 32;
	const int high_bit_offset = h % 32;
	const int high_addr_32bit = h / 32;

	uint32_t value      = fpga_segment_read_word(that, low_addr_32bit),
	         next_value = 0;
	int i=0;
	for( ; i<high_addr_32bit-low_addr_32bit ; i++ ) {
		next_value =((values[i] >> low_bit_offset_revers) & uint32_mask_0(low_bit_offset, 31));  // To prepare next loop
		value      =( value                               & uint32_mask_0(low_bit_offset, 31)) | // To finish the last preparation
		            ((values[i] << low_bit_offset       ) &~uint32_mask_0(low_bit_offset, 31));
		fpga_segment_write_word(that, low_addr_32bit+i, value);
		value = next_value;
	}

	value      =( value                        & uint32_mask_0(low_bit_offset, high_bit_offset)) |
	            ((values[i] << low_bit_offset) &~uint32_mask_0(low_bit_offset, high_bit_offset));
	if(i!=0) // fpga already read in case of non-looping (in 'value')
		value |=( fpga_segment_read_word(that, low_addr_32bit+i) & uint32_mask_0(0, high_bit_offset));
	fpga_segment_write_word(that, low_addr_32bit+i, value);
}

/// Write a range of bits from low to high
/// @param[in,out]  that    Segment object to write
/// @param[in]      low     Low  bit address (index included)
/// @param[in]      high    High bit address (index included)
/// @param[in]      values  Values to write
static void
fpga_segment_write_bits_reverse  (
	fpga_segment_t *that,
	int             l,
	int             h,
	uint32_t const *values
) {
	log("fpga_segment_write_bits_reverse([that = %p], %d, %d, [data])", that, l, h);
	assert(l>=h);
	const int low_bit_offset  = 31 - (l % 32); const int low_bit_offset_revers  = 32-low_bit_offset;
	const int high_bit_offset = 31 - (h % 32);
	const int low_addr_32bit  = l / 32;
	const int high_addr_32bit = h / 32;

	uint32_t value      = fpga_segment_read_word(that, low_addr_32bit),
	         next_value = 0;
	int i=0;
	for( ; i<low_addr_32bit-high_addr_32bit ; i++ ) {
		next_value =((uint32_reverse(values[i] >> low_bit_offset_revers)) & uint32_mask_1(low_bit_offset_revers, 31)); // To prepare next loop
		value      =( value                                               & uint32_mask_1(low_bit_offset_revers, 31))  // To finish the last preparation
		           |((uint32_reverse(values[i] << low_bit_offset)) &~uint32_mask_1(low_bit_offset_revers, 31));
		fpga_segment_write_word(that, low_addr_32bit+i, value);
		value = next_value;
	}

	value      =( value                                      & uint32_mask_0(31-high_bit_offset, 31-low_bit_offset)) |
	            (uint32_reverse(values[i] << low_bit_offset) &~uint32_mask_0(31-high_bit_offset, 31-low_bit_offset));
	if(i!=0) // fpga already read in case of non-looping (in 'value')
		value |=( fpga_segment_read_word(that, low_addr_32bit+i) & uint32_mask_0(0, high_bit_offset));
	fpga_segment_write_word(that, low_addr_32bit+i, value);
}

/// Write a range of bits from low to high
/// @param[in,out]  that    Segment object to write
/// @param[in]      low     Low  bit address (index included)
/// @param[in]      high    High bit address (index included)
/// @param[in]      values  Values to write
void
fpga_segment_write_bits  (
	fpga_segment_t *that,
	int             l,
	int             h,
	uint32_t const *values
) {
	if(l <= h)
		return fpga_segment_write_bits_ordered(that, l, h, values);
	else
		return fpga_segment_write_bits_reverse(that, l, h, values);
}

/// Write a range of words from low to high
/// @param[in,out]  this    Segment object to writer
/// @param[in]      low     Low  word address (index included)
/// @param[in]      high    High word address (index included)
/// @param[in]      values  Values to writer
void
fpga_segment_write_words (
fpga_segment_t *that,
int             low,
int             high,
uint32_t const *values
) {
	log("fpga_segment_write_words([that = %p], %d, %d, [data])", that, low, high);
	for( int addr=low, i=0 ; addr<=high ; i++, addr++ )
		fpga_segment_write_word(that, addr, values[i]);
}
