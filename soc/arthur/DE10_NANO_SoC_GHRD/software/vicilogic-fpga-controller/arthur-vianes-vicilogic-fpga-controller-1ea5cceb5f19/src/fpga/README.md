# FPGA
A namespace for every libraries & class specific for FPGA.
****

## types [C methods for `uint32_t`]
Contains  functions to manipulate 32 bit registers,  such as masking, reversing
bits, ...
****

## fpga [C objects]
A labrary to manipulate fpga file mapping.

An fpga mapping contain fpga_segment.

Provide a  `fpga_t` structure to represente fpga connection and memory mapping.

**Exemple**
```c
#include "fpga/fpga.h"

fpga_t* fpga = FPGA_DEFAULT; //
fpga_open_autoClose(fpga); //
// fpga_open(fpga); // if you want to control when fpga connection was closed

// read a word on output segment of fpga at address 0:
uint32_t value = fpga_segment_read_word( &fpga->out, 0);
// write a word (value) on input segment of fpga at address 5:
fpga_segment_write_word( &fpga->in, 5, value);

// fpga_close(fpga); // if you use `fpga_open` instead of `fpga_open_autoClose`
```
****

## segment [C objects]

A driver to directly interact with an FPGA segment memory, using memory mapped
IO (mmap).

Contains functions to write and read to single bits, words, multiple words, and
unaligned ranges of bits or reverse ordered unaligned ranges of bits.

### segment.read [C methods for `segment`]
A driver to directly interact with an FPGA, using memory mapped IO (mmap).  

### segment.write [C methods for `segment`]
A driver to directly interact with an FPGA, using memory mapped IO (mmap).  

### segment.control [C methods for `segment`]
A driver to directly interact with an FPGA, using memory mapped IO (mmap).  
Contains functions to write and read to control registers in the DLM core.

**TODO** : Need to modify to match new core with a writable 'step' value
****

## boards [C objects]

Defines address start and  address range for  various board configurations with
the DLM core.

New boards will need to be defined here as below.  
```c
/// PYNQ-Z1
fpga_t PYNQ_Z1_FPGA   = { "/dev/mem", 0x7AA00000, 0x10000,
                            {0,   1  }, //< clock segment
                            {0,   128}, //< in  segment
                            {128, 128}  //< out segment
                        };

/// DE10-Nano
fpga_t DE10_NANO_FPGA = { "/dev/mem", 0xff200000, 0x10000,
                            {0,   1  }, //< clock segment
                            {0,   128}, //< in  segment
                            {128, 128}  //< out segment
                        };
```
****

## memory_map_driver [C++ class]
A set of C++ functions to interact with an FPGA.  
The C++ functions call C code from control.c.  

For boards that have different IO methods to PYNQ/DE10-nano, the implementation
of this class could be changed to allow for other IO methods.
****

**Authors**  Arthur Vianes <arthur.vianes@gmail.com>,
             Timlin-Canning Niall <N.TIMLIN1@nuigalway.ie>  
**Date**     July 2017  
**For**      http://vicilogic.com/

Copyright @ 2013-2017, National University of Ireland, Galway (NUI Galway).
All Rights Reserved.  
No part of this software may be reproduced, stored or transmitted in any form,
electronic or otherwise without the prior written permission of NUI Galway.  

**Contact:**  Dr Fearghal Morgan, Room 3042, Engineering Building,
              National University of Ireland Galway,
              Galway, Ireland  
**Email:**    fearghal.morgan@nuigalway.ie
