#include "fpga/segment.h"
/*
void fpga_segment_clock_step(fpga_segment_t *that, int number) {
	if ( number>0 )
		for( int i=0  ;  i < number  ;  i++ )
			fpga_segment_clock_1step(that);
}
//*/
//*
void fpga_segment_clock_step(fpga_segment_t *that, int number) {
	fpga_segment_write_word(that, 1, number);
	usleep(500);
	fpga_segment_write_bit (that, 2, 1);
	usleep(500);
	fpga_segment_write_word(that, 1, 0);
	usleep(500);
	fpga_segment_write_bit (that, 2, 0);
	usleep(500);
}
//*/

/*
void fpga_segment_clock_1step(fpga_segment_t *that) {
	fpga_segment_write_bit(that, 1, 1);
	usleep(500);
	fpga_segment_write_bit(that, 1, 0);
	usleep(500);
}
//*/

//*
void fpga_segment_clock_1step(fpga_segment_t *that) {
	fpga_segment_clock_step(that, 1);
}
//*/

void fpga_segment_clock_freeRun(fpga_segment_t *that, int en) {
	fpga_segment_write_bit(that, 1, en ? 1 : 0);
}

void fpga_segment_clock_reset(fpga_segment_t *that) {
	fpga_segment_write_bit(that, 0, 1);
	usleep(500);
	fpga_segment_write_bit(that, 0, 0);
}
