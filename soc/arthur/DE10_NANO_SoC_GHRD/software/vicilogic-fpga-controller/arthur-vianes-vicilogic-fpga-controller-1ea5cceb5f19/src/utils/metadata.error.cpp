/// @copyright  Copyright @ 2013-2017, National University of Ireland,
///             Galway (NUI Galway). All Rights Reserved.
///
/// @License  No part of this software may be reproduced, stored or transmitted
///           in any form, electronic or otherwise without the prior written
///           permission of NUI Galway.
///
/// Contact:  Dr Fearghal Morgan, Room 3042, Engineering Building,
///           National University of Ireland Galway,
///           Galway, Ireland
/// Email:    fearghal.morgan@nuigalway.ie
///
/// @brief    Library to manage Vicilogic's metadata format on C++
/// @author   Arthur Vianes (arthur.vianes@gmail.com)
/// @date     July 2017

#include "utils/metadata.hpp"

using namespace Metadata;

///
Error::Error ( const std::string& message ) : _message(message) {}

///
Error::Error () : _message("Error null") {}

///
const char*
Error::what() const throw () {
	return _message.c_str();
}
