/// @copyright  Copyright @ 2013-2017, National University of Ireland,
///             Galway (NUI Galway). All Rights Reserved.
///
/// @License  No part of this software may be reproduced, stored or transmitted
///           in any form, electronic or otherwise without the prior written
///           permission of NUI Galway.
///
/// Contact:  Dr Fearghal Morgan, Room 3042, Engineering Building,
///           National University of Ireland Galway,
///           Galway, Ireland
/// Email:    fearghal.morgan@nuigalway.ie
///
/// @brief    Library to manage jsonrpc data on C++
/// @author   Arthur Vianes (arthur.vianes@gmail.com)
/// @date     July 2017

#include "utils/jsonrpc.hpp"

using namespace jsonrpc;

// Consantes ---------------------------------------------------------------------------
const jsonrpc::Error jsonrpc::PARSE_ERROR     (-32700, "Parse error"),
                     jsonrpc::INVALID_REQUEST (-32600, "Invalid Request"),
                     jsonrpc::INVALID_METHOD  (-32601, "Method not found"),
                     jsonrpc::INVALID_PARAMS  (-32602, "Invalid params"),
                     jsonrpc::INTERNAL_ERROR  (-32603, "Internal error"),
                     jsonrpc::SERVER_ERROR    (-32000, "Server error");

// Constructors / Destructors ----------------------------------------------------------

/// Constructor for Jsonrpc Error (code error and message error)
/// @param code    The error code.
/// @param message The error message.
Error::Error (
	int                code,
	const std::string& message
) :	_code (code),
	_message(message)
{}

/// Default constructor for Jsonrpc Error (code error and message error)
/// No error ( code=0, message="No Error")
Error::Error () :
	_code (0),
	_message("No Error")
{}

Error::Error (rapidjson::Value const& tree) {
	parse(tree);
}

// Accessors / Mutator --------------------------------------------------------
const char* Error::what() const throw () {
	return _message.c_str();
}

std::string const Error::message() const throw () {
	return _message;
}

int Error::code() const throw () {
	return _code;
}

void Error::build (rapidjson::Document & json) const {
	if ( ! json.IsObject() ) json.SetObject();

	rapidjson::Value value_code( code() );
	json.AddMember("code", value_code, json.GetAllocator());

	rapidjson::Value value_message;
	value_message.SetString( message().c_str(), message().size() );
	json.AddMember("message", value_message, json.GetAllocator());
}

void Error::parse (rapidjson::Value const& tree) {
	_code = tree["code"].GetInt();
	_message = tree["message"].GetString();
}
