/// @copyright  Copyright @ 2013-2017, National University of Ireland,
///             Galway (NUI Galway). All Rights Reserved.
///
/// @License  No part of this software may be reproduced, stored or transmitted
///           in any form, electronic or otherwise without the prior written
///           permission of NUI Galway.
///
/// Contact:  Dr Fearghal Morgan, Room 3042, Engineering Building,
///           National University of Ireland Galway,
///           Galway, Ireland
/// Email:    fearghal.morgan@nuigalway.ie
///
/// @brief    Library to manage Vicilogic's metadata format on C++
/// @author   Arthur Vianes (arthur.vianes@gmail.com)
/// @date     July 2017

#include "utils/metadata.hpp"

using namespace Metadata;

/// Create a parser for Metadata
/// This parser populate a Signal Map object
/// @param[in]   tree  metadata json tree
/// @param[out]  map   Map to populate
Parser::Parser(
	rapidjson::Document const & tree,
	::Metadata::Map           & map
) :_map(map) {
	_tree.CopyFrom(tree, _tree.GetAllocator());
}

/// Create a parser for Metadata json file
/// This parser populate a Signal Map object
/// @param[in]   file  File to parse
/// @param[out]  map   Map to populate
Parser::Parser(
	std::ifstream   & file,
	::Metadata::Map & map
) :_map(map) {
	rapidjson::IStreamWrapper isw(file);
	_tree.ParseStream(isw);
}

/// Parse every keys (key = signal name)
void Parser::parse() {
	for( rapidjson::Value::ConstMemberIterator itr = _tree.MemberBegin();
	     itr != _tree.MemberEnd();
	     ++itr )
		parse(itr->name.GetString());
}

/// Ask metadata parser to parse a specific key (Signal name)
/// @param[in]  key  Key to parse
/// @return          Parsed signal
/// @throw  Metadata::Error
Signal & Parser::parse(key_t key) {
	if(_map.has(key))
		return _map[key].verify_autoreference();
	if ( ! _tree.HasMember(key.c_str()) )
		throw Error("Key not found");

	::rapidjson::Value const& s_tree = _tree[key.c_str()];
	Signal *s;

	if(s_tree.HasMember("alias"))
		s = new Reference_memory();
	else
		s = new Direct_memory(_map.get_interface().get(key));

	_map.append(key, *s);
	s->parse(*this, s_tree);

	return *s;
}
