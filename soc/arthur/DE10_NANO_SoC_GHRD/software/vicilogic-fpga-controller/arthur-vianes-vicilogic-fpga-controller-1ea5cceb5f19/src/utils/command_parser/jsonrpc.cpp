#include "utils/command_parser/jsonrpc.hpp"

Command_parser_json::Command_parser_json(Metadata::Map & map) : _stop(false), _map(map) {}
Command_parser_json&
Command_parser_json::append (
	std::string const&   name,
	Command_parser_json::Task task
) {
	_tasks.emplace(name, task);
	return *this;
}

Command_parser_json&
Command_parser_json::append (
	const std::vector<std::string> names,
	Command_parser_json::Task task) {
	for (auto name : names)
		append(name, task);
	return *this;
}

void
Command_parser_json::run ( std::string const & name, rapidjson::Document &d, rapidjson::Document &result) {
	get(name) (*this, d, result);
}

void
Command_parser_json::stop () {
	_stop=true;
}

void
Command_parser_json::run (
	jsonrpc::Request  & req,
	jsonrpc::Response & res
) {
	rapidjson::Document result;
	run( req.get_method(),
	     req.get_params(),
	     result            );
	res.set_result(result);
}

Command_parser_json::Task
Command_parser_json::get ( std::string const & name) {
	if( ! _tasks.count(name) )
		throw jsonrpc::INVALID_METHOD;
	return _tasks[name];
}

void
Command_parser_json::run ( std::istream & in,
                      std::ostream & out ) {

	jsonrpc::Request  request;
	jsonrpc::Response response;

	try {
		in >> request;
	}
	catch(jsonrpc::Error& e) {
		response.set_error(e);
	}
	response.from(request);

	if( ! response.get_error() ) {
		try {
			run(request, response);
		}
		catch(jsonrpc::Error& e) {
			response.set_error(e);
		}
	}

	out  << response;
}

void
Command_parser_json::loop ( std::istream & in,
                       std::ostream & out ) {
	for( ; ! _stop ; )
		run(in, out);
}
