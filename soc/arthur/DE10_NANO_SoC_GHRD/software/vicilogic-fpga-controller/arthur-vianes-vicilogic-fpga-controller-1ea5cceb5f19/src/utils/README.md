# Utils
A nasmespace for general programming libraries.

****

## command_parser
A library class to parse an input command from shell.

****

## jsonrpc
A library manipulate jsonrpc objects (requests, responses, errors)

Refering to jsonrpc 2.0 specification  
http://www.jsonrpc.org/specification

**Note** : This implementation dont manage Batch  
**Note** : This implementation accept integer or null `id` field only

### jsonrpc.request
A library class to parse & build & manipulate jsonrpc requests

Refering to jsonrpc 2.0 specification - chapter **4 - Request object**  
http://www.jsonrpc.org/specification#request_object

### jsonrpc.response
A library class to parse & build & manipulate jsonrpc responses

Refering to jsonrpc 2.0 specification - chapter **5 - Response object**  
http://www.jsonrpc.org/specification#response_object

### jsonrpc.error
A library class defining jsonrpc error types & pre-build error

Refering to jsonrpc 2.0 specification - chapter **5.1 - Error Object**  
http://www.jsonrpc.org/specification#error_object

**Note** : This implementation has no `data` field.
****

## metadata


### metadata.parser
A library class to parse an input variable map, and build a metadata map of type
metadata.map

**Exemple :**
```cpp
#include "utils/metadata.hpp"

std::ifstream metadata_file("metadata.json"); // open metadata file

Metadata::Map map; // map to populate with metadata file content
Metadata::Parser metadata_parser(metadata_file, map);  // Initialize Parser

metadata_parser.parse(); // Parse metadata file
```

### metadata.map

A library class representing a metadata map of `{"Name" : Signal_Object}`

Signal objects can be of 2 distinct types:
- `Direct_memory`: Represents a signal which maps directly to memory  
  **e.g.** sysprobe, userctrl, userclk, etc.
- `Reference_memory`: Represents a signal which maps to another signal, through
  aliases  
  **i.e.** user design signals

All signals  must end up pointing at a  Direct_memory  signal,  or  metadata is
invalid.
****

**Authors**  Arthur Vianes <arthur.vianes@gmail.com>,
             Timlin-Canning Niall <N.TIMLIN1@nuigalway.ie>  
**Date**     July 2017  
**For**      http://vicilogic.com/

Copyright @ 2013-2017, National University of Ireland, Galway (NUI Galway).
All Rights Reserved.  
No part of this software may be reproduced, stored or transmitted in any form,
electronic or otherwise without the prior written permission of NUI Galway.  

**Contact:**  Dr Fearghal Morgan, Room 3042, Engineering Building,
              National University of Ireland Galway,
              Galway, Ireland  
**Email:**    fearghal.morgan@nuigalway.ie
