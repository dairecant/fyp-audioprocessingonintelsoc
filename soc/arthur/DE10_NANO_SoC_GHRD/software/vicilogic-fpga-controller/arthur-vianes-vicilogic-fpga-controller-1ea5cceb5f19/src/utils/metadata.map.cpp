/// @copyright  Copyright @ 2013-2017, National University of Ireland,
///             Galway (NUI Galway). All Rights Reserved.
///
/// @License  No part of this software may be reproduced, stored or transmitted
///           in any form, electronic or otherwise without the prior written
///           permission of NUI Galway.
///
/// Contact:  Dr Fearghal Morgan, Room 3042, Engineering Building,
///           National University of Ireland Galway,
///           Galway, Ireland
/// Email:    fearghal.morgan@nuigalway.ie
///
/// @brief    Library to manage Vicilogic's metadata format on C++
/// @author   Arthur Vianes (arthur.vianes@gmail.com)
/// @date     July 2017

#include "utils/metadata.hpp"

using namespace Metadata;


Map::Map(Interface& interface) : _interface(interface) {}

/// Test if a signal key exist on map
/// @param[in]  key  Key to test
/// @return      `true`: exist, `false`: don't exist
bool
Map::has (
	key_t const &key
) const {
	return _map.count(key) > 0;
}

#include "utils/debug.h"
/// Signal map accessor by key
/// @param[in]  key  Key to access signal
/// @return          Signal
Signal&
Map::operator[] (
	key_t const &key
) {
	Signal& value = _map.at(key);
	return value;
}

/// Signal map accessor by key
/// @param[in]  key  Key to access signal
/// @return          Signal
Signal&
Map::get (
	key_t const &key
) {
	boost::optional<Signal&> sig = get_optional(key);
	if ( sig )
		return *sig;
	else
		throw Error("unexisting key");
}

/// Signal map accessor by key
/// @param  key  Key to access
/// @return      Signal if exist
boost::optional<Signal&>
Map::get_optional (
	key_t const &key
) {
	if ( ! has(key) )
		return boost::none;
	else
		return (*this)[key];
}

/// Append a signal to signal map
/// @param[in]  key  Key to access
/// @param[in]  sig  Signal to append
void
Map::append (
	key_t const &key,
	Signal      &sig
) {
	_map.emplace(key, sig);
}

///
Interface &
Map::get_interface () const {
	return _interface;
}
