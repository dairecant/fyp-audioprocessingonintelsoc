/// @copyright  Copyright @ 2013-2017, National University of Ireland,
///             Galway (NUI Galway). All Rights Reserved.
///
/// @License  No part of this software may be reproduced, stored or transmitted
///           in any form, electronic or otherwise without the prior written
///           permission of NUI Galway.
///
/// Contact:  Dr Fearghal Morgan, Room 3042, Engineering Building,
///           National University of Ireland Galway,
///           Galway, Ireland
/// Email:    fearghal.morgan@nuigalway.ie
///
/// @brief    Library to manage Vicilogic's metadata format on C++
/// @author   Arthur Vianes (arthur.vianes@gmail.com)
/// @date     July 2017

#include "utils/metadata.hpp"
#include "utils/debug.h"

using namespace Metadata;


std::ostream& operator<<(std::ostream& stream, ::rapidjson::Document const& d) {
	rapidjson::OStreamWrapper osw(stream);

	rapidjson::Writer<rapidjson::OStreamWrapper> writer(osw);
	d.Accept(writer);

	return stream;
}

std::ostream& operator<<(std::ostream& stream, ::rapidjson::Value const& d) {
	rapidjson::OStreamWrapper osw(stream);

	rapidjson::Writer<rapidjson::OStreamWrapper> writer(osw);
	d.Accept(writer);

	return stream;
}

Direct_memory::Direct_memory (Metadata::Interface::Segment & segment)
: _segment(segment) {
}


void Direct_memory::write (
	int                           low,
	int                           high,
	std::vector<uint32_t> const & data
) {
	_segment.write(low, data, high-low+1);
}

void Direct_memory::read  (
	int                     low,
	int                     high,
	std::vector<uint32_t> & data
) {
	log("Direct_memory::read(%d, %d, [data])",low, high);
	_segment.read(low, data, high-low+1);
}

void Direct_memory::write (
	std::vector<uint32_t> const & data
) {
	write(0, size()-1, data);
}

void Direct_memory::read  (
	std::vector<uint32_t> & data
) {
	read(0, size()-1, data);
}

Signal& Direct_memory::parse (
	::Metadata::Parser      & map,
	::rapidjson::Value const& sign
) {
	if( ! sign.IsObject() ) throw ::Metadata::Error("Signal has to be an Object");

	if( ! sign.HasMember("type") )  throw ::Metadata::Error("Direct memory access signal need `type` field");
	if(sign["type"] == "bool") {
		_length = 1;
		return *this;
	}
	if ( ! sign.HasMember("range") ) throw ::Metadata::Error("Direct memory access signal need `range` field");

	rapidjson::Value const& range = sign["range"];
	if( ! range.IsArray() ) throw ::Metadata::Error("Range has to be an Array");

	switch (range.Size()) {
		case 1: { // Segment
			if(range[0].IsInt()) {
				_length_min = _length_max = _length = range[0].GetInt();
			}
			else  throw ::Metadata::Error("Incorrect Range");
			break;
		}
		case 2: { // Segment
			_length_min = range[0].GetInt();
			_length_max = range[1].GetInt();
			_length     = _length_max;
			break;
		}
		default: throw ::Metadata::Error("Bad range description");
	}

	return *this;
}

Reference_memory::Reference_memory() : _ref(NULL), _loop(0) {}
Reference_memory::Reference_memory(Signal & ref) : _ref(&ref), _loop(0) {}

void Reference_memory::write (
	int                           low,
	int                           high,
	std::vector<uint32_t> const & data
) {
	if(!_ref) throw "Undefined reference";
	_ref->write(low+_low_bit, high+_low_bit, data);
}

void Reference_memory::write (
	std::vector<uint32_t> const & data
) {
	write(0, size()-1, data);
}

void Reference_memory::read  (
	int                     low,
	int                     high,
	std::vector<uint32_t> & data
) {
	if(high>=size()) throw Error("Out of range");
	if(!_ref) throw "Undefined reference";
	_ref->read(low+_low_bit, high+_low_bit, data);
}

void Reference_memory::read  (
	std::vector<uint32_t> & data
) {
	read(0, size()-1, data);
}

Signal& Reference_memory::verify_autoreference() {
	if(_loop) throw Error("Loop !");
	return *this;
}

Signal& Direct_memory::verify_autoreference() {
	return *this;
}

int Reference_memory::size() const {
	return _high_bit - _low_bit+1;
}

int Direct_memory::size() const {
	return _length;
}

Signal& Reference_memory::parse (
	::Metadata::Parser      & map,
	::rapidjson::Value const& sign
) {
	if(_loop ) throw ::Metadata::Error("Alias loop");
	_loop = 1;
	if( ! sign.IsObject() ) throw ::Metadata::Error("Signal has to be an Object");
	if ( ! sign.HasMember("alias") ) throw ::Metadata::Error("Signal need alias");

	rapidjson::Value const& alias = sign["alias"];
	if( ! alias.IsArray() ) throw ::Metadata::Error("Alias has to be an Array");

	for(rapidjson::SizeType i=0 ; i<alias.Size() ; i++) {
		if( ! alias[i].IsArray() ) throw ::Metadata::Error("Alias content has to be Arrays");

		switch (alias[i].Size()) {
			case 1: { // Variable
				key_t parent = alias[i][0].GetString();
				_ref = & map.parse(parent);
				_loop = 0;
				return *this;
			}
			case 2: {
				key_t parent = alias[i][0].GetString();
				_ref = & map.parse(parent);
				_loop = 0;
				if(alias[i][1].IsArray()) {
					switch (alias[i][1].Size()) {
						case 2:
							_low_bit  = alias[i][1][0].GetInt();
							_high_bit = alias[i][1][1].GetInt();
							break;
						case 1:
						default:
							break;
					}
				}
				return *this;
			}
			case 3: { // Segment
				::std::string parent = alias[i][0].GetString();
				_low_bit  = alias[i][1].GetInt();
				_high_bit = alias[i][2].GetInt();
				_ref  = & map.parse(parent);
				_loop = 0;
				return *this;
			}
			default: throw ::Metadata::Error("Bad alias description");
		}
	}
	_loop = 0;
	return *this;
}

void Interface_test::Segment::write (
	int                          addr,
	std::vector<uint32_t> const& data,
	int                          size
) {
	std::cout << "Interface::Segment::write("<<addr<<", "
	                                         <<"[data]"<<", "
	                                         <<size<<
	             ")" << std::endl;
}

void Interface_test::Segment::read  (
	int                    addr,
	std::vector<uint32_t>& data,
	int                    size
) {
	std::cout << "Interface::Segment::read("<<addr<<", "
	                                        <<"[data]"<<", "
	                                        <<size<<
	             ")" << std::endl;
}

void Interface_test::Segment::write (
	int                          addr,
	std::vector<uint32_t> const& data
) {
	std::cout << "Interface::Segment::write("<<addr<<", "
	                                         <<"[data]"
	          << ")" << std::endl;
}

void Interface_test::Segment::read  (
	int                    addr,
	std::vector<uint32_t>& data
) {
	std::cout << "Interface::Segment::read("<<addr<<", "
	                                        <<"[data]"
	          << ")" << std::endl;
}

void Interface_test::Segment::write (
	std::vector<uint32_t> const& data
) {
	std::cout << "Interface::Segment::write("<<"[data]"<<")" << std::endl;
}

void Interface_test::Segment::read  (
	std::vector<uint32_t>& data
) {
	std::cout << "Interface::Segment::read(" <<"[data]"<< ")" << std::endl;
}
