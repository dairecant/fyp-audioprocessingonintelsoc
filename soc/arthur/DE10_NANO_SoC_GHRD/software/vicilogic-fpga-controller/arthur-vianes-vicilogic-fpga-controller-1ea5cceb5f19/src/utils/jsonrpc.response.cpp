/// @copyright  Copyright @ 2013-2017, National University of Ireland,
///             Galway (NUI Galway). All Rights Reserved.
///
/// @License  No part of this software may be reproduced, stored or transmitted
///           in any form, electronic or otherwise without the prior written
///           permission of NUI Galway.
///
/// Contact:  Dr Fearghal Morgan, Room 3042, Engineering Building,
///           National University of Ireland Galway,
///           Galway, Ireland
/// Email:    fearghal.morgan@nuigalway.ie
///
/// @brief    Library to manage jsonrpc data on C++
/// @author   Arthur Vianes (arthur.vianes@gmail.com)
/// @date     July 2017

#include "utils/jsonrpc.hpp"

using namespace jsonrpc;


Response::Response () : Base<2,0>::Base() {}
Response::Response (Request const& request) :
	Base<2,0>::Base(request) {
}

Response::Response (jsonrpc::Error const& err) :
	Base<2,0>::Base(), _error(err) {
}

Response::Response ( jsonrpc::Request const& request,
                     jsonrpc::Error   const& err      ) :
	Base<2,0>::Base(request), _error(err) {
}

/// Build a rapidjson doc object from a jsonrpc response
/// @param[out]  tree  json object to build
void
Response::build(rapidjson::Document& tree) const {
	rapidjson::Document::AllocatorType& allocator = tree.GetAllocator();
	if(! tree.IsObject()) tree.SetObject();
	Base::build(tree);

	if( _error )  {
		rapidjson::Document error(rapidjson::kObjectType, &tree.GetAllocator());
		_error->build(error);
		tree.AddMember("error", error, allocator);
	} else {
		tree.AddMember("result",_result, allocator);
	}
}

boost::optional<jsonrpc::Error>
Response::get_error() {
	return _error;
}

void
Response::set_error(jsonrpc::Error const& error) {
	_error = error;
}

void
Response::from(Request const & req) {
	if(req.has_id())
		this->set_id(req.get_id());
}

void
Response::set_result (rapidjson::Document const& result) {
	_result.CopyFrom(result, _result.GetAllocator());
}
rapidjson::Document&
Response::get_result () {
	return _result;
}

/// Parse a rapidjson object and extract jsonrpc result or error
/// @param[in]  tree  json object to parse
void
Response::parse (rapidjson::Value const & tree) {
	rapidjson::Document json;
	rapidjson::Document::AllocatorType& allocator = json.GetAllocator();
	Base::parse(tree);
	if(tree.HasMember("error"))
		_error = Error(tree["error"]);
	else _result.CopyFrom(tree["result"], allocator);
}

/// Overloading << operator for output streams & jsonrpc::response objects
std::ostream&
operator << ( std::ostream      & stream,
              jsonrpc::Response & response ) {
	rapidjson::Document json;
	response.build(json);
	stream << json;
	return stream;
}

// Overloading >> operator for input streams & jsonrpc::response objects
std::istream&
operator >> ( std::istream      & stream,
              jsonrpc::Response & response ) {
	rapidjson::IStreamWrapper stream_warp(stream);
	rapidjson::Document json;
	json.ParseStream(stream_warp);

	response.parse(json);

	return stream;
}
