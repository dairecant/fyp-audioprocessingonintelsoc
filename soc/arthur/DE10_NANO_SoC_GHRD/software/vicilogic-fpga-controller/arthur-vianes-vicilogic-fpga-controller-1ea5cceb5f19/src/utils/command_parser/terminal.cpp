#include "utils/command_parser/terminal.hpp"

#include <regex>
#include <boost/regex.hpp>

std::vector<std::string>
Command_parser::lexical_analysis (const std::string &line) const {

	std::vector<std::string> tokens;

	const boost::regex re("(?:\"((?:\\\\\"|[^\"])*)\"|([^\\s\"]+))");
	boost::sregex_iterator res(line.begin(), line.end(), re);
	boost::sregex_iterator end;

	for (; res != end; ++res){
		std::string v1 = (*res)[1], v2 = (*res)[2], value=v1+v2;
		tokens.insert (tokens.end(), value);
	}

	return tokens;
}

Command_parser::Arguments::Arguments(Command_parser& shell, const std::vector<std::string>& args) :
 _shell(shell), _args(args) {}

Command_parser::Command_parser(std::string start_message) :
_exit(false),
_start_message(start_message) {}

void
Command_parser::run (const std::vector<std::string> &tokens) {
	try {
		auto func = _commands[tokens[0]];
		Command_parser::Arguments arg = Command_parser::Arguments(*this, tokens);
		if(func) func(arg);
		else     std::cout << "Unknown commands. See `help`.\n";
	}
	catch(std::string error) {
		std::cerr << error << std::endl;
	}
}

void Command_parser::append(const std::vector<std::string> names, Command_parser::Command func, std::string text) {
	if(names.size() == 0)
		return;

	for(auto it : names)
		_commands.emplace(it, func);

	_help.emplace(names[0], text);
}

void Command_parser::help() {
	size_t length = 0;
	for(auto it : _help)
		if(it.first.size() > length)
			length = it.first.size();

	std::cout << std::endl
	          << "Commands:" << std::endl;

	for(auto it : _help)
		std::cout << "    " << std::left << std::setfill(' ') << std::setw(length+2)
		                    << it.first << " - " << it.second << std::endl;
}

void Command_parser::start() {
	std::cout << _start_message << std::endl;

	_exit=false;
	do {
		std::string line;
		std::cout << ">> ";
		getline(std::cin, line);

		std::vector<std::string> tokens = lexical_analysis(line);
		run(tokens);
	} while( ! _exit );
}

void Command_parser::stop() {
	_exit=true;
}
