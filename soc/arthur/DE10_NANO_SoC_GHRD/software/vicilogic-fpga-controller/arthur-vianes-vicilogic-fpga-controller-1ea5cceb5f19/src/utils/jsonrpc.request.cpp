/// @copyright  Copyright @ 2013-2017, National University of Ireland,
///             Galway (NUI Galway). All Rights Reserved.
///
/// @License  No part of this software may be reproduced, stored or transmitted
///           in any form, electronic or otherwise without the prior written
///           permission of NUI Galway.
///
/// Contact:  Dr Fearghal Morgan, Room 3042, Engineering Building,
///           National University of Ireland Galway,
///           Galway, Ireland
/// Email:    fearghal.morgan@nuigalway.ie
///
/// @brief    Library to manage jsonrpc data on C++
/// @author   Arthur Vianes (arthur.vianes@gmail.com)
/// @date     July 2017

#include "utils/jsonrpc.hpp"

using namespace jsonrpc;

Request::Request() :
	Base::Base<>(),
	_method("")
{}

Request::Request( std::string const & method ) :
	Base::Base<>(),
	_method(method)
{}

Request::Request( Base<>      const & base,
                  std::string const & method )
: Base::Base<>(base), _method(method) {}

void
Request::set_method (std::string const & method) {
	_method=method;
}

std::string
Request::get_method ( ) const {
	return _method;
}

rapidjson::Document&
Request::get_params () {
	return _params;
}

/// Build a rapidjson doc object from a jsonrpc request
/// @param[out]  tree  json object to build
void
Request::build (rapidjson::Document& tree) const {
	Base::build(tree);
	rapidjson::Document::AllocatorType& allocator = tree.GetAllocator();

	rapidjson::Value method;
	method.SetString(_method.c_str(), _method.length(), allocator);

	rapidjson::Document params;
	params.CopyFrom(_params, allocator);

	tree.AddMember("method", method, allocator);
	tree.AddMember("params", params, allocator);
}

/// Parse a rapidjson object and extract jsonrpc method & params
/// @param[in]  tree  json object to parse
void
Request::parse (rapidjson::Value const & tree) {
	rapidjson::Document d;
	rapidjson::Document::AllocatorType& allocator = d.GetAllocator();

	if( ! tree.IsObject()          ||
	    ! tree.HasMember("method") ||
	    ! tree.HasMember("params")    )
		throw INVALID_REQUEST;

	Base::parse(tree);
	_method = tree["method"].GetString();
	_params.CopyFrom(tree["params"], allocator);
}

// Stream Operator ------------------------------------------------------------
/// Overloading << operator for output streams & jsonrpc::request objects
std::ostream&
operator << ( std::ostream & stream, jsonrpc::Request & request ) {
	rapidjson::Document json;
	request.build(json);
	stream << json;

	return stream;
}

/// Overloading >> operator for input streams & jsonrpc::request objects
std::istream&
operator >> ( std::istream & stream, jsonrpc::Request & request ) {
	rapidjson::Document json;

	std::vector<char> str;
	char c;
	int count = 0;
	bool start = false;
	do {
		c = stream.get();
		if(c=='{') count++;
		if(c=='}') count--;
		if(c=='\n' || c=='\r') continue;
		str.push_back(c);
		start |= count;
	//	std::cerr <<"char = "<< c << std::endl;
	} while ((count>0 || !start) && c!='\0');

	std::string true_string(str.begin(), str.end());

//	std::cerr <<"true_string = "<< true_string << std::endl;

	json.Parse(true_string.c_str()); //parse a string into a json object / "json tree"
	request.parse(json); //parse a json object into a request (extract method, params, etc)
	/*MyIStreamWrapper stream_warp(stream);
	rapidjson::ParseResult ok = json.ParseStream(stream_warp);
	if(!ok) throw jsonrpc::PARSE_ERROR;

	request.parse(json);
	stream.get();*/
	return stream;
}
