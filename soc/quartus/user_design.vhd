 entity user_design is 
 Port ( audio_in : in STD_LOGIC_VECTOR(15 downto 0);
           audio_out : out STD_LOGIC_VECTOR(15 downto 0);
           user_clk: in std_logic;
			  user_rst: in std_logic
           --options : in STD_LOGIC_VECTOR(0 to 3);--parameter selection
           enable : in STD_LOGIC_VECTOR(0 to 3) --multi-bit bus to enable other effects
			  
       );
end user_design;