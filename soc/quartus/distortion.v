
module distortion (
	 user_clk,
	 user_rst,
	 sound,
	 clipval,
	 enable,
	 out_sound

);

	input user_clk;
	input user_rst;
	input sound [SAMPLE_SIZE-1:0];
	input clipval;
	input enable;
	output out_sound;
//'include "effects_params.v"
always @(posedge user_clk or posedge user_rst) begin
if (enable) 
begin
	if (signed(sound[15:0]) > 0.05/*clipVal*/ )
		sound <= clipVal;
	else if (signed(sound[15:0]) < -1*0.05) 
			sound<=-1*clipVal;
	// end else if
	else
			sound<=sound;
			//end else
 //end if enable statement
				

		
