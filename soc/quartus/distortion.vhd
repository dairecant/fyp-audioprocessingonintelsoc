
--			DISTORTION MODULE
--		
--	Works by clipping either side of audio wave
--	audio_in: input audio (16 bit floating point audio sample between -1 and +1
--	audio_out: output distorted audio 
--	user_clk : input user clock
--	enable : "switch" which enables or disables effect
--	
--	Author: Dáire Canavan
--	Date	: 03/11/2017


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity Distortion is
    Port ( audio_in : in STD_LOGIC_VECTOR(15 downto 0);
           audio_out : out STD_LOGIC_VECTOR(15 downto 0);
           user_clk: in std_logic;
           --options : in STD_LOGIC_VECTOR(0 to 3);
           enable : in STD_LOGIC_VECTOR(0 to 3) --multi-bit bus to enable other effects
       );
end Distortion;

architecture Behavioral of Distortion is

process(user_clk)
begin
if enable(0)= '1' then
if rising_edge(clk_48) then 
    if signed(audio_in(15 downto 0)) >= 70000 then
      y<=std_logic_vector(to_signed(90000,16));    
    elsif signed(audio_in(15 downto 0)) <= -70000 then
      y<=std_logic_vector(to_signed(-90000,16));
    else
      y<=audio_in;
    end if;
	 

      
end if; --rising_edge(clk)
end process;

end Behavioral;