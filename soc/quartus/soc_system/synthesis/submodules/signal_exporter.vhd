library IEEE;

use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity signal_exporter is
	port (
		-- AVALON (AXI) SLAVE
		slave_address    : in  std_logic_vector( 7 downto 0)   := (others => '0'); -- slave.address
		slave_read       : in  std_logic                       := '0';             --      .read
		slave_read_data  : out std_logic_vector(31 downto 0);                      --      .read.data
		slave_write      : in  std_logic                       := '0';             --      .write
		slave_write_data : in  std_logic_vector(31 downto 0)   := (others => '0'); --      .write.data
		slave_clock      : in  std_logic                       := '0';             --      .clock
		slave_reset      : in  std_logic                       := '0';             --      .reset

		-- SIGNALS IMPORTED EXPORTED
		signal_out      : out std_logic_vector(4095 downto 0) ;                   -- signal.out
		signal_in       : in  std_logic_vector(4095 downto 0) := (others => '0'); -- signal.in

		-- USER CLOCK/RESET
		user_clock     : out std_logic;                                           -- user.clock
		user_reset     : out std_logic                                            --     .reset
	);
end entity signal_exporter;

architecture RTL of signal_exporter is
	-- MEMORY REGISTERS -------------------------------------------------------
	signal memory_in  : std_logic_vector(4095 downto 0); -- Input memory
	signal memory_out : std_logic_vector(4095 downto 0); -- Output memory

	-- CONTROL REGISTERS ------------------------------------------------------
	signal mem_addr   : std_logic_vector(7 downto 0);    -- address register
	signal mem_w_en   : std_logic;                       -- memory write enable
	signal mem_r_en   : std_logic;                       -- memory read enable

	-- INTERNAL REGISTERS -----------------------------------------------------
	signal data_in0   : std_logic_vector(7 downto 0);    -- INput registers
	signal data_in1   : std_logic_vector(7 downto 0);    --
	signal data_in2   : std_logic_vector(7 downto 0);    --
	signal data_in3   : std_logic_vector(7 downto 0);    --
	signal data_out0  : std_logic_vector(7 downto 0);    -- OUTput registers
	signal data_out1  : std_logic_vector(7 downto 0);    --
	signal data_out2  : std_logic_vector(7 downto 0);    --
	signal data_out3  : std_logic_vector(7 downto 0);    --

	-- USER CLOCK/RESET CONTROL
	component CLK_RST_CONTROL IS
		port  (
			CLK                    : IN  STD_LOGIC;
			RST                    : IN  STD_LOGIC;
			VL_UserClk             : OUT STD_LOGIC;
			VL_UserRst             : OUT STD_LOGIC;
			COMMAND_RST            : IN  STD_LOGIC;
			COMMAND_CLK_FREE_RUN   : IN  STD_LOGIC;
			COMMAND_CLK_STEP       : IN  STD_LOGIC;
			COMMAND_CLK_HALF_STEP  : IN  STD_LOGIC
		);
	END component;

begin
		--	reset processing
	process(slave_reset, slave_clock, memory_in, signal_in)--, memory_in(2))
	begin
		if slave_reset = '1' then
			memory_out <= ( others => '0' ) ;
		else
			-- memory assignments
			signal_out <= memory_in ;
			memory_out <= signal_in ;
		end if ;
	end process ;


	-- Memory addressing starts here
	mem_addr <= slave_address ;

	-- Implement Block RAM(s)
	mem_w_en <= slave_write ;
	mem_r_en <= slave_read ;


	-- Assigning 8 bit data
	data_in0 <= slave_write_data( 7  downto  0 );
	data_in1 <= slave_write_data( 15 downto  8 );
	data_in2 <= slave_write_data( 23 downto 16 );
	data_in3 <= slave_write_data( 31 downto 24 );


	process(mem_addr, memory_in, memory_out)
	begin
	--mem_addr goes from 0 to 127 for inputs and 128 to 255 for outputs
		if (to_integer(unsigned(mem_addr)) < 128) then
			data_out0 <= memory_in( (to_integer(unsigned(mem_addr))*32)+7 +  0        downto   (to_integer(unsigned(mem_addr))*32) +  0 );
			data_out1 <= memory_in( (to_integer(unsigned(mem_addr))*32)+7 +  8        downto   (to_integer(unsigned(mem_addr))*32) +  8 );
			data_out2 <= memory_in( (to_integer(unsigned(mem_addr))*32)+7 + 16        downto   (to_integer(unsigned(mem_addr))*32) + 16 );
			data_out3 <= memory_in( (to_integer(unsigned(mem_addr))*32)+7 + 24        downto   (to_integer(unsigned(mem_addr))*32) + 24 );
		else
			data_out0 <= memory_out( (to_integer(unsigned(mem_addr))*32)+7-4096 +  0   downto   (to_integer(unsigned(mem_addr))*32)-4096 +  0 );
			data_out1 <= memory_out( (to_integer(unsigned(mem_addr))*32)+7-4096 +  8   downto   (to_integer(unsigned(mem_addr))*32)-4096 +  8 );
			data_out2 <= memory_out( (to_integer(unsigned(mem_addr))*32)+7-4096 + 16   downto   (to_integer(unsigned(mem_addr))*32)-4096 + 16 );
			data_out3 <= memory_out( (to_integer(unsigned(mem_addr))*32)+7-4096 + 24   downto   (to_integer(unsigned(mem_addr))*32)-4096 + 24 );
		end if;
	end process;

	-- WRITING PROCESS
	BYTE_RAM_PROC : process( slave_clock ) is
	begin
		if ( rising_edge (slave_clock) ) then
			if ( mem_w_en = '1' and to_integer(unsigned(mem_addr)) < 128 ) then
				memory_in((to_integer(unsigned(mem_addr))*32)+7 +  0  downto (to_integer(unsigned(mem_addr))*32) +  0) <= data_in0;
				memory_in((to_integer(unsigned(mem_addr))*32)+7 +  8  downto (to_integer(unsigned(mem_addr))*32) +  8) <= data_in1;
				memory_in((to_integer(unsigned(mem_addr))*32)+7 + 16  downto (to_integer(unsigned(mem_addr))*32) + 16) <= data_in2;
				memory_in((to_integer(unsigned(mem_addr))*32)+7 + 24  downto (to_integer(unsigned(mem_addr))*32) + 24) <= data_in3;
			end if;
		end if;

	end process BYTE_RAM_PROC;

	-- READING PROCESS
	process( slave_clock ) is
	begin
		if ( rising_edge(slave_clock) ) then
			if ( mem_r_en = '1') then                         -- bit 31       24 23       16  15       8   7       0
				slave_read_data(  7 downto  0 ) <= data_out0; --      .... ....   .... ....    .... ....   XXXX XXXX
				slave_read_data( 15 downto  8 ) <= data_out1; --      .... ....   .... ....    XXXX XXXX   .... ....
				slave_read_data( 23 downto 16 ) <= data_out2; --      .... ....   XXXX XXXX    .... ....   .... ....
				slave_read_data( 31 downto 24 ) <= data_out3; --      XXXX XXXX   .... ....    .... ....   .... ....
			end if;
		end if;
	end process;

	-- PORT MAPS GO BELOW HERE
	-- Clock controller port map goes here, connected to Avalon interface
	clkControlFSM_i: CLK_RST_CONTROL port map (
		CLK                   => slave_clock,  --: IN  STD_LOGIC;
		RST                   => slave_reset,  --: IN  STD_LOGIC;
		VL_UserClk            => user_clock,   --: OUT STD_LOGIC;
		VL_UserRst            => user_reset,   --: OUT STD_LOGIC;
		COMMAND_RST           => memory_in(0), --: IN  STD_LOGIC;
		COMMAND_CLK_FREE_RUN  => memory_in(1), --: IN  STD_LOGIC;
		COMMAND_CLK_STEP      => memory_in(2), --: IN  STD_LOGIC;
		COMMAND_CLK_HALF_STEP => memory_in(3)  --: IN  STD_LOGIC
	);

end architecture RTL; -- OF signal_exporter
