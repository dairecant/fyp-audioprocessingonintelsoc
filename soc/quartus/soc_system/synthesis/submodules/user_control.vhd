library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity CLK_RST_CONTROL IS
	port  (
		CLK                   : IN  STD_LOGIC;
		RST                   : IN  STD_LOGIC;
		VL_UserClk            : OUT STD_LOGIC;
		VL_UserRst            : OUT STD_LOGIC;
		COMMAND_RST           : IN  STD_LOGIC;
		COMMAND_CLK_FREE_RUN  : IN  STD_LOGIC;
		COMMAND_CLK_STEP      : IN  STD_LOGIC;
		COMMAND_CLK_HALF_STEP : IN  STD_LOGIC
	);
END CLK_RST_CONTROL;

architecture RTL of CLK_RST_CONTROL is
	type stateType is (idle, stepState1, stepState0, halfStepState);
	signal CS, NS: stateType;
	signal toggleClk	 	    : std_logic;
	signal intClk   	 	    : std_logic := '0';

	BEGIN

	asgnVL_UserRst_i: VL_UserRst <= RST or COMMAND_RST;

	clkReg_i: process (clk)
	begin
		if CLK'event and CLK = '1' then
			if toggleClk = '1' then
				intClk <= not intClk;
			end if;
		end if;
	end process;
	asgnVL_UserClk_i: VL_UserClk <= intClk;


	stateReg_i: process (clk, rst)
	begin
		if rst='1' then
			CS <= idle;
		elsif clk'event and clk = '1' then
			CS <= NS;
		end if;
	end process;

	NSAndOPDec_i: process (CS, COMMAND_CLK_FREE_RUN, COMMAND_CLK_STEP, COMMAND_CLK_HALF_STEP)
	begin
		toggleClk <= '0';  -- default assignments
		NS 	     <= CS;
		case CS is

			when idle =>
			if    COMMAND_CLK_FREE_RUN = '1' then
				toggleClk <= '1';          -- synchronously toggle intClk on next clk active edge
			elsif COMMAND_CLK_STEP = '1' then
				toggleClk <= '1';          -- synchronously toggle intClk 0 to 1 on next clk active edge
				NS       <= stepState1;
			elsif COMMAND_CLK_HALF_STEP = '1' then
				toggleClk <= '1';          -- synchronously toggle intClk (0 to 1 or 1 to 0) on next clk active edge
				NS       <= halfStepState;
			end if;

			when stepState1 =>  			   -- high to low part of clk step
			NS       <= stepState0;
			toggleClk <= '1';          -- synchronously toggle intClk 1 to 0 on next clk active edge
			when stepState0 =>
			if COMMAND_CLK_STEP = '0' then      -- wait for command deassertion
			NS       <= idle;
		end if;

		when halfStepState =>
		if COMMAND_CLK_HALF_STEP = '0' then -- wait for command deassertion
		NS       <= idle;
	end if;
	when others =>
	null;
end case;
end process;
end RTL;
