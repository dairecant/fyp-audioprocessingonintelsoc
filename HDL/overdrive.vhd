library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity Distortion
(
	soundIn : in std_logic_vector(15 downto 0);
	soundOut: out std_logic_vector(15 downto 0);
	gain 	: in std_logic_vector(15 downto 0);
	clip 	: in std_logic_vector(15 downto 0);
	clk	 	: in std_logic;
);

architecture Behavioural of Distortion is


begin

process (clk,gain,clip)
	if rising_edge(clk) then
		if(signed(soundIn)>clip)
			soundOut <=std_logic_vector(to_signed(clip),16);
		elsif (signed(soundIn)<-1*clip)
			soundOut <=std_logic_vector(to_signed(-1)*to_signed(clip),16);
		else
			soundOut <=soundIn;
			
	end if;
	soundOut <= gain * soundOut;
end process;

end Behavioural;