library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

entity DSPCtrlr is
port (
-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_dataIn_signed :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_validPipe12_1 :	out std_logic;

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_validIn :	out std_logic;

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_coeff15 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_converterOut :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_31 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_sum4_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_sum6_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_sum0_1 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_coeff14 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_coeff10 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_coeff11 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_coeff12 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_coeff13 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_sum13_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_sum8_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_validPipe1_1 :	out std_logic;

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_validPipe15_1 :	out std_logic;

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_sum15_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_validPipe13_1 :	out std_logic;

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_validPipe9_1 :	out std_logic;

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_coeff2 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_8 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_9 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_4 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_5 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_6 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_7 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_1 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_2 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_3 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_validPipe3_1 :	out std_logic;

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_sum2_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_validPipe5_1 :	out std_logic;

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_sum5_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_21 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_coeff16 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_validPipe7_1 :	out std_logic;

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_validPipe11_1 :	out std_logic;

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_validPipe0_1 :	out std_logic;

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_sum7_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_sum11_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_sum9_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_validPipe14_1 :	out std_logic;

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_sum12_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_dataOut_tmp :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_validPipe8_1 :	out std_logic;

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_sum1_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_validPipe4_1 :	out std_logic;

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_sum3_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_30 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_validPipe10_1 :	out std_logic;

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_sum14_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_validPipe6_1 :	out std_logic;

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_final_sumPipe_1 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_sum10_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_final_sumValid :	out std_logic;

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_29 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_28 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_25 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_24 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_27 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_20 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_23 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_22 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_26 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_coeff4 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_coeff5 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_coeff6 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_coeff7 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_coeff0 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_coeff1 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_validPipe2_1 :	out std_logic;

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_coeff3 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_coeff8 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_coeff9 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_18 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_19 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_10 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_11 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_12 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_13 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_14 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_15 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_16 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_17 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_CSRdAdd :	out std_logic_vector( 9 downto 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_CSWrAdd :	out std_logic_vector( 9 downto 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_NSWrAdd :	out std_logic_vector( 9 downto 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_NSRdAdd :	out std_logic_vector( 9 downto 0 );

-- line auto generated [2018-03-19] DSPCtrlr_i
oo_validOut :	out std_logic;
clk :           in std_logic; 
      rst :        	  in std_logic; 		
      goDSPPulse:     in std_logic;
      selFirstEffect: in std_logic_vector(2 downto 0);  
	  selFinalEffect: in std_logic_vector(1 downto 0);  
	  ctrl0_0:        in std_logic_vector(15 downto 0);  
	  ctrl0_1:        in std_logic_vector(15 downto 0);  
	  ctrl1_0:        in std_logic_vector(15 downto 0);  
	  ctrl1_1:        in std_logic_vector(15 downto 0);  
	  ctrl2_0:        in std_logic_vector(15 downto 0);  
	  ctrl2_1:        in std_logic_vector(15 downto 0);  
	  ctrl3_0:        in std_logic_vector(15 downto 0);  
	  ctrl3_1:        in std_logic_vector(15 downto 0);  
      DSPDatFromMem:  in std_logic_vector(15 downto 0);  
      DSPMemRdAdd:      out std_logic_vector(9 downto 0);
      DSPMemWrAdd:      out std_logic_vector(9 downto 0);
      DSPDatToMem:    out std_logic_vector(15 downto 0);
      DSPMemWr:       out std_logic;
      DSPDone:        out std_logic
	  );
end DSPCtrlr;

architecture RTL of DSPCtrlr is

component DSPFunct is
port  (
-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_dataIn_signed :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_validPipe12_1 :	out std_logic;

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_validIn :	out std_logic;

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_coeff15 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_converterOut :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_31 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_sum4_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_sum6_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_sum0_1 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_coeff14 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_coeff10 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_coeff11 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_coeff12 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_coeff13 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_sum13_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_sum8_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_validPipe1_1 :	out std_logic;

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_validPipe15_1 :	out std_logic;

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_sum15_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_validPipe13_1 :	out std_logic;

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_validPipe9_1 :	out std_logic;

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_coeff2 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_8 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_9 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_4 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_5 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_6 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_7 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_1 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_2 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_3 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_validPipe3_1 :	out std_logic;

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_sum2_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_validPipe5_1 :	out std_logic;

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_sum5_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_21 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_coeff16 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_validPipe7_1 :	out std_logic;

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_validPipe11_1 :	out std_logic;

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_validPipe0_1 :	out std_logic;

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_sum7_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_sum11_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_sum9_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_validPipe14_1 :	out std_logic;

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_sum12_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_dataOut_tmp :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_validPipe8_1 :	out std_logic;

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_sum1_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_validPipe4_1 :	out std_logic;

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_sum3_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_30 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_validPipe10_1 :	out std_logic;

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_sum14_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_validPipe6_1 :	out std_logic;

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_final_sumPipe_1 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_sum10_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_final_sumValid :	out std_logic;

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_29 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_28 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_25 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_24 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_27 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_20 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_23 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_22 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_26 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_coeff4 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_coeff5 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_coeff6 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_coeff7 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_coeff0 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_coeff1 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_validPipe2_1 :	out std_logic;

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_coeff3 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_coeff8 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_coeff9 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_18 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_19 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_10 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_11 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_12 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_13 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_14 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_15 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_16 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_17 :	out std_logic_vector( 31 DOWNTO 0 );
clk :          in std_logic; 
       rst :          in std_logic; 
       selFirstEffect:in std_logic_vector(2 downto 0);  
	   selFinalEffect:in std_logic_vector(1 downto 0);  
	   ctrl0_0:       in std_logic_vector(15 downto 0);  
	   ctrl0_1:       in std_logic_vector(15 downto 0);  
	   ctrl1_0:       in std_logic_vector(15 downto 0);  
	   ctrl1_1:       in std_logic_vector(15 downto 0); 
       validOut:      out std_logic;  
	   ctrl2_0:       in std_logic_vector(15 downto 0);  
	   ctrl2_1:       in std_logic_vector(15 downto 0);  
	   ctrl3_0:       in std_logic_vector(15 downto 0);  
	   ctrl3_1:       in std_logic_vector(15 downto 0);  
       DSPDatFromMem: in std_logic_vector(15 downto 0);  
       DSPDatToMem:   out std_logic_vector(15 downto 0)
	   );
end component;

signal NSRdAdd :          std_logic_vector(9 downto 0);
signal CSRdAdd :          std_logic_vector(9 downto 0);   
signal NSWrAdd :          std_logic_vector(9 downto 0);
signal CSWrAdd :          std_logic_vector(9 downto 0);   
signal validOut:          std_logic;  

begin
-- line auto generated [2018-03-19] DSPCtrlr_i
oo_CSRdAdd <= CSRdAdd;
-- line auto generated [2018-03-19] DSPCtrlr_i
oo_CSWrAdd <= CSWrAdd;
-- line auto generated [2018-03-19] DSPCtrlr_i
oo_NSWrAdd <= NSWrAdd;
-- line auto generated [2018-03-19] DSPCtrlr_i
oo_NSRdAdd <= NSRdAdd;
-- line auto generated [2018-03-19] DSPCtrlr_i
oo_validOut <= validOut;

DSPFunct_i: DSPFunct 
port map (
-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_dataIn_signed => oo_DSPFunct_i_effect0_i_dataIn_signed,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_validPipe12_1 => oo_DSPFunct_i_effect0_i_validPipe12_1,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_validIn => oo_DSPFunct_i_effect0_i_validIn,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_coeff15 => oo_DSPFunct_i_effect0_i_coeff15,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_converterOut => oo_DSPFunct_i_effect0_i_converterOut,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_31 => oo_DSPFunct_i_effect0_i_mulOutput_31,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_sum4_2 => oo_DSPFunct_i_effect0_i_sum4_2,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_sum6_2 => oo_DSPFunct_i_effect0_i_sum6_2,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_sum0_1 => oo_DSPFunct_i_effect0_i_sum0_1,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_coeff14 => oo_DSPFunct_i_effect0_i_coeff14,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_coeff10 => oo_DSPFunct_i_effect0_i_coeff10,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_coeff11 => oo_DSPFunct_i_effect0_i_coeff11,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_coeff12 => oo_DSPFunct_i_effect0_i_coeff12,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_coeff13 => oo_DSPFunct_i_effect0_i_coeff13,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_sum13_2 => oo_DSPFunct_i_effect0_i_sum13_2,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_sum8_2 => oo_DSPFunct_i_effect0_i_sum8_2,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_validPipe1_1 => oo_DSPFunct_i_effect0_i_validPipe1_1,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_validPipe15_1 => oo_DSPFunct_i_effect0_i_validPipe15_1,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_sum15_2 => oo_DSPFunct_i_effect0_i_sum15_2,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_validPipe13_1 => oo_DSPFunct_i_effect0_i_validPipe13_1,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_validPipe9_1 => oo_DSPFunct_i_effect0_i_validPipe9_1,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_coeff2 => oo_DSPFunct_i_effect0_i_coeff2,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_8 => oo_DSPFunct_i_effect0_i_mulOutput_8,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_9 => oo_DSPFunct_i_effect0_i_mulOutput_9,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_4 => oo_DSPFunct_i_effect0_i_mulOutput_4,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_5 => oo_DSPFunct_i_effect0_i_mulOutput_5,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_6 => oo_DSPFunct_i_effect0_i_mulOutput_6,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_7 => oo_DSPFunct_i_effect0_i_mulOutput_7,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_1 => oo_DSPFunct_i_effect0_i_mulOutput_1,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_2 => oo_DSPFunct_i_effect0_i_mulOutput_2,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_3 => oo_DSPFunct_i_effect0_i_mulOutput_3,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_validPipe3_1 => oo_DSPFunct_i_effect0_i_validPipe3_1,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_sum2_2 => oo_DSPFunct_i_effect0_i_sum2_2,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_validPipe5_1 => oo_DSPFunct_i_effect0_i_validPipe5_1,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_sum5_2 => oo_DSPFunct_i_effect0_i_sum5_2,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_21 => oo_DSPFunct_i_effect0_i_mulOutput_21,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_coeff16 => oo_DSPFunct_i_effect0_i_coeff16,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_validPipe7_1 => oo_DSPFunct_i_effect0_i_validPipe7_1,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_validPipe11_1 => oo_DSPFunct_i_effect0_i_validPipe11_1,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_validPipe0_1 => oo_DSPFunct_i_effect0_i_validPipe0_1,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_sum7_2 => oo_DSPFunct_i_effect0_i_sum7_2,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_sum11_2 => oo_DSPFunct_i_effect0_i_sum11_2,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_sum9_2 => oo_DSPFunct_i_effect0_i_sum9_2,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_validPipe14_1 => oo_DSPFunct_i_effect0_i_validPipe14_1,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_sum12_2 => oo_DSPFunct_i_effect0_i_sum12_2,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_dataOut_tmp => oo_DSPFunct_i_effect0_i_dataOut_tmp,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_validPipe8_1 => oo_DSPFunct_i_effect0_i_validPipe8_1,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_sum1_2 => oo_DSPFunct_i_effect0_i_sum1_2,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput => oo_DSPFunct_i_effect0_i_mulOutput,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_validPipe4_1 => oo_DSPFunct_i_effect0_i_validPipe4_1,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_sum3_2 => oo_DSPFunct_i_effect0_i_sum3_2,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_30 => oo_DSPFunct_i_effect0_i_mulOutput_30,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_validPipe10_1 => oo_DSPFunct_i_effect0_i_validPipe10_1,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_sum14_2 => oo_DSPFunct_i_effect0_i_sum14_2,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_validPipe6_1 => oo_DSPFunct_i_effect0_i_validPipe6_1,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_final_sumPipe_1 => oo_DSPFunct_i_effect0_i_final_sumPipe_1,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_sum10_2 => oo_DSPFunct_i_effect0_i_sum10_2,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_final_sumValid => oo_DSPFunct_i_effect0_i_final_sumValid,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_29 => oo_DSPFunct_i_effect0_i_mulOutput_29,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_28 => oo_DSPFunct_i_effect0_i_mulOutput_28,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_25 => oo_DSPFunct_i_effect0_i_mulOutput_25,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_24 => oo_DSPFunct_i_effect0_i_mulOutput_24,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_27 => oo_DSPFunct_i_effect0_i_mulOutput_27,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_20 => oo_DSPFunct_i_effect0_i_mulOutput_20,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_23 => oo_DSPFunct_i_effect0_i_mulOutput_23,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_22 => oo_DSPFunct_i_effect0_i_mulOutput_22,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_26 => oo_DSPFunct_i_effect0_i_mulOutput_26,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_coeff4 => oo_DSPFunct_i_effect0_i_coeff4,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_coeff5 => oo_DSPFunct_i_effect0_i_coeff5,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_coeff6 => oo_DSPFunct_i_effect0_i_coeff6,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_coeff7 => oo_DSPFunct_i_effect0_i_coeff7,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_coeff0 => oo_DSPFunct_i_effect0_i_coeff0,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_coeff1 => oo_DSPFunct_i_effect0_i_coeff1,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_validPipe2_1 => oo_DSPFunct_i_effect0_i_validPipe2_1,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_coeff3 => oo_DSPFunct_i_effect0_i_coeff3,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_coeff8 => oo_DSPFunct_i_effect0_i_coeff8,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_coeff9 => oo_DSPFunct_i_effect0_i_coeff9,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_18 => oo_DSPFunct_i_effect0_i_mulOutput_18,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_19 => oo_DSPFunct_i_effect0_i_mulOutput_19,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_10 => oo_DSPFunct_i_effect0_i_mulOutput_10,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_11 => oo_DSPFunct_i_effect0_i_mulOutput_11,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_12 => oo_DSPFunct_i_effect0_i_mulOutput_12,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_13 => oo_DSPFunct_i_effect0_i_mulOutput_13,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_14 => oo_DSPFunct_i_effect0_i_mulOutput_14,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_15 => oo_DSPFunct_i_effect0_i_mulOutput_15,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_16 => oo_DSPFunct_i_effect0_i_mulOutput_16,

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_17 => oo_DSPFunct_i_effect0_i_mulOutput_17,
clk => clk, 
          rst => rst,         
		  selFirstEffect => selFirstEffect,
		  selFinalEffect => selFinalEffect,
		  ctrl0_0        => ctrl0_0,
		  ctrl0_1        => ctrl0_1,
		  ctrl1_0        => ctrl1_0,
		  ctrl1_1        => ctrl1_1,
          validOut       => validOut,
		  ctrl2_0        => ctrl2_0,
		  ctrl2_1        => ctrl2_1,
		  ctrl3_0        => ctrl3_0,
		  ctrl3_1        => ctrl3_1,
          DSPdatFromMem  => DSPDatFromMem,
          DSPDatToMem    => DSPDatToMem 
	      );

-- generate memory read address, incrementing on every active clk edge when (and after) goDSPPulse is asserted
NSRdAddDecode: process (CSRdAdd, goDSPPulse)
begin
	NSRdAdd <= CSRdAdd; -- default
	-- increment count if initialised (goDSPPulse asserted) or counting 0-1023 
	-- Counting stops after rolling over to 0, until next goDSPPulse assertion
	if (goDSPPulse = '1') or (unsigned(CSRdAdd) > "00000000000") then	
		if (unsigned(CSRdAdd) <= "1111111111") then
			NSRdAdd <= std_logic_vector(unsigned(CSRdAdd) + 1) ; -- increment 
		else  
			NSRdAdd <= (others => '0');                          -- roll over to 0
		end if;
	end if;
end process;

RdAddStateReg_i: process (clk, rst)
begin
    if rst = '1' then
        CSRdAdd <= (others => '0');
    elsif clk'event and clk = '1' then
		CSRdAdd <= NSRdAdd;
    end if;
end process;
DSPMemRdAdd     <= CSRdAdd;




-- generate memory write address, incrementing on every active clk edge when and after 
--  1. goDSPPulse is asserted for non clocked DSP components
--  2. validOut is asserted for clocked DSP components (ctrl1_1(0) asserted)
NSWrAddDecode: process (CSWrAdd, goDSPPulse, ctrl1_1(1), validOut)
begin
	NSWrAdd <= CSWrAdd; -- default
	-- increment count on every clock active edge from validOut assertion, count 0-1023 
	-- Counting stops after rolling over to 0, until next goDSPPulse assertion
	if ctrl1_1(1) = '0' then -- not FIR filter
	    if (goDSPPulse = '1') or (unsigned(CSWrAdd) > "00000000000") then	
		   if (unsigned(CSWrAdd) <= "1111111111") then
			   NSWrAdd <= std_logic_vector(unsigned(CSWrAdd) + 1) ; -- increment 
		   else  
			   NSWrAdd <= (others => '0');                          -- roll over to 0
		   end if;
        end if;
	else
	    if validOut = '1' then	
		   if (unsigned(CSWrAdd) <= "1111111111") then
			   NSWrAdd <= std_logic_vector(unsigned(CSWrAdd) + 1) ; -- increment 
		   else  
			   NSWrAdd <= (others => '0');                          -- roll over to 0
		   end if;
        end if;
    end if;
end process;

WrAddStateReg_i: process (clk, rst)
begin
    if rst = '1' then
        CSWrAdd <= (others => '0');
    elsif clk'event and clk = '1' then
		CSWrAdd <= NSWrAdd;
    end if;
end process;
DSPMemWrAdd     <= CSWrAdd;


-- generate DSPDone signal
DSPDoneDecode_i: process(CSWrAdd)
begin
	DSPDone <= '0'; -- default
	if (unsigned(CSWrAdd) = "00000000000") then
		DSPDone <= '1';
    end if;
end process;


-- generate memory write control signal 
DSPMemWrDecode_i: process(ctrl1_1, goDSPPulse, CSWrAdd, validOut)
begin
    DSPMemWr   <= '0'; 
	if ctrl1_1(1) = '0' then -- not FIR filter
		if (goDSPPulse = '1') or (unsigned(CSWrAdd) > "0000000000") then
			DSPMemWr <= '1'; -- assert in each clock period
		end if;
	else                     -- FIR filter
		if validOut = '1' or (unsigned(CSWrAdd) > "0000000000") then  
			DSPMemWr <= '1';                       
		end if;
	end if;
end process;


end RTL;