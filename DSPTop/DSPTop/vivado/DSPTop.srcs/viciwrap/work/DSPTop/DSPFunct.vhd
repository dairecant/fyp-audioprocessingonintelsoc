-- DSPFunct
-- Selects various DSP configurations

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

entity DSPFunct is
port  (
-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_dataIn_signed :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_validPipe12_1 :	out std_logic;

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_validIn :	out std_logic;

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_coeff15 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_converterOut :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_31 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_sum4_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_sum6_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_sum0_1 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_coeff14 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_coeff10 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_coeff11 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_coeff12 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_coeff13 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_sum13_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_sum8_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_validPipe1_1 :	out std_logic;

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_validPipe15_1 :	out std_logic;

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_sum15_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_validPipe13_1 :	out std_logic;

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_validPipe9_1 :	out std_logic;

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_coeff2 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_8 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_9 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_4 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_5 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_6 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_7 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_1 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_2 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_3 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_validPipe3_1 :	out std_logic;

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_sum2_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_validPipe5_1 :	out std_logic;

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_sum5_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_21 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_coeff16 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_validPipe7_1 :	out std_logic;

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_validPipe11_1 :	out std_logic;

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_validPipe0_1 :	out std_logic;

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_sum7_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_sum11_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_sum9_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_validPipe14_1 :	out std_logic;

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_sum12_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_dataOut_tmp :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_validPipe8_1 :	out std_logic;

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_sum1_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_validPipe4_1 :	out std_logic;

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_sum3_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_30 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_validPipe10_1 :	out std_logic;

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_sum14_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_validPipe6_1 :	out std_logic;

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_final_sumPipe_1 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_sum10_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_final_sumValid :	out std_logic;

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_29 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_28 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_25 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_24 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_27 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_20 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_23 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_22 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_26 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_coeff4 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_coeff5 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_coeff6 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_coeff7 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_coeff0 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_coeff1 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_validPipe2_1 :	out std_logic;

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_coeff3 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_coeff8 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_coeff9 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_18 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_19 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_10 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_11 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_12 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_13 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_14 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_15 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_16 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] DSPFunct_i
oo_effect0_i_mulOutput_17 :	out std_logic_vector( 31 DOWNTO 0 );
clk :          in std_logic; 
       rst :          in std_logic; 
       selFirstEffect:in std_logic_vector(2 downto 0);  
	   selFinalEffect:in std_logic_vector(1 downto 0);  
	   ctrl0_0:       in std_logic_vector(15 downto 0);  
	   ctrl0_1:       in std_logic_vector(15 downto 0);  
	   ctrl1_0:       in std_logic_vector(15 downto 0);  
	   ctrl1_1:       in std_logic_vector(15 downto 0);  
       validOut:      out std_logic;  
	   ctrl2_0:       in std_logic_vector(15 downto 0);  
	   ctrl2_1:       in std_logic_vector(15 downto 0);  
	   ctrl3_0:       in std_logic_vector(15 downto 0);  
	   ctrl3_1:       in std_logic_vector(15 downto 0);  
       DSPDatFromMem: in std_logic_vector(15 downto 0);  
       DSPDatToMem:   out std_logic_vector(15 downto 0)
	   );
end entity;

architecture struct of DSPFunct is

component FIRFilter_ip_src_Discrete_FIR_Filter_HDL_Optimized IS
  PORT(
-- line auto generated [2018-03-19] effect0_i
oo_dataIn_signed :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] effect0_i
oo_validPipe12_1 :	out std_logic;

-- line auto generated [2018-03-19] effect0_i
oo_validIn :	out std_logic;

-- line auto generated [2018-03-19] effect0_i
oo_coeff15 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] effect0_i
oo_converterOut :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] effect0_i
oo_mulOutput_31 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] effect0_i
oo_sum4_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-19] effect0_i
oo_sum6_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-19] effect0_i
oo_sum0_1 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-19] effect0_i
oo_coeff14 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] effect0_i
oo_coeff10 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] effect0_i
oo_coeff11 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] effect0_i
oo_coeff12 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] effect0_i
oo_coeff13 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] effect0_i
oo_sum13_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-19] effect0_i
oo_sum8_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-19] effect0_i
oo_validPipe1_1 :	out std_logic;

-- line auto generated [2018-03-19] effect0_i
oo_validPipe15_1 :	out std_logic;

-- line auto generated [2018-03-19] effect0_i
oo_sum15_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-19] effect0_i
oo_validPipe13_1 :	out std_logic;

-- line auto generated [2018-03-19] effect0_i
oo_validPipe9_1 :	out std_logic;

-- line auto generated [2018-03-19] effect0_i
oo_coeff2 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] effect0_i
oo_mulOutput_8 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] effect0_i
oo_mulOutput_9 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] effect0_i
oo_mulOutput_4 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] effect0_i
oo_mulOutput_5 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] effect0_i
oo_mulOutput_6 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] effect0_i
oo_mulOutput_7 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] effect0_i
oo_mulOutput_1 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] effect0_i
oo_mulOutput_2 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] effect0_i
oo_mulOutput_3 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] effect0_i
oo_validPipe3_1 :	out std_logic;

-- line auto generated [2018-03-19] effect0_i
oo_sum2_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-19] effect0_i
oo_validPipe5_1 :	out std_logic;

-- line auto generated [2018-03-19] effect0_i
oo_sum5_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-19] effect0_i
oo_mulOutput_21 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] effect0_i
oo_coeff16 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] effect0_i
oo_validPipe7_1 :	out std_logic;

-- line auto generated [2018-03-19] effect0_i
oo_validPipe11_1 :	out std_logic;

-- line auto generated [2018-03-19] effect0_i
oo_validPipe0_1 :	out std_logic;

-- line auto generated [2018-03-19] effect0_i
oo_sum7_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-19] effect0_i
oo_sum11_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-19] effect0_i
oo_sum9_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-19] effect0_i
oo_validPipe14_1 :	out std_logic;

-- line auto generated [2018-03-19] effect0_i
oo_sum12_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-19] effect0_i
oo_dataOut_tmp :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] effect0_i
oo_validPipe8_1 :	out std_logic;

-- line auto generated [2018-03-19] effect0_i
oo_sum1_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-19] effect0_i
oo_mulOutput :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] effect0_i
oo_validPipe4_1 :	out std_logic;

-- line auto generated [2018-03-19] effect0_i
oo_sum3_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-19] effect0_i
oo_mulOutput_30 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] effect0_i
oo_validPipe10_1 :	out std_logic;

-- line auto generated [2018-03-19] effect0_i
oo_sum14_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-19] effect0_i
oo_validPipe6_1 :	out std_logic;

-- line auto generated [2018-03-19] effect0_i
oo_final_sumPipe_1 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-19] effect0_i
oo_sum10_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-19] effect0_i
oo_final_sumValid :	out std_logic;

-- line auto generated [2018-03-19] effect0_i
oo_mulOutput_29 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] effect0_i
oo_mulOutput_28 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] effect0_i
oo_mulOutput_25 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] effect0_i
oo_mulOutput_24 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] effect0_i
oo_mulOutput_27 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] effect0_i
oo_mulOutput_20 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] effect0_i
oo_mulOutput_23 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] effect0_i
oo_mulOutput_22 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] effect0_i
oo_mulOutput_26 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] effect0_i
oo_coeff4 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] effect0_i
oo_coeff5 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] effect0_i
oo_coeff6 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] effect0_i
oo_coeff7 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] effect0_i
oo_coeff0 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] effect0_i
oo_coeff1 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] effect0_i
oo_validPipe2_1 :	out std_logic;

-- line auto generated [2018-03-19] effect0_i
oo_coeff3 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] effect0_i
oo_coeff8 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] effect0_i
oo_coeff9 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-19] effect0_i
oo_mulOutput_18 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] effect0_i
oo_mulOutput_19 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] effect0_i
oo_mulOutput_10 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] effect0_i
oo_mulOutput_11 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] effect0_i
oo_mulOutput_12 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] effect0_i
oo_mulOutput_13 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] effect0_i
oo_mulOutput_14 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] effect0_i
oo_mulOutput_15 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] effect0_i
oo_mulOutput_16 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-19] effect0_i
oo_mulOutput_17 :	out std_logic_vector( 31 DOWNTO 0 );
 clk                               :   IN    std_logic;
        reset                             :   IN    std_logic;
        enb                               :   IN    std_logic;
        dataIn                            :   IN    std_logic_vector(15 DOWNTO 0); 
        dataOut                           :   OUT   std_logic_vector(15 DOWNTO 0);   
        validOut                          :   OUT   std_logic
        );
end component;

begin

effect0_i:  FIRFilter_ip_src_Discrete_FIR_Filter_HDL_Optimized
 PORT MAP(
-- line auto generated [2018-03-19] effect0_i
oo_dataIn_signed => oo_effect0_i_dataIn_signed,

-- line auto generated [2018-03-19] effect0_i
oo_validPipe12_1 => oo_effect0_i_validPipe12_1,

-- line auto generated [2018-03-19] effect0_i
oo_validIn => oo_effect0_i_validIn,

-- line auto generated [2018-03-19] effect0_i
oo_coeff15 => oo_effect0_i_coeff15,

-- line auto generated [2018-03-19] effect0_i
oo_converterOut => oo_effect0_i_converterOut,

-- line auto generated [2018-03-19] effect0_i
oo_mulOutput_31 => oo_effect0_i_mulOutput_31,

-- line auto generated [2018-03-19] effect0_i
oo_sum4_2 => oo_effect0_i_sum4_2,

-- line auto generated [2018-03-19] effect0_i
oo_sum6_2 => oo_effect0_i_sum6_2,

-- line auto generated [2018-03-19] effect0_i
oo_sum0_1 => oo_effect0_i_sum0_1,

-- line auto generated [2018-03-19] effect0_i
oo_coeff14 => oo_effect0_i_coeff14,

-- line auto generated [2018-03-19] effect0_i
oo_coeff10 => oo_effect0_i_coeff10,

-- line auto generated [2018-03-19] effect0_i
oo_coeff11 => oo_effect0_i_coeff11,

-- line auto generated [2018-03-19] effect0_i
oo_coeff12 => oo_effect0_i_coeff12,

-- line auto generated [2018-03-19] effect0_i
oo_coeff13 => oo_effect0_i_coeff13,

-- line auto generated [2018-03-19] effect0_i
oo_sum13_2 => oo_effect0_i_sum13_2,

-- line auto generated [2018-03-19] effect0_i
oo_sum8_2 => oo_effect0_i_sum8_2,

-- line auto generated [2018-03-19] effect0_i
oo_validPipe1_1 => oo_effect0_i_validPipe1_1,

-- line auto generated [2018-03-19] effect0_i
oo_validPipe15_1 => oo_effect0_i_validPipe15_1,

-- line auto generated [2018-03-19] effect0_i
oo_sum15_2 => oo_effect0_i_sum15_2,

-- line auto generated [2018-03-19] effect0_i
oo_validPipe13_1 => oo_effect0_i_validPipe13_1,

-- line auto generated [2018-03-19] effect0_i
oo_validPipe9_1 => oo_effect0_i_validPipe9_1,

-- line auto generated [2018-03-19] effect0_i
oo_coeff2 => oo_effect0_i_coeff2,

-- line auto generated [2018-03-19] effect0_i
oo_mulOutput_8 => oo_effect0_i_mulOutput_8,

-- line auto generated [2018-03-19] effect0_i
oo_mulOutput_9 => oo_effect0_i_mulOutput_9,

-- line auto generated [2018-03-19] effect0_i
oo_mulOutput_4 => oo_effect0_i_mulOutput_4,

-- line auto generated [2018-03-19] effect0_i
oo_mulOutput_5 => oo_effect0_i_mulOutput_5,

-- line auto generated [2018-03-19] effect0_i
oo_mulOutput_6 => oo_effect0_i_mulOutput_6,

-- line auto generated [2018-03-19] effect0_i
oo_mulOutput_7 => oo_effect0_i_mulOutput_7,

-- line auto generated [2018-03-19] effect0_i
oo_mulOutput_1 => oo_effect0_i_mulOutput_1,

-- line auto generated [2018-03-19] effect0_i
oo_mulOutput_2 => oo_effect0_i_mulOutput_2,

-- line auto generated [2018-03-19] effect0_i
oo_mulOutput_3 => oo_effect0_i_mulOutput_3,

-- line auto generated [2018-03-19] effect0_i
oo_validPipe3_1 => oo_effect0_i_validPipe3_1,

-- line auto generated [2018-03-19] effect0_i
oo_sum2_2 => oo_effect0_i_sum2_2,

-- line auto generated [2018-03-19] effect0_i
oo_validPipe5_1 => oo_effect0_i_validPipe5_1,

-- line auto generated [2018-03-19] effect0_i
oo_sum5_2 => oo_effect0_i_sum5_2,

-- line auto generated [2018-03-19] effect0_i
oo_mulOutput_21 => oo_effect0_i_mulOutput_21,

-- line auto generated [2018-03-19] effect0_i
oo_coeff16 => oo_effect0_i_coeff16,

-- line auto generated [2018-03-19] effect0_i
oo_validPipe7_1 => oo_effect0_i_validPipe7_1,

-- line auto generated [2018-03-19] effect0_i
oo_validPipe11_1 => oo_effect0_i_validPipe11_1,

-- line auto generated [2018-03-19] effect0_i
oo_validPipe0_1 => oo_effect0_i_validPipe0_1,

-- line auto generated [2018-03-19] effect0_i
oo_sum7_2 => oo_effect0_i_sum7_2,

-- line auto generated [2018-03-19] effect0_i
oo_sum11_2 => oo_effect0_i_sum11_2,

-- line auto generated [2018-03-19] effect0_i
oo_sum9_2 => oo_effect0_i_sum9_2,

-- line auto generated [2018-03-19] effect0_i
oo_validPipe14_1 => oo_effect0_i_validPipe14_1,

-- line auto generated [2018-03-19] effect0_i
oo_sum12_2 => oo_effect0_i_sum12_2,

-- line auto generated [2018-03-19] effect0_i
oo_dataOut_tmp => oo_effect0_i_dataOut_tmp,

-- line auto generated [2018-03-19] effect0_i
oo_validPipe8_1 => oo_effect0_i_validPipe8_1,

-- line auto generated [2018-03-19] effect0_i
oo_sum1_2 => oo_effect0_i_sum1_2,

-- line auto generated [2018-03-19] effect0_i
oo_mulOutput => oo_effect0_i_mulOutput,

-- line auto generated [2018-03-19] effect0_i
oo_validPipe4_1 => oo_effect0_i_validPipe4_1,

-- line auto generated [2018-03-19] effect0_i
oo_sum3_2 => oo_effect0_i_sum3_2,

-- line auto generated [2018-03-19] effect0_i
oo_mulOutput_30 => oo_effect0_i_mulOutput_30,

-- line auto generated [2018-03-19] effect0_i
oo_validPipe10_1 => oo_effect0_i_validPipe10_1,

-- line auto generated [2018-03-19] effect0_i
oo_sum14_2 => oo_effect0_i_sum14_2,

-- line auto generated [2018-03-19] effect0_i
oo_validPipe6_1 => oo_effect0_i_validPipe6_1,

-- line auto generated [2018-03-19] effect0_i
oo_final_sumPipe_1 => oo_effect0_i_final_sumPipe_1,

-- line auto generated [2018-03-19] effect0_i
oo_sum10_2 => oo_effect0_i_sum10_2,

-- line auto generated [2018-03-19] effect0_i
oo_final_sumValid => oo_effect0_i_final_sumValid,

-- line auto generated [2018-03-19] effect0_i
oo_mulOutput_29 => oo_effect0_i_mulOutput_29,

-- line auto generated [2018-03-19] effect0_i
oo_mulOutput_28 => oo_effect0_i_mulOutput_28,

-- line auto generated [2018-03-19] effect0_i
oo_mulOutput_25 => oo_effect0_i_mulOutput_25,

-- line auto generated [2018-03-19] effect0_i
oo_mulOutput_24 => oo_effect0_i_mulOutput_24,

-- line auto generated [2018-03-19] effect0_i
oo_mulOutput_27 => oo_effect0_i_mulOutput_27,

-- line auto generated [2018-03-19] effect0_i
oo_mulOutput_20 => oo_effect0_i_mulOutput_20,

-- line auto generated [2018-03-19] effect0_i
oo_mulOutput_23 => oo_effect0_i_mulOutput_23,

-- line auto generated [2018-03-19] effect0_i
oo_mulOutput_22 => oo_effect0_i_mulOutput_22,

-- line auto generated [2018-03-19] effect0_i
oo_mulOutput_26 => oo_effect0_i_mulOutput_26,

-- line auto generated [2018-03-19] effect0_i
oo_coeff4 => oo_effect0_i_coeff4,

-- line auto generated [2018-03-19] effect0_i
oo_coeff5 => oo_effect0_i_coeff5,

-- line auto generated [2018-03-19] effect0_i
oo_coeff6 => oo_effect0_i_coeff6,

-- line auto generated [2018-03-19] effect0_i
oo_coeff7 => oo_effect0_i_coeff7,

-- line auto generated [2018-03-19] effect0_i
oo_coeff0 => oo_effect0_i_coeff0,

-- line auto generated [2018-03-19] effect0_i
oo_coeff1 => oo_effect0_i_coeff1,

-- line auto generated [2018-03-19] effect0_i
oo_validPipe2_1 => oo_effect0_i_validPipe2_1,

-- line auto generated [2018-03-19] effect0_i
oo_coeff3 => oo_effect0_i_coeff3,

-- line auto generated [2018-03-19] effect0_i
oo_coeff8 => oo_effect0_i_coeff8,

-- line auto generated [2018-03-19] effect0_i
oo_coeff9 => oo_effect0_i_coeff9,

-- line auto generated [2018-03-19] effect0_i
oo_mulOutput_18 => oo_effect0_i_mulOutput_18,

-- line auto generated [2018-03-19] effect0_i
oo_mulOutput_19 => oo_effect0_i_mulOutput_19,

-- line auto generated [2018-03-19] effect0_i
oo_mulOutput_10 => oo_effect0_i_mulOutput_10,

-- line auto generated [2018-03-19] effect0_i
oo_mulOutput_11 => oo_effect0_i_mulOutput_11,

-- line auto generated [2018-03-19] effect0_i
oo_mulOutput_12 => oo_effect0_i_mulOutput_12,

-- line auto generated [2018-03-19] effect0_i
oo_mulOutput_13 => oo_effect0_i_mulOutput_13,

-- line auto generated [2018-03-19] effect0_i
oo_mulOutput_14 => oo_effect0_i_mulOutput_14,

-- line auto generated [2018-03-19] effect0_i
oo_mulOutput_15 => oo_effect0_i_mulOutput_15,

-- line auto generated [2018-03-19] effect0_i
oo_mulOutput_16 => oo_effect0_i_mulOutput_16,

-- line auto generated [2018-03-19] effect0_i
oo_mulOutput_17 => oo_effect0_i_mulOutput_17,
 clk      => clk,
           reset    => rst,
           enb      => ctrl1_1(0),
		   dataIn   => DSPDatFromMem, 
           dataOut  => DSPDatToMem, 
		   validOut => validOut
     );	 
		 		 		 
end struct;