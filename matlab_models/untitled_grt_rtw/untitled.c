/*
 * untitled.c
 *
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * Code generation for model "untitled".
 *
 * Model version              : 1.0
 * Simulink Coder version : 8.13 (R2017b) 24-Jul-2017
 * C source code generated on : Wed Jan 24 16:48:30 2018
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "untitled.h"
#include "untitled_private.h"

/* Block states (auto storage) */
DW_untitled_T untitled_DW;

/* External outputs (root outports fed by signals with auto storage) */
ExtY_untitled_T untitled_Y;

/* Real-time model */
RT_MODEL_untitled_T untitled_M_;
RT_MODEL_untitled_T *const untitled_M = &untitled_M_;

/* Model step function */
void untitled_step(void)
{
  char_T *sErr;
  void *audio;

  /* S-Function (sdspwmmfi2): '<Root>/From Multimedia File1' incorporates:
   *  Outport: '<Root>/Out1'
   */
  sErr = GetErrorBuffer(&untitled_DW.FromMultimediaFile1_HostLib[0U]);
  audio = (void *)&untitled_Y.Out1[0U];
  LibOutputs_FromMMFile(&untitled_DW.FromMultimediaFile1_HostLib[0U],
                        GetNullPointer(), audio, GetNullPointer(),
                        GetNullPointer(), GetNullPointer());
  if (*sErr != 0) {
    rtmSetErrorStatus(untitled_M, sErr);
    rtmSetStopRequested(untitled_M, 1);
  }

  /* End of S-Function (sdspwmmfi2): '<Root>/From Multimedia File1' */

  /* Matfile logging */
  rt_UpdateTXYLogVars(untitled_M->rtwLogInfo, (&untitled_M->Timing.taskTime0));

  /* signal main to stop simulation */
  {                                    /* Sample time: [0.0058049886621315194s, 0.0s] */
    if ((rtmGetTFinal(untitled_M)!=-1) &&
        !((rtmGetTFinal(untitled_M)-untitled_M->Timing.taskTime0) >
          untitled_M->Timing.taskTime0 * (DBL_EPSILON))) {
      rtmSetErrorStatus(untitled_M, "Simulation finished");
    }
  }

  /* Update absolute time for base rate */
  /* The "clockTick0" counts the number of times the code of this task has
   * been executed. The absolute time is the multiplication of "clockTick0"
   * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
   * overflow during the application lifespan selected.
   * Timer of this task consists of two 32 bit unsigned integers.
   * The two integers represent the low bits Timing.clockTick0 and the high bits
   * Timing.clockTickH0. When the low bit overflows to 0, the high bits increment.
   */
  if (!(++untitled_M->Timing.clockTick0)) {
    ++untitled_M->Timing.clockTickH0;
  }

  untitled_M->Timing.taskTime0 = untitled_M->Timing.clockTick0 *
    untitled_M->Timing.stepSize0 + untitled_M->Timing.clockTickH0 *
    untitled_M->Timing.stepSize0 * 4294967296.0;
}

/* Model initialize function */
void untitled_initialize(void)
{
  /* Registration code */

  /* initialize non-finites */
  rt_InitInfAndNaN(sizeof(real_T));

  /* initialize real-time model */
  (void) memset((void *)untitled_M, 0,
                sizeof(RT_MODEL_untitled_T));
  rtmSetTFinal(untitled_M, 9.9961904761904758);
  untitled_M->Timing.stepSize0 = 0.0058049886621315194;

  /* Setup for data logging */
  {
    static RTWLogInfo rt_DataLoggingInfo;
    rt_DataLoggingInfo.loggingInterval = NULL;
    untitled_M->rtwLogInfo = &rt_DataLoggingInfo;
  }

  /* Setup for data logging */
  {
    rtliSetLogXSignalInfo(untitled_M->rtwLogInfo, (NULL));
    rtliSetLogXSignalPtrs(untitled_M->rtwLogInfo, (NULL));
    rtliSetLogT(untitled_M->rtwLogInfo, "tout");
    rtliSetLogX(untitled_M->rtwLogInfo, "");
    rtliSetLogXFinal(untitled_M->rtwLogInfo, "");
    rtliSetLogVarNameModifier(untitled_M->rtwLogInfo, "rt_");
    rtliSetLogFormat(untitled_M->rtwLogInfo, 4);
    rtliSetLogMaxRows(untitled_M->rtwLogInfo, 0);
    rtliSetLogDecimation(untitled_M->rtwLogInfo, 1);
    rtliSetLogY(untitled_M->rtwLogInfo, "");
    rtliSetLogYSignalInfo(untitled_M->rtwLogInfo, (NULL));
    rtliSetLogYSignalPtrs(untitled_M->rtwLogInfo, (NULL));
  }

  /* states (dwork) */
  (void) memset((void *)&untitled_DW, 0,
                sizeof(DW_untitled_T));

  /* external outputs */
  (void) memset(&untitled_Y.Out1[0], 0,
                256U*sizeof(real_T));

  /* Matfile logging */
  rt_StartDataLoggingWithStartTime(untitled_M->rtwLogInfo, 0.0, rtmGetTFinal
    (untitled_M), untitled_M->Timing.stepSize0, (&rtmGetErrorStatus(untitled_M)));

  {
    char_T *sErr;

    /* Start for S-Function (sdspwmmfi2): '<Root>/From Multimedia File1' */
    sErr = GetErrorBuffer(&untitled_DW.FromMultimediaFile1_HostLib[0U]);
    CreateHostLibrary("frommmfile.dll",
                      &untitled_DW.FromMultimediaFile1_HostLib[0U]);
    createAudioInfo(&untitled_DW.FromMultimediaFile1_AudioInfo[0U], 1U, 0U,
                    44100.0, 16, 1, 256, 0, GetNullPointer());
    createVideoInfo(&untitled_DW.FromMultimediaFile1_VideoInfo[0U], 0U, 0.0, 0.0,
                    "", 0, 0, 0, 0, 1U, 0, 0, GetNullPointer());
    if (*sErr == 0) {
      LibCreate_FromMMFile(&untitled_DW.FromMultimediaFile1_HostLib[0U], 0,
                           (void *)
                           "C:\\\\Users\\\\D\xe1ire\\\\Documents\\\\Final_Year\\\\FYP\\\\git\\\\matlab_models\\\\Kool_Comp_16.wav",
                           1,
                           "C:\\Program Files\\MATLAB\\R2017b\\toolbox\\shared\\multimedia\\bin\\win64\\audio\\audiofilesndfilereaderplugin.dll",
                           "C:\\Program Files\\MATLAB\\R2017b\\toolbox\\shared\\multimedia\\bin\\win64\\audioslconverter",
                           &untitled_DW.FromMultimediaFile1_AudioInfo[0U],
                           &untitled_DW.FromMultimediaFile1_VideoInfo[0U], 9U,
                           0U, 1U, 0U, 0U, 1U);
    }

    if (*sErr == 0) {
      LibStart(&untitled_DW.FromMultimediaFile1_HostLib[0U]);
    }

    if (*sErr != 0) {
      DestroyHostLibrary(&untitled_DW.FromMultimediaFile1_HostLib[0U]);
      if (*sErr != 0) {
        rtmSetErrorStatus(untitled_M, sErr);
        rtmSetStopRequested(untitled_M, 1);
      }
    }

    /* End of Start for S-Function (sdspwmmfi2): '<Root>/From Multimedia File1' */
  }

  /* InitializeConditions for S-Function (sdspwmmfi2): '<Root>/From Multimedia File1' */
  LibReset(&untitled_DW.FromMultimediaFile1_HostLib[0U]);
}

/* Model terminate function */
void untitled_terminate(void)
{
  char_T *sErr;

  /* Terminate for S-Function (sdspwmmfi2): '<Root>/From Multimedia File1' */
  sErr = GetErrorBuffer(&untitled_DW.FromMultimediaFile1_HostLib[0U]);
  LibTerminate(&untitled_DW.FromMultimediaFile1_HostLib[0U]);
  if (*sErr != 0) {
    rtmSetErrorStatus(untitled_M, sErr);
    rtmSetStopRequested(untitled_M, 1);
  }

  LibDestroy(&untitled_DW.FromMultimediaFile1_HostLib[0U], 0);
  DestroyHostLibrary(&untitled_DW.FromMultimediaFile1_HostLib[0U]);

  /* End of Terminate for S-Function (sdspwmmfi2): '<Root>/From Multimedia File1' */
}
