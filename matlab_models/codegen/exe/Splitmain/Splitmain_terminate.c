/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * Splitmain_terminate.c
 *
 * Code generation for function 'Splitmain_terminate'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "Splitmain.h"
#include "Splitmain_terminate.h"

/* Function Definitions */
void Splitmain_terminate(void)
{
  /* (no terminate code required) */
}

/* End of code generation (Splitmain_terminate.c) */
