/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * Splitmain.c
 *
 * Code generation for function 'Splitmain'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "Splitmain.h"
#include "Splitmain_emxutil.h"
#include "fread.h"
#include "fileManager.h"
#include "Splitmain_rtwutil.h"
#include <stdio.h>

/* Type Definitions */

/* Function Definitions */
void Splitmain(const char audio_data[], const int audio_size[2], emxArray_real_T
               *b_Splitmain, emxArray_real_T *frames)
{
  signed char fileid;
  int wherefrom;
  FILE * filestar;
  boolean_T p;
  long position_t;
  double size;
  FILE * b_NULL;
  emxArray_real_T *a;
  unsigned int i;
  int k;
  int x;
  int j;
  int loop_ub;
  unsigned char u0;
  unsigned char b_x[2];
  short y;

  /* [audio]= audioread(audio); */
  fileid = cfopen(audio_data, audio_size, "rb");
  wherefrom = SEEK_END;
  filestar = fileManager(fileid);
  if ((fileid != 0) && (fileid != 1) && (fileid != 2)) {
    p = true;
  } else {
    p = false;
  }

  if (!p) {
    filestar = NULL;
  }

  if (!(filestar == NULL)) {
    fseek(filestar, (long int)0.0, wherefrom);
  }

  filestar = fileManager(fileid);
  if ((fileid != 0) && (fileid != 1) && (fileid != 2)) {
    p = true;
  } else {
    p = false;
  }

  if (!p) {
    filestar = NULL;
  }

  if (filestar == NULL) {
    size = -1.0;
  } else {
    position_t = ftell(filestar);
    size = (double)position_t;
  }

  b_NULL = NULL;
  filestar = fileManager(fileid);
  if (!(filestar == b_NULL)) {
    if ((fileid == 0) || (fileid == 1) || (fileid == 2)) {
      p = true;
    } else {
      p = false;
    }

    if (!p) {
      fseek(filestar, 0, SEEK_SET);
    }
  }

  emxInit_real_T(&a, 1);
  b_fread(fileid, size, a);
  i = 1U;
  k = 0;
  x = (int)floor((double)a->size[0] / 512.0) + 1;
  size = (double)a->size[0] / 2.0;
  j = b_Splitmain->size[0];
  b_Splitmain->size[0] = (int)size;
  emxEnsureCapacity_real_T(b_Splitmain, j);
  loop_ub = (int)size;
  for (j = 0; j < loop_ub; j++) {
    b_Splitmain->data[j] = 0.0;
  }

  while (i < (unsigned int)a->size[0]) {
    size = rt_roundd_snf(a->data[(int)i - 1]);
    if (size < 256.0) {
      if (size >= 0.0) {
        u0 = (unsigned char)size;
      } else {
        u0 = 0;
      }
    } else if (size >= 256.0) {
      u0 = MAX_uint8_T;
    } else {
      u0 = 0;
    }

    b_x[0] = u0;
    size = rt_roundd_snf(a->data[(int)i]);
    if (size < 256.0) {
      if (size >= 0.0) {
        u0 = (unsigned char)size;
      } else {
        u0 = 0;
      }
    } else if (size >= 256.0) {
      u0 = MAX_uint8_T;
    } else {
      u0 = 0;
    }

    b_x[1] = u0;
    memcpy((void *)&y, (void *)&b_x[0], (unsigned int)((size_t)1 * sizeof(short)));
    b_Splitmain->data[k] = y;
    k++;
    i += 2U;
  }

  emxFree_real_T(&a);
  j = b_Splitmain->size[0];
  emxEnsureCapacity_real_T(b_Splitmain, j);
  loop_ub = b_Splitmain->size[0];
  for (j = 0; j < loop_ub; j++) {
    b_Splitmain->data[j] /= 32767.0;
  }

  i = 1U;
  k = 1;
  j = frames->size[0] * frames->size[1];
  frames->size[0] = 256;
  frames->size[1] = x;
  emxEnsureCapacity_real_T1(frames, j);
  loop_ub = x << 8;
  for (j = 0; j < loop_ub; j++) {
    frames->data[j] = 0.0;
  }

  while ((int)i < x) {
    j = 1;
    while ((j < 257) && (k < b_Splitmain->size[0])) {
      frames->data[(j + frames->size[0] * ((int)i - 1)) - 1] = b_Splitmain->
        data[k - 1];
      j++;
      k++;
    }

    i = (unsigned int)((int)i + 1);
  }
}

/* End of code generation (Splitmain.c) */
