/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * fread.c
 *
 * Code generation for function 'fread'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "Splitmain.h"
#include "fread.h"
#include "Splitmain_emxutil.h"
#include "fileManager.h"

/* Type Definitions */
#include <stddef.h>

/* Function Definitions */
void b_fread(double fileID, double sizeA, emxArray_real_T *A)
{
  int dims_idx_0;
  boolean_T doEOF;
  size_t nBytes;
  FILE * filestar;
  boolean_T p;
  emxArray_uint8_T *b_A;
  int buf_size_idx_0;
  int c;
  int bytesOut;
  int bdims[2];
  int numRead;
  int num2Read;
  size_t numReadSizeT;
  unsigned char buf_data[1024];
  if (rtIsInf(sizeA)) {
    dims_idx_0 = 1024;
    doEOF = true;
  } else {
    dims_idx_0 = (int)sizeA;
    doEOF = false;
  }

  nBytes = sizeof(unsigned char);
  filestar = fileManager(fileID);
  if ((fileID != 0.0) && (fileID != 1.0) && (fileID != 2.0)) {
    p = true;
  } else {
    p = false;
  }

  if (!p) {
    filestar = NULL;
  }

  emxInit_uint8_T(&b_A, 1);
  if (!doEOF) {
    if (filestar == NULL) {
      buf_size_idx_0 = A->size[0];
      A->size[0] = 0;
      emxEnsureCapacity_real_T(A, buf_size_idx_0);
    } else {
      buf_size_idx_0 = A->size[0];
      A->size[0] = (int)sizeA;
      emxEnsureCapacity_real_T(A, buf_size_idx_0);
      if ((A->size[0] == 0) || (dims_idx_0 == 0)) {
        bytesOut = 0;
      } else {
        if (dims_idx_0 > 1024) {
          for (buf_size_idx_0 = 0; buf_size_idx_0 < 2; buf_size_idx_0++) {
            bdims[buf_size_idx_0] = 1024 + -1023 * buf_size_idx_0;
          }
        } else {
          bdims[0] = dims_idx_0;
        }

        bytesOut = 0;
        numRead = 1;
        buf_size_idx_0 = bdims[0];
        while ((bytesOut < dims_idx_0) && (numRead > 0)) {
          num2Read = buf_size_idx_0;
          numRead = dims_idx_0 - bytesOut;
          if (buf_size_idx_0 > numRead) {
            num2Read = numRead;
          }

          buf_size_idx_0 = (short)bdims[0];
          numRead = 0;
          c = 1;
          while ((numRead < num2Read) && (c > 0)) {
            numReadSizeT = fread(&buf_data[numRead], nBytes, num2Read - numRead,
                                 filestar);
            c = (int)numReadSizeT;
            numRead += (int)numReadSizeT;
          }

          for (c = 0; c + 1 <= numRead; c++) {
            A->data[c + bytesOut] = buf_data[c];
          }

          bytesOut += numRead;
        }

        buf_size_idx_0 = A->size[0];
        for (c = bytesOut; c + 1 <= buf_size_idx_0; c++) {
          A->data[c] = 0.0;
        }
      }

      if (bytesOut < sizeA) {
        if (1 > bytesOut) {
          num2Read = 0;
        } else {
          num2Read = bytesOut;
        }

        buf_size_idx_0 = b_A->size[0];
        b_A->size[0] = A->size[0];
        emxEnsureCapacity_uint8_T(b_A, buf_size_idx_0);
        numRead = A->size[0];
        for (buf_size_idx_0 = 0; buf_size_idx_0 < numRead; buf_size_idx_0++) {
          b_A->data[buf_size_idx_0] = (unsigned char)A->data[buf_size_idx_0];
        }

        buf_size_idx_0 = A->size[0];
        A->size[0] = num2Read;
        emxEnsureCapacity_real_T(A, buf_size_idx_0);
        for (buf_size_idx_0 = 0; buf_size_idx_0 < num2Read; buf_size_idx_0++) {
          A->data[buf_size_idx_0] = b_A->data[buf_size_idx_0];
        }
      }
    }
  } else {
    buf_size_idx_0 = A->size[0];
    A->size[0] = 0;
    emxEnsureCapacity_real_T(A, buf_size_idx_0);
    if (!(filestar == NULL)) {
      c = 1;
      while (c > 0) {
        c = 0;
        numRead = 1;
        while ((c < 1024) && (numRead > 0)) {
          numReadSizeT = fread(&buf_data[c], nBytes, 1024 - c, filestar);
          numRead = (int)numReadSizeT;
          c += (int)numReadSizeT;
        }

        if (1 > c) {
          num2Read = -1;
        } else {
          num2Read = c - 1;
        }

        numRead = A->size[0];
        buf_size_idx_0 = A->size[0];
        A->size[0] = (numRead + num2Read) + 1;
        emxEnsureCapacity_real_T(A, buf_size_idx_0);
        for (buf_size_idx_0 = 0; buf_size_idx_0 <= num2Read; buf_size_idx_0++) {
          A->data[numRead + buf_size_idx_0] = buf_data[buf_size_idx_0];
        }
      }
    }
  }

  emxFree_uint8_T(&b_A);
}

/* End of code generation (fread.c) */
