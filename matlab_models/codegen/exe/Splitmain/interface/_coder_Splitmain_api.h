/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * _coder_Splitmain_api.h
 *
 * Code generation for function '_coder_Splitmain_api'
 *
 */

#ifndef _CODER_SPLITMAIN_API_H
#define _CODER_SPLITMAIN_API_H

/* Include files */
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include <stddef.h>
#include <stdlib.h>
#include "_coder_Splitmain_api.h"

/* Type Definitions */
#ifndef struct_emxArray_real_T
#define struct_emxArray_real_T

struct emxArray_real_T
{
  real_T *data;
  int32_T *size;
  int32_T allocatedSize;
  int32_T numDimensions;
  boolean_T canFreeData;
};

#endif                                 /*struct_emxArray_real_T*/

#ifndef typedef_emxArray_real_T
#define typedef_emxArray_real_T

typedef struct emxArray_real_T emxArray_real_T;

#endif                                 /*typedef_emxArray_real_T*/

/* Variable Declarations */
extern emlrtCTX emlrtRootTLSGlobal;
extern emlrtContext emlrtContextGlobal;

/* Function Declarations */
extern void Splitmain(char_T audio_data[], int32_T audio_size[2],
                      emxArray_real_T *b_Splitmain, emxArray_real_T *frames);
extern void Splitmain_api(const mxArray * const prhs[1], const mxArray *plhs[2]);
extern void Splitmain_atexit(void);
extern void Splitmain_initialize(void);
extern void Splitmain_terminate(void);
extern void Splitmain_xil_terminate(void);

#endif

/* End of code generation (_coder_Splitmain_api.h) */
