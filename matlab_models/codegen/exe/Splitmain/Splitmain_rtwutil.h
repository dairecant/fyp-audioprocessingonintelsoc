/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * Splitmain_rtwutil.h
 *
 * Code generation for function 'Splitmain_rtwutil'
 *
 */

#ifndef SPLITMAIN_RTWUTIL_H
#define SPLITMAIN_RTWUTIL_H

/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "Splitmain_types.h"

/* Function Declarations */
extern double rt_roundd_snf(double u);

#endif

/* End of code generation (Splitmain_rtwutil.h) */
