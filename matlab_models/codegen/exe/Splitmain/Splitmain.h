/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * Splitmain.h
 *
 * Code generation for function 'Splitmain'
 *
 */

#ifndef SPLITMAIN_H
#define SPLITMAIN_H

/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "Splitmain_types.h"

/* Type Definitions */
#include <stdio.h>

/* Function Declarations */
extern void Splitmain(const char audio_data[], const int audio_size[2],
                      emxArray_real_T *b_Splitmain, emxArray_real_T *frames);

#endif

/* End of code generation (Splitmain.h) */
