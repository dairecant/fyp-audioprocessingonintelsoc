/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * _coder_tremelo_float_mex_api.c
 *
 * Code generation for function '_coder_tremelo_float_mex_api'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "customFetchLoggedData.h"
#include "forcePushIntoCloud.h"
#include "logStmts.h"
#include "tremelo.h"
#include "_coder_tremelo_float_mex_api.h"
#include "tremelo_float_mex_emxutil.h"
#include "tremelo_float_mex_data.h"

/* Variable Definitions */
static emlrtRTEInfo e_emlrtRTEI = { 1, /* lineNo */
  1,                                   /* colNo */
  "_coder_tremelo_float_mex_api",      /* fName */
  ""                                   /* pName */
};

/* Function Declarations */
static const mxArray *b_emlrt_marshallOut(const emlrtStack *sp, const
  emxArray_struct0_T *u);
static int16_T (*c_emlrt_marshallIn(const emlrtStack *sp, const mxArray *sound,
  const char_T *identifier))[256];
static const mxArray *c_emlrt_marshallOut(const emlrtStack *sp, const
  emxArray_struct1_T *u);
static int16_T (*d_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u,
  const emlrtMsgIdentifier *parentId))[256];
static const mxArray *d_emlrt_marshallOut(const emlrtStack *sp, const char_T u
  [138]);
static real_T e_emlrt_marshallIn(const emlrtStack *sp, const mxArray *fs, const
  char_T *identifier);
static const mxArray *e_emlrt_marshallOut(const real_T u);
static const mxArray *emlrt_marshallOut(const real_T u[256]);
static real_T f_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId);
static int16_T (*h_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src,
  const emlrtMsgIdentifier *msgId))[256];
static real_T i_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId);

/* Function Definitions */
static const mxArray *b_emlrt_marshallOut(const emlrtStack *sp, const
  emxArray_struct0_T *u)
{
  const mxArray *y;
  int32_T i21;
  int32_T iv7[2];
  static const char * sv0[7] = { "Class", "Dims", "Varsize", "NumericType",
    "Fimath", "Data", "DataSize" };

  int32_T i;
  int32_T b_j1;
  emxArray_uint8_T *b_u;
  int32_T u_size[2];
  int32_T b_u_size[2];
  int32_T loop_ub;
  const mxArray *b_y;
  char_T u_data[6];
  const mxArray *m3;
  real_T *pData;
  int32_T c_u_size[2];
  uint8_T *b_pData;
  emlrtHeapReferenceStackEnterFcnR2012b(sp);
  y = NULL;
  for (i21 = 0; i21 < 2; i21++) {
    iv7[i21] = u->size[i21];
  }

  emlrtAssign(&y, emlrtCreateStructArray(2, iv7, 7, sv0));
  emlrtCreateField(y, "Class");
  emlrtCreateField(y, "Dims");
  emlrtCreateField(y, "Varsize");
  emlrtCreateField(y, "NumericType");
  emlrtCreateField(y, "Fimath");
  emlrtCreateField(y, "Data");
  emlrtCreateField(y, "DataSize");
  i = 0;
  b_j1 = 0;
  emxInit_uint8_T(sp, &b_u, 2, (emlrtRTEInfo *)NULL, true);
  if (0 < u->size[1U]) {
    u_size[0] = 1;
    b_u_size[0] = 1;
    b_u_size[1] = 2;
  }

  while (b_j1 < u->size[1U]) {
    u_size[1] = u->data[u->size[0] * b_j1].Class.size[1];
    loop_ub = u->data[u->size[0] * b_j1].Class.size[0] * u->data[u->size[0] *
      b_j1].Class.size[1];
    for (i21 = 0; i21 < loop_ub; i21++) {
      u_data[i21] = u->data[u->size[0] * b_j1].Class.data[i21];
    }

    b_y = NULL;
    m3 = emlrtCreateCharArray(2, u_size);
    emlrtInitCharArrayR2013a(sp, u->data[u->size[0] * b_j1].Class.size[1], m3,
      &u_data[0]);
    emlrtAssign(&b_y, m3);
    emlrtSetFieldR2017b(y, i, "Class", b_y, 0);
    b_y = NULL;
    m3 = emlrtCreateNumericArray(2, b_u_size, mxDOUBLE_CLASS, mxREAL);
    pData = (real_T *)emlrtMxGetPr(m3);
    i21 = 0;
    for (loop_ub = 0; loop_ub < 2; loop_ub++) {
      pData[i21] = u->data[u->size[0] * b_j1].Dims.data[u->data[u->size[0] *
        b_j1].Dims.size[0] * loop_ub];
      i21++;
    }

    emlrtAssign(&b_y, m3);
    emlrtSetFieldR2017b(y, i, "Dims", b_y, 1);
    b_y = NULL;
    m3 = emlrtCreateLogicalScalar(u->data[u->size[0] * b_j1].Varsize);
    emlrtAssign(&b_y, m3);
    emlrtSetFieldR2017b(y, i, "Varsize", b_y, 2);
    c_u_size[0] = 1;
    c_u_size[1] = 0;
    b_y = NULL;
    m3 = emlrtCreateCharArray(2, c_u_size);
    emlrtInitCharArrayR2013a(sp, 0, m3, NULL);
    emlrtAssign(&b_y, m3);
    emlrtSetFieldR2017b(y, i, "NumericType", b_y, 3);
    c_u_size[0] = 1;
    c_u_size[1] = 0;
    b_y = NULL;
    m3 = emlrtCreateCharArray(2, c_u_size);
    emlrtInitCharArrayR2013a(sp, 0, m3, NULL);
    emlrtAssign(&b_y, m3);
    emlrtSetFieldR2017b(y, i, "Fimath", b_y, 4);
    i21 = b_u->size[0] * b_u->size[1];
    b_u->size[0] = 1;
    b_u->size[1] = u->data[u->size[0] * b_j1].Data->size[1];
    emxEnsureCapacity_uint8_T(sp, b_u, i21, (emlrtRTEInfo *)NULL);
    loop_ub = u->data[u->size[0] * b_j1].Data->size[0] * u->data[u->size[0] *
      b_j1].Data->size[1];
    for (i21 = 0; i21 < loop_ub; i21++) {
      b_u->data[i21] = u->data[u->size[0] * b_j1].Data->data[i21];
    }

    b_y = NULL;
    m3 = emlrtCreateNumericArray(2, *(int32_T (*)[1])b_u->size, mxUINT8_CLASS,
      mxREAL);
    b_pData = (uint8_T *)emlrtMxGetData(m3);
    i21 = 0;
    for (loop_ub = 0; loop_ub < u->data[u->size[0] * b_j1].Data->size[1];
         loop_ub++) {
      b_pData[i21] = u->data[u->size[0] * b_j1].Data->data[u->data[u->size[0] *
        b_j1].Data->size[0] * loop_ub];
      i21++;
    }

    emlrtAssign(&b_y, m3);
    emlrtSetFieldR2017b(y, i, "Data", b_y, 5);
    b_y = NULL;
    m3 = emlrtCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL);
    *(uint32_T *)emlrtMxGetData(m3) = u->data[u->size[0] * b_j1].DataSize;
    emlrtAssign(&b_y, m3);
    emlrtSetFieldR2017b(y, i, "DataSize", b_y, 6);
    i++;
    b_j1++;
  }

  emxFree_uint8_T(&b_u);
  emlrtHeapReferenceStackLeaveFcnR2012b(sp);
  return y;
}

static int16_T (*c_emlrt_marshallIn(const emlrtStack *sp, const mxArray *sound,
  const char_T *identifier))[256]
{
  int16_T (*y)[256];
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = (const char *)identifier;
  thisId.fParent = NULL;
  thisId.bParentIsCell = false;
  y = d_emlrt_marshallIn(sp, emlrtAlias(sound), &thisId);
  emlrtDestroyArray(&sound);
  return y;
}
  static const mxArray *c_emlrt_marshallOut(const emlrtStack *sp, const
  emxArray_struct1_T *u)
{
  const mxArray *y;
  int32_T i;
  int32_T iv8[2];
  static const char * sv1[2] = { "ActualIndex", "FieldNames" };

  int32_T u_size[2];
  int32_T b_j1;
  const mxArray *b_y;
  const mxArray *m4;
  y = NULL;
  for (i = 0; i < 2; i++) {
    iv8[i] = u->size[i];
  }

  emlrtAssign(&y, emlrtCreateStructArray(2, iv8, 2, sv1));
  emlrtCreateField(y, "ActualIndex");
  emlrtCreateField(y, "FieldNames");
  i = 0;
  if (0 < u->size[1U]) {
    u_size[0] = 1;
    u_size[1] = 0;
  }

  for (b_j1 = 0; b_j1 < u->size[1U]; b_j1++) {
    b_y = NULL;
    m4 = emlrtCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL);
    *(uint32_T *)emlrtMxGetData(m4) = u->data[u->size[0] * b_j1].ActualIndex;
    emlrtAssign(&b_y, m4);
    emlrtSetFieldR2017b(y, i, "ActualIndex", b_y, 0);
    b_y = NULL;
    m4 = emlrtCreateCharArray(2, u_size);
    emlrtInitCharArrayR2013a(sp, 0, m4, NULL);
    emlrtAssign(&b_y, m4);
    emlrtSetFieldR2017b(y, i, "FieldNames", b_y, 1);
    i++;
  }

  return y;
}

static int16_T (*d_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u,
  const emlrtMsgIdentifier *parentId))[256]
{
  int16_T (*y)[256];
  y = h_emlrt_marshallIn(sp, emlrtAlias(u), parentId);
  emlrtDestroyArray(&u);
  return y;
}
  static const mxArray *d_emlrt_marshallOut(const emlrtStack *sp, const char_T
  u[138])
{
  const mxArray *y;
  const mxArray *m5;
  static const int32_T iv9[2] = { 1, 138 };

  y = NULL;
  m5 = emlrtCreateCharArray(2, iv9);
  emlrtInitCharArrayR2013a(sp, 138, m5, &u[0]);
  emlrtAssign(&y, m5);
  return y;
}

static real_T e_emlrt_marshallIn(const emlrtStack *sp, const mxArray *fs, const
  char_T *identifier)
{
  real_T y;
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = (const char *)identifier;
  thisId.fParent = NULL;
  thisId.bParentIsCell = false;
  y = f_emlrt_marshallIn(sp, emlrtAlias(fs), &thisId);
  emlrtDestroyArray(&fs);
  return y;
}

static const mxArray *e_emlrt_marshallOut(const real_T u)
{
  const mxArray *y;
  const mxArray *m6;
  y = NULL;
  m6 = emlrtCreateDoubleScalar(u);
  emlrtAssign(&y, m6);
  return y;
}

static const mxArray *emlrt_marshallOut(const real_T u[256])
{
  const mxArray *y;
  const mxArray *m2;
  static const int32_T iv5[1] = { 0 };

  static const int32_T iv6[1] = { 256 };

  y = NULL;
  m2 = emlrtCreateNumericArray(1, iv5, mxDOUBLE_CLASS, mxREAL);
  emlrtMxSetData((mxArray *)m2, (void *)&u[0]);
  emlrtSetDimensions((mxArray *)m2, iv6, 1);
  emlrtAssign(&y, m2);
  return y;
}

static real_T f_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId)
{
  real_T y;
  y = i_emlrt_marshallIn(sp, emlrtAlias(u), parentId);
  emlrtDestroyArray(&u);
  return y;
}

static int16_T (*h_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src,
  const emlrtMsgIdentifier *msgId))[256]
{
  int16_T (*ret)[256];
  static const int32_T dims[1] = { 256 };

  emlrtCheckBuiltInR2012b(sp, msgId, src, "int16", false, 1U, dims);
  ret = (int16_T (*)[256])emlrtMxGetData(src);
  emlrtDestroyArray(&src);
  return ret;
}
  static real_T i_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src,
  const emlrtMsgIdentifier *msgId)
{
  real_T ret;
  static const int32_T dims = 0;
  emlrtCheckBuiltInR2012b(sp, msgId, src, "double", false, 0U, &dims);
  ret = *(real_T *)emlrtMxGetData(src);
  emlrtDestroyArray(&src);
  return ret;
}

void customFetchLoggedData_api(const mxArray *plhs[4])
{
  emxArray_struct0_T *data;
  emxArray_struct1_T *dataInfo;
  char_T dataExprIdMapping[138];
  real_T numLoggedExpr;
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  st.tls = emlrtRootTLSGlobal;
  emlrtHeapReferenceStackEnterFcnR2012b(&st);
  emxInit_struct0_T(&st, &data, 2, &e_emlrtRTEI, true);
  emxInit_struct1_T(&st, &dataInfo, 2, &e_emlrtRTEI, true);

  /* Invoke the target function */
  customFetchLoggedData(&st, data, dataInfo, dataExprIdMapping, &numLoggedExpr);

  /* Marshall function outputs */
  plhs[0] = b_emlrt_marshallOut(&st, data);
  plhs[1] = c_emlrt_marshallOut(&st, dataInfo);
  plhs[2] = d_emlrt_marshallOut(&st, dataExprIdMapping);
  plhs[3] = e_emlrt_marshallOut(numLoggedExpr);
  emxFree_struct1_T(&dataInfo);
  emxFree_struct0_T(&data);
  emlrtHeapReferenceStackLeaveFcnR2012b(&st);
}

void forcePushIntoCloud_api(void)
{
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  st.tls = emlrtRootTLSGlobal;

  /* Invoke the target function */
  forcePushIntoCloud(&st);
}

void logStmts_api(void)
{
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  st.tls = emlrtRootTLSGlobal;

  /* Invoke the target function */
  logStmts(&st);
}

void tremelo_api(const mxArray * const prhs[4], const mxArray *plhs[1])
{
  real_T (*b_tremelo)[256];
  int16_T (*sound)[256];
  real_T fs;
  real_T freq;
  real_T gain;
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  st.tls = emlrtRootTLSGlobal;
  b_tremelo = (real_T (*)[256])mxMalloc(sizeof(real_T [256]));

  /* Marshall function inputs */
  sound = c_emlrt_marshallIn(&st, emlrtAlias(prhs[0]), "sound");
  fs = e_emlrt_marshallIn(&st, emlrtAliasP(prhs[1]), "fs");
  freq = e_emlrt_marshallIn(&st, emlrtAliasP(prhs[2]), "freq");
  gain = e_emlrt_marshallIn(&st, emlrtAliasP(prhs[3]), "gain");

  /* Invoke the target function */
  tremelo(&st, *sound, fs, freq, gain, *b_tremelo);

  /* Marshall function outputs */
  plhs[0] = emlrt_marshallOut(*b_tremelo);
}

/* End of code generation (_coder_tremelo_float_mex_api.c) */
