/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * _coder_tremelo_float_mex_api.h
 *
 * Code generation for function '_coder_tremelo_float_mex_api'
 *
 */

#ifndef _CODER_TREMELO_FLOAT_MEX_API_H
#define _CODER_TREMELO_FLOAT_MEX_API_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mwmathutil.h"
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "covrt.h"
#include "rtwtypes.h"
#include "tremelo_float_mex_types.h"

/* Function Declarations */
extern void customFetchLoggedData_api(const mxArray *plhs[4]);
extern void forcePushIntoCloud_api(void);
extern void logStmts_api(void);
extern void tremelo_api(const mxArray * const prhs[4], const mxArray *plhs[1]);

#endif

/* End of code generation (_coder_tremelo_float_mex_api.h) */
