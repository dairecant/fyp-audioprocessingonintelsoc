/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * customFetchLoggedData.c
 *
 * Code generation for function 'customFetchLoggedData'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "customFetchLoggedData.h"
#include "forcePushIntoCloud.h"
#include "logStmts.h"
#include "tremelo.h"
#include "custom_mex_logger.h"

/* Variable Definitions */
static emlrtRSInfo f_emlrtRSI = { 1,   /* lineNo */
  "customFetchLoggedData",             /* fcnName */
  "C:\\Program Files\\MATLAB\\R2017b\\toolbox\\coder\\float2fixed\\custom_logger\\customFetchLoggedData.p"/* pathName */
};

/* Function Definitions */
void customFetchLoggedData(const emlrtStack *sp, emxArray_struct0_T *data,
  emxArray_struct1_T *dataInfo, char_T dataExprIdMapping[138], real_T
  *numLoggedExpr)
{
  int32_T i19;
  static const char_T cv0[138] = { 't', 'r', 'e', 'm', 'e', 'l', 'o', ',', 'C',
    ':', '\\', 'U', 's', 'e', 'r', 's', '\\', 'D', '\xe1', 'i', 'r', 'e', '\\',
    'D', 'o', 'c', 'u', 'm', 'e', 'n', 't', 's', '\\', 'F', 'i', 'n', 'a', 'l',
    '_', 'Y', 'e', 'a', 'r', '\\', 'F', 'Y', 'P', '\\', 'g', 'i', 't', '\\', 'm',
    'a', 't', 'l', 'a', 'b', '_', 'm', 'o', 'd', 'e', 'l', 's', '\\', 't', 'r',
    'e', 'm', 'e', 'l', 'o', '.', 'm', '$', '$', 'i', 'n', 'p', 'u', 't', 's',
    '$', '$', '<', '>', 's', 'o', 'u', 'n', 'd', ',', '2', '<', '>', 'f', 's',
    ',', '3', '<', '>', 'f', 'r', 'e', 'q', ',', '4', '<', '>', 'g', 'a', 'i',
    'n', ',', '5', '$', '$', 'o', 'u', 't', 'p', 'u', 't', 's', '$', '$', '<',
    '>', 't', 'r', 'e', 'm', 'e', 'l', 'o', ',', '6' };

  real_T b_numLoggedExpr;
  emlrtStack st;
  st.prev = sp;
  st.tls = sp->tls;
  for (i19 = 0; i19 < 138; i19++) {
    dataExprIdMapping[i19] = cv0[i19];
  }

  st.site = &f_emlrtRSI;
  d_custom_mex_logger(&st, data, dataInfo);
  b_numLoggedExpr = (real_T)dataInfo->size[1] - 1.0;
  *numLoggedExpr = b_numLoggedExpr;
}

/* End of code generation (customFetchLoggedData.c) */
