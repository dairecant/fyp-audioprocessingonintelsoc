/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * tremelo.c
 *
 * Code generation for function 'tremelo'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "customFetchLoggedData.h"
#include "forcePushIntoCloud.h"
#include "logStmts.h"
#include "tremelo.h"
#include "custom_mex_logger.h"
#include "tremelo_float_mex_mexutil.h"
#include "tremelo_float_mex_data.h"

/* Variable Definitions */
static emlrtRSInfo emlrtRSI = { 1,     /* lineNo */
  "tremelo",                           /* fcnName */
  "C:\\\\Users\\\\D\xc3\xa1ire\\\\Documents\\\\Final_Year\\\\FYP\\\\git\\\\matlab_models\\\\tremelo.m"/* pathName */
};

/* Function Declarations */
static void b_emlrt_update_log_1(const int16_T in[256],
  emlrtLocationLoggingDataType table[], int32_T b_index);
static void b_emlrt_update_log_3(const real_T in[256],
  emlrtLocationLoggingDataType table[], int32_T b_index);
static void emlrt_update_log_1(const int16_T in[256],
  emlrtLocationLoggingDataType table[], int32_T b_index, int16_T out[256]);
static real_T emlrt_update_log_2(real_T in, emlrtLocationLoggingDataType table[],
  int32_T b_index);
static void emlrt_update_log_3(const real_T in[256],
  emlrtLocationLoggingDataType table[], int32_T b_index, real_T out[256]);
static int16_T emlrt_update_log_4(int16_T in, emlrtLocationLoggingDataType
  table[], int32_T b_index);

/* Function Definitions */
static void b_emlrt_update_log_1(const int16_T in[256],
  emlrtLocationLoggingDataType table[], int32_T b_index)
{
  emlrtLocationLoggingDataType *b_table;
  real_T d4;
  int16_T localMin;
  int16_T localMax;
  int32_T i;
  if (b_index >= 0) {
    b_table = (emlrtLocationLoggingDataType *)&table[b_index];
    d4 = muDoubleScalarFloor(b_table[0U].SimMin);
    if (d4 < 32768.0) {
      if (d4 >= -32768.0) {
        localMin = (int16_T)d4;
      } else {
        localMin = MIN_int16_T;
      }
    } else if (d4 >= 32768.0) {
      localMin = MAX_int16_T;
    } else {
      localMin = 0;
    }

    d4 = muDoubleScalarFloor(b_table[0U].SimMax);
    if (d4 < 32768.0) {
      if (d4 >= -32768.0) {
        localMax = (int16_T)d4;
      } else {
        localMax = MIN_int16_T;
      }
    } else if (d4 >= 32768.0) {
      localMax = MAX_int16_T;
    } else {
      localMax = 0;
    }

    for (i = 0; i < 256; i++) {
      /* Simulation Min-Max logging. */
      if (in[i] < localMin) {
        localMin = in[i];
      }

      if (in[i] > localMax) {
        localMax = in[i];
      }
    }

    b_table[0U].SimMin = localMin;
    b_table[0U].SimMax = localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }
}

static void b_emlrt_update_log_3(const real_T in[256],
  emlrtLocationLoggingDataType table[], int32_T b_index)
{
  emlrtLocationLoggingDataType *b_table;
  real_T localMin;
  real_T localMax;
  int32_T i;
  if (b_index >= 0) {
    b_table = (emlrtLocationLoggingDataType *)&table[b_index];
    localMin = b_table[0U].SimMin;
    localMax = b_table[0U].SimMax;
    for (i = 0; i < 256; i++) {
      /* Simulation Min-Max logging. */
      if (in[i] < localMin) {
        localMin = in[i];
      }

      if (in[i] > localMax) {
        localMax = in[i];
      }
    }

    b_table[0U].SimMin = localMin;
    b_table[0U].SimMax = localMax;

    /* IsAlwaysInteger logging. */
    i = 0;
    while (b_table[0U].IsAlwaysInteger && (i < 256)) {
      if (in[i] != muDoubleScalarFloor(in[i])) {
        b_table[0U].IsAlwaysInteger = false;
      }

      i++;
    }
  }
}

static void emlrt_update_log_1(const int16_T in[256],
  emlrtLocationLoggingDataType table[], int32_T b_index, int16_T out[256])
{
  memcpy(&out[0], &in[0], sizeof(int16_T) << 8);
  b_emlrt_update_log_1(out, table, b_index);
}

static real_T emlrt_update_log_2(real_T in, emlrtLocationLoggingDataType table[],
  int32_T b_index)
{
  emlrtLocationLoggingDataType *b_table;
  real_T localMin;
  real_T localMax;
  if (b_index >= 0) {
    b_table = (emlrtLocationLoggingDataType *)&table[b_index];
    localMin = b_table[0U].SimMin;
    localMax = b_table[0U].SimMax;

    /* Simulation Min-Max logging. */
    if (in < localMin) {
      localMin = in;
    }

    if (in > localMax) {
      localMax = in;
    }

    b_table[0U].SimMin = localMin;
    b_table[0U].SimMax = localMax;

    /* IsAlwaysInteger logging. */
    if (in != muDoubleScalarFloor(in)) {
      b_table[0U].IsAlwaysInteger = false;
    }
  }

  return in;
}

static void emlrt_update_log_3(const real_T in[256],
  emlrtLocationLoggingDataType table[], int32_T b_index, real_T out[256])
{
  memcpy(&out[0], &in[0], sizeof(real_T) << 8);
  b_emlrt_update_log_3(out, table, b_index);
}

static int16_T emlrt_update_log_4(int16_T in, emlrtLocationLoggingDataType
  table[], int32_T b_index)
{
  emlrtLocationLoggingDataType *b_table;
  real_T d3;
  int16_T localMin;
  int16_T localMax;
  if (b_index >= 0) {
    b_table = (emlrtLocationLoggingDataType *)&table[b_index];
    d3 = muDoubleScalarFloor(b_table[0U].SimMin);
    if (d3 < 32768.0) {
      if (d3 >= -32768.0) {
        localMin = (int16_T)d3;
      } else {
        localMin = MIN_int16_T;
      }
    } else if (d3 >= 32768.0) {
      localMin = MAX_int16_T;
    } else {
      localMin = 0;
    }

    d3 = muDoubleScalarFloor(b_table[0U].SimMax);
    if (d3 < 32768.0) {
      if (d3 >= -32768.0) {
        localMax = (int16_T)d3;
      } else {
        localMax = MIN_int16_T;
      }
    } else if (d3 >= 32768.0) {
      localMax = MAX_int16_T;
    } else {
      localMax = 0;
    }

    /* Simulation Min-Max logging. */
    if (in < localMin) {
      localMin = in;
    }

    if (in > localMax) {
      localMax = in;
    }

    b_table[0U].SimMin = localMin;
    b_table[0U].SimMax = localMax;

    /* IsAlwaysInteger logging. */
    /* Data type is always integer. */
  }

  return in;
}

void tremelo(const emlrtStack *sp, const int16_T sound[256], real_T fs, real_T
             freq, real_T gain, real_T b_tremelo[256])
{
  int16_T unusedExpr[256];
  real_T dv0[256];
  real_T b_unusedExpr[256];
  int32_T i;
  real_T b_i;
  real_T x;
  int16_T i0;
  emlrtStack st;
  st.prev = sp;
  st.tls = sp->tls;
  emlrtInitVarDataTables(emlrtLocationLoggingDataTables);

  /* logging input variable 'sound' for function 'tremelo' */
  emlrt_update_log_1(sound, emlrtLocationLoggingDataTables, 0, unusedExpr);

  /* logging input variable 'fs' for function 'tremelo' */
  emlrt_update_log_2(fs, emlrtLocationLoggingDataTables, 1);

  /* logging input variable 'freq' for function 'tremelo' */
  emlrt_update_log_2(freq, emlrtLocationLoggingDataTables, 2);

  /* logging input variable 'gain' for function 'tremelo' */
  emlrt_update_log_2(gain, emlrtLocationLoggingDataTables, 3);
  st.site = &emlrtRSI;
  c_custom_mex_logger(&st, 2U, sound);
  st.site = &emlrtRSI;
  custom_mex_logger(&st, 3U, fs);
  st.site = &emlrtRSI;
  custom_mex_logger(&st, 4U, freq);
  st.site = &emlrtRSI;
  custom_mex_logger(&st, 5U, gain);
  covrtLogFcn(&emlrtCoverageInstance, 0U, 0U);
  covrtLogBasicBlock(&emlrtCoverageInstance, 0U, 0U);

  /* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
  /*            TREMELO EFFECT             % */
  /*  Author:   D�ire Canavan 30/10/2017   % */
  /*  Implements tremelo effect on audio   % */
  /*  Works on modulating gain value       % */
  /*   sound: inputted wav file            % */
  /*   fs:    sampling frequency           % */
  /*   freq:  frequency of gain modulation % */
  /*   gain:  amplitude overall of the gain% */
  /* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
  memset(&dv0[0], 0, sizeof(real_T) << 8);
  emlrt_update_log_3(dv0, emlrtLocationLoggingDataTables, 4, b_unusedExpr);
  i = 0;
  while (i < 256) {
    b_i = emlrt_update_log_2(1.0 + (real_T)i, emlrtLocationLoggingDataTables, 12);
    covrtLogFor(&emlrtCoverageInstance, 0U, 0U, 0, 1);
    covrtLogBasicBlock(&emlrtCoverageInstance, 0U, 1U);
    x = emlrt_update_log_2(6.2831853071795862 * freq,
      emlrtLocationLoggingDataTables, 11);
    x = emlrt_update_log_2(emlrt_update_log_2(x / fs,
      emlrtLocationLoggingDataTables, 10) * b_i, emlrtLocationLoggingDataTables,
      9);
    x = muDoubleScalarSin(x);
    x = emlrt_update_log_2(gain * emlrt_update_log_2(x,
      emlrtLocationLoggingDataTables, 8), emlrtLocationLoggingDataTables, 7);
    x = muDoubleScalarRound(x * (real_T)sound[(int32_T)b_i - 1]);
    if (x < 32768.0) {
      if (x >= -32768.0) {
        i0 = (int16_T)x;
      } else {
        i0 = MIN_int16_T;
      }
    } else if (x >= 32768.0) {
      i0 = MAX_int16_T;
    } else {
      i0 = 0;
    }

    b_tremelo[(int32_T)b_i - 1] = emlrt_update_log_2(emlrt_update_log_4(i0,
      emlrtLocationLoggingDataTables, 6), emlrtLocationLoggingDataTables, 5);
    i++;
    if (*emlrtBreakCheckR2012bFlagVar != 0) {
      emlrtBreakCheckR2012b(sp);
    }
  }

  covrtLogFor(&emlrtCoverageInstance, 0U, 0U, 0, 0);

  /* tremelo=transpose(tremelo); */
  st.site = &emlrtRSI;
  b_custom_mex_logger(&st, 6U, b_tremelo);
}

/* End of code generation (tremelo.c) */
