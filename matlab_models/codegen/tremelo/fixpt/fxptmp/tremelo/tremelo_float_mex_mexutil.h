/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * tremelo_float_mex_mexutil.h
 *
 * Code generation for function 'tremelo_float_mex_mexutil'
 *
 */

#ifndef TREMELO_FLOAT_MEX_MEXUTIL_H
#define TREMELO_FLOAT_MEX_MEXUTIL_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mwmathutil.h"
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "covrt.h"
#include "rtwtypes.h"
#include "tremelo_float_mex_types.h"

/* Function Declarations */
extern void emlrtInitVarDataTables(emlrtLocationLoggingDataType dataTables[13]);

#endif

/* End of code generation (tremelo_float_mex_mexutil.h) */
