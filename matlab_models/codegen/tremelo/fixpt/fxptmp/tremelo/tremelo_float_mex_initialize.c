/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * tremelo_float_mex_initialize.c
 *
 * Code generation for function 'tremelo_float_mex_initialize'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "customFetchLoggedData.h"
#include "forcePushIntoCloud.h"
#include "logStmts.h"
#include "tremelo.h"
#include "tremelo_float_mex_initialize.h"
#include "custom_mex_logger.h"
#include "_coder_tremelo_float_mex_mex.h"
#include "tremelo_float_mex_data.h"

/* Function Declarations */
static void tremelo_float_mex_once(const emlrtStack *sp);

/* Function Definitions */
static void tremelo_float_mex_once(const emlrtStack *sp)
{
  covrtInstanceData *t3_data = NULL;
  emlrtStack st;
  st.prev = sp;
  st.tls = sp->tls;
  emlrtCoverageInstance.data = t3_data;
  pInit_not_empty_init();
  pBufferLen_not_empty_init();

  /* Allocate instance data */
  covrtAllocateInstanceData(&emlrtCoverageInstance);

  /* Initialize Coverage Information */
  covrtScriptInit(&emlrtCoverageInstance,
                  "C:\\\\Users\\\\D\xe1ire\\\\Documents\\\\Final_Year\\\\FYP\\\\git\\\\matlab_models\\\\tremelo.m",
                  0U, 1U, 2U, 0U, 0U, 0U, 0U, 1U, 0U, 0U, 0U);

  /* Initialize Function Information */
  covrtFcnInit(&emlrtCoverageInstance, 0U, 0U, "tremelo", 411, -1, 572);

  /* Initialize Basic Block Information */
  covrtBasicBlockInit(&emlrtCoverageInstance, 0U, 1U, 520, -1, 565);
  covrtBasicBlockInit(&emlrtCoverageInstance, 0U, 0U, 458, -1, 490);

  /* Initialize If Information */
  /* Initialize MCDC Information */
  /* Initialize For Information */
  covrtForInit(&emlrtCoverageInstance, 0U, 0U, 492, 516, 570);

  /* Initialize While Information */
  /* Initialize Switch Information */
  /* Start callback for coverage engine */
  covrtScriptStart(&emlrtCoverageInstance, 0U);
  st.site = NULL;
  indexMapper_init(&st);
  st.site = NULL;
  buffers_init(&st);
  st.site = NULL;
  customCoderEnableLog_init(&st);
}

void tremelo_float_mex_initialize(void)
{
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  mexFunctionCreateRootTLS();
  emlrtBreakCheckR2012bFlagVar = emlrtGetBreakCheckFlagAddressR2012b();
  st.tls = emlrtRootTLSGlobal;
  emlrtClearAllocCountR2012b(&st, false, 0U, 0);
  emlrtEnterRtStackR2012b(&st);
  if (emlrtFirstTimeR2012b(emlrtRootTLSGlobal)) {
    tremelo_float_mex_once(&st);
  }
}

/* End of code generation (tremelo_float_mex_initialize.c) */
