/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * tremelo_float_mex_terminate.h
 *
 * Code generation for function 'tremelo_float_mex_terminate'
 *
 */

#ifndef TREMELO_FLOAT_MEX_TERMINATE_H
#define TREMELO_FLOAT_MEX_TERMINATE_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mwmathutil.h"
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "covrt.h"
#include "rtwtypes.h"
#include "tremelo_float_mex_types.h"

/* Function Declarations */
extern void emlrtLocationLoggingAccessor(void **info, size_t *size);
extern int32_T emlrtLocationLoggingGetVersion(void);
extern void tremelo_float_mex_atexit(void);
extern void tremelo_float_mex_terminate(void);

#endif

/* End of code generation (tremelo_float_mex_terminate.h) */
