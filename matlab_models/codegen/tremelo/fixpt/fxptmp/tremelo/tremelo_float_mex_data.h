/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * tremelo_float_mex_data.h
 *
 * Code generation for function 'tremelo_float_mex_data'
 *
 */

#ifndef TREMELO_FLOAT_MEX_DATA_H
#define TREMELO_FLOAT_MEX_DATA_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mwmathutil.h"
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "covrt.h"
#include "rtwtypes.h"
#include "tremelo_float_mex_types.h"

/* Variable Declarations */
extern emlrtLocationLoggingDataType emlrtLocationLoggingDataTables[13];
extern emlrtCTX emlrtRootTLSGlobal;
extern const volatile char_T *emlrtBreakCheckR2012bFlagVar;
extern covrtInstance emlrtCoverageInstance;
extern emlrtContext emlrtContextGlobal;

#endif

/* End of code generation (tremelo_float_mex_data.h) */
