/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * logStmts.h
 *
 * Code generation for function 'logStmts'
 *
 */

#ifndef LOGSTMTS_H
#define LOGSTMTS_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mwmathutil.h"
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "covrt.h"
#include "rtwtypes.h"
#include "tremelo_float_mex_types.h"

/* Function Declarations */
extern void logStmts(const emlrtStack *sp);

#endif

/* End of code generation (logStmts.h) */
