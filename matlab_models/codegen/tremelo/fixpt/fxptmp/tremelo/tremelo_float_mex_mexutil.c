/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * tremelo_float_mex_mexutil.c
 *
 * Code generation for function 'tremelo_float_mex_mexutil'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "customFetchLoggedData.h"
#include "forcePushIntoCloud.h"
#include "logStmts.h"
#include "tremelo.h"
#include "tremelo_float_mex_mexutil.h"

/* Function Definitions */
void emlrtInitVarDataTables(emlrtLocationLoggingDataType dataTables[13])
{
  int32_T i;
  for (i = 0; i < 13; i++) {
    dataTables[i].SimMin = rtInf;
    dataTables[i].SimMax = rtMinusInf;
    dataTables[i].OverflowWraps = 0;
    dataTables[i].Saturations = 0;
    dataTables[i].IsAlwaysInteger = true;
    dataTables[i].HistogramTable = (emlrtLocationLoggingHistogramType *)NULL;
  }
}

/* End of code generation (tremelo_float_mex_mexutil.c) */
