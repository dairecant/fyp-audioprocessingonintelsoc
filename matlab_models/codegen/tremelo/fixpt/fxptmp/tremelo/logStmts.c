/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * logStmts.c
 *
 * Code generation for function 'logStmts'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "customFetchLoggedData.h"
#include "forcePushIntoCloud.h"
#include "logStmts.h"
#include "tremelo.h"
#include "custom_mex_logger.h"

/* Variable Definitions */
static emlrtRSInfo b_emlrtRSI = { 1,   /* lineNo */
  "logStmts",                          /* fcnName */
  "C:\\Program Files\\MATLAB\\R2017b\\toolbox\\coder\\float2fixed\\custom_logger\\logStmts.p"/* pathName */
};

/* Function Definitions */
void logStmts(const emlrtStack *sp)
{
  real_T dv1[256];
  int16_T iv0[256];
  emlrtStack st;
  st.prev = sp;
  st.tls = sp->tls;
  st.site = &b_emlrtRSI;
  custom_mex_logger(&st, 0U, 0.0);
  memset(&dv1[0], 0, sizeof(real_T) << 8);
  st.site = &b_emlrtRSI;
  b_custom_mex_logger(&st, 0U, dv1);
  memset(&iv0[0], 0, sizeof(int16_T) << 8);
  st.site = &b_emlrtRSI;
  c_custom_mex_logger(&st, 0U, iv0);
}

/* End of code generation (logStmts.c) */
