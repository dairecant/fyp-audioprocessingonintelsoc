@echo off
set MATLAB=C:\PROGRA~1\MATLAB\R2017b
set MATLAB_ARCH=win64
set MATLAB_BIN="C:\Program Files\MATLAB\R2017b\bin"
set ENTRYPOINT=mexFunction
set OUTDIR=.\
set LIB_NAME=tremelo_float_mex
set MEX_NAME=tremelo_float_mex
set MEX_EXT=.mexw64
call setEnv.bat
echo # Make settings for tremelo_float_mex > tremelo_float_mex_mex.mki
echo COMPILER=%COMPILER%>> tremelo_float_mex_mex.mki
echo COMPFLAGS=%COMPFLAGS%>> tremelo_float_mex_mex.mki
echo OPTIMFLAGS=%OPTIMFLAGS%>> tremelo_float_mex_mex.mki
echo DEBUGFLAGS=%DEBUGFLAGS%>> tremelo_float_mex_mex.mki
echo LINKER=%LINKER%>> tremelo_float_mex_mex.mki
echo LINKFLAGS=%LINKFLAGS%>> tremelo_float_mex_mex.mki
echo LINKOPTIMFLAGS=%LINKOPTIMFLAGS%>> tremelo_float_mex_mex.mki
echo LINKDEBUGFLAGS=%LINKDEBUGFLAGS%>> tremelo_float_mex_mex.mki
echo MATLAB_ARCH=%MATLAB_ARCH%>> tremelo_float_mex_mex.mki
echo OMPFLAGS= >> tremelo_float_mex_mex.mki
echo OMPLINKFLAGS= >> tremelo_float_mex_mex.mki
echo EMC_COMPILER=msvc150>> tremelo_float_mex_mex.mki
echo EMC_CONFIG=optim>> tremelo_float_mex_mex.mki
"C:\Program Files\MATLAB\R2017b\bin\win64\gmake" -B -f tremelo_float_mex_mex.mk
