/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * forcePushIntoCloud.c
 *
 * Code generation for function 'forcePushIntoCloud'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "customFetchLoggedData.h"
#include "forcePushIntoCloud.h"
#include "logStmts.h"
#include "tremelo.h"
#include "tremelo_float_mex_mexutil.h"
#include "tremelo_float_mex_data.h"

/* Function Definitions */
void forcePushIntoCloud(const emlrtStack *sp)
{
  (void)sp;
  emlrtInitVarDataTables(emlrtLocationLoggingDataTables);
}

/* End of code generation (forcePushIntoCloud.c) */
