/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * tremelo.h
 *
 * Code generation for function 'tremelo'
 *
 */

#ifndef TREMELO_H
#define TREMELO_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mwmathutil.h"
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "covrt.h"
#include "rtwtypes.h"
#include "tremelo_float_mex_types.h"

/* Function Declarations */
extern void tremelo(const emlrtStack *sp, const int16_T sound[256], real_T fs,
                    real_T freq, real_T gain, real_T b_tremelo[256]);

#endif

/* End of code generation (tremelo.h) */
