/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * fileManager.c
 *
 * Code generation for function 'fileManager'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "Splitmain.h"
#include "fileManager.h"
#include "Splitmain_rtwutil.h"
#include "error.h"
#include <stdio.h>
#include <stdlib.h>

/* Variable Definitions */
static FILE * eml_openfiles[20];
static boolean_T eml_openfiles_not_empty;
static boolean_T eml_autoflush[20];
static rtRunTimeErrorInfo c_emlrtRTEI = { 156,/* lineNo */
  5,                                   /* colNo */
  "fileManager",                       /* fName */
  "C:\\Program Files\\MATLAB\\R2017b\\toolbox\\eml\\lib\\matlab\\iofun\\private\\fileManager.m"/* pName */
};

static rtRunTimeErrorInfo d_emlrtRTEI = { 299,/* lineNo */
  1,                                   /* colNo */
  "fileManager",                       /* fName */
  "C:\\Program Files\\MATLAB\\R2017b\\toolbox\\eml\\lib\\matlab\\iofun\\private\\fileManager.m"/* pName */
};

static rtRunTimeErrorInfo e_emlrtRTEI = { 295,/* lineNo */
  1,                                   /* colNo */
  "fileManager",                       /* fName */
  "C:\\Program Files\\MATLAB\\R2017b\\toolbox\\eml\\lib\\matlab\\iofun\\private\\fileManager.m"/* pName */
};

/* Function Declarations */
static signed char filedata(void);

/* Function Definitions */
static signed char filedata(void)
{
  signed char f;
  FILE * a;
  int i;
  signed char k;
  boolean_T exitg1;
  if (!eml_openfiles_not_empty) {
    a = NULL;
    for (i = 0; i < 20; i++) {
      eml_autoflush[i] = false;
      eml_openfiles[i] = a;
    }

    eml_openfiles_not_empty = true;
  }

  f = 0;
  k = 1;
  exitg1 = false;
  while ((!exitg1) && (k < 21)) {
    if (eml_openfiles[k - 1] == NULL) {
      f = k;
      exitg1 = true;
    } else {
      k++;
    }
  }

  return f;
}

signed char cfopen(const char cfilename_data[], const int cfilename_size[2],
                   const char * cpermission)
{
  signed char fileid;
  signed char j;
  int i;
  int i2;
  char ccfilename_data[17];
  FILE * filestar;
  FILE * a;
  fileid = -1;
  j = filedata();
  if (j < 1) {
    error();
  } else {
    i = cfilename_size[1];
    for (i2 = 0; i2 < i; i2++) {
      ccfilename_data[i2] = cfilename_data[cfilename_size[0] * i2];
    }

    ccfilename_data[cfilename_size[1]] = '\x00';
    filestar = fopen(&ccfilename_data[0], cpermission);
    if (filestar != NULL) {
      if (!eml_openfiles_not_empty) {
        a = NULL;
        for (i = 0; i < 20; i++) {
          eml_autoflush[i] = false;
          eml_openfiles[i] = a;
        }

        eml_openfiles_not_empty = true;
      }

      eml_openfiles[j - 1] = filestar;
      eml_autoflush[j - 1] = true;
      i2 = j + 2;
      if (i2 > 127) {
        i2 = 127;
      }

      fileid = (signed char)i2;
    }
  }

  return fileid;
}

void eml_openfiles_not_empty_init(void)
{
  eml_openfiles_not_empty = false;
}

FILE * fileManager(double varargin_1)
{
  FILE * f;
  signed char fileid;
  FILE * a;
  int i;
  if (varargin_1 < 0.0) {
    d_rtErrorWithMessageID(&e_emlrtRTEI);
  }

  fileid = (signed char)rt_roundd_snf(varargin_1);
  if (varargin_1 != fileid) {
    d_rtErrorWithMessageID(&d_emlrtRTEI);
  }

  if ((fileid < 0) || (varargin_1 != fileid)) {
    fileid = -1;
  }

  if (fileid >= 3) {
    if (!eml_openfiles_not_empty) {
      a = NULL;
      for (i = 0; i < 20; i++) {
        eml_autoflush[i] = false;
        eml_openfiles[i] = a;
      }

      eml_openfiles_not_empty = true;
    }

    f = eml_openfiles[fileid - 3];
    if (eml_openfiles[fileid - 3] == NULL) {
      c_rtErrorWithMessageID(&c_emlrtRTEI);
    }
  } else if (fileid == 0) {
    f = stdin;
  } else if (fileid == 1) {
    f = stdout;
  } else if (fileid == 2) {
    f = stderr;
  } else {
    f = NULL;
  }

  return f;
}

/* End of code generation (fileManager.c) */
