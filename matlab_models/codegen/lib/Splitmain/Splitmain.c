/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * Splitmain.c
 *
 * Code generation for function 'Splitmain'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "Splitmain.h"
#include "Splitmain_rtwutil.h"
#include "Splitmain_emxutil.h"
#include "fread.h"
#include "error.h"
#include "fileManager.h"
#include <stdio.h>
#include <stdlib.h>

/* Type Definitions */

/* Variable Definitions */
static rtRunTimeErrorInfo emlrtRTEI = { 25,/* lineNo */
  19,                                  /* colNo */
  "frewind",                           /* fName */
  "C:\\Program Files\\MATLAB\\R2017b\\toolbox\\eml\\lib\\matlab\\iofun\\frewind.m"/* pName */
};

static rtDoubleCheckInfo emlrtDCI = { 16,/* lineNo */
  17,                                  /* colNo */
  "Splitmain",                         /* fName */
  "C:\\\\Users\\\\D\xc3\xa1ire\\\\Documents\\\\Final_Year\\\\FYP\\\\git\\\\matlab_models\\\\Splitmain.m",/* pName */
  1                                    /* checkKind */
};

static rtBoundsCheckInfo emlrtBCI = { -1,/* iFirst */
  -1,                                  /* iLast */
  29,                                  /* lineNo */
  28,                                  /* colNo */
  "Splitmain",                         /* aName */
  "Splitmain",                         /* fName */
  "C:\\\\Users\\\\D\xc3\xa1ire\\\\Documents\\\\Final_Year\\\\FYP\\\\git\\\\matlab_models\\\\Splitmain.m",/* pName */
  0                                    /* checkKind */
};

static rtBoundsCheckInfo b_emlrtBCI = { -1,/* iFirst */
  -1,                                  /* iLast */
  29,                                  /* lineNo */
  14,                                  /* colNo */
  "frames",                            /* aName */
  "Splitmain",                         /* fName */
  "C:\\\\Users\\\\D\xc3\xa1ire\\\\Documents\\\\Final_Year\\\\FYP\\\\git\\\\matlab_models\\\\Splitmain.m",/* pName */
  0                                    /* checkKind */
};

static rtBoundsCheckInfo c_emlrtBCI = { -1,/* iFirst */
  -1,                                  /* iLast */
  18,                                  /* lineNo */
  37,                                  /* colNo */
  "a",                                 /* aName */
  "Splitmain",                         /* fName */
  "C:\\\\Users\\\\D\xc3\xa1ire\\\\Documents\\\\Final_Year\\\\FYP\\\\git\\\\matlab_models\\\\Splitmain.m",/* pName */
  0                                    /* checkKind */
};

static rtBoundsCheckInfo d_emlrtBCI = { -1,/* iFirst */
  -1,                                  /* iLast */
  18,                                  /* lineNo */
  42,                                  /* colNo */
  "a",                                 /* aName */
  "Splitmain",                         /* fName */
  "C:\\\\Users\\\\D\xc3\xa1ire\\\\Documents\\\\Final_Year\\\\FYP\\\\git\\\\matlab_models\\\\Splitmain.m",/* pName */
  0                                    /* checkKind */
};

static rtBoundsCheckInfo e_emlrtBCI = { -1,/* iFirst */
  -1,                                  /* iLast */
  18,                                  /* lineNo */
  13,                                  /* colNo */
  "Splitmain",                         /* aName */
  "Splitmain",                         /* fName */
  "C:\\\\Users\\\\D\xc3\xa1ire\\\\Documents\\\\Final_Year\\\\FYP\\\\git\\\\matlab_models\\\\Splitmain.m",/* pName */
  0                                    /* checkKind */
};

/* Function Definitions */
void Splitmain(const char audio_data[], const int audio_size[2], emxArray_real_T
               *b_Splitmain, emxArray_real_T *frames)
{
  signed char fileid;
  int wherefrom;
  FILE * filestar;
  boolean_T p;
  long position_t;
  double size;
  FILE * b_NULL;
  emxArray_real_T *a;
  unsigned int i;
  int k;
  int x;
  int i0;
  int status;
  int j;
  int i1;
  unsigned char u0;
  unsigned char b_x[2];
  short y;

  /* [audio]= audioread(audio); */
  fileid = cfopen(audio_data, audio_size, "rb");
  wherefrom = SEEK_END;
  filestar = fileManager(fileid);
  if (filestar == NULL) {
    b_error();
  }

  if ((fileid != 0) && (fileid != 1) && (fileid != 2)) {
    p = true;
  } else {
    p = false;
  }

  if (!p) {
    filestar = NULL;
  }

  if (!(filestar == NULL)) {
    fseek(filestar, (long int)0.0, wherefrom);
  }

  filestar = fileManager(fileid);
  if (filestar == NULL) {
    b_error();
  }

  if ((fileid != 0) && (fileid != 1) && (fileid != 2)) {
    p = true;
  } else {
    p = false;
  }

  if (!p) {
    filestar = NULL;
  }

  if (filestar == NULL) {
    size = -1.0;
  } else {
    position_t = ftell(filestar);
    size = (double)position_t;
  }

  b_NULL = NULL;
  filestar = fileManager(fileid);
  if (filestar == b_NULL) {
    b_error();
  } else {
    if ((fileid == 0) || (fileid == 1) || (fileid == 2)) {
      p = true;
    } else {
      p = false;
    }

    if (p) {
      rtErrorWithMessageID(&emlrtRTEI);
    }

    if ((fileid == 0) || (fileid == 1) || (fileid == 2)) {
      p = true;
    } else {
      p = false;
    }

    if (p) {
      c_error();
    } else {
      status = fseek(filestar, 0, SEEK_SET);
      if (status != 0) {
        c_error();
      }
    }
  }

  emxInit_real_T(&a, 1);
  b_fread(fileid, size, a);
  i = 1U;
  k = 1;
  x = (int)floor((double)a->size[0] / 512.0) + 1;
  size = (double)a->size[0] / 2.0;
  i0 = b_Splitmain->size[0];
  if (size != floor(size)) {
    rtIntegerError(size, &emlrtDCI);
  }

  b_Splitmain->size[0] = (int)size;
  emxEnsureCapacity_real_T(b_Splitmain, i0);
  if (size != floor(size)) {
    rtIntegerError(size, &emlrtDCI);
  }

  j = (int)size;
  for (i0 = 0; i0 < j; i0++) {
    b_Splitmain->data[i0] = 0.0;
  }

  while (i < (unsigned int)a->size[0]) {
    i0 = a->size[0];
    i1 = (int)i;
    if (!((i1 >= 1) && (i1 <= i0))) {
      rtDynamicBoundsError(i1, 1, i0, &c_emlrtBCI);
    }

    size = rt_roundd_snf(a->data[i1 - 1]);
    if (size < 256.0) {
      if (size >= 0.0) {
        u0 = (unsigned char)size;
      } else {
        u0 = 0;
      }
    } else if (size >= 256.0) {
      u0 = MAX_uint8_T;
    } else {
      u0 = 0;
    }

    b_x[0] = u0;
    i0 = a->size[0];
    i1 = (int)i + 1;
    if (!((i1 >= 1) && (i1 <= i0))) {
      rtDynamicBoundsError(i1, 1, i0, &d_emlrtBCI);
    }

    size = rt_roundd_snf(a->data[i1 - 1]);
    if (size < 256.0) {
      if (size >= 0.0) {
        u0 = (unsigned char)size;
      } else {
        u0 = 0;
      }
    } else if (size >= 256.0) {
      u0 = MAX_uint8_T;
    } else {
      u0 = 0;
    }

    b_x[1] = u0;
    memcpy((void *)&y, (void *)&b_x[0], (unsigned int)((size_t)1 * sizeof(short)));
    i0 = b_Splitmain->size[0];
    if (!((k >= 1) && (k <= i0))) {
      rtDynamicBoundsError(k, 1, i0, &e_emlrtBCI);
    }

    b_Splitmain->data[k - 1] = y;
    k++;
    i += 2U;
  }

  emxFree_real_T(&a);
  i0 = b_Splitmain->size[0];
  emxEnsureCapacity_real_T(b_Splitmain, i0);
  j = b_Splitmain->size[0];
  for (i0 = 0; i0 < j; i0++) {
    b_Splitmain->data[i0] /= 32767.0;
  }

  i = 1U;
  k = 1;
  i0 = frames->size[0] * frames->size[1];
  frames->size[0] = 256;
  frames->size[1] = x;
  emxEnsureCapacity_real_T1(frames, i0);
  j = x << 8;
  for (i0 = 0; i0 < j; i0++) {
    frames->data[i0] = 0.0;
  }

  while ((int)i < x) {
    j = 1;
    while ((j < 257) && (k < b_Splitmain->size[0])) {
      i0 = b_Splitmain->size[0];
      if (!(k <= i0)) {
        rtDynamicBoundsError(k, 1, i0, &emlrtBCI);
      }

      i0 = frames->size[1];
      i1 = (int)i;
      if (!((i1 >= 1) && (i1 <= i0))) {
        rtDynamicBoundsError(i1, 1, i0, &b_emlrtBCI);
      }

      frames->data[(j + frames->size[0] * (i1 - 1)) - 1] = b_Splitmain->data[k -
        1];
      j++;
      k++;
    }

    i = (unsigned int)((int)i + 1);
  }
}

/* End of code generation (Splitmain.c) */
