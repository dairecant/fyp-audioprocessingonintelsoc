/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * fread.h
 *
 * Code generation for function 'fread'
 *
 */

#ifndef FREAD_H
#define FREAD_H

/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "Splitmain_types.h"

/* Type Definitions */

/* Function Declarations */
extern void b_fread(double fileID, double sizeA, emxArray_real_T *A);

#endif

/* End of code generation (fread.h) */
