/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * fread.c
 *
 * Code generation for function 'fread'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "Splitmain.h"
#include "fread.h"
#include "Splitmain_emxutil.h"
#include "Splitmain_rtwutil.h"
#include "eml_int_forloop_overflow_check.h"
#include "error.h"
#include "fileManager.h"
#include <stdio.h>
#include <stdlib.h>

/* Type Definitions */
#include <stddef.h>

/* Variable Definitions */
static rtRunTimeErrorInfo f_emlrtRTEI = { 277,/* lineNo */
  17,                                  /* colNo */
  "fread",                             /* fName */
  "C:\\Program Files\\MATLAB\\R2017b\\toolbox\\eml\\lib\\matlab\\iofun\\fread.m"/* pName */
};

static rtRunTimeErrorInfo g_emlrtRTEI = { 449,/* lineNo */
  15,                                  /* colNo */
  "fread",                             /* fName */
  "C:\\Program Files\\MATLAB\\R2017b\\toolbox\\eml\\lib\\matlab\\iofun\\fread.m"/* pName */
};

static rtDoubleCheckInfo b_emlrtDCI = { 210,/* lineNo */
  56,                                  /* colNo */
  "fread",                             /* fName */
  "C:\\Program Files\\MATLAB\\R2017b\\toolbox\\eml\\lib\\matlab\\iofun\\fread.m",/* pName */
  4                                    /* checkKind */
};

static rtDoubleCheckInfo c_emlrtDCI = { 234,/* lineNo */
  48,                                  /* colNo */
  "fread",                             /* fName */
  "C:\\Program Files\\MATLAB\\R2017b\\toolbox\\eml\\lib\\matlab\\iofun\\fread.m",/* pName */
  4                                    /* checkKind */
};

/* Function Definitions */
void b_fread(double fileID, double sizeA, emxArray_real_T *A)
{
  boolean_T overflow;
  int dims_idx_0;
  boolean_T doEOF;
  size_t nBytes;
  FILE * filestar;
  emxArray_uint8_T *b_A;
  int buf_size_idx_0;
  int c;
  int bytesOut;
  int numRead;
  int bdims[2];
  size_t numReadSizeT;
  unsigned char buf_data[1024];
  int num2Read;
  int other2Read;
  if ((fileID != 0.0) && (fileID != 1.0) && (fileID != 2.0)) {
    overflow = true;
  } else {
    overflow = false;
  }

  if (!overflow) {
    d_error();
  }

  if (rtIsInf(sizeA)) {
    dims_idx_0 = 1024;
    doEOF = true;
  } else {
    if ((!rtIsInf(sizeA)) && (!rtIsNaN(sizeA)) && (sizeA >= 0.0) &&
        (2.147483647E+9 >= sizeA)) {
      overflow = true;
    } else {
      overflow = false;
    }

    if (!overflow) {
      f_rtErrorWithMessageID(&g_emlrtRTEI);
    }

    dims_idx_0 = (int)sizeA;
    doEOF = false;
  }

  nBytes = sizeof(unsigned char);
  filestar = fileManager(fileID);
  if (filestar == NULL) {
    b_error();
  }

  if ((fileID != 0.0) && (fileID != 1.0) && (fileID != 2.0)) {
    overflow = true;
  } else {
    overflow = false;
  }

  if (!overflow) {
    filestar = NULL;
  }

  emxInit_uint8_T(&b_A, 1);
  if (!doEOF) {
    if (filestar == NULL) {
      buf_size_idx_0 = A->size[0];
      A->size[0] = 0;
      emxEnsureCapacity_real_T(A, buf_size_idx_0);
    } else {
      buf_size_idx_0 = A->size[0];
      if (!(sizeA >= 0.0)) {
        rtNonNegativeError(sizeA, &b_emlrtDCI);
      }

      A->size[0] = (int)sizeA;
      emxEnsureCapacity_real_T(A, buf_size_idx_0);
      if ((A->size[0] == 0) || (dims_idx_0 == 0)) {
        bytesOut = 0;
      } else {
        if (dims_idx_0 > 1024) {
          for (buf_size_idx_0 = 0; buf_size_idx_0 < 2; buf_size_idx_0++) {
            bdims[buf_size_idx_0] = 1024 + -1023 * buf_size_idx_0;
          }
        } else {
          bdims[0] = dims_idx_0;
          bdims[1] = 1;
        }

        bytesOut = 0;
        numRead = 1;
        for (buf_size_idx_0 = 0; buf_size_idx_0 < 2; buf_size_idx_0++) {
          other2Read = bdims[buf_size_idx_0];
          if (!(other2Read >= 0)) {
            rtNonNegativeError(other2Read, &c_emlrtDCI);
          }
        }

        buf_size_idx_0 = bdims[0];
        while ((bytesOut < dims_idx_0) && (numRead > 0)) {
          num2Read = buf_size_idx_0;
          other2Read = dims_idx_0 - bytesOut;
          if (buf_size_idx_0 > other2Read) {
            num2Read = other2Read;
          }

          buf_size_idx_0 = (short)bdims[0];
          numRead = 0;
          c = 1;
          while ((numRead < num2Read) && (c > 0)) {
            numReadSizeT = fread(&buf_data[numRead], nBytes, num2Read - numRead,
                                 filestar);
            c = (int)numReadSizeT;
            numRead += (int)numReadSizeT;
          }

          if ((!(1 > numRead)) && (numRead > 2147483646)) {
            check_forloop_overflow_error();
          }

          for (c = 0; c + 1 <= numRead; c++) {
            A->data[c + bytesOut] = buf_data[c];
          }

          bytesOut += numRead;
        }

        other2Read = A->size[0];
        overflow = ((!(bytesOut + 1 > A->size[0])) && (A->size[0] > 2147483646));
        if (overflow) {
          check_forloop_overflow_error();
        }

        for (c = bytesOut; c + 1 <= other2Read; c++) {
          A->data[c] = 0.0;
        }
      }

      if (bytesOut < sizeA) {
        if (!(bytesOut <= A->size[0])) {
          e_rtErrorWithMessageID(&f_emlrtRTEI);
        }

        if (1 > bytesOut) {
          num2Read = 0;
        } else {
          num2Read = bytesOut;
        }

        buf_size_idx_0 = b_A->size[0];
        b_A->size[0] = A->size[0];
        emxEnsureCapacity_uint8_T(b_A, buf_size_idx_0);
        other2Read = A->size[0];
        for (buf_size_idx_0 = 0; buf_size_idx_0 < other2Read; buf_size_idx_0++)
        {
          b_A->data[buf_size_idx_0] = (unsigned char)A->data[buf_size_idx_0];
        }

        buf_size_idx_0 = A->size[0];
        A->size[0] = num2Read;
        emxEnsureCapacity_real_T(A, buf_size_idx_0);
        for (buf_size_idx_0 = 0; buf_size_idx_0 < num2Read; buf_size_idx_0++) {
          A->data[buf_size_idx_0] = b_A->data[buf_size_idx_0];
        }
      }
    }
  } else {
    buf_size_idx_0 = A->size[0];
    A->size[0] = 0;
    emxEnsureCapacity_real_T(A, buf_size_idx_0);
    if (!(filestar == NULL)) {
      c = 1;
      while (c > 0) {
        c = 0;
        numRead = 1;
        while ((c < 1024) && (numRead > 0)) {
          numReadSizeT = fread(&buf_data[c], nBytes, 1024 - c, filestar);
          numRead = (int)numReadSizeT;
          c += (int)numReadSizeT;
        }

        if (1 > c) {
          num2Read = -1;
        } else {
          num2Read = c - 1;
        }

        other2Read = A->size[0];
        buf_size_idx_0 = A->size[0];
        A->size[0] = (other2Read + num2Read) + 1;
        emxEnsureCapacity_real_T(A, buf_size_idx_0);
        for (buf_size_idx_0 = 0; buf_size_idx_0 <= num2Read; buf_size_idx_0++) {
          A->data[other2Read + buf_size_idx_0] = buf_data[buf_size_idx_0];
        }
      }
    }
  }

  emxFree_uint8_T(&b_A);
}

/* End of code generation (fread.c) */
