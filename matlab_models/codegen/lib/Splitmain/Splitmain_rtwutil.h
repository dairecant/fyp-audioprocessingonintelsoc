/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * Splitmain_rtwutil.h
 *
 * Code generation for function 'Splitmain_rtwutil'
 *
 */

#ifndef SPLITMAIN_RTWUTIL_H
#define SPLITMAIN_RTWUTIL_H

/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "Splitmain_types.h"

/* Function Declarations */
extern void b_rtErrorWithMessageID(const int b, const rtRunTimeErrorInfo *aInfo);
extern void c_rtErrorWithMessageID(const rtRunTimeErrorInfo *aInfo);
extern void d_rtErrorWithMessageID(const rtRunTimeErrorInfo *aInfo);
extern void e_rtErrorWithMessageID(const rtRunTimeErrorInfo *aInfo);
extern void f_rtErrorWithMessageID(const rtRunTimeErrorInfo *aInfo);
extern void g_rtErrorWithMessageID(const rtRunTimeErrorInfo *aInfo);
extern void h_rtErrorWithMessageID(const int b, const char *c, const
  rtRunTimeErrorInfo *aInfo);
extern void rtDynamicBoundsError(int aIndexValue, int aLoBound, int aHiBound,
  const rtBoundsCheckInfo *aInfo);
extern void rtErrorWithMessageID(const rtRunTimeErrorInfo *aInfo);
extern void rtIntegerError(const double aInteger, const rtDoubleCheckInfo *aInfo);
extern void rtNonNegativeError(const double aPositive, const rtDoubleCheckInfo
  *aInfo);
extern double rt_roundd_snf(double u);

#endif

/* End of code generation (Splitmain_rtwutil.h) */
