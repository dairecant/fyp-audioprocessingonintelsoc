/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * error.c
 *
 * Code generation for function 'error'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "Splitmain.h"
#include "error.h"
#include "Splitmain_rtwutil.h"
#include <stdio.h>
#include <stdlib.h>

/* Variable Definitions */
static rtRunTimeErrorInfo b_emlrtRTEI = { 19,/* lineNo */
  5,                                   /* colNo */
  "error",                             /* fName */
  "C:\\Program Files\\MATLAB\\R2017b\\toolbox\\shared\\coder\\coder\\+coder\\+internal\\error.m"/* pName */
};

/* Function Definitions */
void b_error(void)
{
  d_rtErrorWithMessageID(&b_emlrtRTEI);
}

void c_error(void)
{
  rtErrorWithMessageID(&b_emlrtRTEI);
}

void d_error(void)
{
  g_rtErrorWithMessageID(&b_emlrtRTEI);
}

void error(void)
{
  b_rtErrorWithMessageID(20, &b_emlrtRTEI);
}

/* End of code generation (error.c) */
