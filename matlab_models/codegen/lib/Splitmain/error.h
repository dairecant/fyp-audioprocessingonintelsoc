/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * error.h
 *
 * Code generation for function 'error'
 *
 */

#ifndef ERROR_H
#define ERROR_H

/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "Splitmain_types.h"

/* Function Declarations */
extern void b_error(void);
extern void c_error(void);
extern void d_error(void);
extern void error(void);

#endif

/* End of code generation (error.h) */
