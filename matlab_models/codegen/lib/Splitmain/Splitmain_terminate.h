/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * Splitmain_terminate.h
 *
 * Code generation for function 'Splitmain_terminate'
 *
 */

#ifndef SPLITMAIN_TERMINATE_H
#define SPLITMAIN_TERMINATE_H

/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "Splitmain_types.h"

/* Function Declarations */
extern void Splitmain_terminate(void);

#endif

/* End of code generation (Splitmain_terminate.h) */
