/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * Splitmain_rtwutil.c
 *
 * Code generation for function 'Splitmain_rtwutil'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "Splitmain.h"
#include "Splitmain_rtwutil.h"
#include <stdio.h>
#include <stdlib.h>

/* Function Declarations */
static boolean_T rtIsNullOrEmptyString(const char *aString);
static void rtReportErrorLocation(const char * aFcnName, const int aLineNo);

/* Function Definitions */
static boolean_T rtIsNullOrEmptyString(const char *aString)
{
  return (aString == NULL) || (*aString == '\x00');
}

static void rtReportErrorLocation(const char * aFcnName, const int aLineNo)
{
  fprintf(stderr, "Error in %s (line %d)", aFcnName, aLineNo);
  fprintf(stderr, "\n");
}

void b_rtErrorWithMessageID(const int b, const rtRunTimeErrorInfo *aInfo)
{
  fprintf(stderr, "For code generation, maximum number of open files is %d.", b);
  fprintf(stderr, "\n");
  if (aInfo != NULL) {
    rtReportErrorLocation(aInfo->fName, aInfo->lineNo);
  }

  fflush(stderr);
  abort();
}

void c_rtErrorWithMessageID(const rtRunTimeErrorInfo *aInfo)
{
  fprintf(stderr,
          "Invalid file identifier.  Use fopen to generate a valid file identifier.");
  fprintf(stderr, "\n");
  if (aInfo != NULL) {
    rtReportErrorLocation(aInfo->fName, aInfo->lineNo);
  }

  fflush(stderr);
  abort();
}

void d_rtErrorWithMessageID(const rtRunTimeErrorInfo *aInfo)
{
  fprintf(stderr,
          "Invalid file identifier. Use fopen to generate a valid file identifier.");
  fprintf(stderr, "\n");
  if (aInfo != NULL) {
    rtReportErrorLocation(aInfo->fName, aInfo->lineNo);
  }

  fflush(stderr);
  abort();
}

void e_rtErrorWithMessageID(const rtRunTimeErrorInfo *aInfo)
{
  fprintf(stderr, "Assertion failed.");
  fprintf(stderr, "\n");
  if (aInfo != NULL) {
    rtReportErrorLocation(aInfo->fName, aInfo->lineNo);
  }

  fflush(stderr);
  abort();
}

void f_rtErrorWithMessageID(const rtRunTimeErrorInfo *aInfo)
{
  fprintf(stderr, "Invalid size.");
  fprintf(stderr, "\n");
  if (aInfo != NULL) {
    rtReportErrorLocation(aInfo->fName, aInfo->lineNo);
  }

  fflush(stderr);
  abort();
}

void g_rtErrorWithMessageID(const rtRunTimeErrorInfo *aInfo)
{
  fprintf(stderr, "Operation is not implemented for requested file identifier.");
  fprintf(stderr, "\n");
  if (aInfo != NULL) {
    rtReportErrorLocation(aInfo->fName, aInfo->lineNo);
  }

  fflush(stderr);
  abort();
}

void h_rtErrorWithMessageID(const int b, const char *c, const rtRunTimeErrorInfo
  *aInfo)
{
  fprintf(stderr,
          "The loop variable of class %.*s might overflow on the last iteration of the for loop. This could lead to an infinite loop.",
          b, c);
  fprintf(stderr, "\n");
  if (aInfo != NULL) {
    rtReportErrorLocation(aInfo->fName, aInfo->lineNo);
  }

  fflush(stderr);
  abort();
}

void rtDynamicBoundsError(int aIndexValue, int aLoBound, int aHiBound, const
  rtBoundsCheckInfo *aInfo)
{
  if (aLoBound == 0) {
    aIndexValue++;
    aLoBound = 1;
    aHiBound++;
  }

  if (rtIsNullOrEmptyString(aInfo->aName)) {
    fprintf(stderr,
            "Index exceeds array dimensions.  Index value %d exceeds valid range [%d-%d].",
            aIndexValue, aLoBound, aHiBound);
    fprintf(stderr, "\n");
  } else {
    fprintf(stderr,
            "Index exceeds array dimensions.  Index value %d exceeds valid range [%d-%d] of array %s.",
            aIndexValue, aLoBound, aHiBound, aInfo->aName);
    fprintf(stderr, "\n");
  }

  if (aInfo != NULL) {
    rtReportErrorLocation(aInfo->fName, aInfo->lineNo);
  }

  fflush(stderr);
  abort();
}

void rtErrorWithMessageID(const rtRunTimeErrorInfo *aInfo)
{
  fprintf(stderr, "Rewind failed.");
  fprintf(stderr, "\n");
  if (aInfo != NULL) {
    rtReportErrorLocation(aInfo->fName, aInfo->lineNo);
  }

  fflush(stderr);
  abort();
}

void rtIntegerError(const double aInteger, const rtDoubleCheckInfo *aInfo)
{
  fprintf(stderr,
          "Expected a value representable in the C type \'int\'.  Found %g instead.",
          aInteger);
  fprintf(stderr, "\n");
  if (aInfo != NULL) {
    rtReportErrorLocation(aInfo->fName, aInfo->lineNo);
  }

  fflush(stderr);
  abort();
}

void rtNonNegativeError(const double aPositive, const rtDoubleCheckInfo *aInfo)
{
  fprintf(stderr,
          "Value %g is not greater than or equal to zero.\nExiting to prevent memory corruption.",
          aPositive);
  fprintf(stderr, "\n");
  if (aInfo != NULL) {
    rtReportErrorLocation(aInfo->fName, aInfo->lineNo);
  }

  fflush(stderr);
  abort();
}

double rt_roundd_snf(double u)
{
  double y;
  if (fabs(u) < 4.503599627370496E+15) {
    if (u >= 0.5) {
      y = floor(u + 0.5);
    } else if (u > -0.5) {
      y = u * 0.0;
    } else {
      y = ceil(u - 0.5);
    }
  } else {
    y = u;
  }

  return y;
}

/* End of code generation (Splitmain_rtwutil.c) */
