/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * fileManager.h
 *
 * Code generation for function 'fileManager'
 *
 */

#ifndef FILEMANAGER_H
#define FILEMANAGER_H

/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "Splitmain_types.h"

/* Function Declarations */
extern signed char cfopen(const char cfilename_data[], const int cfilename_size
  [2], const char * cpermission);
extern void eml_openfiles_not_empty_init(void);
extern FILE * fileManager(double varargin_1);

#endif

/* End of code generation (fileManager.h) */
