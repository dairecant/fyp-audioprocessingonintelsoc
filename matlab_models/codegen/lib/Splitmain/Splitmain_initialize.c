/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * Splitmain_initialize.c
 *
 * Code generation for function 'Splitmain_initialize'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "Splitmain.h"
#include "Splitmain_initialize.h"
#include "fileManager.h"
#include <stdio.h>
#include <stdlib.h>

/* Function Definitions */
void Splitmain_initialize(void)
{
  rt_InitInfAndNaN(8U);
  eml_openfiles_not_empty_init();
}

/* End of code generation (Splitmain_initialize.c) */
