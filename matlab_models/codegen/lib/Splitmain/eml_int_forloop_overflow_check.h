/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * eml_int_forloop_overflow_check.h
 *
 * Code generation for function 'eml_int_forloop_overflow_check'
 *
 */

#ifndef EML_INT_FORLOOP_OVERFLOW_CHECK_H
#define EML_INT_FORLOOP_OVERFLOW_CHECK_H

/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "Splitmain_types.h"

/* Function Declarations */
extern void check_forloop_overflow_error(void);

#endif

/* End of code generation (eml_int_forloop_overflow_check.h) */
