/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * distortion_terminate.h
 *
 * Code generation for function 'distortion_terminate'
 *
 */

#ifndef DISTORTION_TERMINATE_H
#define DISTORTION_TERMINATE_H

/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rtwtypes.h"
#include "distortion_types.h"

/* Function Declarations */
extern void distortion_terminate(void);

#endif

/* End of code generation (distortion_terminate.h) */
