/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * distortion_initialize.c
 *
 * Code generation for function 'distortion_initialize'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "distortion.h"
#include "distortion_initialize.h"

/* Function Definitions */
void distortion_initialize(void)
{
  rt_InitInfAndNaN(8U);
}

/* End of code generation (distortion_initialize.c) */
