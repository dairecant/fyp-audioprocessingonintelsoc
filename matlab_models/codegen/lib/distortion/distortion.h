/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * distortion.h
 *
 * Code generation for function 'distortion'
 *
 */

#ifndef DISTORTION_H
#define DISTORTION_H

/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rtwtypes.h"
#include "distortion_types.h"

/* Function Declarations */
extern void distortion(const double sample[155647], double clipVal, double
  distorted[155647]);

#endif

/* End of code generation (distortion.h) */
