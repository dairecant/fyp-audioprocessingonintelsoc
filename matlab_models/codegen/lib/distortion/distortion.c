/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * distortion.c
 *
 * Code generation for function 'distortion'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "distortion.h"

/* Function Definitions */
void distortion(const double sample[155647], double clipVal, double distorted
                [155647])
{
  double k;
  int b_k;

  /* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
  /*            Distortion EFFECT                         % */
  /*  Author:   D�ire Canavan 30/10/2017                  % */
  /*  Implements distortion effect on audio               % */
  /*  Works by hard-clipping the audio @ pos/neg values   % */
  /*   sample: inputted wav file                          % */
  /*   fs:    sampling frequency (not implemented)        % */
  /*   clipVal: Max/Min clip value of audio               % */
  /*   gain: Output gain of distortion(not in)            % */
  /* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
  /*  for k = 1:length(sample) USED FOR STEREO INPUT */
  /*      left(k) = sample(k,1); */
  /*      %right(k)= sample(k,2); */
  /*  end */
  /* %%%%PREVIOUS IMPLEMENTATION */
  /* highClipValLeft = max(left)*clipVal; */
  /* lowClipValLeft = min(left)*clipVal; */
  /* highClipValRight = max(right)*clipVal; */
  /* lowClipValRight = min(right)*clipVal; */
  /*  clipVal = clipVal*max(right); */
  /*  for j = 1:length(left) */
  /*          if (left(j)>clipVal) */
  /*              left(j) = left(j)-clipVal; */
  /*          elseif (left(j)<-clipVal) */
  /*                  left(j) = left(j)+clipVal; */
  /*          else */
  /*                  left(j) = left(j); */
  /*          end */
  /*   */
  /*  end */
  /*  for j = 1:length(right) */
  /*          if (right(j)>clipVal) */
  /*              right(j) = right(j)-clipVal; */
  /*          elseif (right(j)<-clipVal) */
  /*                  right(j) = right(j)+clipVal; */
  /*          else */
  /*                  right(j) = right(j); */
  /*          end */
  /*   */
  /*  end */
  /*       */
  k = 2.0 * clipVal / (1.0 - clipVal);

  /* right = (1+k)*(right)./(1+k*abs(right)); */
  for (b_k = 0; b_k < 155647; b_k++) {
    distorted[b_k] = (1.0 + k) * sample[b_k] / (1.0 + k * fabs(sample[b_k]));
  }

  /*  distorted = transpose(distorted); */
}

/* End of code generation (distortion.c) */
