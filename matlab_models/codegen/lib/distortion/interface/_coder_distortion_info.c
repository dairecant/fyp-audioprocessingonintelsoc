/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * _coder_distortion_info.c
 *
 * Code generation for function 'distortion'
 *
 */

/* Include files */
#include "_coder_distortion_info.h"

/* Function Definitions */
const mxArray *emlrtMexFcnResolvedFunctionsInfo(void)
{
  const mxArray *nameCaptureInfo;
  const char * data[23] = {
    "789ced5dcb6fdbc8199f64937453605b6e8bee6e506c3749d374dba4961f9265a7875a9628597e897af821a7814d8a94449b2f91942cf9e44351f458f4dc3fa0"
    "871e163dedadc29eb67beaa10f606f457be8a5408feda1402951b445461331d18834e919c0198ebee1fc66be7cf37d9cf9e6016e64b76e0000be66fcdd34fea8",
    "cf403fbc63468018c437813d38e93706f17d47da0ab7c12ddb7b16fdd783b8224b3ad7d6cd84c04bdc76536438d54848b4c85d14c3ca222fd1925eea281c5039"
    "4d165a1cdba75479812bf122b7290f25d6782321a6874817891ea9f79cac7395936253046a5dbbacae309c0043fcf90fa4fdb75cf2270be1cfbb83f85783f839",
    "20c10b9004cf4004ec000d18ad35fe8d8014f81be08d67aeff2c830a6802d1484940efd3d30655023410c021281bbfd346dedeaf65401971cda0ea462c1abfeb"
    "461e1a30463ed1288735f20afd1258238f665065e34ddd78968df2668c3c763e1c41da79c7251f9cb115ee82b78752999571786ef9feb6230d2ef299145165f9",
    "16cf72a8f0ee38d297782685959b8cc05df2f39309f1d6a17876faf3ece63ef922f92c42a9724da5c5fbbd3ea045b612a5cdc46aa4303f3b176722ba2c0b8cdc"
    "8e70a21011782622d2ba40331159d122169f6614b33c0fe5e1de172ffe92f056fe3c6ddf87e97b7ff6ad7d6d48796ee5ef5b103cc2416f970f0ec85a82571bd1",
    "9648c64ae544fe981eaa073506675c3d0024ed55f9b81f8faebf5deefefd13547af62b103c62401934736cfbaeaa5e877d2f100efaa4f2608983f776fefcfdaf"
    "e6fdd37b61c7f34aaf2f9de6ab3bfbd1c5f85a4b282ca7969636e59d0ac07afdaaf46305523fb77277d391b6c25d477e333c5c31e3c717dfefe790f2ddf2ef23",
    "083ee1a05764965367786330a94ab430c36bab4d5ed0b392319ce454bee29b1df87442bc7d289e9dfedaf2d3fb7bd2e75ae489c5b688936d33a287f6e0bbfff8"
    "12db83ab6a0fde83e0110e7aaed54826aa6bada5523edaa990a2349f3e10d6b03db82af6e008523fb472f74364faff3e048f70d01dfa5fabd002d75692b2a8d0",
    "3a6f6864bff47f7742bc3214cf4e47a3ff5f629b1ff2f3c5efbe19eaf91e5ff1bcb2037556510ff6cfc84a2d9eda8c77169a7422bfb48aedc0f5b203cf5650c9"
    "dd3720788483eeb00346ebcddf833a0fb401c5b3d3d1e87f835d0349c1fa3e1c785ee9fbdaea6a878d2f90e4417bbeb5b651e25365b510a2ef7eecfff5507ecf",
    "33c8ecc6bb8e3470e4b3e8863a3c14755ee4b4c33a2728fd1508bd30a9dd7006583dac806adea83806cfa24ffa7df112dfbcb71fdfc3f346c1b71fec41878e32"
    "157a3f15a513673259128fcb4a3a3cf6e373c8fb6ef94843ca271cf429f4e70715591465e9b0d25b31652d8ff2c5bf705e5c311f4ad8bf00b07f61b8ded8bfe0",
    "2f1ef62fa0291f8f33fc1967c0f0dcf2fd2d081e31a0d08c6939fdb213bf99102f03c5b3d327f9fee0846a538a189c32c6101eda85a7a9c7d82e04dd2e341886"
    "ad46152657a28a6a5e9ecb6484cd6888fc0db8ffda835ddeeee3f100c0e381e17ae3f180bf78783c80a67cacf7edc12e6fb3c8f4fe3d081ee1a08f586744ab33",
    "d6d73df617b8d3fb4f4cbe0d6466b81d47907a22d4fbfffbc333acf783aef78f63997c8dcdef6697d3db73cbbb4c3e56ab5249acf7af9bdeff05a43cb77c7a08"
    "c1231c7487dea71545e814fb4a2cdd942a3a2f4b66be717e11b7f5fafa987a59f4ea00fdb04e4bac70b9dfad3b21fef331f8161d8d3d18c14eeffdc87fffef22",
    "5e871474bb90a06aba146dc74a0aab14b29b79f560fb201e22bbd085bc8ffbb53dd8e5308b6c9cf021048f70d01df6a2a97114ad566535294b5aefb41354f343"
    "b71ce9cbfa98944a9dee2f6542354ed885e2d9e968e4c7ceb69ee878384ef87d16db83c0db834e435496f797d785e82e5fcf73ab6bed56ab9c0a8f3dc0fd7974",
    "6c05bbfc2d21b303df86e0110ebac30e18495eaaa5653543edf4e9419d2f2a8dc1b3e868e466986de607848776e0236c07826f07ea0bf1da72ed94d4b9e26e93"
    "1263358a5acd85685c80fbf3e8d80a76f99b41b6bfe07d081ee1a09b76a0a634676a9c9e519aa4443302c7faa6ffbb6f8867957f3006cfa23fcfba1717cd18ab",
    "706cc4149681c80c228371111be3ac0d8d1ecacd97e7716c07826e0772db796dbdb03f1f6dc64aed84a4288dd35c2e44fb0cf07860746c05bbfcfd18991df800"
    "824738e8b079a1a08e03f263f02c3a6279f1e3bcbacccff03820f0fa7f714fa6b3f3c2ee4634bdb3c56e6ad13385ac87689d6817f23ef60fd8837d1f4001d9bc",
    "d07720788483dedb6767b0e1d0d065822c2b87728b53ab827c6aeeb2f36f5ea8fb867856f9478eb413cfa24fb4eec014a757f0cfd375a4bffdeb6d6c17826e17"
    "b40db24da6cecea8f5f67c746b7e36994fcccd92e1b10bff84bcef968f3f87944f38e8d3eed70f5e9de1e2f0066fe515dd3eb271fe5c5eabf6eeb640d63f5e6f",
    "feea72bf83c472edaca4235baf961e530f8bfe46f255e5db1cabc846ed233dfe793cce7c84f71904df3e143b0a2fc61a85bd399e61a3d9833cb535d70a911f19"
    "f75f7b80ad375520e5b9e5d33b8e3470e4b3e8bc26995ba2f4de8d47fead17fa6442bc1c14cf4e9f5c2e86f8e5ed7ae47b7ededf12763caff47b66e1a4536cd4",
    "7821594a9e141aabb5d2c90e1922fd8ebfffedf59dd6fa52181eaafdbe46d345ba1d5c7be0c53d479c603c444c4e79ef273e7f74e72e9e0f0aba3d501bb3ca31"
    "af4513e222159728667f6163570b919f00db037b7dd1fa13bcb507bc84ed813b7bc04b3ed883ef637b107c7b50a3cbf1dcc2de7623ce24f4648cce45b9683e13",
    "1e7bd085bc8ffdc6f66093c32e3abff155397fc2addd417dee103e7f625a7866b82e78f8fc0934e5e37348bd1c2fa0f31fe37b91cd10dc7b93f0bdc8d3c4c3f7"
    "22a329bf0b791fdf6b680f303f320c0f959ee735aed1a485c0eaf9a9dd7bf6f2bc90c929efc701785dd014f13c5b17b4d368eadbd9dddd9d4231bfa836126b31",
    "8e0e919f00f7e3d1f5879d3fad40ca73cb2fd8ff0fe188fb6e0fb3c58715590decfdc62fa078767a4f3ea8c9044451f916ad731127ebfa12e3a1deffd7a71f63"
    "7f40d0f53eb9c82c6f1fa7a9ce5eba5aacee91b9f85cf92444e74e7721efe37e6d0f7639fcd8effdc412add678a9523f09eafeb19f8ec1b3e8aff5bdf08a8325",
    "2e8688179cf3fc7ca1b79e16fe84edc194f03c3b67ae7d9a5da6d3d55a6cb69223e5e271ba193f0dd1feb12ee4fda0f66b985cb89543d87d5fd6bd951fd87e7d"
    "b862c68f07f15364f340b721f5200694aa20cbbd053b419d07f262bedf3cc7bccf293fe6057f84ef1f0bbefe5f6ac473a586908a9dacd64bf94c516fa4529510",
    "9d2b84fbf1e8fae37bc8a6b31e08df43362d3c335c173c7c0f199af2b1fe1f5d7fbbdc3d42a6ffdff07ce9c17a50ebabdfbf738426d5ff3b63f02c3adaf5a097"
    "b2e3a1fe7f0fef1f0ebefe67ea8bd4be24e86c5116c9a6b4b6c090abeb21da1f80f5ffe8fac3f4ff2f21e5b9e5d70f207884833efe5eb2ac440974851be447e5",
    "a79ef47eb2cf27c467c6e05bf4a9ed3719b0d58773a8174e63d87f1c747bb1b9be512937a8e544792baa6af30a3fdf8aaf87e8bc09dcbf47b7cb9d3cce846e1c"
    "e1d73c121e474c0bcf0cd7050f8f23d094ff47c8fb6ef92840ca271cf4a9ae2b7a6025cdee3edcbe2348fd91cae5ca9167fe635e93680960ffb19bf5c706a7fc",
    "d82f86f7114c11cfabfd628d443327463bfbd1d9823adf4cce71cb2d711b8447efe37e3cbafed3f21f60ff310ccf4ec7fee3d7c533c375c1c3fee3c9caff3f50"
    "a28d08", "" };

  nameCaptureInfo = NULL;
  emlrtNameCaptureMxArrayR2016a(data, 49768U, &nameCaptureInfo);
  return nameCaptureInfo;
}

mxArray *emlrtMexFcnProperties(void)
{
  mxArray *xResult;
  mxArray *xEntryPoints;
  const char * fldNames[4] = { "Name", "NumberOfInputs", "NumberOfOutputs",
    "ConstantInputs" };

  mxArray *xInputs;
  const char * b_fldNames[4] = { "Version", "ResolvedFunctions", "EntryPoints",
    "CoverageInfo" };

  xEntryPoints = emlrtCreateStructMatrix(1, 1, 4, fldNames);
  xInputs = emlrtCreateLogicalMatrix(1, 2);
  emlrtSetField(xEntryPoints, 0, "Name", emlrtMxCreateString("distortion"));
  emlrtSetField(xEntryPoints, 0, "NumberOfInputs", emlrtMxCreateDoubleScalar(2.0));
  emlrtSetField(xEntryPoints, 0, "NumberOfOutputs", emlrtMxCreateDoubleScalar
                (1.0));
  emlrtSetField(xEntryPoints, 0, "ConstantInputs", xInputs);
  xResult = emlrtCreateStructMatrix(1, 1, 4, b_fldNames);
  emlrtSetField(xResult, 0, "Version", emlrtMxCreateString(
    "9.3.0.713579 (R2017b)"));
  emlrtSetField(xResult, 0, "ResolvedFunctions", (mxArray *)
                emlrtMexFcnResolvedFunctionsInfo());
  emlrtSetField(xResult, 0, "EntryPoints", xEntryPoints);
  return xResult;
}

/* End of code generation (_coder_distortion_info.c) */
