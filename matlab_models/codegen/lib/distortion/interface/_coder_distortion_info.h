/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * _coder_distortion_info.h
 *
 * Code generation for function 'distortion'
 *
 */

#ifndef _CODER_DISTORTION_INFO_H
#define _CODER_DISTORTION_INFO_H
/* Include files */
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"


/* Function Declarations */
extern const mxArray *emlrtMexFcnResolvedFunctionsInfo(void);
MEXFUNCTION_LINKAGE mxArray *emlrtMexFcnProperties(void);

#endif
/* End of code generation (_coder_distortion_info.h) */
