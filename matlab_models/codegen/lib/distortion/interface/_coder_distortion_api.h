/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * _coder_distortion_api.h
 *
 * Code generation for function '_coder_distortion_api'
 *
 */

#ifndef _CODER_DISTORTION_API_H
#define _CODER_DISTORTION_API_H

/* Include files */
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include <stddef.h>
#include <stdlib.h>
#include "_coder_distortion_api.h"

/* Variable Declarations */
extern emlrtCTX emlrtRootTLSGlobal;
extern emlrtContext emlrtContextGlobal;

/* Function Declarations */
extern void distortion(real_T sample[155647], real_T clipVal, real_T distorted
  [155647]);
extern void distortion_api(const mxArray *prhs[2], const mxArray *plhs[1]);
extern void distortion_atexit(void);
extern void distortion_initialize(void);
extern void distortion_terminate(void);
extern void distortion_xil_terminate(void);

#endif

/* End of code generation (_coder_distortion_api.h) */
