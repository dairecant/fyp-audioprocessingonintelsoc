/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * distortion_terminate.c
 *
 * Code generation for function 'distortion_terminate'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "distortion.h"
#include "distortion_terminate.h"

/* Function Definitions */
void distortion_terminate(void)
{
  /* (no terminate code required) */
}

/* End of code generation (distortion_terminate.c) */
