/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * distortion_types.h
 *
 * Code generation for function 'distortion'
 *
 */

#ifndef DISTORTION_TYPES_H
#define DISTORTION_TYPES_H

/* Include files */
#include "rtwtypes.h"
#endif

/* End of code generation (distortion_types.h) */
