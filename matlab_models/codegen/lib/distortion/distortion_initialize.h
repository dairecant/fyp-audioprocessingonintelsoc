/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * distortion_initialize.h
 *
 * Code generation for function 'distortion_initialize'
 *
 */

#ifndef DISTORTION_INITIALIZE_H
#define DISTORTION_INITIALIZE_H

/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rtwtypes.h"
#include "distortion_types.h"

/* Function Declarations */
extern void distortion_initialize(void);

#endif

/* End of code generation (distortion_initialize.h) */
