/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * tremelo_initialize.c
 *
 * Code generation for function 'tremelo_initialize'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "tremelo.h"
#include "tremelo_initialize.h"

/* Function Definitions */
void tremelo_initialize(void)
{
  rt_InitInfAndNaN(8U);
}

/* End of code generation (tremelo_initialize.c) */
