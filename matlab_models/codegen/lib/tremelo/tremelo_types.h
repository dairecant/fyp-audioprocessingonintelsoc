/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * tremelo_types.h
 *
 * Code generation for function 'tremelo'
 *
 */

#ifndef TREMELO_TYPES_H
#define TREMELO_TYPES_H

/* Include files */
#include "rtwtypes.h"
#endif

/* End of code generation (tremelo_types.h) */
