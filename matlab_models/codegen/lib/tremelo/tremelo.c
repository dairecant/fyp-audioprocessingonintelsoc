/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * tremelo.c
 *
 * Code generation for function 'tremelo'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "tremelo.h"

/* Function Definitions */
void tremelo(const double sound[155647], double fs, double freq, double gain,
             double b_tremelo[155647])
{
  int i;

  /* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
  /*            TREMELO EFFECT             % */
  /*  Author:   D�ire Canavan 30/10/2017   % */
  /*  Implements tremelo effect on audio   % */
  /*  Works on modulating gain value       % */
  /*   sound: inputted wav file            % */
  /*   fs:    sampling frequency           % */
  /*   freq:  frequency of gain modulation % */
  /*   gain:  amplitude overall of the gain% */
  /* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
  for (i = 0; i < 155647; i++) {
    b_tremelo[i] = gain * sin(6.2831853071795862 * freq / fs * (1.0 + (double)i))
      * sound[i];
  }

  /* tremelo=transpose(tremelo); */
}

/* End of code generation (tremelo.c) */
