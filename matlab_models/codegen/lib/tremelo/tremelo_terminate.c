/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * tremelo_terminate.c
 *
 * Code generation for function 'tremelo_terminate'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "tremelo.h"
#include "tremelo_terminate.h"

/* Function Definitions */
void tremelo_terminate(void)
{
  /* (no terminate code required) */
}

/* End of code generation (tremelo_terminate.c) */
