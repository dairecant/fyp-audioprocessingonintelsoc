/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * _coder_tremelo_info.c
 *
 * Code generation for function 'tremelo'
 *
 */

/* Include files */
#include "_coder_tremelo_info.h"

/* Function Definitions */
const mxArray *emlrtMexFcnResolvedFunctionsInfo(void)
{
  const mxArray *nameCaptureInfo;
  const char * data[11] = {
    "789ced5b4f6fe344149f96a5db5d09288b00f58058015a892dc4d934dda49510cd5f9236cd266dda4db2aab68eed2466676cd7b1f3674f3d70e4b01f800f0012"
    "07c4892347b477387043e203f42330b13d6d623a4ab6f63a4dea91d2f1f36ff27e334f6fde1b3fa7602ebb330700781b7fe6f1e7c557c0686f991d58b2fa7930",
    "dcecf89cd5dfb6c9a4bd096e0c7d8fe03f583d274b9ad0d54c018a9290d7514d50b120b1483853c3cb489458492bf51401a8424b866d813790ba08859288849c"
    "3c2064442ca0f4007426f4a1fe75a22970cff67404d466eb7cba70500003f639a5acffc698f64953ecf3aea5ff7b4b7e0252e01024c00660c03e6801bc5afc97",
    "0149f00f10f1b5605ccb80033a4058928066e0698c4a8005103c05157c9fc563fb772ba080fb064635dc237c5fc3635850c3e310d6c3e3b1d0d0a019da9121cb"
    "2080af06d7af50d6b730e6fae76d3269b76ce3cd767fd3ecbfd824fc5d8afe71edff3e857fc9d24f704ee605352062a754251606a02035b42638b7c3d125e761",
    "6fb4799046f87eb9241fd19f1fc147f027d95c397598d8600aaadc505974b7bf475acc4eac948bc599dd50f041a4c668b20c6b72971110343e2b86b59815622e"
    "c63457008db6d7b87e63ef49bb0516c9e5cb5fdffb2be6219fd1ae0b9f57fbae943ba8ae23beacc61e8723dd07ab072898df1e98476104cfa879008aec95feeb",
    "9e3f8e28eb73d76f039b4ee334d1bf6893c1d9b845433f5279b12df2825b7c0b36f99c6fc1d0cfcb7a0d0aeee5852d2a9fa99fe0af9c17a0586310ab41b6c6c8"
    "4a8b21760a5807080ffd61f9e5e14ce7858fd2cb7f4e6c7d5ee5856ea55a4d3562a27a1c6ea3d45aa9122b7ecb82d9c90bfe3ebe78fec37e77fab55b71f62685",
    "6f09237dfdd6321d9ff72715d7b3543e533fc19dfa03710764e9f3ce1f4e3ebc5d9c5cdc9b753eafe27ab453acef97c30f239936dc5d4f46a339799f037e5cbf"
    "2afbd8db7acf279b667fefecfc7e42d13faefd3ea6f013fb11dc56ef115b715d845a56caeb4850456e6279e037877c652a9fa99fe0eed47dec660b200ff3c1a7",
    "fffeede783ab9a0f3ea0f0113f24f8a3f6712256cfb4a3a562b8c7a590144a5761c6cf0757251f1c51e6e7aedfdd772dfedfa5f0117b11dc16ff5b1c0b85ae92"
    "9091c26a228ec8938affbf3be4ab50f94cfd047727feffcf6c93f01fff3dc06be4f32a0f347945ad969fa7b84624998bf4567536568cc6fd3c70bdf2c0866bef",
    "7def50f888bd086ecb0378f5e6fd69ad036d53f94cfd047727fe6373053c7fdfe3c7fbd7c8e755bc6fc4e33d3eb29a4a55bba17666bb24262beaee0c9dfb4f29"
    "dff7dffb8eb7fef1fcd6bdf7be6f50f89630d2d7df1225439e545ef8d121df37543e533fc19d9c1f0458d725065b0ae7040ffde0cbe43dbf0e34edf9806533f1",
    "6ae759463cd849e8301e979291423e393bf9c0dfbfc36dd8df3e73adfeb34ce1237622f805f51f560d90283fb9df7d3aadffef8de023b83be7ff15d36e96cf0c"
    "aee388324f17ebff3f7ff7931ff7a73dee07d7320fa389cee35030dee9c0a002c3c9c6f319aafbf8717fb8d1e2fe0b8abe71edf439858fd889e0b6b8cf2a0aec",
    "ed19412cad4b9c26ca52562a409623ff8232eabdf8b8f37b67c4fc085eb766f1b4c94a3c3cff3dd21f0ef96b23f809ee4e5ea09b9514163df4b3d5ce9a5f2f9a"
    "f63c91dbdae62ac785f5586527acb6428a186a47b666e8f9c0dfdf17afeb55eb4027147dd3f6fc70d93a91fffc70714f9afffce00d9ffffce04cff7fd7b08b86",
    "" };

  nameCaptureInfo = NULL;
  emlrtNameCaptureMxArrayR2016a(data, 15784U, &nameCaptureInfo);
  return nameCaptureInfo;
}

mxArray *emlrtMexFcnProperties(void)
{
  mxArray *xResult;
  mxArray *xEntryPoints;
  const char * fldNames[4] = { "Name", "NumberOfInputs", "NumberOfOutputs",
    "ConstantInputs" };

  mxArray *xInputs;
  const char * b_fldNames[4] = { "Version", "ResolvedFunctions", "EntryPoints",
    "CoverageInfo" };

  xEntryPoints = emlrtCreateStructMatrix(1, 1, 4, fldNames);
  xInputs = emlrtCreateLogicalMatrix(1, 4);
  emlrtSetField(xEntryPoints, 0, "Name", emlrtMxCreateString("tremelo"));
  emlrtSetField(xEntryPoints, 0, "NumberOfInputs", emlrtMxCreateDoubleScalar(4.0));
  emlrtSetField(xEntryPoints, 0, "NumberOfOutputs", emlrtMxCreateDoubleScalar
                (1.0));
  emlrtSetField(xEntryPoints, 0, "ConstantInputs", xInputs);
  xResult = emlrtCreateStructMatrix(1, 1, 4, b_fldNames);
  emlrtSetField(xResult, 0, "Version", emlrtMxCreateString(
    "9.3.0.713579 (R2017b)"));
  emlrtSetField(xResult, 0, "ResolvedFunctions", (mxArray *)
                emlrtMexFcnResolvedFunctionsInfo());
  emlrtSetField(xResult, 0, "EntryPoints", xEntryPoints);
  return xResult;
}

/* End of code generation (_coder_tremelo_info.c) */
