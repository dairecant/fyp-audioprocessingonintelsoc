/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * _coder_tremelo_api.h
 *
 * Code generation for function '_coder_tremelo_api'
 *
 */

#ifndef _CODER_TREMELO_API_H
#define _CODER_TREMELO_API_H

/* Include files */
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include <stddef.h>
#include <stdlib.h>
#include "_coder_tremelo_api.h"

/* Variable Declarations */
extern emlrtCTX emlrtRootTLSGlobal;
extern emlrtContext emlrtContextGlobal;

/* Function Declarations */
extern void tremelo(real_T sound[155647], real_T fs, real_T freq, real_T gain,
                    real_T b_tremelo[155647]);
extern void tremelo_api(const mxArray * const prhs[4], const mxArray *plhs[1]);
extern void tremelo_atexit(void);
extern void tremelo_initialize(void);
extern void tremelo_terminate(void);
extern void tremelo_xil_terminate(void);

#endif

/* End of code generation (_coder_tremelo_api.h) */
