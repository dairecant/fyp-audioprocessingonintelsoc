/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * tremelo.h
 *
 * Code generation for function 'tremelo'
 *
 */

#ifndef TREMELO_H
#define TREMELO_H

/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rtwtypes.h"
#include "tremelo_types.h"

/* Function Declarations */
extern void tremelo(const double sound[155647], double fs, double freq, double
                    gain, double b_tremelo[155647]);

#endif

/* End of code generation (tremelo.h) */
