/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * tremelo_terminate.h
 *
 * Code generation for function 'tremelo_terminate'
 *
 */

#ifndef TREMELO_TERMINATE_H
#define TREMELO_TERMINATE_H

/* Include files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rtwtypes.h"
#include "tremelo_types.h"

/* Function Declarations */
extern void tremelo_terminate(void);

#endif

/* End of code generation (tremelo_terminate.h) */
