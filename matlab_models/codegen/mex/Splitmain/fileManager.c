/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * fileManager.c
 *
 * Code generation for function 'fileManager'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "Splitmain.h"
#include "fileManager.h"

/* Function Definitions */
void eml_autoflush_not_empty_init(void)
{
}

void eml_openfiles_not_empty_init(void)
{
}

/* End of code generation (fileManager.c) */
