/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * fopen.c
 *
 * Code generation for function 'fopen'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "Splitmain.h"
#include "fopen.h"

/* Function Definitions */
void fileioAtExit(void)
{
}

/* End of code generation (fopen.c) */
