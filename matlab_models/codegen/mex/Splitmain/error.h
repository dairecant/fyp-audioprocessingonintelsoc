/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * error.h
 *
 * Code generation for function 'error'
 *
 */

#ifndef ERROR_H
#define ERROR_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mwmathutil.h"
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "rtwtypes.h"
#include "Splitmain_types.h"

/* Function Declarations */
extern void error(const emlrtStack *sp);

#endif

/* End of code generation (error.h) */
