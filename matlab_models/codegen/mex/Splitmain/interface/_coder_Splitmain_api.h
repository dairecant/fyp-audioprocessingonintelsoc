/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * _coder_Splitmain_api.h
 *
 * Code generation for function '_coder_Splitmain_api'
 *
 */

#ifndef _CODER_SPLITMAIN_API_H
#define _CODER_SPLITMAIN_API_H

/* Include files */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mwmathutil.h"
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "rtwtypes.h"
#include "Splitmain_types.h"

/* Function Declarations */
extern void Splitmain_api(const mxArray * const prhs[1], const mxArray *plhs[2]);

#endif

/* End of code generation (_coder_Splitmain_api.h) */
