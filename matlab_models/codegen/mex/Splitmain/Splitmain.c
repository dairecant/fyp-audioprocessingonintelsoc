/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * Splitmain.c
 *
 * Code generation for function 'Splitmain'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "Splitmain.h"
#include "Splitmain_emxutil.h"
#include "fread.h"
#include "Splitmain_mexutil.h"
#include "Splitmain_data.h"
#include <stdio.h>

/* Type Definitions */
#include <stddef.h>

/* Variable Definitions */
static emlrtRSInfo emlrtRSI = { 6,     /* lineNo */
  "Splitmain",                         /* fcnName */
  "C:\\\\Users\\\\D\xc3\xa1ire\\\\Documents\\\\Final_Year\\\\FYP\\\\git\\\\matlab_models\\\\Splitmain.m"/* pathName */
};

static emlrtRSInfo b_emlrtRSI = { 7,   /* lineNo */
  "Splitmain",                         /* fcnName */
  "C:\\\\Users\\\\D\xc3\xa1ire\\\\Documents\\\\Final_Year\\\\FYP\\\\git\\\\matlab_models\\\\Splitmain.m"/* pathName */
};

static emlrtRSInfo c_emlrtRSI = { 8,   /* lineNo */
  "Splitmain",                         /* fcnName */
  "C:\\\\Users\\\\D\xc3\xa1ire\\\\Documents\\\\Final_Year\\\\FYP\\\\git\\\\matlab_models\\\\Splitmain.m"/* pathName */
};

static emlrtRSInfo d_emlrtRSI = { 9,   /* lineNo */
  "Splitmain",                         /* fcnName */
  "C:\\\\Users\\\\D\xc3\xa1ire\\\\Documents\\\\Final_Year\\\\FYP\\\\git\\\\matlab_models\\\\Splitmain.m"/* pathName */
};

static emlrtRSInfo e_emlrtRSI = { 10,  /* lineNo */
  "Splitmain",                         /* fcnName */
  "C:\\\\Users\\\\D\xc3\xa1ire\\\\Documents\\\\Final_Year\\\\FYP\\\\git\\\\matlab_models\\\\Splitmain.m"/* pathName */
};

static emlrtRSInfo f_emlrtRSI = { 18,  /* lineNo */
  "Splitmain",                         /* fcnName */
  "C:\\\\Users\\\\D\xc3\xa1ire\\\\Documents\\\\Final_Year\\\\FYP\\\\git\\\\matlab_models\\\\Splitmain.m"/* pathName */
};

static emlrtRSInfo g_emlrtRSI = { 51,  /* lineNo */
  "fopen",                             /* fcnName */
  "C:\\Program Files\\MATLAB\\R2017b\\toolbox\\eml\\lib\\matlab\\iofun\\fopen.m"/* pathName */
};

static emlrtRSInfo h_emlrtRSI = { 35,  /* lineNo */
  "fileManager",                       /* fcnName */
  "C:\\Program Files\\MATLAB\\R2017b\\toolbox\\eml\\lib\\matlab\\iofun\\private\\fileManager.m"/* pathName */
};

static emlrtMCInfo emlrtMCI = { 36,    /* lineNo */
  1,                                   /* colNo */
  "Splitmain",                         /* fName */
  "C:\\\\Users\\\\D\xc3\xa1ire\\\\Documents\\\\Final_Year\\\\FYP\\\\git\\\\matlab_models\\\\Splitmain.m"/* pName */
};

static emlrtMCInfo b_emlrtMCI = { 111, /* lineNo */
  17,                                  /* colNo */
  "fileManager",                       /* fName */
  "C:\\Program Files\\MATLAB\\R2017b\\toolbox\\eml\\lib\\matlab\\iofun\\private\\fileManager.m"/* pName */
};

static emlrtMCInfo c_emlrtMCI = { 16,  /* lineNo */
  14,                                  /* colNo */
  "fseek",                             /* fName */
  "C:\\Program Files\\MATLAB\\R2017b\\toolbox\\eml\\lib\\matlab\\iofun\\fseek.m"/* pName */
};

static emlrtMCInfo d_emlrtMCI = { 10,  /* lineNo */
  16,                                  /* colNo */
  "ftell",                             /* fName */
  "C:\\Program Files\\MATLAB\\R2017b\\toolbox\\eml\\lib\\matlab\\iofun\\ftell.m"/* pName */
};

static emlrtMCInfo e_emlrtMCI = { 12,  /* lineNo */
  5,                                   /* colNo */
  "frewind",                           /* fName */
  "C:\\Program Files\\MATLAB\\R2017b\\toolbox\\eml\\lib\\matlab\\iofun\\frewind.m"/* pName */
};

static emlrtRTEInfo emlrtRTEI = { 2,   /* lineNo */
  31,                                  /* colNo */
  "Splitmain",                         /* fName */
  "C:\\\\Users\\\\D\xc3\xa1ire\\\\Documents\\\\Final_Year\\\\FYP\\\\git\\\\matlab_models\\\\Splitmain.m"/* pName */
};

static emlrtRTEInfo b_emlrtRTEI = { 10,/* lineNo */
  1,                                   /* colNo */
  "Splitmain",                         /* fName */
  "C:\\\\Users\\\\D\xc3\xa1ire\\\\Documents\\\\Final_Year\\\\FYP\\\\git\\\\matlab_models\\\\Splitmain.m"/* pName */
};

static emlrtDCInfo emlrtDCI = { 16,    /* lineNo */
  17,                                  /* colNo */
  "Splitmain",                         /* fName */
  "C:\\\\Users\\\\D\xc3\xa1ire\\\\Documents\\\\Final_Year\\\\FYP\\\\git\\\\matlab_models\\\\Splitmain.m",/* pName */
  1                                    /* checkKind */
};

static emlrtBCInfo emlrtBCI = { -1,    /* iFirst */
  -1,                                  /* iLast */
  29,                                  /* lineNo */
  28,                                  /* colNo */
  "Splitmain",                         /* aName */
  "Splitmain",                         /* fName */
  "C:\\\\Users\\\\D\xc3\xa1ire\\\\Documents\\\\Final_Year\\\\FYP\\\\git\\\\matlab_models\\\\Splitmain.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo b_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  29,                                  /* lineNo */
  14,                                  /* colNo */
  "frames",                            /* aName */
  "Splitmain",                         /* fName */
  "C:\\\\Users\\\\D\xc3\xa1ire\\\\Documents\\\\Final_Year\\\\FYP\\\\git\\\\matlab_models\\\\Splitmain.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo c_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  18,                                  /* lineNo */
  37,                                  /* colNo */
  "a",                                 /* aName */
  "Splitmain",                         /* fName */
  "C:\\\\Users\\\\D\xc3\xa1ire\\\\Documents\\\\Final_Year\\\\FYP\\\\git\\\\matlab_models\\\\Splitmain.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo d_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  18,                                  /* lineNo */
  42,                                  /* colNo */
  "a",                                 /* aName */
  "Splitmain",                         /* fName */
  "C:\\\\Users\\\\D\xc3\xa1ire\\\\Documents\\\\Final_Year\\\\FYP\\\\git\\\\matlab_models\\\\Splitmain.m",/* pName */
  0                                    /* checkKind */
};

static emlrtBCInfo e_emlrtBCI = { -1,  /* iFirst */
  -1,                                  /* iLast */
  18,                                  /* lineNo */
  13,                                  /* colNo */
  "Splitmain",                         /* aName */
  "Splitmain",                         /* fName */
  "C:\\\\Users\\\\D\xc3\xa1ire\\\\Documents\\\\Final_Year\\\\FYP\\\\git\\\\matlab_models\\\\Splitmain.m",/* pName */
  0                                    /* checkKind */
};

static emlrtRSInfo o_emlrtRSI = { 36,  /* lineNo */
  "Splitmain",                         /* fcnName */
  "C:\\\\Users\\\\D\xc3\xa1ire\\\\Documents\\\\Final_Year\\\\FYP\\\\git\\\\matlab_models\\\\Splitmain.m"/* pathName */
};

static emlrtRSInfo p_emlrtRSI = { 10,  /* lineNo */
  "ftell",                             /* fcnName */
  "C:\\Program Files\\MATLAB\\R2017b\\toolbox\\eml\\lib\\matlab\\iofun\\ftell.m"/* pathName */
};

static emlrtRSInfo q_emlrtRSI = { 111, /* lineNo */
  "fileManager",                       /* fcnName */
  "C:\\Program Files\\MATLAB\\R2017b\\toolbox\\eml\\lib\\matlab\\iofun\\private\\fileManager.m"/* pathName */
};

static emlrtRSInfo r_emlrtRSI = { 16,  /* lineNo */
  "fseek",                             /* fcnName */
  "C:\\Program Files\\MATLAB\\R2017b\\toolbox\\eml\\lib\\matlab\\iofun\\fseek.m"/* pathName */
};

static emlrtRSInfo s_emlrtRSI = { 12,  /* lineNo */
  "frewind",                           /* fcnName */
  "C:\\Program Files\\MATLAB\\R2017b\\toolbox\\eml\\lib\\matlab\\iofun\\frewind.m"/* pathName */
};

/* Function Declarations */
static const mxArray *b_feval(const emlrtStack *sp, const mxArray *b, const
  mxArray *c, const mxArray *d, emlrtMCInfo *location);
static const mxArray *c_feval(const emlrtStack *sp, const mxArray *b, const
  mxArray *c, const mxArray *d, const mxArray *e, emlrtMCInfo *location);
static void d_feval(const emlrtStack *sp, const mxArray *b, const mxArray *c,
                    emlrtMCInfo *location);
static void disp(const emlrtStack *sp, const mxArray *b, emlrtMCInfo *location);
static const mxArray *feval(const emlrtStack *sp, const mxArray *b, const
  mxArray *c, emlrtMCInfo *location);

/* Function Definitions */
static const mxArray *b_feval(const emlrtStack *sp, const mxArray *b, const
  mxArray *c, const mxArray *d, emlrtMCInfo *location)
{
  const mxArray *pArrays[3];
  const mxArray *m5;
  pArrays[0] = b;
  pArrays[1] = c;
  pArrays[2] = d;
  return emlrtCallMATLABR2012b(sp, 1, &m5, 3, pArrays, "feval", true, location);
}

static const mxArray *c_feval(const emlrtStack *sp, const mxArray *b, const
  mxArray *c, const mxArray *d, const mxArray *e, emlrtMCInfo *location)
{
  const mxArray *pArrays[4];
  const mxArray *m6;
  pArrays[0] = b;
  pArrays[1] = c;
  pArrays[2] = d;
  pArrays[3] = e;
  return emlrtCallMATLABR2012b(sp, 1, &m6, 4, pArrays, "feval", true, location);
}

static void d_feval(const emlrtStack *sp, const mxArray *b, const mxArray *c,
                    emlrtMCInfo *location)
{
  const mxArray *pArrays[2];
  pArrays[0] = b;
  pArrays[1] = c;
  emlrtCallMATLABR2012b(sp, 0, NULL, 2, pArrays, "feval", true, location);
}

static void disp(const emlrtStack *sp, const mxArray *b, emlrtMCInfo *location)
{
  const mxArray *pArray;
  pArray = b;
  emlrtCallMATLABR2012b(sp, 0, NULL, 1, &pArray, "disp", true, location);
}

static const mxArray *feval(const emlrtStack *sp, const mxArray *b, const
  mxArray *c, emlrtMCInfo *location)
{
  const mxArray *pArrays[2];
  const mxArray *m4;
  pArrays[0] = b;
  pArrays[1] = c;
  return emlrtCallMATLABR2012b(sp, 1, &m4, 2, pArrays, "feval", true, location);
}

void Splitmain(const emlrtStack *sp, const char_T audio_data[], const int32_T
               audio_size[2], emxArray_real_T *b_Splitmain, emxArray_real_T
               *frames)
{
  emxArray_real_T *a;
  const mxArray *y;
  const mxArray *m0;
  static const int32_T iv0[2] = { 1, 5 };

  static const char_T u[5] = { 'f', 'o', 'p', 'e', 'n' };

  const mxArray *b_y;
  const mxArray *c_y;
  real_T file;
  static const int32_T iv1[2] = { 1, 5 };

  static const char_T b_u[5] = { 'f', 's', 'e', 'e', 'k' };

  const mxArray *d_y;
  static const int32_T iv2[2] = { 1, 3 };

  static const char_T orig[3] = { 'e', 'o', 'f' };

  static const int32_T iv3[2] = { 1, 5 };

  static const char_T c_u[5] = { 'f', 't', 'e', 'l', 'l' };

  real_T b_size;
  static const int32_T iv4[2] = { 1, 7 };

  static const char_T d_u[7] = { 'f', 'r', 'e', 'w', 'i', 'n', 'd' };

  uint32_T i;
  int32_T k;
  int32_T x;
  int32_T i0;
  int32_T loop_ub;
  uint8_T u0;
  uint8_T b_x[2];
  int32_T j;
  real_T *pData;
  int16_T e_y;
  emlrtStack st;
  emlrtStack b_st;
  emlrtStack c_st;
  emlrtStack d_st;
  st.prev = sp;
  st.tls = sp->tls;
  b_st.prev = &st;
  b_st.tls = st.tls;
  c_st.prev = &b_st;
  c_st.tls = b_st.tls;
  d_st.prev = &c_st;
  d_st.tls = c_st.tls;
  emlrtHeapReferenceStackEnterFcnR2012b(sp);
  emxInit_real_T(sp, &a, 1, &b_emlrtRTEI, true);

  /* [audio]= audioread(audio); */
  st.site = &emlrtRSI;
  b_st.site = &g_emlrtRSI;
  c_st.site = &h_emlrtRSI;
  y = NULL;
  m0 = emlrtCreateCharArray(2, iv0);
  emlrtInitCharArrayR2013a(&c_st, 5, m0, &u[0]);
  emlrtAssign(&y, m0);
  b_y = NULL;
  m0 = emlrtCreateCharArray(2, audio_size);
  emlrtInitCharArrayR2013a(&c_st, audio_size[1], m0, &audio_data[0]);
  emlrtAssign(&b_y, m0);
  c_y = NULL;
  m0 = emlrtCreateString1('r');
  emlrtAssign(&c_y, m0);
  d_st.site = &q_emlrtRSI;
  file = emlrt_marshallIn(&d_st, b_feval(&d_st, y, b_y, c_y, &b_emlrtMCI),
    "feval");
  st.site = &b_emlrtRSI;
  y = NULL;
  m0 = emlrtCreateCharArray(2, iv1);
  emlrtInitCharArrayR2013a(&st, 5, m0, &b_u[0]);
  emlrtAssign(&y, m0);
  b_y = NULL;
  m0 = emlrtCreateDoubleScalar(file);
  emlrtAssign(&b_y, m0);
  c_y = NULL;
  m0 = emlrtCreateDoubleScalar(0.0);
  emlrtAssign(&c_y, m0);
  d_y = NULL;
  m0 = emlrtCreateCharArray(2, iv2);
  emlrtInitCharArrayR2013a(&st, 3, m0, &orig[0]);
  emlrtAssign(&d_y, m0);
  b_st.site = &r_emlrtRSI;
  emlrt_marshallIn(&b_st, c_feval(&b_st, y, b_y, c_y, d_y, &c_emlrtMCI), "feval");
  st.site = &c_emlrtRSI;
  y = NULL;
  m0 = emlrtCreateCharArray(2, iv3);
  emlrtInitCharArrayR2013a(&st, 5, m0, &c_u[0]);
  emlrtAssign(&y, m0);
  b_y = NULL;
  m0 = emlrtCreateDoubleScalar(file);
  emlrtAssign(&b_y, m0);
  b_st.site = &p_emlrtRSI;
  b_size = emlrt_marshallIn(&b_st, feval(&b_st, y, b_y, &d_emlrtMCI), "feval");
  st.site = &d_emlrtRSI;
  y = NULL;
  m0 = emlrtCreateCharArray(2, iv4);
  emlrtInitCharArrayR2013a(&st, 7, m0, &d_u[0]);
  emlrtAssign(&y, m0);
  b_y = NULL;
  m0 = emlrtCreateDoubleScalar(file);
  emlrtAssign(&b_y, m0);
  b_st.site = &s_emlrtRSI;
  d_feval(&b_st, y, b_y, &e_emlrtMCI);
  st.site = &e_emlrtRSI;
  b_fread(&st, file, b_size, a);
  i = 1U;
  k = 1;
  x = (int32_T)muDoubleScalarFloor((real_T)a->size[0] / 512.0) + 1;
  file = (real_T)a->size[0] / 2.0;
  i0 = b_Splitmain->size[0];
  if (file != muDoubleScalarFloor(file)) {
    emlrtIntegerCheckR2012b(file, &emlrtDCI, sp);
  }

  b_Splitmain->size[0] = (int32_T)file;
  emxEnsureCapacity_real_T(sp, b_Splitmain, i0, &emlrtRTEI);
  if (file != muDoubleScalarFloor(file)) {
    emlrtIntegerCheckR2012b(file, &emlrtDCI, sp);
  }

  loop_ub = (int32_T)file;
  for (i0 = 0; i0 < loop_ub; i0++) {
    b_Splitmain->data[i0] = 0.0;
  }

  while (i < (uint32_T)a->size[0]) {
    st.site = &f_emlrtRSI;
    i0 = a->size[0];
    loop_ub = (int32_T)i;
    if (!((loop_ub >= 1) && (loop_ub <= i0))) {
      emlrtDynamicBoundsCheckR2012b(loop_ub, 1, i0, &c_emlrtBCI, &st);
    }

    file = muDoubleScalarRound(a->data[loop_ub - 1]);
    if (file < 256.0) {
      if (file >= 0.0) {
        u0 = (uint8_T)file;
      } else {
        u0 = 0;
      }
    } else if (file >= 256.0) {
      u0 = MAX_uint8_T;
    } else {
      u0 = 0;
    }

    b_x[0] = u0;
    i0 = a->size[0];
    loop_ub = (int32_T)i + 1;
    if (!((loop_ub >= 1) && (loop_ub <= i0))) {
      emlrtDynamicBoundsCheckR2012b(loop_ub, 1, i0, &d_emlrtBCI, &st);
    }

    file = muDoubleScalarRound(a->data[loop_ub - 1]);
    if (file < 256.0) {
      if (file >= 0.0) {
        u0 = (uint8_T)file;
      } else {
        u0 = 0;
      }
    } else if (file >= 256.0) {
      u0 = MAX_uint8_T;
    } else {
      u0 = 0;
    }

    b_x[1] = u0;
    memcpy((void *)&e_y, (void *)&b_x[0], (uint32_T)((size_t)1 * sizeof(int16_T)));
    i0 = b_Splitmain->size[0];
    if (!((k >= 1) && (k <= i0))) {
      emlrtDynamicBoundsCheckR2012b(k, 1, i0, &e_emlrtBCI, sp);
    }

    b_Splitmain->data[k - 1] = e_y;
    k++;
    i += 2U;
    if (*emlrtBreakCheckR2012bFlagVar != 0) {
      emlrtBreakCheckR2012b(sp);
    }
  }

  emxFree_real_T(&a);
  i0 = b_Splitmain->size[0];
  emxEnsureCapacity_real_T(sp, b_Splitmain, i0, &emlrtRTEI);
  loop_ub = b_Splitmain->size[0];
  for (i0 = 0; i0 < loop_ub; i0++) {
    b_Splitmain->data[i0] /= 32767.0;
  }

  i = 1U;
  k = 1;
  i0 = frames->size[0] * frames->size[1];
  frames->size[0] = 256;
  frames->size[1] = x;
  emxEnsureCapacity_real_T1(sp, frames, i0, &emlrtRTEI);
  loop_ub = x << 8;
  for (i0 = 0; i0 < loop_ub; i0++) {
    frames->data[i0] = 0.0;
  }

  while ((int32_T)i < x) {
    j = 1;
    while ((j < 257) && (k < b_Splitmain->size[0])) {
      i0 = b_Splitmain->size[0];
      if (!(k <= i0)) {
        emlrtDynamicBoundsCheckR2012b(k, 1, i0, &emlrtBCI, sp);
      }

      i0 = frames->size[1];
      loop_ub = (int32_T)i;
      if (!((loop_ub >= 1) && (loop_ub <= i0))) {
        emlrtDynamicBoundsCheckR2012b(loop_ub, 1, i0, &b_emlrtBCI, sp);
      }

      frames->data[(j + frames->size[0] * (loop_ub - 1)) - 1] =
        b_Splitmain->data[k - 1];
      j++;
      k++;
      if (*emlrtBreakCheckR2012bFlagVar != 0) {
        emlrtBreakCheckR2012b(sp);
      }
    }

    i = (uint32_T)((int32_T)i + 1);
    if (*emlrtBreakCheckR2012bFlagVar != 0) {
      emlrtBreakCheckR2012b(sp);
    }
  }

  y = NULL;
  m0 = emlrtCreateNumericArray(2, *(int32_T (*)[2])frames->size, mxDOUBLE_CLASS,
    mxREAL);
  pData = (real_T *)emlrtMxGetPr(m0);
  i0 = 0;
  for (loop_ub = 0; loop_ub < frames->size[1]; loop_ub++) {
    for (j = 0; j < 256; j++) {
      pData[i0] = frames->data[j + frames->size[0] * loop_ub];
      i0++;
    }
  }

  emlrtAssign(&y, m0);
  st.site = &o_emlrtRSI;
  disp(&st, y, &emlrtMCI);
  emlrtHeapReferenceStackLeaveFcnR2012b(sp);
}

/* End of code generation (Splitmain.c) */
