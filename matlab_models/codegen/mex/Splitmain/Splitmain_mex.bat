@echo off
set MATLAB=C:\PROGRA~1\MATLAB\R2017b
set MATLAB_ARCH=win64
set MATLAB_BIN="C:\Program Files\MATLAB\R2017b\bin"
set ENTRYPOINT=mexFunction
set OUTDIR=.\
set LIB_NAME=Splitmain_mex
set MEX_NAME=Splitmain_mex
set MEX_EXT=.mexw64
call setEnv.bat
echo # Make settings for Splitmain > Splitmain_mex.mki
echo COMPILER=%COMPILER%>> Splitmain_mex.mki
echo COMPFLAGS=%COMPFLAGS%>> Splitmain_mex.mki
echo OPTIMFLAGS=%OPTIMFLAGS%>> Splitmain_mex.mki
echo DEBUGFLAGS=%DEBUGFLAGS%>> Splitmain_mex.mki
echo LINKER=%LINKER%>> Splitmain_mex.mki
echo LINKFLAGS=%LINKFLAGS%>> Splitmain_mex.mki
echo LINKOPTIMFLAGS=%LINKOPTIMFLAGS%>> Splitmain_mex.mki
echo LINKDEBUGFLAGS=%LINKDEBUGFLAGS%>> Splitmain_mex.mki
echo MATLAB_ARCH=%MATLAB_ARCH%>> Splitmain_mex.mki
echo OMPFLAGS= >> Splitmain_mex.mki
echo OMPLINKFLAGS= >> Splitmain_mex.mki
echo EMC_COMPILER=msvc150>> Splitmain_mex.mki
echo EMC_CONFIG=optim>> Splitmain_mex.mki
"C:\Program Files\MATLAB\R2017b\bin\win64\gmake" -B -f Splitmain_mex.mk
