/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * Splitmain_initialize.c
 *
 * Code generation for function 'Splitmain_initialize'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "Splitmain.h"
#include "Splitmain_initialize.h"
#include "fileManager.h"
#include "_coder_Splitmain_mex.h"
#include "Splitmain_data.h"

/* Function Declarations */
static void Splitmain_once(void);

/* Function Definitions */
static void Splitmain_once(void)
{
  eml_autoflush_not_empty_init();
  eml_openfiles_not_empty_init();
}

void Splitmain_initialize(void)
{
  emlrtStack st = { NULL,              /* site */
    NULL,                              /* tls */
    NULL                               /* prev */
  };

  mexFunctionCreateRootTLS();
  emlrtBreakCheckR2012bFlagVar = emlrtGetBreakCheckFlagAddressR2012b();
  st.tls = emlrtRootTLSGlobal;
  emlrtClearAllocCountR2012b(&st, false, 0U, 0);
  emlrtEnterRtStackR2012b(&st);
  if (emlrtFirstTimeR2012b(emlrtRootTLSGlobal)) {
    Splitmain_once();
  }
}

/* End of code generation (Splitmain_initialize.c) */
