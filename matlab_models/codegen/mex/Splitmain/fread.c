/*
 * Student License - for use by students to meet course requirements and
 * perform academic research at degree granting institutions only.  Not
 * for government, commercial, or other organizational use.
 *
 * fread.c
 *
 * Code generation for function 'fread'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "Splitmain.h"
#include "fread.h"
#include "Splitmain_emxutil.h"
#include "error.h"
#include "Splitmain_mexutil.h"

/* Variable Definitions */
static emlrtRSInfo i_emlrtRSI = { 50,  /* lineNo */
  "fread",                             /* fcnName */
  "C:\\Program Files\\MATLAB\\R2017b\\toolbox\\eml\\lib\\matlab\\iofun\\fread.m"/* pathName */
};

static emlrtRSInfo j_emlrtRSI = { 66,  /* lineNo */
  "fread",                             /* fcnName */
  "C:\\Program Files\\MATLAB\\R2017b\\toolbox\\eml\\lib\\matlab\\iofun\\fread.m"/* pathName */
};

static emlrtRSInfo k_emlrtRSI = { 80,  /* lineNo */
  "fread",                             /* fcnName */
  "C:\\Program Files\\MATLAB\\R2017b\\toolbox\\eml\\lib\\matlab\\iofun\\fread.m"/* pathName */
};

static emlrtRSInfo l_emlrtRSI = { 113, /* lineNo */
  "fread",                             /* fcnName */
  "C:\\Program Files\\MATLAB\\R2017b\\toolbox\\eml\\lib\\matlab\\iofun\\fread.m"/* pathName */
};

static emlrtRSInfo m_emlrtRSI = { 163, /* lineNo */
  "fread",                             /* fcnName */
  "C:\\Program Files\\MATLAB\\R2017b\\toolbox\\eml\\lib\\matlab\\iofun\\fread.m"/* pathName */
};

static emlrtRSInfo n_emlrtRSI = { 424, /* lineNo */
  "fread",                             /* fcnName */
  "C:\\Program Files\\MATLAB\\R2017b\\toolbox\\eml\\lib\\matlab\\iofun\\fread.m"/* pathName */
};

static emlrtMCInfo f_emlrtMCI = { 350, /* lineNo */
  1,                                   /* colNo */
  "fread",                             /* fName */
  "C:\\Program Files\\MATLAB\\R2017b\\toolbox\\eml\\lib\\matlab\\iofun\\fread.m"/* pName */
};

static emlrtMCInfo g_emlrtMCI = { 354, /* lineNo */
  9,                                   /* colNo */
  "fread",                             /* fName */
  "C:\\Program Files\\MATLAB\\R2017b\\toolbox\\eml\\lib\\matlab\\iofun\\fread.m"/* pName */
};

static emlrtRTEInfo c_emlrtRTEI = { 1, /* lineNo */
  22,                                  /* colNo */
  "fread",                             /* fName */
  "C:\\Program Files\\MATLAB\\R2017b\\toolbox\\eml\\lib\\matlab\\iofun\\fread.m"/* pName */
};

static emlrtRTEInfo e_emlrtRTEI = { 449,/* lineNo */
  15,                                  /* colNo */
  "fread",                             /* fName */
  "C:\\Program Files\\MATLAB\\R2017b\\toolbox\\eml\\lib\\matlab\\iofun\\fread.m"/* pName */
};

static emlrtDCInfo b_emlrtDCI = { 386, /* lineNo */
  45,                                  /* colNo */
  "fread",                             /* fName */
  "C:\\Program Files\\MATLAB\\R2017b\\toolbox\\eml\\lib\\matlab\\iofun\\fread.m",/* pName */
  4                                    /* checkKind */
};

static emlrtRSInfo t_emlrtRSI = { 350, /* lineNo */
  "fread",                             /* fcnName */
  "C:\\Program Files\\MATLAB\\R2017b\\toolbox\\eml\\lib\\matlab\\iofun\\fread.m"/* pathName */
};

static emlrtRSInfo u_emlrtRSI = { 354, /* lineNo */
  "fread",                             /* fcnName */
  "C:\\Program Files\\MATLAB\\R2017b\\toolbox\\eml\\lib\\matlab\\iofun\\fread.m"/* pathName */
};

/* Function Declarations */
static void c_emlrt_marshallIn(const emlrtStack *sp, const mxArray *t, const
  char_T *identifier, emxArray_real_T *y);
static void d_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, emxArray_real_T *y);
static void e_feval(const emlrtStack *sp, const mxArray *b, const mxArray *c,
                    const mxArray *d, const mxArray *e, const mxArray *f,
                    emlrtMCInfo *location, const mxArray **g, const mxArray **h);
static void h_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId, emxArray_real_T *ret);
static const mxArray *size(const emlrtStack *sp, const mxArray *b, const mxArray
  *c, emlrtMCInfo *location);

/* Function Definitions */
static void c_emlrt_marshallIn(const emlrtStack *sp, const mxArray *t, const
  char_T *identifier, emxArray_real_T *y)
{
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = (const char *)identifier;
  thisId.fParent = NULL;
  thisId.bParentIsCell = false;
  d_emlrt_marshallIn(sp, emlrtAlias(t), &thisId, y);
  emlrtDestroyArray(&t);
}

static void d_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId, emxArray_real_T *y)
{
  h_emlrt_marshallIn(sp, emlrtAlias(u), parentId, y);
  emlrtDestroyArray(&u);
}

static void e_feval(const emlrtStack *sp, const mxArray *b, const mxArray *c,
                    const mxArray *d, const mxArray *e, const mxArray *f,
                    emlrtMCInfo *location, const mxArray **g, const mxArray **h)
{
  const mxArray *pArrays[5];
  const mxArray *mv0[2];
  pArrays[0] = b;
  pArrays[1] = c;
  pArrays[2] = d;
  pArrays[3] = e;
  pArrays[4] = f;
  emlrtAssign(g, emlrtCallMATLABR2012b(sp, 2, &mv0[0], 5, pArrays, "feval", true,
    location));
  emlrtAssign(h, mv0[1]);
}

static void h_emlrt_marshallIn(const emlrtStack *sp, const mxArray *src, const
  emlrtMsgIdentifier *msgId, emxArray_real_T *ret)
{
  static const int32_T dims[1] = { -1 };

  const boolean_T bv0[1] = { true };

  int32_T iv9[1];
  int32_T i2;
  emlrtCheckVsBuiltInR2012b(sp, msgId, src, "double", false, 1U, dims, &bv0[0],
    iv9);
  i2 = ret->size[0];
  ret->size[0] = iv9[0];
  emxEnsureCapacity_real_T(sp, ret, i2, (emlrtRTEInfo *)NULL);
  emlrtImportArrayR2015b(sp, src, ret->data, 8, false);
  emlrtDestroyArray(&src);
}

static const mxArray *size(const emlrtStack *sp, const mxArray *b, const mxArray
  *c, emlrtMCInfo *location)
{
  const mxArray *pArrays[2];
  const mxArray *m7;
  pArrays[0] = b;
  pArrays[1] = c;
  return emlrtCallMATLABR2012b(sp, 1, &m7, 2, pArrays, "size", true, location);
}

void b_fread(const emlrtStack *sp, real_T fileID, real_T sizeA, emxArray_real_T *
             A)
{
  boolean_T p;
  const mxArray *y;
  const mxArray *m1;
  static const int32_T iv5[2] = { 1, 5 };

  static const char_T u[5] = { 'f', 'r', 'e', 'a', 'd' };

  const mxArray *b_y;
  const mxArray *c_y;
  const mxArray *d_y;
  static const int32_T iv6[2] = { 1, 5 };

  static const char_T precision[5] = { 'u', 'i', 'n', 't', '8' };

  const mxArray *e_y;
  const mxArray *t = NULL;
  const mxArray *count = NULL;
  real_T nrows;
  int32_T i1;
  emlrtStack st;
  emlrtStack b_st;
  emlrtStack c_st;
  emlrtStack d_st;
  st.prev = sp;
  st.tls = sp->tls;
  st.site = &i_emlrtRSI;
  b_st.prev = &st;
  b_st.tls = st.tls;
  c_st.prev = &b_st;
  c_st.tls = b_st.tls;
  d_st.prev = &c_st;
  d_st.tls = c_st.tls;
  if ((fileID != 0.0) && (fileID != 1.0) && (fileID != 2.0)) {
    p = true;
  } else {
    p = false;
  }

  if (!p) {
    b_st.site = &j_emlrtRSI;
    c_st.site = &j_emlrtRSI;
    error(&c_st);
  }

  b_st.site = &k_emlrtRSI;
  c_st.site = &l_emlrtRSI;
  if (!muDoubleScalarIsInf(sizeA)) {
    d_st.site = &n_emlrtRSI;
    if ((!muDoubleScalarIsInf(sizeA)) && (!muDoubleScalarIsNaN(sizeA)) && (sizeA
         >= 0.0) && (2.147483647E+9 >= sizeA)) {
      p = true;
    } else {
      p = false;
    }

    if (!p) {
      emlrtErrorWithMessageIdR2012b(&d_st, &e_emlrtRTEI, "MATLAB:badsize_mx", 0);
    }
  }

  c_st.site = &m_emlrtRSI;
  y = NULL;
  m1 = emlrtCreateCharArray(2, iv5);
  emlrtInitCharArrayR2013a(&c_st, 5, m1, &u[0]);
  emlrtAssign(&y, m1);
  b_y = NULL;
  m1 = emlrtCreateDoubleScalar(fileID);
  emlrtAssign(&b_y, m1);
  c_y = NULL;
  m1 = emlrtCreateDoubleScalar(sizeA);
  emlrtAssign(&c_y, m1);
  d_y = NULL;
  m1 = emlrtCreateCharArray(2, iv6);
  emlrtInitCharArrayR2013a(&c_st, 5, m1, &precision[0]);
  emlrtAssign(&d_y, m1);
  e_y = NULL;
  m1 = emlrtCreateNumericMatrix(1, 1, mxINT32_CLASS, mxREAL);
  *(int32_T *)emlrtMxGetData(m1) = 0;
  emlrtAssign(&e_y, m1);
  d_st.site = &t_emlrtRSI;
  e_feval(&d_st, y, b_y, c_y, d_y, e_y, &f_emlrtMCI, &t, &count);
  d_st.site = &t_emlrtRSI;
  emlrt_marshallIn(&d_st, emlrtAlias(count), "count");
  y = NULL;
  m1 = emlrtCreateDoubleScalar(1.0);
  emlrtAssign(&y, m1);
  d_st.site = &u_emlrtRSI;
  nrows = emlrt_marshallIn(&d_st, size(&d_st, emlrtAlias(t), y, &g_emlrtMCI),
    "size");
  if (!(nrows >= 0.0)) {
    emlrtNonNegativeCheckR2012b(nrows, &b_emlrtDCI, &c_st);
  }

  if (nrows == 0.0) {
    i1 = A->size[0];
    A->size[0] = 0;
    emxEnsureCapacity_real_T(&c_st, A, i1, &c_emlrtRTEI);
  } else {
    c_emlrt_marshallIn(&c_st, emlrtAlias(t), "t", A);
  }

  emlrtDestroyArray(&t);
  emlrtDestroyArray(&count);
}

/* End of code generation (fread.c) */
