/*
 * File Name:         hdl_prj\ipcore\LowPassFilter1kHz_v1_0\include\LowPassFilter1kHz_addr.h
 * Description:       C Header File
 * Created:           2018-03-05 22:21:40
*/

#ifndef LOWPASSFILTER1KHZ_H_
#define LOWPASSFILTER1KHZ_H_

#define  IPCore_Reset_LowPassFilter1kHz       0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_LowPassFilter1kHz      0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_LowPassFilter1kHz   0x8  //contains unique IP timestamp (yymmddHHMM): 1803052221

#endif /* LOWPASSFILTER1KHZ_H_ */
