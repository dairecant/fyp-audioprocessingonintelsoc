/*
 * File Name:         hdl_prj\ipcore\tremeloNo_ip_v1_0\include\tremeloNo_ip_addr.h
 * Description:       C Header File
 * Created:           2018-03-05 20:06:45
*/

#ifndef TREMELONO_IP_H_
#define TREMELONO_IP_H_

#define  IPCore_Reset_tremeloNo_ip       0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_tremeloNo_ip      0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_tremeloNo_ip   0x8  //contains unique IP timestamp (yymmddHHMM): 1803052006

#endif /* TREMELONO_IP_H_ */
