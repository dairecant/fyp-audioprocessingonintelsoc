/*
 * File Name:         hdl_prj\ipcore\Tremelo_v1_0\include\Tremelo_addr.h
 * Description:       C Header File
 * Created:           2018-03-08 16:53:38
*/

#ifndef TREMELO_H_
#define TREMELO_H_

#define  IPCore_Reset_Tremelo       0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_Tremelo      0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_Tremelo   0x8  //contains unique IP timestamp (yymmddHHMM): 1803081653

#endif /* TREMELO_H_ */
