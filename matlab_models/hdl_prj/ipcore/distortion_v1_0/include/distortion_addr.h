/*
 * File Name:         hdl_prj\ipcore\distortion_v1_0\include\distortion_addr.h
 * Description:       C Header File
 * Created:           2018-02-22 09:16:37
*/

#ifndef DISTORTION_H_
#define DISTORTION_H_

#define  IPCore_Reset_distortion       0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_distortion      0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_distortion   0x8  //contains unique IP timestamp (yymmddHHMM): 1802220916

#endif /* DISTORTION_H_ */
