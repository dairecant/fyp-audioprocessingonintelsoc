/*
 * File Name:         hdl_prj\ipcore\Subsystem_ip_v1_0\include\Subsystem_ip_addr.h
 * Description:       C Header File
 * Created:           2018-03-08 16:50:52
*/

#ifndef SUBSYSTEM_IP_H_
#define SUBSYSTEM_IP_H_

#define  IPCore_Reset_Subsystem_ip       0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_Subsystem_ip      0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_Subsystem_ip   0x8  //contains unique IP timestamp (yymmddHHMM): 1803081650

#endif /* SUBSYSTEM_IP_H_ */
