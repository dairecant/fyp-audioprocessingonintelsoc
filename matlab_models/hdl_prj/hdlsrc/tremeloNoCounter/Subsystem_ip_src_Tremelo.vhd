-- -------------------------------------------------------------
-- 
-- File Name: hdl_prj\hdlsrc\tremeloNoCounter\Subsystem_ip_src_Tremelo.vhd
-- Created: 2018-03-08 16:50:41
-- 
-- Generated by MATLAB 9.3 and HDL Coder 3.11
-- 
-- 
-- -------------------------------------------------------------
-- Rate and Clocking Details
-- -------------------------------------------------------------
-- Model base rate: 0.000125
-- Target subsystem base rate: 0.000125
-- 
-- 
-- Clock Enable  Sample Time
-- -------------------------------------------------------------
-- ce_out        0.000125
-- -------------------------------------------------------------
-- 
-- 
-- Output Signal                 Clock Enable  Sample Time
-- -------------------------------------------------------------
-- soundOut                      ce_out        0.000125
-- -------------------------------------------------------------
-- 
-- -------------------------------------------------------------


-- -------------------------------------------------------------
-- 
-- Module: Subsystem_ip_src_Tremelo
-- Source Path: tremeloNoCounter/Tremelo
-- Hierarchy Level: 0
-- 
-- -------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

ENTITY Subsystem_ip_src_Tremelo IS
  PORT( clk                               :   IN    std_logic;
        reset                             :   IN    std_logic;
        clk_enable                        :   IN    std_logic;
        sound                             :   IN    std_logic_vector(15 DOWNTO 0);  -- int16
        freq                              :   IN    std_logic_vector(15 DOWNTO 0);  -- int16
        ce_out                            :   OUT   std_logic;
        soundOut                          :   OUT   std_logic_vector(15 DOWNTO 0)  -- int16
        );
END Subsystem_ip_src_Tremelo;


ARCHITECTURE rtl OF Subsystem_ip_src_Tremelo IS

  -- Component Declarations
  COMPONENT Subsystem_ip_src_MATLAB_Function
    PORT( sound                           :   IN    std_logic_vector(15 DOWNTO 0);  -- int16
          freq                            :   IN    std_logic_vector(15 DOWNTO 0);  -- int16
          count                           :   IN    std_logic_vector(15 DOWNTO 0);  -- int16
          tremelo                         :   OUT   std_logic_vector(15 DOWNTO 0)  -- int16
          );
  END COMPONENT;

  -- Component Configuration Statements
  FOR ALL : Subsystem_ip_src_MATLAB_Function
    USE ENTITY work.Subsystem_ip_src_MATLAB_Function(rtl);

  -- Signals
  SIGNAL enb                              : std_logic;
  SIGNAL HDL_Counter_out1                 : signed(15 DOWNTO 0);  -- int16
  SIGNAL tremelo                          : std_logic_vector(15 DOWNTO 0);  -- ufix16

BEGIN
  u_MATLAB_Function : Subsystem_ip_src_MATLAB_Function
    PORT MAP( sound => sound,  -- int16
              freq => freq,  -- int16
              count => std_logic_vector(HDL_Counter_out1),  -- int16
              tremelo => tremelo  -- int16
              );

  enb <= clk_enable;

  -- Count limited, Signed Counter
  --  initial value   = 0
  --  step value      = 1
  --  count to value  = 1000
  HDL_Counter_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      HDL_Counter_out1 <= to_signed(16#0000#, 16);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb = '1' THEN
        IF HDL_Counter_out1 >= to_signed(16#03E8#, 16) THEN 
          HDL_Counter_out1 <= to_signed(16#0000#, 16);
        ELSE 
          HDL_Counter_out1 <= HDL_Counter_out1 + to_signed(16#0001#, 16);
        END IF;
      END IF;
    END IF;
  END PROCESS HDL_Counter_process;


  ce_out <= clk_enable;

  soundOut <= tremelo;

END rtl;

