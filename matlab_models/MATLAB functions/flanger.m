%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%           FLANGER EFFECT                                      %
% Author:   D�ire Canavan 30/10/2017                            %
% Implements flanger effect on audio                            %
% Mathematical function using modulated delay                   %
% By increasing range value, a 'chorus' effect can also be made %
%  sound: inputted wav file                              %
%  fs:    sampling frequency                             %
%  sweep_freq: 'sweeping' frequency of delay modulation  %
%               This is the cause of the sweeping sound  %
%  range: max/min delay value of modulator               %             
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function flange = flanger(clip,sweep_freq,sweep_range,fs)

given_delay = 15; % starting delay

%%%%Using loop that looks at samples behind current sample in array (regular method) %%
for i=given_delay+sweep_range:length(clip)
    flange(i) = clip(i)+clip(i-given_delay-round(sweep_range*sin(2*pi*i*sweep_freq/fs)));
end
flange = transpose(flange);



%%%Commented OUt version used when attempted HDL generation through
%%%function block in SImulink
%count = 0;
%dir = 1;
% %for i = 1:length(clip)-delay-range
%     if((i-delay-count)>=1)
%         flange(i) = clip(i)+clip(i-delay-count);
%         if(count<range && dir ==1)
%             count = count +1;
%         else
%             count = count -1;
%         end
%         if(count ==range && dir ==1)
%             dir = 0;
%         end
%         
%     end
% end
% 
% %flange = transpose(flange);