function tremelo_real_time = tremelo_real_time(sound,gain)
gain = abs(gain/32768);
tremelo_real_time= gain*sound;

end