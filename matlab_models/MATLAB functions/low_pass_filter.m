%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%           Low Pass Filter EFFECT                          %
% Author:   D�ire Canavan 3/12/2017                         %
% Implements 15th order low pass filter effect on audio     %
% Works on fixed delay value input                          %
%   sound: Input sound                                      %
%   cutoff: cut-off frequency                               %
%   fs: sampling frequency of the input                     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [filtered,b] = low_pass_filter(sound,cutoff,fs)

f_nyq = fs/2;
b = fir1(15,cutoff/f_nyq);
filtered=filter(b,1,sound);