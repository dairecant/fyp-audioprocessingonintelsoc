function hexVals = convertCoeffsToHex(c)

hexVals=dec2hex(floor(c.*32767));