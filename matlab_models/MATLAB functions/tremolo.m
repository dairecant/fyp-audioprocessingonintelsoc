%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%           TREMELO EFFECT             %
% Author:   D�ire Canavan 30/10/2017   %
% Implements tremelo effect on audio   %
% Works on modulating gain value       %
%  sound: inputted wav file            %
%  fs:    sampling frequency           %
%  freq:  frequency of gain modulation %
%  gain:  amplitude overall of the gain%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function tremolo = tremolo(sound,freq,fs)
    tremolo = zeros(length(sound),1);
    for i=1:length(sound)
           tremolo(i)= sin(2*pi*freq/fs*i)*sound(i); 
    end
    
%%FEEDBACK LOOP VERSION%%%%%
%% In Simulink, the 'count' value is fed back into the function
%% Creates a triangle wave for amplitude modulation
%     if(direction)
%         count = count +1;
%         if(count==freq)
%             direction = ~direction;
%             
%         end
%     else
%         count = count -1;
%         if(count==0)
%             direction = ~direction;
%         end
%     end
%    tremelo= sound*(count/freq);

