function sineVals = find_sine_vals(freq)
i = 1;
sinval = 0;

while (freq/44100)*i<=0.25
    sineVals(i) = sin(2*pi*freq/44100*i); 
    i = i+1;
    
end