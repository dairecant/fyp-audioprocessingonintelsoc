
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%           Delay EFFECT                    %
% Author:   D�ire Canavan 30/10/2017        %
% Implements delay effect on audio          %
% Works on fixed delay value input          %
%   sound: Input sound                      %
%   gain: Delay amplitude                   %
%   delaytime: delay time in milliseconds   %
%   fs: frequency sample of the input       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [delayed] = delay(sound, feedback,delaytime, fs)

if nargin == 4 %if sampling frequency given
    delaySamples=floor(delaytime./1000.*fs); %delay in samples

else %else assume delay given is in samples
    delaySamples=floor(delaytime);

end
delayed = sound;

for sample = delaySamples+1:length(sound)
    if(sample-delaySamples>0)
        delayed(sample) = sound(sample)+feedback*(delayed(sample-delaySamples));
    end
end;