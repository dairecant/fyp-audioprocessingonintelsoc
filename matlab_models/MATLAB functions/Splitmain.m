
function [Splitmain,frames] = Splitmain(audio)

%[audio]= audioread(audio);

file = fopen(audio,'r');
fseek(file,0,'eof');
size = ftell(file);
frewind(file);
a = fread(file,size,'uint8');
size = length(a);

i=1;
k=1;
main_length=floor(size/512)+1;
Splitmain=zeros(size/2,1);
while(i<size)
  Splitmain(k) =  typecast(uint8([a(i) a(i+1)]),'int16');  
    k=k+1;
    i=i+2; 
end
Splitmain = Splitmain/32767;
i=1;
k=1;
frames = zeros(256,main_length);
while(i<main_length)
    j=1;
    while(j<257&&k<length(Splitmain))
    frames(j,i)= Splitmain(k);
    j=j+1;
    k=k+1;
    end
  i=i+1;
end

disp (frames);

   


