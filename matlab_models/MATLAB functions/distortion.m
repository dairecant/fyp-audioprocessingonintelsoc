%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%           Distortion EFFECT                         %
% Author:   D�ire Canavan 30/10/2017                  %
% Implements distortion effect on audio               %
% Works by hard-clipping the audio @ pos/neg values   %
%  sample: inputted wav file                          %
%  fs:    sampling frequency (not implemented)        %
%  clipVal: Max/Min clip value of audio               %
%  gain: Output gain of distortion(not in)            %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function distorted = distortion(sample,clipVal)

% for k = 1:length(sample) USED FOR STEREO INPUT
%     left(k) = sample(k,1);
%     %right(k)= sample(k,2);
% end


 for j = 1:length(sample)
         if (sample(j)>clipVal)
             sample(j) = clipVal;
         elseif (sample(j)<-clipVal)
                 sample(j) = -clipVal;
         else
                 sample(j) = sample(j);
         end
 
 end
distorted = sample;
%     
%%%%%PREVIOUS IMPLEMENTATION
%k = 2*clipVal/(1-clipVal);
%sample = (1+k)*(sample)./(1+k*abs(sample));
%right = (1+k)*(right)./(1+k*abs(right));




    
