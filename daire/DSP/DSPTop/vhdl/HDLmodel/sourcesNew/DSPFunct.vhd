-- DSP function
-- Feb 2018, Fearghal Morgan
-- Currently includes clk, rst inputs, though implements only a combinational function
-- May use clk, rst in a future application

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

entity DSPFunct is
port (clk :          in std_logic; 
      rst :          in std_logic;  
      DSPDatFromMem: in std_logic_vector(15 downto 0);  
      DSPDatToMem:   out std_logic_vector(15 downto 0)
	  );
end DSPFunct;

architecture RTL of DSPFunct is

begin

DSPTask_i: process (DSPDatFromMem)	
begin
	DSPDatToMem(15 downto 8) <= not DSPDatFromMem(15 downto 8);
	DSPDatToMem( 7 downto 0) <= not DSPDatFromMem( 7 downto 0);
end process;

end RTL;