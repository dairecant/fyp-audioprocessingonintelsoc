-- Description: DSPTop
-- Feb 2018, Fearghal Morgan

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.DSPPackage.all;

entity DSPTop is
Port ( clk :        		 in std_logic;						-- system clock strobe
       rst :        		 in std_logic; 						-- asynchronous system reset, asserted

       goDSP :				 in  std_logic;
       DSPDone : 			 out std_logic;
	   
       selFirstEffect: in std_logic_vector(2 downto 0);  
	   selFinalEffect: in std_logic_vector(1 downto 0);  
	   ctrl0_0:        in std_logic_vector(15 downto 0);  
	   ctrl0_1:        in std_logic_vector(15 downto 0);  
	   ctrl1_0:        in std_logic_vector(15 downto 0);  
	   ctrl1_1:        in std_logic_vector(15 downto 0);  
	   ctrl2_0:        in std_logic_vector(15 downto 0);  
	   ctrl2_1:        in std_logic_vector(15 downto 0);  
	   ctrl3_0:        in std_logic_vector(15 downto 0);  
	   ctrl3_1:        in std_logic_vector(15 downto 0);  
	   
       host_EnDSPMem :     	 in  std_logic;
	   host_DSPMem_Load :    in  std_logic;                      
	   host_DSPMem_LoadDat : in  std_logic_vector(15 downto 0); 
	   host_DSPMem_Add :     in  std_logic_vector(9 downto 0);  
	   host_DSPMem_DatIn :   in  std_logic_vector(15 downto 0);  
	   host_DSPMem_Wr :      in  std_logic;                      
	   host_DSPMem_DatOut :  out std_logic_vector(15 downto 0)
--	   host_DSPMem_DatArrayOut : out std_logic_vector(511 downto 0)  
      );
end DSPTop;

architecture struct of DSPTop is                 

component singleShot is
Port (clk   : 	in 	std_logic;
      rst   : 	in 	std_logic;
      sw    : 	in 	std_logic; 
      aShot :   out std_logic  
	 ); 
end component;

component dualPortRegBlk1024x16WithLoad is
 Port (  clk		: in  std_logic;    					 
		 rst		: in  std_logic;    					 
         enPort0    : in  std_logic;

		 p0Load	    : in  std_logic;   
		 p0LoadDat  : in  std_logic_vector(15 downto 0);                      
		 p0Add 	 	: in  std_logic_vector( 9 downto 0);   
		 p0DatIn  	: in  std_logic_vector(15 downto 0);                      
		 p0Wr 	    : in  std_logic;   
	     p0DatOut   : out std_logic_vector(15 downto 0);                      
--		 p0DatArrayOut : out std_logic_vector(511 downto 0);

		 p1Load	    : in  std_logic;   
		 p1LoadDat  : in  std_logic_vector(15 downto 0);                      
		 p1RdAdd 	 	: in  std_logic_vector( 9 downto 0);   
		 p1WrAdd 	 	: in  std_logic_vector( 9 downto 0);   
		 p1DatIn  	: in  std_logic_vector(15 downto 0);                      
		 p1Wr 	    : in  std_logic;   
	     p1DatOut   : out std_logic_vector(15 downto 0)                     
 		 );         
end component;

component DSPCtrlr is
port (clk :           in std_logic; 
      rst :        	  in std_logic; 		
      goDSPPulse:     in std_logic;
      selFirstEffect: in std_logic_vector(2 downto 0);  
	  selFinalEffect: in std_logic_vector(1 downto 0);  
	  ctrl0_0:        in std_logic_vector(15 downto 0);  
	  ctrl0_1:        in std_logic_vector(15 downto 0);  
	  ctrl1_0:        in std_logic_vector(15 downto 0);  
	  ctrl1_1:        in std_logic_vector(15 downto 0);  
	  ctrl2_0:        in std_logic_vector(15 downto 0);  
	  ctrl2_1:        in std_logic_vector(15 downto 0);  
	  ctrl3_0:        in std_logic_vector(15 downto 0);  
	  ctrl3_1:        in std_logic_vector(15 downto 0);  
      DSPDatFromMem:  in std_logic_vector(15 downto 0);  
      DSPMemRdAdd:      out std_logic_vector(9 downto 0);
      DSPMemWrAdd:      out std_logic_vector(9 downto 0);
      DSPDatToMem:    out std_logic_vector(15 downto 0);
      DSPMemWr:       out std_logic;
      DSPDone:        out std_logic
     );
end component;

signal  goDSPPulse:     std_logic;
signal  DSPDatFromMem:  std_logic_vector(15 downto 0);  
signal  DSPMemRdAdd:    std_logic_vector(9 downto 0);
signal  DSPMemWrAdd:    std_logic_vector(9 downto 0);
signal  DSPDatToMem:    std_logic_vector(15 downto 0);
signal  DSPMemWr:       std_logic;
signal  DSPMemDone:     std_logic;

begin 

singleShot_i: singleShot 
port map (clk   => clk,
          rst   => rst,
          sw    => goDSP,
          aShot => goDSPPulse
	      ); 
	 		 
DSPCtrlr_i: DSPCtrlr
port map (clk  		    => clk,  		
          rst 			=> rst, 			
          goDSPPulse	=> goDSPPulse,	
          selFirstEffect=> selFirstEffect,
	      selFinalEffect=> selFinalEffect, 
	      ctrl0_0		=> ctrl0_0,		 
	      ctrl0_1		=> ctrl0_1,		 
	      ctrl1_0		=> ctrl1_0,		 
	      ctrl1_1		=> ctrl1_1,		 
	      ctrl2_0		=> ctrl2_0,		 
	      ctrl2_1		=> ctrl2_1,		 
	      ctrl3_0		=> ctrl3_0,		 
	      ctrl3_1		=> ctrl3_1,		   
          DSPDatFromMem => DSPDatFromMem,
          DSPMemRdAdd	=> DSPMemRdAdd,	
          DSPMemWrAdd	=> DSPMemWrAdd,	
          DSPDatToMem   => DSPDatToMem,	
          DSPMemWr		=> DSPMemWr,		
          DSPDone	    => DSPDone	
	      );            
		  		  
DSPMem_i: dualPortRegBlk1024x16WithLoad
Port map (clk		=> clk,
		 rst		=> '0',
         enPort0    => host_EnDSPMem,
		 p0Load	    => host_DSPMem_Load,
		 p0LoadDat  => host_DSPMem_LoadDat,                     
		 p0Add 	 	=> host_DSPMem_Add,  
		 p0DatIn  	=> host_DSPMem_DatIn,                     
		 p0Wr 	    => host_DSPMem_Wr,
	     p0DatOut   => host_DSPMem_DatOut,                     
--		 p0DatArrayOut => host_DSPMem_DatArrayOut,
                    
		 p1Load	    => '0', 
		 p1LoadDat  => (others => '0'),                     
		 p1RdAdd 	=> DSPMemRdAdd,  
		 p1WrAdd 	=> DSPMemWrAdd,  
		 p1DatIn  	=> DSPDatToMem,                     
		 p1Wr 	    => DSPMemWr,
	     p1DatOut   => DSPDatFromMem                  
 		 );         
	   
end struct;