-- dualPortRegBlk1024x16WithLoad
-- Synthesisable VHDL model for 1024, writeable word (16-bit)-wide dual port register array, 
-- with load of all elements by either port 0 or port 1
-- Created :    Dec 2017. Fearghal Morgan
--
-- Supports two address/data/write/read ports (p0 and p1)
-- read
--   Port 0/1 can simultaneously read their addressed (p0Add/p1RdAdd) array data (on buses P0DatOut/P1DatOut respectively) 
--
-- write
--   If enPort0 is asserted,   
--      assertion of p0Load (has priority over p0Wr): all elements in the array are synchronously loaded with p0LoadDat
--      port 0 data write access to the memory array is active (writing data p0Dat to p0Add, on assertion of p0Wr)
--   If enPort0 is deasserted,   
--      assertion of p1Load (has priority over p1Wr): all elements in the array are synchronously loaded with p1LoadDat
--      port 1 data write access to the memory array is active (writing data p1Dat to p1WrAdd, on assertion of p1Wr)

-- Signal data dictionary
-- clk			      System clock strobe, rising edge active
-- rst			      System reset, assertion (high) clears all registers
-- enPort0			  Assertion (H) enables memory port 0 load (on assertion of p0Load), or writes (on assertion of p0Wr) 
--
-- p0Load 	 		  port 0 load enable. Assertion synchronously loads all memory array elements with p0LoadDat(15:0)
-- 	p0LoadDatIndex    allows selection of different load patterns
-- p0LoadDat(15:0)	  port 0 load data 
-- p0Add(7:0)  		  port 0 memory array address 
-- p0DatIn(15:0) 	  port 0 input data 
-- p0Wr 			  Assertion (H) enables synchronous write of addressed port 1 memory
-- P0DatOut(15:0)     port 0 data at p0Add
--
-- p1Load 	 		  port 1 load enable. Assertion synchronously loads all memory array elements with p1LoadDat(15:0)
-- p1LoadDat(15:0)	  port 1 load data 
-- p1RdAdd(7:0)       port 1 memory rd array address 
-- p1WrAdd(7:0)       port 1 memory wr array address 
-- p1DatIn(15:0) 	  port 1 input data 
-- p1Wr 			  Assertion (H) enables synchronous write of addressed port 1 memory
-- P1DatOut(15:0)     port 1 data at p1RdAdd

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.arrayPackage.all; 

entity dualPortRegBlk1024x16WithLoad_2Port is
 Port (  clk		: in  std_logic;    					 
		 rst		: in  std_logic;    					 
         enPort0    : in  std_logic;

		 p0Load	    : in  std_logic;   
		 p0LoadDatIndex  : in  std_logic_vector(1 downto 0);                      
		 p0LoadDat  : in  std_logic_vector(15 downto 0);                      
		 p0Add 	 	: in  std_logic_vector( 9 downto 0);   
		 p0DatIn  	: in  std_logic_vector(15 downto 0);                      
		 p0Wr 	    : in  std_logic;   
	     p0DatOut   : out std_logic_vector(15 downto 0);                      

		 p1Load	    : in  std_logic;   
		 p1LoadDat  : in  std_logic_vector(15 downto 0);                      
		 p1RdAdd 	 	: in  std_logic_vector( 9 downto 0);   
		 p1WrAdd 	 	: in  std_logic_vector( 9 downto 0);   
		 p1DatIn  	: in  std_logic_vector(15 downto 0);                      
		 p1Wr 	    : in  std_logic;   
	     p1DatOut   : out std_logic_vector(15 downto 0)                     
 		 );         
end dualPortRegBlk1024x16WithLoad_2Port;

architecture RTL of dualPortRegBlk1024x16WithLoad_2Port is
signal XX_NS   : array1024x16; -- next state
signal XX_CS   : array1024x16; -- current state 
signal XX_load    : std_logic;   
signal XX_loadDat : std_logic_vector(15 downto 0);                      
signal XX_datIn   : std_logic_vector(15 downto 0);                      
signal XX_add     : std_logic_vector( 9 downto 0);                      
signal XX_wr      : std_logic;   
signal intP0LoadDat : array1024x16;

begin

genwrAddAndDat: process (enPort0,   p0Load, p0LoadDat, p0Add, p0DatIn, p0Wr,     p1Load, p1LoadDat, p1WrAdd, p1DatIn, p1Wr)
begin
  XX_load    <= p1Load; -- defaults to port 1
  XX_loadDat <= p1LoadDat; 
  XX_add     <= p1WrAdd; 
  XX_datIn   <= p1DatIn; 
  XX_wr      <= p1Wr; 
  if enPort0 = '1' then
    XX_load      <= p0Load;     
	XX_loadDat   <= p0LoadDat; 
    XX_add       <= p0Add; 
    XX_datIn     <= p0DatIn; 
    XX_wr        <= p0Wr; 
  end if;
end process;

-- XX_NSDecode_i: process(XX_CS, XX_load, XX_loadDat, XX_wr, XX_add, XX_datIn) 
-- begin
  -- XX_NS    <= XX_CS;                            -- default
  -- if XX_load = '1' then 
     -- XX_NS <= (others => XX_loadDat);              -- load all elements
  -- elsif XX_wr = '1' then
     -- XX_NS(TO_INTEGER(unsigned(XX_add))) <= XX_datIn; -- write addressed element (convert vector to integers, as index)
  -- end if;
-- end process;

-- stateReg_i: process(clk, rst) 
-- begin
 -- if rst = '1' then
    -- XX_CS <= (others => (others => '0')); 
 -- elsif clk'event and clk = '1' then
    -- XX_CS <= XX_NS; 
 -- end if;
-- end process;

XX_NSDecode_i: process(XX_CS, XX_wr, XX_add, XX_datIn) 
begin
  XX_NS    <= XX_CS;                            -- default
  if XX_wr = '1' then
     XX_NS(TO_INTEGER(unsigned(XX_add))) <= XX_datIn; -- write addressed element (convert vector to integers, as index)
  end if;
end process;

process (p0LoadDatIndex)
begin 
 intP0LoadDat <= (others => XX_loadDat); -- default
 if p0LoadDatIndex = "01" then 
   intP0LoadDat <=
        (  0 => X"3608",    1 => X"F40B",    2 => X"0240",    3 => X"04C4",   4 => X"048C",     5 => X"A00B",    6 => X"A20E",    7 => X"F00B",    
		   8 => X"A40F",    9 => X"A60E",   10 => X"AA02",   11 => X"A802",   12 => X"F80F",   13 => X"F800",   14 => X"3081",   15 => X"34C1",   
		  16 => X"3319",   17 => X"069C",   18 => X"3768",   19 => X"F54B",   20 => X"3768",   21 => X"2180",   22 => X"2BC6",   23 => X"2C00",   
		  24 => X"F04B",   25 => X"044C",   26 => X"040C",   27 => X"F700",   28 => X"F702",   29 => X"0488",   30 => X"0448",   31 => X"F620",   
		  32 => X"F622",   33 => X"F205",   34 => X"F203",   35 => X"F200",   36 => X"F204",   37 => X"22D8",   38 => X"24D8",   39 => X"26D8",   
		  40 => X"28FB",   41 => X"2AD3",   42 => X"2EDF",   43 => X"F4CE",   44 => X"641F",   45 => X"6698",   46 => X"FE00",   47 => X"0EC0",   
		  48 => X"0C80",   49 => X"8037",   50 => X"0880",   51 => X"5FD7",   52 => X"0900",   53 => X"5FE7",   54 => X"0000",                  
                                                                                                                               
          55 => X"FA80",   56 => X"9000",                                                                                      
          57 => X"FE00",   58 => X"FE00",   59 => X"FE00",   60 => X"FE00",   61 => X"FE00",   62 => X"FE00",   63 => X"FE00",   64 => X"FE00", 
          65 => X"FE00",   66 => X"FE00",   67 => X"FE00",   68 => X"FE00",   69 => X"FE00",   70 => X"FE00",   71 => X"FE00",   72 => X"FE00", 
          73 => X"FE00",   74 => X"FE00",   75 => X"FE00",   76 => X"FE00",   77 => X"FE00",   78 => X"FE00",   79 => X"FE00",   80 => X"FE00",  
          81 => X"FE00",   82 => X"FE00",   83 => X"FE00",   84 => X"FE00",   85 => X"FE00",   86 => X"FE00",   87 => X"FE00",   88 => X"FE00",  
          89 => X"FE00",   90 => X"FE00",   91 => X"FE00",      
                       
          92 => X"2000",   93 => X"FE00",   94 => X"FE00",   95 => X"FE00",   96 => X"FE00",   97 => X"FE00",   98 => X"FE00",   99 => X"FE00", 
         100 => X"FE00",  101 => X"FE00",  102 => X"FE00",  103 => X"FE00",
               
         104 => X"2000",  105 => X"FE00",  106 => X"FE00",  107 => X"FE00",  108 => X"FE00",  109 => X"FE00",  110 => X"FE00",  111 => X"FE00",
         112 => X"FE00",  113 => X"FE00",  114 => X"FE00",  115 => X"FE00",  
		 
		 116 => X"F603",  117 => X"FB60",  118 => X"6001",  119 => X"6280",  120 => X"6800",  121 => X"FE00",  122 => X"FE00",  123 => X"FE00",	 
		 124 => X"FE00",  125 => X"FE00",  126 => X"FE00",  127 => X"FE00",
		 others => X"0000"		 );
 end if;
end process;

stateReg_i: process(clk, rst) 
begin
 if rst = '1' then
    XX_CS <= (others => (others => '0')); 
 elsif clk'event and clk = '1' then
   if XX_load = '1' then 
     XX_CS <= intP0LoadDat; -- load array
   else
    XX_CS <= XX_NS; 
   end if;
 end if;
end process;


p0DatOut_i: p0DatOut <= XX_CS(TO_INTEGER(unsigned(p0Add))); -- output the addressed port 0 element
p1DatOut_i: p1DatOut <= XX_CS(TO_INTEGER(unsigned(p1RdAdd))); -- output the addressed port 1 element

end RTL;