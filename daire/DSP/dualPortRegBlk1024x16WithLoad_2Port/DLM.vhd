library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity DLM is
	generic (
		-- Users to add parameters here

		-- User parameters ends
		-- Do not modify the parameters beyond this line

		-- Parameters of Axi Slave Bus Interface S00_AXI
		C_S00_AXI_ID_WIDTH	: integer	:= 12;
		C_S00_AXI_DATA_WIDTH	: integer	:= 32;
		C_S00_AXI_ADDR_WIDTH	: integer	:= 11;
		C_S00_AXI_AWUSER_WIDTH	: integer	:= 0;
		C_S00_AXI_ARUSER_WIDTH	: integer	:= 0;
		C_S00_AXI_WUSER_WIDTH	: integer	:= 0;
		C_S00_AXI_RUSER_WIDTH	: integer	:= 0;
		C_S00_AXI_BUSER_WIDTH	: integer	:= 0
	);
	port (
		-- Users to add ports here
			
		-- ports for 6x LEDs (2x RGB 4x general)
		EXTDISPLAYLED : OUT Std_logic_vector(3 downto 0);
		-- Should use PWM for tri-color/RGB LEDs, max 50% duty cycle
		EXTRGBLED : OUT Std_logic_vector(5 downto 0); 
		-- ports for 2x Switches
		EXTSWITCHES : IN std_logic_vector(1 downto 0);
		-- ports for 4x buttons
		EXTBUTTONS	: IN Std_logic_vector(3 downto 0);
		-- User ports ends
		-- Do not modify the ports beyond this line


		-- Ports of Axi Slave Bus Interface S00_AXI
		s00_axi_aclk	: in std_logic;
		s00_axi_aresetn	: in std_logic;
		s00_axi_awid	: in std_logic_vector(C_S00_AXI_ID_WIDTH-1 downto 0);
		s00_axi_awaddr	: in std_logic_vector(C_S00_AXI_ADDR_WIDTH-1 downto 0);
		s00_axi_awlen	: in std_logic_vector(7 downto 0);
		s00_axi_awsize	: in std_logic_vector(2 downto 0);
		s00_axi_awburst	: in std_logic_vector(1 downto 0);
		s00_axi_awlock	: in std_logic;
		s00_axi_awcache	: in std_logic_vector(3 downto 0);
		s00_axi_awprot	: in std_logic_vector(2 downto 0);
		s00_axi_awqos	: in std_logic_vector(3 downto 0);
		s00_axi_awregion	: in std_logic_vector(3 downto 0);
		s00_axi_awuser	: in std_logic_vector(C_S00_AXI_AWUSER_WIDTH-1 downto 0);
		s00_axi_awvalid	: in std_logic;
		s00_axi_awready	: out std_logic;
		s00_axi_wdata	: in std_logic_vector(C_S00_AXI_DATA_WIDTH-1 downto 0);
		s00_axi_wstrb	: in std_logic_vector((C_S00_AXI_DATA_WIDTH/8)-1 downto 0);
		s00_axi_wlast	: in std_logic;
		s00_axi_wuser	: in std_logic_vector(C_S00_AXI_WUSER_WIDTH-1 downto 0);
		s00_axi_wvalid	: in std_logic;
		s00_axi_wready	: out std_logic;
		s00_axi_bid	: out std_logic_vector(C_S00_AXI_ID_WIDTH-1 downto 0);
		s00_axi_bresp	: out std_logic_vector(1 downto 0);
		s00_axi_buser	: out std_logic_vector(C_S00_AXI_BUSER_WIDTH-1 downto 0);
		s00_axi_bvalid	: out std_logic;
		s00_axi_bready	: in std_logic;
		s00_axi_arid	: in std_logic_vector(C_S00_AXI_ID_WIDTH-1 downto 0);
		s00_axi_araddr	: in std_logic_vector(C_S00_AXI_ADDR_WIDTH-1 downto 0);
		s00_axi_arlen	: in std_logic_vector(7 downto 0);
		s00_axi_arsize	: in std_logic_vector(2 downto 0);
		s00_axi_arburst	: in std_logic_vector(1 downto 0);
		s00_axi_arlock	: in std_logic;
		s00_axi_arcache	: in std_logic_vector(3 downto 0);
		s00_axi_arprot	: in std_logic_vector(2 downto 0);
		s00_axi_arqos	: in std_logic_vector(3 downto 0);
		s00_axi_arregion	: in std_logic_vector(3 downto 0);
		s00_axi_aruser	: in std_logic_vector(C_S00_AXI_ARUSER_WIDTH-1 downto 0);
		s00_axi_arvalid	: in std_logic;
		s00_axi_arready	: out std_logic;
		s00_axi_rid	: out std_logic_vector(C_S00_AXI_ID_WIDTH-1 downto 0);
		s00_axi_rdata	: out std_logic_vector(C_S00_AXI_DATA_WIDTH-1 downto 0);
		s00_axi_rresp	: out std_logic_vector(1 downto 0);
		s00_axi_rlast	: out std_logic;
		s00_axi_ruser	: out std_logic_vector(C_S00_AXI_RUSER_WIDTH-1 downto 0);
		s00_axi_rvalid	: out std_logic;
		s00_axi_rready	: in std_logic
	);
end DLM;

architecture arch_imp of DLM is

	signal UserClk : std_logic;
	signal UserRst : std_logic;
	signal Switches : std_logic_vector(1 downto 0);
	signal Buttons 	: std_logic_vector(3 downto 0);
	
	signal FULL_DisplayLEDs : std_logic_vector(7 downto 0);
	alias  DisplayLEDs 	: std_logic_vector is FULL_DisplayLEDs(3 downto 0);
	signal RGBLEDs		: std_logic_vector(5 downto 0);
	signal UserControl	: std_logic_vector(511 downto 0);
	signal SysProbe		: std_logic_vector(4095 downto 0);
	
	signal FULL_BRAM0ADDRESS : Std_logic_vector(24 downto 0);
	alias  BRAM0ADDRESS : Std_logic_vector is FULL_BRAM0ADDRESS(13 downto 0);
	signal BRAM0WDATA 	: Std_logic_vector(31 downto 0);
	signal BRAM0WRITE 	: Std_logic;
	signal BRAM0READ 	: Std_logic; --do we need custom read signal handling?
	signal BRAM0RDATA 	: Std_logic_vector(31 downto 0);
	
	signal FULL_BRAM1ADDRESS : Std_logic_vector(24 downto 0);
	alias  BRAM1ADDRESS : Std_logic_vector is FULL_BRAM1ADDRESS(13 downto 0);
	signal BRAM1WDATA 	: Std_logic_vector(31 downto 0);
	signal BRAM1WRITE 	: Std_logic;
	signal BRAM1READ 	: Std_logic; --do we need custom read signal handling?
	signal BRAM1RDATA 	: Std_logic_vector(31 downto 0);
	

	-- component declaration
	component DLM_core is
		generic (
		C_S_AXI_ID_WIDTH	: integer	:= 1;
		C_S_AXI_DATA_WIDTH	: integer	:= 32;
		C_S_AXI_ADDR_WIDTH	: integer	:= 11;
		C_S_AXI_AWUSER_WIDTH	: integer	:= 0;
		C_S_AXI_ARUSER_WIDTH	: integer	:= 0;
		C_S_AXI_WUSER_WIDTH	: integer	:= 0;
		C_S_AXI_RUSER_WIDTH	: integer	:= 0;
		C_S_AXI_BUSER_WIDTH	: integer	:= 0
		);
		port (
		variableInMemory : out std_logic_vector(511 downto 0);
		variableOutMemory : in std_logic_vector(4095 downto 0);
		VL_UserClk : out std_logic;
		VL_UserRst : out std_logic;

		BRAM0ADDRESS : in Std_logic_vector(13 downto 0);
		BRAM0WDATA 	: in Std_logic_vector(31 downto 0);
		BRAM0WRITE 	: in Std_logic;
		BRAM0READ 	: in Std_logic; 				
		BRAM0RDATA 	: out Std_logic_vector(31 downto 0);
		
		BRAM1ADDRESS : in Std_logic_vector(13 downto 0);
		BRAM1WDATA 	: in Std_logic_vector(31 downto 0);
		BRAM1WRITE 	: in Std_logic;
		BRAM1READ 	: in Std_logic;
		BRAM1RDATA 	: out Std_logic_vector(31 downto 0);
														
		DISPLAYLED 		: IN Std_logic_vector(3 downto 0);
		EXTDISPLAYLED 	: OUT Std_logic_vector(3 downto 0);
		RGBLED 		: in Std_logic_vector(5 downto 0); 
		EXTRGBLED 	: OUT Std_logic_vector(5 downto 0); 
		EXTSWITCHES : IN std_logic_vector(1 downto 0);
		SWITCHES 	: OUT std_logic_vector(1 downto 0);
		EXTBUTTONS	: IN Std_logic_vector(3 downto 0);
		BUTTONS 	: OUT Std_logic_vector(3 downto 0);


		S_AXI_ACLK	: in std_logic;
		S_AXI_ARESETN	: in std_logic;
		S_AXI_AWID	: in std_logic_vector(C_S_AXI_ID_WIDTH-1 downto 0);
		S_AXI_AWADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		S_AXI_AWLEN	: in std_logic_vector(7 downto 0);
		S_AXI_AWSIZE	: in std_logic_vector(2 downto 0);
		S_AXI_AWBURST	: in std_logic_vector(1 downto 0);
		S_AXI_AWLOCK	: in std_logic;
		S_AXI_AWCACHE	: in std_logic_vector(3 downto 0);
		S_AXI_AWPROT	: in std_logic_vector(2 downto 0);
		S_AXI_AWQOS	: in std_logic_vector(3 downto 0);
		S_AXI_AWREGION	: in std_logic_vector(3 downto 0);
		S_AXI_AWVALID	: in std_logic;
		S_AXI_AWREADY	: out std_logic;
		S_AXI_WDATA	: in std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		S_AXI_WSTRB	: in std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0);
		S_AXI_WLAST	: in std_logic;
		S_AXI_WVALID	: in std_logic;
		S_AXI_WREADY	: out std_logic;
		S_AXI_BID	: out std_logic_vector(C_S_AXI_ID_WIDTH-1 downto 0);
		S_AXI_BRESP	: out std_logic_vector(1 downto 0);
		S_AXI_BVALID	: out std_logic;
		S_AXI_BREADY	: in std_logic;
		S_AXI_ARID	: in std_logic_vector(C_S_AXI_ID_WIDTH-1 downto 0);
		S_AXI_ARADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		S_AXI_ARLEN	: in std_logic_vector(7 downto 0);
		S_AXI_ARSIZE	: in std_logic_vector(2 downto 0);
		S_AXI_ARBURST	: in std_logic_vector(1 downto 0);
		S_AXI_ARLOCK	: in std_logic;
		S_AXI_ARCACHE	: in std_logic_vector(3 downto 0);
		S_AXI_ARPROT	: in std_logic_vector(2 downto 0);
		S_AXI_ARQOS	: in std_logic_vector(3 downto 0);
		S_AXI_ARREGION	: in std_logic_vector(3 downto 0);
		S_AXI_ARVALID	: in std_logic;
		S_AXI_ARREADY	: out std_logic;
		S_AXI_RID	: out std_logic_vector(C_S_AXI_ID_WIDTH-1 downto 0);
		S_AXI_RDATA	: out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		S_AXI_RRESP	: out std_logic_vector(1 downto 0);
		S_AXI_RLAST	: out std_logic;
		S_AXI_RVALID	: out std_logic;
		S_AXI_RREADY	: in std_logic
	);
	end component DLM_core;

--BEGIN DLM USERLOGIC DECLARATION--
component DLM_UserLogic is
	port(

        VL_UserClk              : IN  Std_logic;

        VL_UserRst              : IN  Std_logic;

        --

        -- The display interface.

        --

        VL_DisplayDigit3        : OUT Std_logic_vector(7 downto 0);

        VL_DisplayDigit2        : OUT Std_logic_vector(7 downto 0);

        VL_DisplayDigit1        : OUT Std_logic_vector(7 downto 0);

        VL_DisplayDigit0        : OUT Std_logic_vector(7 downto 0);

        VL_DisplayLED           : OUT Std_logic_vector(7 downto 0);

        --

        -- User control, status and probe.

        --

        VL_LogicAnalyser        : OUT Std_logic_vector(31 downto 0);

        VL_SysProbe             : OUT Std_logic_vector(4095 downto 0);

        VL_UserControl          : IN  Std_logic_vector(511 downto 0);

        VL_PatternGen           : IN  Std_logic_vector(7 downto 0);

        --

        -- General purpose I/O.

        --

        VL_Switches : IN  Std_logic_vector(7 downto 0);

        VL_Buttons  : IN  Std_logic_vector(4 downto 0);

        VL_GPIn     : IN  Std_logic_vector(31 downto 0);

        VL_GPOut    : OUT Std_logic_vector(31 downto 0);

        --

        -- The Cellular Ram interface

        --

        VL_CellRamAddress       : OUT Std_logic_vector(22 downto 0);

        VL_CellRamWData         : OUT Std_logic_vector(15 downto 0);

        VL_CellRamRData         : IN  Std_logic_vector(15 downto 0);

        VL_CellRamWrite         : OUT Std_logic;

        VL_CellRamRead          : OUT Std_logic;

        VL_CellRamAck           : IN  Std_logic;

        VL_CellRamOeL           : OUT Std_logic;

        VL_CellRamWeL           : OUT Std_logic;

        VL_CellRamClk           : OUT Std_logic;

        VL_CellRamAdvL          : OUT Std_logic;

        VL_CellRamWait          : IN  Std_logic;

        VL_CellRamMtCeL         : OUT Std_logic;

        VL_CellRamMtUbL         : OUT Std_logic;

        VL_CellRamMtLbL         : OUT Std_logic;

        VL_CellRamMtCre         : OUT Std_logic;

        --

        -- The BRAM0 interface

        --

        VL_BRam0Address          : OUT Std_logic_vector(24 downto 0);

        VL_BRam0WData            : OUT Std_logic_vector(31 downto 0);

        VL_BRam0RData            : IN  Std_logic_vector(31 downto 0);

        VL_BRam0Write            : OUT Std_logic;

        VL_BRam0Read             : OUT Std_logic;

        VL_BRam0Ack              : IN  Std_logic;

        --

        -- The BRAM1 interface

        --

        VL_BRam1Address         : OUT Std_logic_vector(24 downto 0);

        VL_BRam1WData           : OUT Std_logic_vector(31 downto 0);

        VL_BRam1RData           : IN  Std_logic_vector(31 downto 0);

        VL_BRam1Write           : OUT Std_logic;

        VL_BRam1Read            : OUT Std_logic;

        VL_BRam1Ack             : IN  Std_logic);



end component;
--END DLM_USERLOGIC DECLARATION--

begin
-- Instantiation of DLM_core with AXI interface
DLM_core_inst : DLM_core
	generic map (
		C_S_AXI_ID_WIDTH	=> C_S00_AXI_ID_WIDTH,
		C_S_AXI_DATA_WIDTH	=> C_S00_AXI_DATA_WIDTH,
		C_S_AXI_ADDR_WIDTH	=> C_S00_AXI_ADDR_WIDTH,
		C_S_AXI_AWUSER_WIDTH	=> C_S00_AXI_AWUSER_WIDTH,
		C_S_AXI_ARUSER_WIDTH	=> C_S00_AXI_ARUSER_WIDTH,
		C_S_AXI_WUSER_WIDTH	=> C_S00_AXI_WUSER_WIDTH,
		C_S_AXI_RUSER_WIDTH	=> C_S00_AXI_RUSER_WIDTH,
		C_S_AXI_BUSER_WIDTH	=> C_S00_AXI_BUSER_WIDTH
	)
	port map (
		-- Register for inputs to User Design
		variableInMemory => UserControl,--: out std_logic_vector(511 downto 0); --less as first 32 is clock?
		-- Register for outputs from User Design
		variableOutMemory => SysProbe,--: in std_logic_vector(4095 downto 0);
		-- User Clock signal
		VL_UserClk => UserClk,
		-- User Reset signal
		VL_UserRst => UserRst,
	
		BRAM0ADDRESS => BRAM0ADDRESS,
		-- BRAM0 32bit data for write
		BRAM0WDATA => BRAM0WDATA,
		-- BRAM0 write enable
		BRAM0WRITE	=>  BRAM0WRITE,
		-- BRAM0 read enable
		BRAM0READ => BRAM0READ,
		-- BRAM0 32bit read data
		BRAM0RDATA => BRAM0RDATA,
		
		BRAM1ADDRESS => BRAM1ADDRESS,
		-- BRAM1 32bit data for write
		BRAM1WDATA => BRAM1WDATA,
		-- BRAM1 write enable
		BRAM1WRITE	=>  BRAM1WRITE,
		-- BRAM1 read enable
		BRAM1READ => BRAM1READ,
		-- BRAM1 32bit read data
		BRAM1RDATA => BRAM1RDATA,
			
		-- ports for 6x LEDs (2x RGB 4x general)
		DISPLAYLED => DisplayLEDs,
		EXTDISPLAYLED => EXTDISPLAYLED,
		-- Should use PWM for tri-color/RGB LEDs, max 50% duty cycle
		RGBLED => RGBLEDs,
		EXTRGBLED => EXTRGBLED,
		-- ports for 2x Switches
		EXTSWITCHES => EXTSWITCHES,
		SWITCHES => Switches,
		-- ports for 4x buttons
		EXTBUTTONS => EXTBUTTONS,
		BUTTONS => Buttons,
	
		S_AXI_ACLK	=> s00_axi_aclk,
		S_AXI_ARESETN	=> s00_axi_aresetn,
		S_AXI_AWID	=> s00_axi_awid,
		S_AXI_AWADDR	=> s00_axi_awaddr,
		S_AXI_AWLEN	=> s00_axi_awlen,
		S_AXI_AWSIZE	=> s00_axi_awsize,
		S_AXI_AWBURST	=> s00_axi_awburst,
		S_AXI_AWLOCK	=> s00_axi_awlock,
		S_AXI_AWCACHE	=> s00_axi_awcache,
		S_AXI_AWPROT	=> s00_axi_awprot,
		S_AXI_AWQOS	=> s00_axi_awqos,
		S_AXI_AWREGION	=> s00_axi_awregion,
		--S_AXI_AWUSER	=> s00_axi_awuser,
		S_AXI_AWVALID	=> s00_axi_awvalid,
		S_AXI_AWREADY	=> s00_axi_awready,
		S_AXI_WDATA	=> s00_axi_wdata,
		S_AXI_WSTRB	=> s00_axi_wstrb,
		S_AXI_WLAST	=> s00_axi_wlast,
		--S_AXI_WUSER	=> s00_axi_wuser,
		S_AXI_WVALID	=> s00_axi_wvalid,
		S_AXI_WREADY	=> s00_axi_wready,
		S_AXI_BID	=> s00_axi_bid,
		S_AXI_BRESP	=> s00_axi_bresp,
		--S_AXI_BUSER	=> s00_axi_buser,
		S_AXI_BVALID	=> s00_axi_bvalid,
		S_AXI_BREADY	=> s00_axi_bready,
		S_AXI_ARID	=> s00_axi_arid,
		S_AXI_ARADDR	=> s00_axi_araddr,
		S_AXI_ARLEN	=> s00_axi_arlen,
		S_AXI_ARSIZE	=> s00_axi_arsize,
		S_AXI_ARBURST	=> s00_axi_arburst,
		S_AXI_ARLOCK	=> s00_axi_arlock,
		S_AXI_ARCACHE	=> s00_axi_arcache,
		S_AXI_ARPROT	=> s00_axi_arprot,
		S_AXI_ARQOS	=> s00_axi_arqos,
		S_AXI_ARREGION	=> s00_axi_arregion,
		--S_AXI_ARUSER	=> s00_axi_aruser,
		S_AXI_ARVALID	=> s00_axi_arvalid,
		S_AXI_ARREADY	=> s00_axi_arready,
		S_AXI_RID	=> s00_axi_rid,
		S_AXI_RDATA	=> s00_axi_rdata,
		S_AXI_RRESP	=> s00_axi_rresp,
		S_AXI_RLAST	=> s00_axi_rlast,
		--S_AXI_RUSER	=> s00_axi_ruser,
		S_AXI_RVALID	=> s00_axi_rvalid,
		S_AXI_RREADY	=> s00_axi_rready
	);

--BEGIN DLM_USERLOGIC INSTANTIATION--
DLM_Userlogic_inst: DLM_UserLogic
	port map (
-- FM        VL_UserClk => UserClk,
		VL_UserClk => s00_axi_aclk,
        VL_UserRst => UserRst,

        VL_DisplayDigit3 => open,
        VL_DisplayDigit2 => open,
        VL_DisplayDigit1 => open,
        VL_DisplayDigit0 => open,

        VL_DisplayLED => FULL_DisplayLEDs,
		
        VL_LogicAnalyser => open,
        VL_SysProbe      => SysProbe,
        VL_UserControl   => UserControl,
        VL_PatternGen    => (others => '0'),

        VL_Switches(1 downto 0) => Switches,
		VL_Switches(7 downto 2) => "000000",
        VL_Buttons(3 downto 0) => Buttons,
		VL_Buttons(4) => '0',
        VL_GPIn     => (others => '0'),
        VL_GPOut    => open,

        VL_CellRamAddress       => open,
        VL_CellRamWData         => open,
        VL_CellRamRData         => (others => '0'),
        VL_CellRamWrite         => open,
        VL_CellRamRead          => open,
        VL_CellRamAck           => '0',
        VL_CellRamOeL           => open,
        VL_CellRamWeL           => open,
        VL_CellRamClk           => open,
        VL_CellRamAdvL          => open,
        VL_CellRamWait          => '0',
        VL_CellRamMtCeL         => open,
        VL_CellRamMtUbL         => open,

        VL_CellRamMtLbL         => open,

        VL_CellRamMtCre         => open,

        --

        -- The BRAM0 interface

        --

        VL_BRam0Address => FULL_BRAM0ADDRESS,

        VL_BRam0WData            => BRAM0WDATA,

        VL_BRam0RData            => BRAM0RDATA,

        VL_BRam0Write            => BRAM0WRITE,

        VL_BRam0Read             => BRAM0READ,

        VL_BRam0Ack              => '0',

        --

        -- The BRAM1 interface

        --

        VL_BRam1Address => FULL_BRAM1ADDRESS,

        VL_BRam1WData            => BRAM1WDATA,

        VL_BRam1RData            => BRAM1RDATA,

        VL_BRam1Write            => BRAM1WRITE,

        VL_BRam1Read             => BRAM1READ,

        VL_BRam1Ack              => '0'
	);
--END DLM_USERLOGIC INSTANTIATION--

end arch_imp;
