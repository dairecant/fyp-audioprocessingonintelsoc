viciLab folder stucture 


spec
1. 	design
	Documentation related to the application design
2.	drawings
		visio 	source
		png 	png format drawings
3. 	test spec


vhdl folder
1. 	HDLmodel
		HDL models 
		HDL testbench 
2. 	xilinxprj
		Vivado project	
		Simulation folder includes the Vivado waveform configuration file
3. 	intelprj
		Quartus project	


verilog folder
1. 	HDL models 
2. 	HDL testbench 



vicilabprj folder
1. 	cfg folder
	FPGA/SoC .bit file and json file (extracted from the vicilab build)
	Does not require the other vicilab build files.

2. 	GUI
	viciLab Graphical User Interface (GUI)-related files, e.g, 
	apps 	GUI application-specific python files in apps folder
	icons 	application-specific widget icons (icons folder)
			When icons become generic icons, move generic icons to vicilab viciClient/icons folder
	GUI 	vicilab GUI metatdata file
	
3. 	macro
	Folder stores viciLab macros, used to automate application of stimulus to the FPGA design 
	Refer to http://vicilogic.com/static/download/viciLabMacro/viciLabMacro_Template.txt
	
4. 	prj
	viciLab project files (.vlw, vla, vll)
