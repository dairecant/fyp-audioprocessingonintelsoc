-- Description: DSPTop
-- Feb 2018, Fearghal Morgan

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.DSPPackage.all;

entity DSPTop is
Port (
-- line auto generated [2018-03-18] DSPTop
oo_singleShot_i_NS :	out std_logic_vector(0 downto 0);

-- line auto generated [2018-03-18] DSPTop
oo_singleShot_i_CS :	out std_logic_vector(0 downto 0);

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_dataIn_signed :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_validPipe12_1 :	out std_logic;

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_validIn :	out std_logic;

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_coeff15 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_converterOut :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_mulOutput_31 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_sum4_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_sum6_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_sum0_1 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_coeff14 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_coeff10 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_coeff11 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_coeff12 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_coeff13 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_sum13_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_sum8_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_validPipe1_1 :	out std_logic;

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_validPipe15_1 :	out std_logic;

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_sum15_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_validPipe13_1 :	out std_logic;

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_validPipe9_1 :	out std_logic;

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_coeff2 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_mulOutput_8 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_mulOutput_9 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_mulOutput_4 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_mulOutput_5 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_mulOutput_6 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_mulOutput_7 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_mulOutput_1 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_mulOutput_2 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_mulOutput_3 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_validPipe3_1 :	out std_logic;

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_sum2_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_validPipe5_1 :	out std_logic;

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_sum5_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_mulOutput_21 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_coeff16 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_validPipe7_1 :	out std_logic;

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_validPipe11_1 :	out std_logic;

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_validPipe0_1 :	out std_logic;

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_sum7_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_sum11_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_sum9_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_validPipe14_1 :	out std_logic;

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_sum12_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_dataOut_tmp :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_validPipe8_1 :	out std_logic;

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_sum1_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_mulOutput :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_validPipe4_1 :	out std_logic;

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_sum3_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_mulOutput_30 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_validPipe10_1 :	out std_logic;

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_sum14_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_validPipe6_1 :	out std_logic;

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_final_sumPipe_1 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_sum10_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_final_sumValid :	out std_logic;

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_mulOutput_29 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_mulOutput_28 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_mulOutput_25 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_mulOutput_24 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_mulOutput_27 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_mulOutput_20 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_mulOutput_23 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_mulOutput_22 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_mulOutput_26 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_coeff4 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_coeff5 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_coeff6 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_coeff7 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_coeff0 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_coeff1 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_validPipe2_1 :	out std_logic;

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_coeff3 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_coeff8 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_coeff9 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_mulOutput_18 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_mulOutput_19 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_mulOutput_10 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_mulOutput_11 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_mulOutput_12 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_mulOutput_13 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_mulOutput_14 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_mulOutput_15 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_mulOutput_16 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_DSPFunct_i_effect0_i_mulOutput_17 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_CSRdAdd :	out std_logic_vector(9 downto 0);

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_CSWrAdd :	out std_logic_vector(9 downto 0);

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_NSWrAdd :	out std_logic_vector(9 downto 0);

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_NSRdAdd :	out std_logic_vector(9 downto 0);

-- line auto generated [2018-03-18] DSPTop
oo_DSPCtrlr_i_validOut :	out std_logic;

-- line auto generated [2018-03-18] DSPTop
oo_DSPDatToMem :	out std_logic_vector(15 downto 0);

-- line auto generated [2018-03-18] DSPTop
oo_DSPMemDone :	out std_logic;

-- line auto generated [2018-03-18] DSPTop
oo_DSPMemWr :	out std_logic;

-- line auto generated [2018-03-18] DSPTop
oo_DSPMemRdAdd :	out std_logic_vector(9 downto 0);

-- line auto generated [2018-03-18] DSPTop
oo_DSPMemWrAdd :	out std_logic_vector(9 downto 0);

-- line auto generated [2018-03-18] DSPTop
oo_DSPDatFromMem :	out std_logic_vector(15 downto 0);

-- line auto generated [2018-03-18] DSPTop
oo_goDSPPulse :	out std_logic;
 clk :        		 in std_logic;						-- system clock strobe
       rst :        		 in std_logic; 						-- asynchronous system reset, asserted

       goDSP :				 in  std_logic;
       DSPDone : 			 out std_logic;
	   
       selFirstEffect: in std_logic_vector(2 downto 0);  
	   selFinalEffect: in std_logic_vector(1 downto 0);  
	   ctrl0_0:        in std_logic_vector(15 downto 0);  
	   ctrl0_1:        in std_logic_vector(15 downto 0);  
	   ctrl1_0:        in std_logic_vector(15 downto 0);  
	   ctrl1_1:        in std_logic_vector(15 downto 0);  
	   ctrl2_0:        in std_logic_vector(15 downto 0);  
	   ctrl2_1:        in std_logic_vector(15 downto 0);  
	   ctrl3_0:        in std_logic_vector(15 downto 0);  
	   ctrl3_1:        in std_logic_vector(15 downto 0);  
	   
       host_EnDSPMem :     	 in  std_logic;
	   host_DSPMem_Load :    in  std_logic;                      
	   host_DSPMem_LoadDat : in  std_logic_vector(15 downto 0); 
	   host_DSPMem_Add :     in  std_logic_vector(9 downto 0);  
	   host_DSPMem_DatIn :   in  std_logic_vector(15 downto 0);  
	   host_DSPMem_Wr :      in  std_logic;                      
	   host_DSPMem_DatOut :  out std_logic_vector(15 downto 0)
--	   host_DSPMem_DatArrayOut : out std_logic_vector(511 downto 0)  
      );
end DSPTop;

architecture struct of DSPTop is                 

component singleShot is
Port (
-- line auto generated [2018-03-18] singleShot_i
oo_NS :	out std_logic_vector(0 downto 0);

-- line auto generated [2018-03-18] singleShot_i
oo_CS :	out std_logic_vector(0 downto 0);
clk   : 	in 	std_logic;
      rst   : 	in 	std_logic;
      sw    : 	in 	std_logic; 
      aShot :   out std_logic  
	 ); 
end component;

component dualPortRegBlk1024x16WithLoad is
 Port (  clk		: in  std_logic;    					 
		 rst		: in  std_logic;    					 
         enPort0    : in  std_logic;

		 p0Load	    : in  std_logic;   
		 p0LoadDat  : in  std_logic_vector(15 downto 0);                      
		 p0Add 	 	: in  std_logic_vector( 9 downto 0);   
		 p0DatIn  	: in  std_logic_vector(15 downto 0);                      
		 p0Wr 	    : in  std_logic;   
	     p0DatOut   : out std_logic_vector(15 downto 0);                      
--		 p0DatArrayOut : out std_logic_vector(511 downto 0);

		 p1Load	    : in  std_logic;   
		 p1LoadDat  : in  std_logic_vector(15 downto 0);                      
		 p1RdAdd 	 	: in  std_logic_vector( 9 downto 0);   
		 p1WrAdd 	 	: in  std_logic_vector( 9 downto 0);   
		 p1DatIn  	: in  std_logic_vector(15 downto 0);                      
		 p1Wr 	    : in  std_logic;   
	     p1DatOut   : out std_logic_vector(15 downto 0)                     
 		 );         
end component;

component DSPCtrlr is
port (
-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_dataIn_signed :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_validPipe12_1 :	out std_logic;

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_validIn :	out std_logic;

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_coeff15 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_converterOut :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_31 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_sum4_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_sum6_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_sum0_1 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_coeff14 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_coeff10 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_coeff11 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_coeff12 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_coeff13 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_sum13_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_sum8_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_validPipe1_1 :	out std_logic;

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_validPipe15_1 :	out std_logic;

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_sum15_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_validPipe13_1 :	out std_logic;

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_validPipe9_1 :	out std_logic;

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_coeff2 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_8 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_9 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_4 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_5 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_6 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_7 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_1 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_2 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_3 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_validPipe3_1 :	out std_logic;

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_sum2_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_validPipe5_1 :	out std_logic;

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_sum5_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_21 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_coeff16 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_validPipe7_1 :	out std_logic;

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_validPipe11_1 :	out std_logic;

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_validPipe0_1 :	out std_logic;

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_sum7_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_sum11_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_sum9_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_validPipe14_1 :	out std_logic;

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_sum12_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_dataOut_tmp :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_validPipe8_1 :	out std_logic;

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_sum1_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_validPipe4_1 :	out std_logic;

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_sum3_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_30 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_validPipe10_1 :	out std_logic;

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_sum14_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_validPipe6_1 :	out std_logic;

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_final_sumPipe_1 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_sum10_2 :	out std_logic_vector( 32 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_final_sumValid :	out std_logic;

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_29 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_28 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_25 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_24 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_27 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_20 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_23 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_22 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_26 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_coeff4 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_coeff5 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_coeff6 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_coeff7 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_coeff0 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_coeff1 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_validPipe2_1 :	out std_logic;

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_coeff3 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_coeff8 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_coeff9 :	out std_logic_vector( 15 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_18 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_19 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_10 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_11 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_12 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_13 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_14 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_15 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_16 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_17 :	out std_logic_vector( 31 DOWNTO 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_CSRdAdd :	out std_logic_vector( 9 downto 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_CSWrAdd :	out std_logic_vector( 9 downto 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_NSWrAdd :	out std_logic_vector( 9 downto 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_NSRdAdd :	out std_logic_vector( 9 downto 0 );

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_validOut :	out std_logic;
clk :           in std_logic; 
      rst :        	  in std_logic; 		
      goDSPPulse:     in std_logic;
      selFirstEffect: in std_logic_vector(2 downto 0);  
	  selFinalEffect: in std_logic_vector(1 downto 0);  
	  ctrl0_0:        in std_logic_vector(15 downto 0);  
	  ctrl0_1:        in std_logic_vector(15 downto 0);  
	  ctrl1_0:        in std_logic_vector(15 downto 0);  
	  ctrl1_1:        in std_logic_vector(15 downto 0);  
	  ctrl2_0:        in std_logic_vector(15 downto 0);  
	  ctrl2_1:        in std_logic_vector(15 downto 0);  
	  ctrl3_0:        in std_logic_vector(15 downto 0);  
	  ctrl3_1:        in std_logic_vector(15 downto 0);  
      DSPDatFromMem:  in std_logic_vector(15 downto 0);  
      DSPMemRdAdd:      out std_logic_vector(9 downto 0);
      DSPMemWrAdd:      out std_logic_vector(9 downto 0);
      DSPDatToMem:    out std_logic_vector(15 downto 0);
      DSPMemWr:       out std_logic;
      DSPDone:        out std_logic
     );
end component;

signal  goDSPPulse:     std_logic;
signal  DSPDatFromMem:  std_logic_vector(15 downto 0);  
signal  DSPMemRdAdd:    std_logic_vector(9 downto 0);
signal  DSPMemWrAdd:    std_logic_vector(9 downto 0);
signal  DSPDatToMem:    std_logic_vector(15 downto 0);
signal  DSPMemWr:       std_logic;
signal  DSPMemDone:     std_logic;

begin 
-- line auto generated [2018-03-18] DSPTop
oo_DSPDatToMem <= DSPDatToMem;
-- line auto generated [2018-03-18] DSPTop
oo_DSPMemDone <= DSPMemDone;
-- line auto generated [2018-03-18] DSPTop
oo_DSPMemWr <= DSPMemWr;
-- line auto generated [2018-03-18] DSPTop
oo_DSPMemRdAdd <= DSPMemRdAdd;
-- line auto generated [2018-03-18] DSPTop
oo_DSPMemWrAdd <= DSPMemWrAdd;
-- line auto generated [2018-03-18] DSPTop
oo_DSPDatFromMem <= DSPDatFromMem;
-- line auto generated [2018-03-18] DSPTop
oo_goDSPPulse <= goDSPPulse;

singleShot_i: singleShot 
port map (
-- line auto generated [2018-03-18] singleShot_i
oo_NS => oo_singleShot_i_NS,

-- line auto generated [2018-03-18] singleShot_i
oo_CS => oo_singleShot_i_CS,
clk   => clk,
          rst   => rst,
          sw    => goDSP,
          aShot => goDSPPulse
	      ); 
	 		 
DSPCtrlr_i: DSPCtrlr
port map (
-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_dataIn_signed => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_dataIn_signed,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_validPipe12_1 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_validPipe12_1,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_validIn => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_validIn,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_coeff15 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_coeff15,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_converterOut => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_converterOut,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_31 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_mulOutput_31,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_sum4_2 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_sum4_2,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_sum6_2 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_sum6_2,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_sum0_1 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_sum0_1,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_coeff14 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_coeff14,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_coeff10 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_coeff10,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_coeff11 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_coeff11,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_coeff12 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_coeff12,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_coeff13 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_coeff13,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_sum13_2 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_sum13_2,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_sum8_2 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_sum8_2,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_validPipe1_1 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_validPipe1_1,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_validPipe15_1 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_validPipe15_1,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_sum15_2 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_sum15_2,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_validPipe13_1 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_validPipe13_1,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_validPipe9_1 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_validPipe9_1,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_coeff2 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_coeff2,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_8 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_mulOutput_8,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_9 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_mulOutput_9,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_4 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_mulOutput_4,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_5 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_mulOutput_5,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_6 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_mulOutput_6,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_7 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_mulOutput_7,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_1 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_mulOutput_1,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_2 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_mulOutput_2,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_3 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_mulOutput_3,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_validPipe3_1 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_validPipe3_1,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_sum2_2 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_sum2_2,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_validPipe5_1 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_validPipe5_1,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_sum5_2 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_sum5_2,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_21 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_mulOutput_21,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_coeff16 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_coeff16,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_validPipe7_1 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_validPipe7_1,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_validPipe11_1 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_validPipe11_1,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_validPipe0_1 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_validPipe0_1,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_sum7_2 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_sum7_2,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_sum11_2 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_sum11_2,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_sum9_2 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_sum9_2,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_validPipe14_1 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_validPipe14_1,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_sum12_2 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_sum12_2,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_dataOut_tmp => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_dataOut_tmp,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_validPipe8_1 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_validPipe8_1,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_sum1_2 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_sum1_2,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_mulOutput,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_validPipe4_1 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_validPipe4_1,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_sum3_2 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_sum3_2,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_30 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_mulOutput_30,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_validPipe10_1 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_validPipe10_1,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_sum14_2 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_sum14_2,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_validPipe6_1 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_validPipe6_1,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_final_sumPipe_1 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_final_sumPipe_1,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_sum10_2 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_sum10_2,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_final_sumValid => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_final_sumValid,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_29 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_mulOutput_29,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_28 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_mulOutput_28,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_25 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_mulOutput_25,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_24 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_mulOutput_24,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_27 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_mulOutput_27,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_20 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_mulOutput_20,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_23 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_mulOutput_23,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_22 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_mulOutput_22,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_26 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_mulOutput_26,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_coeff4 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_coeff4,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_coeff5 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_coeff5,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_coeff6 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_coeff6,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_coeff7 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_coeff7,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_coeff0 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_coeff0,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_coeff1 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_coeff1,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_validPipe2_1 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_validPipe2_1,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_coeff3 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_coeff3,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_coeff8 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_coeff8,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_coeff9 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_coeff9,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_18 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_mulOutput_18,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_19 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_mulOutput_19,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_10 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_mulOutput_10,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_11 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_mulOutput_11,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_12 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_mulOutput_12,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_13 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_mulOutput_13,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_14 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_mulOutput_14,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_15 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_mulOutput_15,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_16 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_mulOutput_16,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_DSPFunct_i_effect0_i_mulOutput_17 => oo_DSPCtrlr_i_DSPFunct_i_effect0_i_mulOutput_17,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_CSRdAdd => oo_DSPCtrlr_i_CSRdAdd,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_CSWrAdd => oo_DSPCtrlr_i_CSWrAdd,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_NSWrAdd => oo_DSPCtrlr_i_NSWrAdd,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_NSRdAdd => oo_DSPCtrlr_i_NSRdAdd,

-- line auto generated [2018-03-18] DSPCtrlr_i
oo_validOut => oo_DSPCtrlr_i_validOut,
clk  		    => clk,  		
          rst 			=> rst, 			
          goDSPPulse	=> goDSPPulse,	
          selFirstEffect=> selFirstEffect,
	      selFinalEffect=> selFinalEffect, 
	      ctrl0_0		=> ctrl0_0,		 
	      ctrl0_1		=> ctrl0_1,		 
	      ctrl1_0		=> ctrl1_0,		 
	      ctrl1_1		=> ctrl1_1,		 
	      ctrl2_0		=> ctrl2_0,		 
	      ctrl2_1		=> ctrl2_1,		 
	      ctrl3_0		=> ctrl3_0,		 
	      ctrl3_1		=> ctrl3_1,		   
          DSPDatFromMem => DSPDatFromMem,
          DSPMemRdAdd	=> DSPMemRdAdd,	
          DSPMemWrAdd	=> DSPMemWrAdd,	
          DSPDatToMem   => DSPDatToMem,	
          DSPMemWr		=> DSPMemWr,		
          DSPDone	    => DSPDone	
	      );            
		  		  
DSPMem_i: dualPortRegBlk1024x16WithLoad
Port map (clk		=> clk,
		 rst		=> '0',
         enPort0    => host_EnDSPMem,
		 p0Load	    => host_DSPMem_Load,
		 p0LoadDat  => host_DSPMem_LoadDat,                     
		 p0Add 	 	=> host_DSPMem_Add,  
		 p0DatIn  	=> host_DSPMem_DatIn,                     
		 p0Wr 	    => host_DSPMem_Wr,
	     p0DatOut   => host_DSPMem_DatOut,                     
--		 p0DatArrayOut => host_DSPMem_DatArrayOut,
                    
		 p1Load	    => '0', 
		 p1LoadDat  => (others => '0'),                     
		 p1RdAdd 	=> DSPMemRdAdd,  
		 p1WrAdd 	=> DSPMemWrAdd,  
		 p1DatIn  	=> DSPDatToMem,                     
		 p1Wr 	    => DSPMemWr,
	     p1DatOut   => DSPDatFromMem                  
 		 );         
	   
end struct;